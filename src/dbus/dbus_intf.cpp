/*
    Copyright (C) 2015  Christof Meerwald

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "dbus_intf.h"
#include "userintf.h"

#include <giomm.h>
#include <glibmm.h>

#include <iostream>

namespace
{
  Glib::ustring introspection_xml =
    R"(<node name="/net/cmeerw/GTwinkle/Phone">
  <interface name="net.cmeerw.GTwinkle.Phone">
    <method name="answer"></method>
    <method name="bye"></method>
    <method name="call"><arg name="dest" type="s" direction="in"/></method>
    <method name="reject"></method>
    <method name="quit"></method>
  </interface>
</node>)";
  Glib::RefPtr<Gio::DBus::NodeInfo> introspection_data;

  guint registered_id = -1;
  guint name_id = -1;

  void on_method_call(const Glib::RefPtr<Gio::DBus::Connection> &connection,
		      const Glib::ustring &sender,
		      const Glib::ustring &object_path,
		      const Glib::ustring &interface_name,
		      const Glib::ustring &method_name,
		      const Glib::VariantContainerBase &parameters,
		      const Glib::RefPtr<Gio::DBus::MethodInvocation> &invocation)
  {
    if (method_name == "answer")
    {
      ui->cmd_cli("answer");
      invocation->return_value(Glib::VariantContainerBase());
    }
    else if (method_name == "bye")
    {
      ui->cmd_cli("bye");
      invocation->return_value(Glib::VariantContainerBase());
    }
    else if (method_name == "call")
    {
      Glib::Variant<Glib::ustring> dest;
      parameters.get_child(dest);

      ui->cmd_call(dest.get());
      invocation->return_value(Glib::VariantContainerBase());
    }
    else if (method_name == "reject")
    {
      ui->cmd_cli("reject");
      invocation->return_value(Glib::VariantContainerBase());
    }
    else if (method_name == "quit")
    {
      ui->cmd_quit();
      invocation->return_value(Glib::VariantContainerBase());
    }
    else
    {
      // Non-existent method on the interface.
      Gio::DBus::Error error(Gio::DBus::Error::UNKNOWN_METHOD,
			     "Method does not exist.");
      invocation->return_error(error);
    }
  }
}

const Gio::DBus::InterfaceVTable interface_vtable(sigc::ptr_fun(&on_method_call));

void on_bus_acquired(const Glib::RefPtr<Gio::DBus::Connection>& connection,
		     const Glib::ustring &name)
{
  // Export an object to the bus:

  // See https://bugzilla.gnome.org/show_bug.cgi?id=646417 about avoiding
  // the repetition of the interface name:
  try
  {
    registered_id = connection->register_object("/net/cmeerw/GTwinkle/Phone",
						introspection_data->lookup_interface(),
						interface_vtable);
  }
  catch(const Glib::Error &ex)
  {
    std::cerr << "Registration of object failed." << std::endl;
  }
}

void on_name_acquired(const Glib::RefPtr<Gio::DBus::Connection> &connection,
		      const Glib::ustring &name)
{ }

void on_name_lost(const Glib::RefPtr<Gio::DBus::Connection> &connection,
		  const Glib::ustring &name)
{
  if (registered_id != static_cast<guint>(-1))
  {
    connection->unregister_object(registered_id);
  }
}

bool dbus_intf_init()
{
  try
  {
    introspection_data = Gio::DBus::NodeInfo::create_for_xml(introspection_xml);
  }
  catch (const Glib::Error& ex)
  {
    std::cerr << "Unable to create introspection data: "
	      << ex.what() << "." << std::endl;
    return false;
  }

  name_id = Gio::DBus::own_name(Gio::DBus::BUS_TYPE_SESSION,
				"net.cmeerw.GTwinkle",
				sigc::ptr_fun(&on_bus_acquired),
				sigc::ptr_fun(&on_name_acquired),
				sigc::ptr_fun(&on_name_lost));

  return true;
}

void dbus_intf_destroy()
{
  Gio::DBus::unown_name(name_id);
}
