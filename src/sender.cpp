/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <cassert>
#include <iostream>
#include <ctime>
#include <cstring>
#include <cerrno>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "events.h"
#include "log.h"
#include "phone.h"
#include "sender.h"
#include "translator.h"
#include "userintf.h"
#include "util.h"
#include "sockets/connection_table.h"
#include "sockets/socket.h"
#include "parser/parse_ctrl.h"
#include "parser/sip_message.h"
#include "stun/stun.h"

#include <memory>

#define MAX_TRANSMIT_RETRIES	3

// Maximum size of a message to be sent over an existing connection.
// For larger message always a new connection is opened to avoid
// head of line blocking.
#define MAX_REUSE_CONN_SIZE	10240

extern std::unique_ptr<t_connection_table> connection_table;
extern std::unique_ptr<t_event_queue> evq_sender;
extern std::unique_ptr<t_event_queue> evq_trans_mgr;
extern std::unique_ptr<t_event_queue> evq_trans_layer;
extern std::unique_ptr<t_phone> phone;

namespace
{
  // Number of consecutive non-icmp errors received
  int num_non_icmp_errors = 0;

  // Check if the error is caused by an incoming ICMP error. If so, then deliver
  // the ICMP error to the transaction manager.
  //
  // err - error returned by sendto
  // dst_addr - destination IP address of packet that failed to be sent
  // dst_port - destination port of packet that failed to be sent
  //
  // Returns true if the packet that failed to be sent, should still be sent.
  // Returns false if the packet that failed to be sent, should be discarded.
  bool handle_socket_err(t_socket_udp & sock, std::error_code const &ec,
			 net::ip::endpoint const &dst)
  {
    std::string log_msg;

    // Check if an ICMP error has been received
    t_icmp_msg icmp;
    if (sock.get_icmp(icmp))
    {
      log_msg = "Received ICMP from: ";
      log_msg += h_ip2str(icmp.icmp_src_ipaddr);
      log_msg += "\nICMP type: ";
      log_msg += int2str(icmp.type);
      log_msg += "\nICMP code: ";
      log_msg += int2str(icmp.code);
      log_msg += "\nDestination of packet causing ICMP: ";
      log_msg += h_ip2str(icmp.dest.addr());
      log_msg += ":";
      log_msg += int2str(icmp.dest.port());
      log_msg += "\nSocket error: ";
      log_msg += std::to_string(ec.value());
      log_msg += " ";
      log_msg += ec.message();
      log_file->write_report(log_msg, "::hanlde_socket_err", LOG_NORMAL);

      evq_trans_mgr->push_icmp(icmp);

      num_non_icmp_errors = 0;

      // If the ICMP error comes from the same destination as the
      // destination of the packet that failed to be sent, then the
      // packet should be discarded as it can most likely not be
      // delivered and would cause an infinite loop of ICMP errors
      // otherwise.
      if (icmp.dest == dst)
      {
	return false;
      }
    }
    else
    {
      // Even if an ICMP message is received this code can get executed.
      // Sometimes the error is already present on the socket, but the ICMP
      // message is not yet queued.
      log_msg = "Failed to send to SIP UDP socket.\n";
      log_msg += "Error code: ";
      log_msg += std::to_string(ec.value());
      log_msg += "\n";
      log_msg += ec.message();
      log_file->write_report(log_msg, "::handle_socket_err");

      num_non_icmp_errors++;

      /*
       * non-ICMP errors occur when a destination on the same
       * subnet cannot be reached. So this code seems to be
       * harmful.
       if (num_non_icmp_errors > 100) {
       log_msg = "Excessive number of socket errors.";
       log_file->write_report(log_msg, "::handle_socket_err",
       LOG_NORMAL, LOG_CRITICAL);
       log_msg = TRANSLATE("Excessive number of socket errors.");
       ui->cb_show_msg(log_msg, MSG_CRITICAL);
       exit(1);
       }
      */
    }

    return true;
  }

  void send_sip_udp(t_event_network const &e,
		    t_socket_udp *sip_socket, t_socket_udp *sip_socket_v6)
  {
    net::ip::endpoint const &dst(e.dst);
    assert(!dst.addr().is_unspec() && (dst.port() != 0));

    t_socket_udp &sock(*(dst.addr().is_v6() ? sip_socket_v6 : sip_socket));

    // Set correct transport in topmost Via header of a request.
    // For a response the Via header is copied from the incoming request.
    t_sip_message &sip_msg(e.get_msg());
    if (sip_msg.get_type() == MSG_REQUEST)
    {
      sip_msg.hdr_via.via_list.front().transport = "UDP";
    }

    std::string m = sip_msg.encode();
    log_file->write_header("::send_sip_udp", LOG_SIP);
    log_file->write_raw("Send to: udp:");
    log_file->write_raw(h_ip2str(dst.addr()));
    log_file->write_raw(":");
    log_file->write_raw(dst.port());
    log_file->write_endl();
    log_file->write_raw(m);
    log_file->write_endl();
    log_file->write_footer();

    bool msg_sent = false;
    int transmit_count = 0;
    while (!msg_sent && transmit_count++ <= MAX_TRANSMIT_RETRIES)
    {
      std::error_code ec;
      sock.sendto(m.c_str(), m.size(), dst, ec);
      if (!ec)
      {
	num_non_icmp_errors = 0;
	msg_sent = true;
      }
      else
      {
	if (!handle_socket_err(sock, ec, dst))
	{
	  // Discard packet.
	  msg_sent = true;
	}
	else
	{
	  if (transmit_count <= MAX_TRANSMIT_RETRIES)
	  {
	    // Sleep 100 ms
	    struct timespec sleeptimer { 0, 100'000'000 };
	    nanosleep(&sleeptimer, nullptr);
	  }
	}
      }
    }
  }

  void send_sip_tcp(t_event_network const &e)
  {
    net::ip::endpoint dst(e.dst);
    assert(!dst.addr().is_unspec() && (dst.port() != 0));

    // Set correct transport in topmost Via header of a request.
    // For a response the Via header is copied from the incoming request.
    t_sip_message &sip_msg(e.get_msg());
    if (sip_msg.get_type() == MSG_REQUEST)
    {
      sip_msg.hdr_via.via_list.front().transport = "TCP";
    }

    std::unique_ptr<t_connection> new_conn;
    t_connection *conn(nullptr);

    // If a connection exists then re-use this connection. Otherwise a
    // new connection must be opened.
    // For a request a connection to the destination address and port
    // of the event must be opened.
    // For a response a connection to the sent-by address and port in
    // the Via header must be opened.

    if (sip_msg.get_encoded_size() <= MAX_REUSE_CONN_SIZE)
    {
      // Re-use a connection only for small messages. Large messages cause
      // head of line blocking.
      conn = connection_table->get_connection(dst);
    }
    else
    {
      log_file->write_report(
	"Open new connection for large message.",
	"::send_sip_tcp", LOG_SIP, LOG_DEBUG);
    }

    if (!conn)
    {
      if (sip_msg.get_type() == MSG_RESPONSE)
      {
	dst = sip_msg.hdr_via.get_response_dst().endpoint;
      }

      std::unique_ptr<t_socket_tcp> tcp(new t_socket_tcp(dst.addr().type()));

      log_file->write_header("::send_sip_tcp", LOG_SIP, LOG_DEBUG);
      log_file->write_raw("Open connection to ");
      log_file->write_raw(h_ip2str(dst.addr()));
      log_file->write_raw(":");
      log_file->write_raw(dst.port());
      log_file->write_endl();
      log_file->write_footer();

      std::error_code ec;
      tcp->connect(dst, ec);
      if (ec)
      {
	evq_trans_mgr->push_failure(FAIL_TRANSPORT,
				    sip_msg.hdr_via.via_list.front().branch,
				    sip_msg.hdr_cseq.method);

	log_file->write_header("::send_sip_tcp", LOG_SIP, LOG_WARNING);
	log_file->write_raw("Failed to open connection to ");
	log_file->write_raw(h_ip2str(dst.addr()));
	log_file->write_raw(":");
	log_file->write_raw(dst.port());
	log_file->write_endl();
	log_file->write_footer();
	return;
      }

      new_conn.reset(new t_connection(std::move(tcp)));
      conn = new_conn.get();

      // For large messages always a new connection is established.
      // No other messages should be sent via this connection to avoid
      // head of line blocking.
      if (sip_msg.get_encoded_size() > MAX_REUSE_CONN_SIZE)
      {
	conn->set_reuse(false);
      }
    }

    // NOTE: if an existing connection was found, the connection table is now locked.

    // If persistent TCP connections are required, then
    // 1) If the SIP message is a registration request, then add the URI from the To-header
    //    to the registered URI set of the connection.
    // 2) If the SIP message is a de-registration request then remove the URI from the
    //    To-header from the registered URI set of the connection.
    if (sip_msg.get_type() == MSG_REQUEST)
    {
      auto *req(dynamic_cast<t_request *>(&sip_msg));
      if (req->method == REGISTER)
      {
	if (t_phone_user *pu = phone->find_phone_user(req->hdr_to.uri))
	{
	  t_user &user_config(pu->get_user_profile());

	  if (user_config.get_persistent_tcp())
	  {
	    conn->update_registered_uri_set(req);
	  }
	}
	else
	{
	  log_file->write_header("::send_sip_tcp", LOG_NORMAL, LOG_WARNING);
	  log_file->write_raw("Cannot find phone user for ");
	  log_file->write_raw(req->hdr_to.uri.encode());
	  log_file->write_endl();
	  log_file->write_footer();
	}
      }
    }

    std::string m = sip_msg.encode();
    log_file->write_header("::send_sip_tcp", LOG_SIP);
    log_file->write_raw("Send to: tcp:");
    log_file->write_raw(h_ip2str(dst.addr()));
    log_file->write_raw(":");
    log_file->write_raw(dst.port());
    log_file->write_endl();
    log_file->write_raw(m);
    log_file->write_endl();
    log_file->write_footer();

    conn->async_send(m.c_str(), m.size());

    if (new_conn)
    {
      connection_table->add_connection(std::move(new_conn));
    }
    else
    {
      connection_table->unlock();
    }
  }

  void send_stun(t_event_stun_request const &e,
		 t_socket_udp *sip_socket, t_socket_udp *sip_socket_v6)
  {
    net::ip::endpoint const &dst(e.dst);
    assert(!dst.addr().is_unspec() && (dst.port() != 0));

    t_socket_udp &sock(*(dst.addr().is_v6() ? sip_socket_v6 : sip_socket));

    log_file->write_header("::send_stun", LOG_STUN);
    log_file->write_raw("Send to: ");
    log_file->write_raw(h_ip2str(dst.addr()));
    log_file->write_raw(":");
    log_file->write_raw(dst.port());
    log_file->write_endl();
    log_file->write_raw(stunMsg2Str(e.get_msg()));
    log_file->write_footer();

    StunAtrString stun_pass;
    stun_pass.sizeValue = 0;
    char m[STUN_MAX_MESSAGE_SIZE];
    const unsigned msg_size(stunEncodeMessage(e.get_msg(), m,
	    STUN_MAX_MESSAGE_SIZE, stun_pass, false));

    bool msg_sent = false;
    int transmit_count = 0;
    while (!msg_sent && transmit_count++ <= MAX_TRANSMIT_RETRIES)
    {
      std::error_code ec;
      sock.sendto(m, msg_size, dst, ec);
      if (!ec)
      {
	num_non_icmp_errors = 0;
	msg_sent = true;
      }
      else
      {
	if (!handle_socket_err(sock, ec, dst))
	{
	  // Discard packet.
	  msg_sent = true;
	}
	else
	{
	  if (transmit_count <= MAX_TRANSMIT_RETRIES)
	  {
	    // Sleep 100 ms
	    struct timespec sleeptimer { 0, 100'000'000 };
	    nanosleep(&sleeptimer, nullptr);
	  }
	}
      }
    }
  }

  void send_nat_keepalive(t_event_nat_keepalive const &e,
			  t_socket_udp *sip_socket, t_socket_udp *sip_socket_v6)
  {
    net::ip::endpoint const &dst(e.dst);
    assert(!dst.addr().is_unspec() && (dst.port() != 0));

    t_socket_udp &sock(*(dst.addr().is_v6() ? sip_socket_v6 : sip_socket));

    char m[2] = { '\r', '\n' };

    bool msg_sent = false;
    int transmit_count = 0;
    while (!msg_sent && transmit_count++ <= MAX_TRANSMIT_RETRIES)
    {
      std::error_code ec;
      sock.sendto(m, 2, dst, ec);
      if (!ec)
      {
	num_non_icmp_errors = 0;
	msg_sent = true;
      }
      else
      {
	if (!handle_socket_err(sock, ec, dst))
	{
	  // Discard packet.
	  msg_sent = true;
	}
	else
	{
	  if (transmit_count <= MAX_TRANSMIT_RETRIES)
	  {
	    // Sleep 100 ms
	    struct timespec sleeptimer { 0, 100'000'000 };
	    nanosleep(&sleeptimer, nullptr);
	  }
	}
      }
    }
  }

  void send_tcp_ping(t_event_tcp_ping const &e)
  {
    net::ip::endpoint const &dst(e.get_dst());

    std::string log_msg;

    if (t_connection *conn = connection_table->get_connection(dst))
    {
      conn->async_send(TCP_PING_PACKET, strlen(TCP_PING_PACKET));
      connection_table->unlock();
    }
    else
    {
      // There is no connection to send the ping.
      log_msg = "Connection to ";
      log_msg += h_ip2str(dst.addr());
      log_msg += ":";
      log_msg += int2str(dst.port());
      log_msg += " is gone.";
      log_file->write_report(log_msg, "::send_tcp_ping", LOG_SIP, LOG_WARNING);

      // Signal the transaction layer that the connection is gone.
      evq_trans_layer->push_broken_connection(e.get_user_uri());
    }
  }
}

void tcp_sender_loop()
{
  std::string log_msg;

  while (true)
  {
    auto writable_connections(connection_table->select_write(nullptr));

    if (writable_connections.empty())
    {
      // Another thread cancelled the select command.
      // Stop listening.
      break;
    }

    // NOTE: The connection table is now locked.

    for (auto * const conn : writable_connections)
    {
      try
      {
	conn->write();
      }
      catch (const std::system_error &err)
      {
	if ((err.code().category() == std::system_category()) &&
	    (err.code().value() == EAGAIN || err.code().value() == EINTR))
	{
	  continue;
	}

	net::ip::endpoint remote(conn->get_remote_address());

	log_msg = "Got error on socket to ";
	log_msg += h_ip2str(remote.addr());
	log_msg += ":";
	log_msg += int2str(remote.port());
	log_msg += " - ";
	log_msg += err.what();
	log_file->write_report(log_msg, "::tcp_sender_loop", LOG_SIP, LOG_WARNING);

	// Connection is broken.
	// Signal the transaction layer that the connection is broken for
	// all associated registered URI's.
	for (auto const & uri : conn->get_registered_uri_set())
	{
	  evq_trans_layer->push_broken_connection(uri);
	}

	// Remove the broken connection.
	connection_table->remove_connection(conn);
	continue;
      }
    }

    connection_table->unlock();
  }

  log_file->write_report("TCP sender terminated.", "::tcp_sender_loop");
}

void sender_loop(t_socket_udp *sip_socket, t_socket_udp *sip_socket_v6)
{
  bool quit = false;
  while (!quit)
  {
    std::unique_ptr<t_event> event(evq_sender->pop());

    switch(event->get_type())
    {
    case EV_NETWORK:
    {
      t_event_network const &ev_network(static_cast<t_event_network &>(*event));
      net::ip::address local_ipaddr(get_src_address_for_dst(ev_network.dst.addr()));

      if (local_ipaddr.is_unspec())
      {
	log_file->write_header("::sender_loop", LOG_NORMAL, LOG_CRITICAL);
	log_file->write_raw("Cannot get source IP address for destination: ");
	log_file->write_raw(ev_network.dst.addr().get_v4().s_addr);
	log_file->write_endl();
	log_file->write_footer();

	evq_trans_mgr->push_failure(FAIL_TRANSPORT,
				    ev_network.get_msg().hdr_via.via_list.front().branch,
				    ev_network.get_msg().hdr_cseq.method);
	break;
      }

      if (!ev_network.get_msg().local_ip_check())
      {
	log_file->write_report("Local IP check failed",
			       "::sender_loop", LOG_NORMAL, LOG_CRITICAL);
	break;
      }

      if (ev_network.transport == "udp")
      {
	send_sip_udp(ev_network, sip_socket, sip_socket_v6);
      }
      else if (ev_network.transport == "tcp")
      {
	send_sip_tcp(ev_network);
      }
      else
      {
	log_file->write_header("::sender_loop", LOG_NORMAL, LOG_WARNING);
	log_file->write_raw("Received unsupported transport: ");
	log_file->write_raw(ev_network.transport);
	log_file->write_endl();
	log_file->write_footer();
      }
    }
    break;

    case EV_STUN_REQUEST:
     send_stun(static_cast<t_event_stun_request const &>(*event),
	       sip_socket, sip_socket_v6);
     break;

    case EV_NAT_KEEPALIVE:
     send_nat_keepalive(static_cast<t_event_nat_keepalive const &>(*event),
			sip_socket, sip_socket_v6);
     break;

    case EV_TCP_PING:
     send_tcp_ping(static_cast<t_event_tcp_ping const &>(*event));
     break;

    case EV_QUIT:
     quit = true;
     break;

    default:
     assert(false);
    }
  }
}
