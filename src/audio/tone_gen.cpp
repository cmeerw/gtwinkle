/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <sys/soundcard.h>
#include <iostream>
#include "tone_gen.h"
#include "log.h"
#include "sys_settings.h"
#include "user.h"
#include "userintf.h"
#include "util.h"
#include "audio_device.h"

// Number of samples read at once from the wav file
#define NUM_SAMPLES_PER_TURN	1024

// Duration of one turn in ms
#define DURATION_TURN	(NUM_SAMPLES_PER_TURN * 1000 / wav_info.samplerate)


t_tone_gen::t_tone_gen(const std::string &filename,
		       std::string _dev_tone)
  : wav_file(nullptr, &sf_close),
    wav_info{},
    dev_tone(std::move(_dev_tone)),
    valid(false), stop_playing(false), loop(false), pause(0),
    finished(false)
{
  if (filename.empty()) return;

  // Add share directory to filename
  if (filename[0] != '/')
  {
    wav_filename = sys_config->get_dir_share();
    wav_filename += '/';
    wav_filename += filename;
  }
  else
  {
    wav_filename = filename;
  }

  wav_file.reset(sf_open(wav_filename.c_str(), SFM_READ, &wav_info));
  if (!wav_file)
  {
    std::string msg("Cannot open ");
    msg += wav_filename;
    log_file->write_report(msg, "t_tone_gen::t_tone_gen",
			   LOG_NORMAL, LOG_WARNING);
    ui->cb_display_msg(msg, MSG_WARNING);
    return;
  }

  log_file->write_header("t_tone_gen::t_tone_gen");
  log_file->write_raw("Opened ");
  log_file->write_raw(wav_filename);
  log_file->write_endl();
  log_file->write_footer();

  valid = true;
}

t_tone_gen::~t_tone_gen()
{
  log_file->write_report("Deleted tone generator.",
			 "t_tone_gen::~t_tone_gen");
}

bool t_tone_gen::is_valid() const
{
  return valid;
}

void t_tone_gen::play()
{
  if (!valid)
  {
    log_file->write_report(
      "Tone generator is invalid. Cannot play tone",
      "t_tone_gen::play", LOG_NORMAL, LOG_WARNING);

    {
      std::lock_guard<std::mutex> g(mtx_finished);
      finished = true;
    }
    cond_finished.notify_one();

    return;
  }

  aio = t_audio_io::open(dev_tone, true, false, true, wav_info.channels,
			 SAMPLEFORMAT_S16, wav_info.samplerate, false);
  if (!aio)
  {
    std::string msg("Failed to open sound card: ");
    msg += get_error_str(errno);
    log_file->write_report(msg, "t_tone_gen::play",
			   LOG_NORMAL, LOG_WARNING);
    ui->cb_display_msg(msg, MSG_WARNING);

    {
      std::lock_guard<std::mutex> g(mtx_finished);
      finished = true;
    }
    cond_finished.notify_one();

    return;
  }

  log_file->write_report("Start playing tone.",
			 "t_tone_gen::play");

  do
  {
    // Each samples consists of #channels shorts
    data_buf.reset(new short[NUM_SAMPLES_PER_TURN * wav_info.channels]);

    sf_count_t frames_read = NUM_SAMPLES_PER_TURN;
    while (frames_read == NUM_SAMPLES_PER_TURN)
    {
      if (stop_playing) break;

      // Play sample
      frames_read = sf_readf_short(wav_file.get(), data_buf.get(), NUM_SAMPLES_PER_TURN);
      if (frames_read > 0)
      {
	aio->write(reinterpret_cast<unsigned char*>(data_buf.get()),
		   frames_read * wav_info.channels * 2);
      }
    }

    data_buf.reset();

    if (stop_playing) break;

    // Pause between repetitions
    if (loop)
    {
      // Play silence
      if (pause > 0)
      {
	data_buf.reset(new short[NUM_SAMPLES_PER_TURN * wav_info.channels] { });

	for (int i = 0; i < pause; i += DURATION_TURN)
	{
	  aio->write(reinterpret_cast<unsigned char*>(data_buf.get()),
		     NUM_SAMPLES_PER_TURN * wav_info.channels * 2);
	  if (stop_playing) break;
	}

	data_buf.reset();
      }

      if (stop_playing) break;

      // Set file pointer back to start of data
      sf_seek(wav_file.get(), 0, SEEK_SET);
    }
  } while (loop);

  log_file->write_report("Tone ended.",
			 "t_tone_gen::play_tone");

  {
    std::lock_guard<std::mutex> g(mtx_finished);
    finished = true;
  }
  cond_finished.notify_one();
}

void t_tone_gen::start_play_thread(bool _loop, int _pause)
{
  loop = _loop;
  pause = _pause;
  thr_play = std::thread([this] ()
			 {
			   ui->add_prohibited_thread();
			   this->play();
			   ui->remove_prohibited_thread();
			 });

  thr_play.detach();
}

void t_tone_gen::stop()
{
  log_file->write_report("Stopping tone.",
			 "t_tone_gen::stop");

  if (stop_playing)
  {
    log_file->write_report("Tone has stopped already.",
			   "t_tone_gen::stop");
    return;
  }

  // This will stop the playing thread.
  stop_playing = true;

  // The semaphore will be upped by the playing thread as soon
  // as playing finishes.
  {
    std::unique_lock<std::mutex> l(mtx_finished);
    cond_finished.wait(l, [this] ()
		       { return finished; });
  }

  log_file->write_report("Tone stopped.",
			 "t_tone_gen::stop");

  aio.reset();
}
