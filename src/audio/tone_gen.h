/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _TONE_GEN_H
#define _TONE_GEN_H

#include <sndfile.h>
#include "sys_settings.h"

#include <atomic>
#include <condition_variable>
#include <memory>
#include <mutex>
#include <string>
#include <thread>

#ifndef _AUDIO_DEVICE_H
class t_audio_io;
#endif


class t_tone_gen
{
private:
  std::string wav_filename;	// name of wav file
  std::unique_ptr<SNDFILE, decltype(&sf_close)> wav_file; // SNDFILE pointer to wav file
  SF_INFO wav_info;	   // Information about format of the wav file
  std::string dev_tone;    // device to play tone
  std::unique_ptr<t_audio_io> aio; // soundcard
  bool valid;			   // wav file is in a valid format
  std::atomic<bool> stop_playing;  // indicates if playing should stop
  std::thread thr_play;		   // playing thread
  bool loop;			   // repeat playing
  int pause;			   // pause (ms) between repetitions
  std::unique_ptr<short[]> data_buf; // buffer for reading sound samples

  bool finished;
  std::mutex mtx_finished;
  std::condition_variable cond_finished; // indicates if playing finished

public:
  t_tone_gen(const std::string &filename, std::string _dev_tone);
  ~t_tone_gen();

  bool is_valid() const;

  // Play the wav file
  // loop = true -> repeat playing
  // pause is pause in ms between repetitions
  void start_play_thread(bool _loop, int _pause);
  void play();

  // Stop playing
  void stop();
};

#endif
