/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <algorithm>
#include <cassert>
#include <iostream>
#include <cstdio>
#include <ctime>
#include <sys/types.h>
#include <sys/time.h>
#include "audio_tx.h"
#include "log.h"
#include "phone.h"
#include "userintf.h"
#include "util.h"
#include "line.h"
#include "sequence_number.h"

extern t_phone *phone;

#define SAMPLE_BUF_SIZE (MAX_PTIME * sc_sample_rate/1000 * AUDIO_SAMPLE_SIZE/8)

// Debug macro to print timestamp
#define DEBUG_TS(s) {							\
    gettimeofday(&debug_timer, NULL);					\
    cout << "DEBUG: ";							\
    cout << debug_timer.tv_sec * 1000 +					\
	debug_timer.tv_usec / 1000;					\
    cout << ":" << debug_timer.tv_sec * 1000 + debug_timer.tv_usec / 1000 - (debug_timer_prev.tv_sec * 1000 + debug_timer_prev.tv_usec / 1000);	\
    cout << " " << (s) << endl;						\
    debug_timer_prev = debug_timer;					\
  }

t_audio_decoder *t_audio_tx::t_audio_decoder_getter::operator [] (
  enum t_audio_codec codec)
{
  if (!decoders_[codec])
  {
    switch (codec)
    {
    case CODEC_G711_ALAW:
     decoders_[codec] = std::make_unique<t_g711a_audio_decoder>(ptime_,
	 user_config_);
     break;

    case CODEC_G711_ULAW:
     decoders_[codec] = std::make_unique<t_g711u_audio_decoder>(ptime_,
	 user_config_);
     break;

    case CODEC_GSM:
     decoders_[codec] = std::make_unique<t_gsm_audio_decoder>(user_config_);
     break;

    case CODEC_G722:
     decoders_[codec] = std::make_unique<t_g722_audio_decoder>(ptime_,
	 user_config_);
     break;

#ifdef HAVE_SPEEX
    case CODEC_SPEEX_NB:
     decoders_[codec] = std::make_unique<t_speex_audio_decoder>(t_speex_audio_decoder::MODE_NB,
	 user_config_);
     break;

    case CODEC_SPEEX_WB:
     decoders_[codec] = std::make_unique<t_speex_audio_decoder>(t_speex_audio_decoder::MODE_WB,
	 user_config_);
     break;

    case CODEC_SPEEX_UWB:
     decoders_[codec] = std::make_unique<t_speex_audio_decoder>(t_speex_audio_decoder::MODE_UWB,
	 user_config_);
     break;
#endif

#ifdef HAVE_OPUS
    case CODEC_OPUS:
     decoders_[codec] = std::make_unique<t_opus_audio_decoder>(ptime_,
	 user_config_);
     break;
#endif

#ifdef HAVE_ILBC
    case CODEC_ILBC:
     decoders_[codec] = std::make_unique<t_ilbc_audio_decoder>(ptime_,
	 user_config_);
     break;
#endif

    case CODEC_G726_16:
     decoders_[codec] = std::make_unique<t_g726_audio_decoder>(t_g726_audio_decoder::BIT_RATE_16,
	 ptime_, user_config_);
     break;

    case CODEC_G726_24:
     decoders_[codec] = std::make_unique<t_g726_audio_decoder>(t_g726_audio_decoder::BIT_RATE_24,
	 ptime_, user_config_);
     break;

    case CODEC_G726_32:
     decoders_[codec] = std::make_unique<t_g726_audio_decoder>(t_g726_audio_decoder::BIT_RATE_32,
	 ptime_, user_config_);
     break;

    case CODEC_G726_40:
     decoders_[codec] = std::make_unique<t_g726_audio_decoder>(t_g726_audio_decoder::BIT_RATE_40,
	 ptime_, user_config_);
     break;
    }
  }

  return decoders_[codec].get();
}

//////////
// PUBLIC
//////////

t_audio_tx::t_audio_tx(t_audio_session &_audio_session,
		       t_audio_io &_playback_device,
		       t_twinkle_rtp_session &_rtp_session,
		       t_audio_codec _codec,
		       std::unordered_map<unsigned short, t_audio_codec> _payload2codec,
		       unsigned short ptime)
  : audio_session(_audio_session),
    user_config(*audio_session.get_line()->get_user()),
    playback_device(_playback_device),
    rtp_session(_rtp_session),
    is_3way(false), is_3way_mixer(false),
    peer_tx_3way(nullptr), peer_rx_3way(nullptr),
    map_audio_decoder(user_config, ptime),
    codec(_codec),
    audio_decoder(map_audio_decoder[codec]),
    payload2codec(std::move(_payload2codec)),
    sc_sample_rate(audio_sample_rate(codec)),
    sample_buf(new unsigned char[SAMPLE_BUF_SIZE]),
    jitter_buf(new unsigned char[JITTER_BUF_SIZE(sc_sample_rate)]),
    jitter_buf_len(0), load_jitter_buf(true),
    conceal_pos(0), conceal_num(0),
    soundcard_buf_size(playback_device.get_buffer_size(false)),
    pt_telephone_event(-1), pt_telephone_event_alt(1),
    dtmf_previous_timestamp(0),
    is_running(false), stop_running(false)
{
  // Create concealment buffers
  for (int i = 0; i < MAX_CONCEALMENT; i++)
  {
    conceal_buf[i].reset(new unsigned char[SAMPLE_BUF_SIZE]);
    conceal_buflen[i] = 0;
  }
}

t_audio_tx::~t_audio_tx()
{
  if (is_running)
  {
    stop_running = true;
    do
    {
      struct timespec sleeptimer;
      sleeptimer.tv_sec = 0;
      sleeptimer.tv_nsec = 10000000;
      nanosleep(&sleeptimer, NULL);
      continue;
    } while (is_running);
  }
}

void t_audio_tx::retain_for_concealment(unsigned char *buf, unsigned short len)
{
  if (conceal_num == 0)
  {
    memcpy(conceal_buf[0].get(), buf, len);
    conceal_buflen[0] = len;
    conceal_num = 1;
    conceal_pos = 0;
    return;
  }

  if (conceal_num < MAX_CONCEALMENT)
  {
    memcpy(conceal_buf[conceal_num].get(), buf, len);
    conceal_buflen[conceal_num] = len;
    conceal_num++;
    return;
  }

  memcpy(conceal_buf[conceal_pos].get(), buf, len);
  conceal_buflen[conceal_pos] = len;
  conceal_pos = (conceal_pos + 1) % MAX_CONCEALMENT;
}

void t_audio_tx::conceal(short num)
{
  // Some codecs have a PLC.
  // Only use this PLC is the sound card sample rate equals the codec
  // sample rate. If they differ, then we should resample the codec
  // samples. As this should be a rare case, we are lazy here. In
  // this rare case, use Twinkle's low-tech PLC.
  if (audio_decoder->has_plc() && audio_sample_rate(codec) == sc_sample_rate)
  {
    auto * const sb(reinterpret_cast<short *>(sample_buf.get()));
    for (int i = 0; i < num; i++)
    {
      int nsamples;
      nsamples = audio_decoder->conceal(sb, SAMPLE_BUF_SIZE);
      if (nsamples > 0)
      {
	play_pcm(sample_buf.get(), nsamples * 2);
      }
    }

    return;
  }

  // Replay previous packets for other codecs
  short i = (conceal_pos + (MAX_CONCEALMENT - num)) % MAX_CONCEALMENT;

  if (i >= conceal_pos)
  {
    for (int j = i; j < MAX_CONCEALMENT; j++)
    {
      play_pcm(conceal_buf[j].get(), conceal_buflen[j]);
    }

    for (int j = 0; j < conceal_pos; j++)
    {
      play_pcm(conceal_buf[j].get(), conceal_buflen[j]);
    }
  }
  else
  {
    for (int j = i; j < conceal_pos; j++)
    {
      play_pcm(conceal_buf[j].get(), conceal_buflen[j]);
    }
  }
}

void t_audio_tx::clear_conceal_buf()
{
  conceal_pos = 0;
  conceal_num = 0;
}

void t_audio_tx::play_pcm(unsigned char *buf, unsigned short len, bool only_3rd_party)
{
  int status;
  //struct timeval debug_timer, debug_timer_prev;

  unsigned char *playbuf = buf;

  // If there is only sound from the 3rd party in a 3-way, then check
  // if there is still enough sound in the buffer of the DSP to be
  // played. If not, then play out the sound from the 3rd party only.
  if (only_3rd_party)
  {
    /* Does not work on all ALSA implementations.
       if (playback_device.get_buffer_space(false) < soundcard_buf_size - len)
       {
    */
    if (!playback_device.play_buffer_underrun())
    {
      // There is still sound in the DSP buffers to be
      // played, so let's wait. Maybe in the next cycle
      // an RTP packet from the far-end will be received.
      return;
    }
  }

  // If we are in a 3-way then send the samples to the peer audio
  // receiver for mixing
  if (!only_3rd_party && is_3way && peer_rx_3way)
  {
    peer_rx_3way->post_media_peer_tx_3way(buf, len, sc_sample_rate);
  }

  // If we are in a 3-way conference and we are not the mixer then
  // send the sound samples to the mixer
  if (is_3way && !is_3way_mixer)
  {
    if (peer_tx_3way)
    {
      peer_tx_3way->post_media_peer_tx_3way(buf, len, sc_sample_rate);
      return;
    }
    else
    {
      // There is no peer.
      return;
    }
  }

  // Mix audio for 3-way conference
  if (is_3way && is_3way_mixer)
  {
    if (media_3way_peer_tx->get(mix_buf_3way.get(), len))
    {
      auto * const mix_sb(reinterpret_cast<short *>(mix_buf_3way.get()));
      auto * const sb(reinterpret_cast<short *>(buf));
      for (int i = 0; i < len / 2; i++)
      {
	mix_sb[i] = mix_linear_pcm(sb[i], mix_sb[i]);
      }

      playbuf = mix_buf_3way.get();
    }
  }

  // Fill jitter buffer before playing
  if (load_jitter_buf)
  {
    if (jitter_buf_len + len < JITTER_BUF_SIZE(sc_sample_rate))
    {
      memcpy(jitter_buf.get() + jitter_buf_len, playbuf, len);
      jitter_buf_len += len;
    }
    else
    {
      // Write the contents of the jitter buffer to the DSP.
      // The buffers in the DSP will now function as jitter
      // buffer.
      status = playback_device.write(jitter_buf.get(), jitter_buf_len);
      if (status != jitter_buf_len)
      {
	std::string msg("Writing to dsp failed: ");
	msg += get_error_str(errno);
	log_file->write_report(msg, "t_audio_tx::play_pcm",
			       LOG_NORMAL, LOG_CRITICAL);
      }

      // Write passed sound samples to DSP.
      status = playback_device.write(playbuf, len);
      if (status != len)
      {
	std::string msg("Writing to dsp failed: ");
	msg += get_error_str(errno);
	log_file->write_report(msg, "t_audio_tx::play_pcm",
			       LOG_NORMAL, LOG_CRITICAL);
      }

      load_jitter_buf = false;
    }

    return;
  }

  // If buffer on soundcard is empty, then the jitter buffer needs
  // to be refilled. This should only occur when no RTP packets
  // have been received for a while (silence suppression or packet loss)
  /*
   * This code does not work on all ALSA implementations, e.g. ALSA via pulse audio
   int bufferspace = playback_device.get_buffer_space(false);
   if (bufferspace == soundcard_buf_size && len <= JITTER_BUF_SIZE(sc_sample_rate))
   {
  */
  if (playback_device.play_buffer_underrun())
  {
    memcpy(jitter_buf.get(), playbuf, len);
    jitter_buf_len = len;
    load_jitter_buf = true;
    log_file->write_header("t_audio_tx::play_pcm", LOG_NORMAL, LOG_DEBUG);
    log_file->write_raw("Audio tx line ");
    log_file->write_raw(get_line()->get_line_number()+1);
    log_file->write_raw(": jitter buffer empty.\n");
    log_file->write_footer();
    return;
  }

  // If the play-out buffer contains the maximum number of
  // packets then start skipping packets to prevent
  // unacceptable delay.
  // This can only happen if the thread did not get
  // processing time for a while and RTP packets start to
  // pile up.
  // Or if a soundcard plays out the samples at just less then
  // the requested sample rate.
  /* Not needed anymore, the ::run loop already discards incoming RTP packets
     with a late timestamp. This seems to solve the slow soundcard problem
     better. The solution below caused annoying ticks in the playout.

     if (soundcard_buf_size - bufferspace > JITTER_BUF_SIZE + len)
     {
     log_file->write_header("t_audio_tx::play_pcm", LOG_NORMAL, LOG_DEBUG);
     log_file->write_raw("Audio tx line ");
     log_file->write_raw(get_line()->get_line_number()+1);
     log_file->write_raw(": jitter buffer overflow: ");
     log_file->write_raw(bufferspace);
     log_file->write_raw(" bytes.\n");
     log_file->write_footer();
     return;
     }
  */

  // Write passed sound samples to DSP.
  status = playback_device.write(playbuf, len);

  if (status != len)
  {
    std::string msg("Writing to dsp failed: ");
    msg += get_error_str(errno);
    log_file->write_report(msg, "t_audio_tx::play_pcm",
			   LOG_NORMAL, LOG_CRITICAL);
    return;
  }
}

void t_audio_tx::set_running(bool running)
{
  is_running = running;
}

void t_audio_tx::run()
{
  //struct timeval debug_timer, debug_timer_prev;
  int last_seqnum = -1; // seqnum of last received RTP packet

  // RTP packets with multiple SSRCs may be received. Each SSRC
  // represents an audio stream. Twinkle will only play 1 audio stream.
  // On a reception of a new SSRC, Twinkle will switch over to play the
  // new stream. This supports devices that change SSRC during a call.
  uint32 ssrc_current = 0;

  bool recvd_dtmf = false; // indicates if last RTP packets is a DTMF event

  // The running flag is set already in t_audio_session::run to prevent
  // a crash when the thread gets destroyed before it starts running.
  // is_running = true;

  uint32 rtp_timestamp = 0;

  // This thread may not take the lock on the transaction layer to
  // prevent dead locks
  phone->add_prohibited_thread();
  ui->add_prohibited_thread();

  unsigned short ptime(audio_decoder->get_default_ptime()); // in milliseconds

  while (true)
  {
    std::unique_ptr<const ost::AppDataUnit> adu;

    do
    {
      adu.reset();
      if (stop_running) break;

      rtp_timestamp = rtp_session.getFirstTimestamp();
      adu.reset(rtp_session.getData(
		  rtp_session.getFirstTimestamp()));
      if (!adu || adu->getSize() <= 0)
      {
	// There is no packet available. This may have
	// several reasons:
	// - the thread scheduling granularity does
	//   not match ptime
	// - packet lost
	// - packet delayed
	// Wait another cycle for a packet. The
	// jitter buffer will cope with this variation.
	adu.reset();

	// If we are the mixer in a 3-way call and there
	// is enough media from the other far-end then
	// this must be sent to the dsp.
	if (is_3way && is_3way_mixer &&
	    media_3way_peer_tx->size_content() >=
	    ptime * (audio_sample_rate(codec) / 1000) * 2)
	{
	  // Fill the sample buffer with silence
	  int len = ptime * (audio_sample_rate(codec) / 1000) * 2;
	  std::fill(sample_buf.get(),
		    sample_buf.get() + len,
		    0);
	  play_pcm(sample_buf.get(), len, true);
	}

	// Sleep ptime ms
	struct timespec sleeptimer;
	sleeptimer.tv_sec = 0;

	if (ptime >= 20)
	{
	  sleeptimer.tv_nsec = ptime * 1000000 - 10000000;
	}
	else
	{
	  // With a thread schedule of 10ms
	  // granularity, this will schedule the
	  // thread every 10ms.
	  sleeptimer.tv_nsec = 5000000;
	}
	nanosleep(&sleeptimer, nullptr);
      }
    } while (!adu || (adu->getSize() <= 0));

    if (stop_running)
    {
      adu.reset();
      break;
    }

    // Check for a codec change
    auto it_codec = payload2codec.find(adu->getType());
    t_audio_codec recvd_codec = CODEC_NULL;
    if (it_codec != payload2codec.end())
    {
      recvd_codec = it_codec->second;
    }

    // Switch over to new SSRC
    if (last_seqnum == -1 || ssrc_current != adu->getSource().getID())
    {
      if (recvd_codec != CODEC_NULL)
      {
	ssrc_current = adu->getSource().getID();

	// An SSRC defines a sequence number space. So a new
	// SSRC starts with a new random sequence number
	last_seqnum = -1;

	log_file->write_header("t_audio_tx::run",
			       LOG_NORMAL);
	log_file->write_raw("Audio tx line ");
	log_file->write_raw(get_line()->get_line_number()+1);
	log_file->write_raw(": play codec ");
	log_file->write_raw(recvd_codec);
	log_file->write_raw(", SSRC ");
	log_file->write_raw(ssrc_current);
	log_file->write_endl();
	log_file->write_footer();
      }
      else
      {
	// SSRC received had an unsupported codec
	// Discard.
	// KLUDGE: for now this supports a scenario where a
	// far-end starts ZRTP negotiation by sending CN
	// packets with a separate SSRC while ZRTP is disabled
	// in Twinkle. Twinkle will then receive the CN packets
	// and discard them here as CN is an unsupported codec.
	log_file->write_header("t_audio_tx::run",
			       LOG_NORMAL, LOG_DEBUG);
	log_file->write_raw("Audio tx line ");
	log_file->write_raw(get_line()->get_line_number()+1);
	log_file->write_raw(": SSRC received (");
	log_file->write_raw(adu->getSource().getID());
	log_file->write_raw(") has unsupported codec ");
	log_file->write_raw(adu->getType());
	log_file->write_endl();
	log_file->write_footer();

	continue;
      }
    }

    if (t_audio_decoder *recvd_audio_decoder = map_audio_decoder[recvd_codec])
    {
      if (codec != recvd_codec)
      {
	codec = recvd_codec;
	audio_decoder = recvd_audio_decoder;

	get_line()->ci_set_recv_codec(codec);
	ui->cb_recv_codec_changed(get_line()->get_line_number(), codec);

	log_file->write_header("t_audio_tx::run",
			       LOG_NORMAL, LOG_DEBUG);
	log_file->write_raw("Audio tx line ");
	log_file->write_raw(get_line()->get_line_number()+1);
	log_file->write_raw(": codec change to ");
	log_file->write_raw(ui->format_codec(codec));
	log_file->write_endl();
	log_file->write_footer();
      }
    }
    else
    {
      if (adu->getType() == pt_telephone_event ||
	  adu->getType() == pt_telephone_event_alt)
      {
	recvd_dtmf = true;
      }
      else
      {
	if (codec != CODEC_UNSUPPORTED)
	{
	  codec = CODEC_UNSUPPORTED;
	  get_line()->ci_set_recv_codec(codec);
	  ui->cb_recv_codec_changed(get_line()->get_line_number(), codec);

	  log_file->write_header("t_audio_tx::run",
				 LOG_NORMAL, LOG_DEBUG);
	  log_file->write_raw("Audio tx line ");
	  log_file->write_raw(get_line()->get_line_number()+1);
	  log_file->write_raw(": payload type ");
	  log_file->write_raw(adu->getType());
	  log_file->write_raw(" not supported\n");
	  log_file->write_footer();
	}

	last_seqnum = adu->getSeqNum();
	continue;
      }
    }

    // DTMF event
    if (recvd_dtmf)
    {
      // NOTE: the DTMF tone will be detected here
      // while there might still be data in the jitter
      // buffer. If the jitter buffer was already sent
      // to the DSP, then the DSP will continue to play
      // out the buffer sound samples.

      if (dtmf_previous_timestamp != rtp_timestamp)
      {
	// A new DTMF tone has been received.
	dtmf_previous_timestamp = rtp_timestamp;
	auto * const e(reinterpret_cast<const t_rtp_telephone_event *>(adu->getData()));
	ui->cb_dtmf_detected(get_line()->get_line_number(), e->get_event());

	// Log DTMF event
	log_file->write_header("t_audio_tx::run");
	log_file->write_raw("Audio tx line ");
	log_file->write_raw(get_line()->get_line_number()+1);
	log_file->write_raw(": detected DTMF event - ");
	log_file->write_raw(e->get_event());
	log_file->write_endl();
	log_file->write_footer();
      }

      recvd_dtmf = false;
      last_seqnum = adu->getSeqNum();
      continue;
    }

    // Discard invalide payload sizes
    if (!audio_decoder->valid_payload_size(adu->getSize(), SAMPLE_BUF_SIZE / 2))
    {
      log_file->write_header("t_audio_tx::run", LOG_NORMAL, LOG_DEBUG);
      log_file->write_raw("Audio tx line ");
      log_file->write_raw(get_line()->get_line_number()+1);
      log_file->write_raw(": RTP payload size (");
      log_file->write_raw((unsigned long)(adu->getSize()));
      log_file->write_raw(" bytes) invalid for \n");
      log_file->write_raw(ui->format_codec(codec));
      log_file->write_footer();
      last_seqnum = adu->getSeqNum();
      continue;
    }

    unsigned short recvd_ptime = audio_decoder->get_ptime(adu->getSize());

    // Log a change of ptime
    if (ptime != recvd_ptime)
    {
      log_file->write_header("t_audio_tx::run", LOG_NORMAL, LOG_DEBUG);
      log_file->write_raw("Audio tx line ");
      log_file->write_raw(get_line()->get_line_number()+1);
      log_file->write_raw(": ptime changed from ");
      log_file->write_raw(ptime);
      log_file->write_raw(" ms to ");
      log_file->write_raw(recvd_ptime);
      log_file->write_raw(" ms\n");
      log_file->write_footer();
      ptime = recvd_ptime;
    }

    // Check for lost packets
    // This must be done before decoding the received samples as the
    // speex decoder has its own PLC algorithm for which it needs the decoding
    // state before decoding the new samples.
    seq16_t seq_recvd(adu->getSeqNum());
    seq16_t seq_last(static_cast<uint16>(last_seqnum));
    if (last_seqnum != -1 && seq_recvd - seq_last > 1)
    {
      // Packets have been lost
      uint16 num_lost = (seq_recvd - seq_last) - 1;
      log_file->write_header("t_audio_tx::run", LOG_NORMAL, LOG_DEBUG);
      log_file->write_raw("Audio tx line ");
      log_file->write_raw(get_line()->get_line_number()+1);
      log_file->write_raw(": ");
      log_file->write_raw(num_lost);
      log_file->write_raw(" RTP packets lost.\n");
      log_file->write_footer();

      if (num_lost <= conceal_num)
      {
	// Conceal packet loss
	conceal(num_lost);
      }
      clear_conceal_buf();
    }

    // Determine if resampling is needed due to dynamic change to
    // codec with other sample rate.
    short downsample_factor = 1;
    short upsample_factor = 1;
    if (audio_sample_rate(codec) > sc_sample_rate)
    {
      downsample_factor = audio_sample_rate(codec) / sc_sample_rate;
    }
    else if (audio_sample_rate(codec) < sc_sample_rate)
    {
      upsample_factor = sc_sample_rate / audio_sample_rate(codec);
    }

    // Create sample buffer. If no resampling is needed, the sample
    // buffer from the audio_tx object can be used directly.
    // Otherwise a temporary sample buffers is created that will
    // be resampled to the object's sample buffer later.
    std::unique_ptr<short[]> tmp_ptr;
    short *sb;
    int sb_size;
    if (downsample_factor > 1)
    {
      sb_size = SAMPLE_BUF_SIZE / 2 * downsample_factor;
      tmp_ptr.reset(sb = new short[sb_size]);
    }
    else if (upsample_factor > 1)
    {
      sb_size = SAMPLE_BUF_SIZE / 2;
      tmp_ptr.reset(sb = new short[SAMPLE_BUF_SIZE / 2]);
    }
    else
    {
      sb_size = SAMPLE_BUF_SIZE / 2;
      sb = reinterpret_cast<short *>(sample_buf.get());
    }


    // Decode the audio
    auto * const payload(const_cast<uint8 *>(adu->getData()));
    short sample_size; // size in bytes

    sample_size = 2 * audio_decoder->decode(payload, adu->getSize(), sb, sb_size);

    // Resample if needed
    if (downsample_factor > 1)
    {
      short *p = sb;
      sb = reinterpret_cast<short *>(sample_buf.get());
      for (int i = 0; i < sample_size / 2; i += downsample_factor)
      {
	sb[i / downsample_factor] = p[i];
      }
      sample_size /= downsample_factor;
    }
    else if (upsample_factor > 1)
    {
      short *p = sb;
      sb = reinterpret_cast<short *>(sample_buf.get());
      for (int i = 0; i < sample_size / 2; i++)
      {
	for (int j = 0; j < upsample_factor; j++)
	{
	  sb[i * upsample_factor + j] = p[i];
	}
      }
      sample_size *= upsample_factor;
    }
    tmp_ptr.reset();

    // If the decoder deliverd 0 bytes, then it failed
    if (sample_size == 0)
    {
      last_seqnum = adu->getSeqNum();
      continue;
    }

    // Discard packet if we are lacking behind. This happens if the
    // soundcard plays at a rate less than the requested sample rate.
    if (rtp_session.isWaiting(&(adu->getSource())))
    {

      uint32 last_ts = rtp_session.getLastTimestamp(&(adu->getSource()));
      uint32 diff;

      diff = last_ts - rtp_timestamp;

      if (diff > (uint32_t)(JITTER_BUF_SIZE(sc_sample_rate) / AUDIO_SAMPLE_SIZE) * 8)
      {
	log_file->write_header("t_audio_tx::run", LOG_NORMAL, LOG_DEBUG);
	log_file->write_raw("Audio tx line ");
	log_file->write_raw(get_line()->get_line_number()+1);
	log_file->write_raw(": discard delayed packet.\n");
	log_file->write_raw("Timestamp: ");
	log_file->write_raw(rtp_timestamp);
	log_file->write_raw(", Last timestamp: ");
	log_file->write_raw((long unsigned int)last_ts);
	log_file->write_endl();
	log_file->write_footer();

	last_seqnum = adu->getSeqNum();
	continue;
      }
    }

    play_pcm(sample_buf.get(), sample_size);
    retain_for_concealment(sample_buf.get(), sample_size);
    last_seqnum = adu->getSeqNum();

    // No sleep is done here but in the loop waiting
    // for a new packet. If a packet is already available
    // it can be send to the sound card immediately so
    // the play-out buffer keeps filled.
    // If the play-out buffer gets empty you hear a
    // crack in the sound.


#ifdef HAVE_SPEEX
    // store decoded output for (optional) echo cancellation
    if (audio_session.get_do_echo_cancellation())
    {
      if (audio_session.get_echo_captured_last())
      {
	speex_echo_playback(audio_session.get_speex_echo_state(), (spx_int16_t *) sb);
	audio_session.set_echo_captured_last(false);;
      }
    }
#endif

  }

  phone->remove_prohibited_thread();
  ui->remove_prohibited_thread();
  is_running = false;
}

void t_audio_tx::set_pt_telephone_event(int pt, int pt_alt)
{
  pt_telephone_event = pt;
  pt_telephone_event_alt = pt_alt;
}

t_line *t_audio_tx::get_line() const
{
  return audio_session.get_line();
}

void t_audio_tx::join_3way(bool mixer, t_audio_tx *peer_tx, t_audio_rx *peer_rx)
{
  std::lock_guard<std::mutex> guard (mtx_3way);

  if (is_3way)
  {
    log_file->write_header("t_audio_tx::join_3way");
    log_file->write_raw("ERROR: audio tx line ");
    log_file->write_raw(get_line()->get_line_number()+1);
    log_file->write_raw(" - 3way is already active.\n");
    log_file->write_footer();
    return;
  }

  // Logging
  log_file->write_header("t_audio_tx::join_3way");
  log_file->write_raw("Audio tx line ");
  log_file->write_raw(get_line()->get_line_number()+1);
  log_file->write_raw(": join 3-way.\n");
  if (mixer)
  {
    log_file->write_raw("Role is: mixer.\n");
  }
  else
  {
    log_file->write_raw("Role is: non-mixing.\n");
  }
  if (peer_tx)
  {
    log_file->write_raw("A peer transmitter already exists.\n");
  }
  else
  {
    log_file->write_raw("A peer transmitter does not exist.\n");
  }
  if (peer_rx)
  {
    log_file->write_raw("A peer receiver already exists.\n");
  }
  else
  {
    log_file->write_raw("A peer receiver does not exist.\n");
  }
  log_file->write_footer();

  peer_tx_3way = peer_tx;
  peer_rx_3way = peer_rx;
  is_3way_mixer = mixer;
  is_3way = true;

  // Create buffers for mixing
  mix_buf_3way.reset(new unsigned char[SAMPLE_BUF_SIZE]);

  // See comments in audio_rx.cpp for the size of this buffer.
  media_3way_peer_tx.reset(new t_media_buffer(JITTER_BUF_SIZE(sc_sample_rate)));
}

void t_audio_tx::set_peer_tx_3way(t_audio_tx *peer_tx)
{
  std::lock_guard<std::mutex> guard (mtx_3way);

  if (!is_3way)
  {
    return;
  }

  // Logging
  log_file->write_header("t_audio_tx::set_peer_tx_3way");
  log_file->write_raw("Audio tx line ");
  log_file->write_raw(get_line()->get_line_number()+1);
  if (peer_tx)
  {
    log_file->write_raw(": set peer transmitter.\n");
  }
  else
  {
    log_file->write_raw(": erase peer transmitter.\n");
  }
  if (is_3way_mixer)
  {
    log_file->write_raw("Role is: mixer.\n");
  }
  else
  {
    log_file->write_raw("Role is: non-mixing.\n");
  }
  log_file->write_footer();


  peer_tx_3way = peer_tx;
}

void t_audio_tx::set_peer_rx_3way(t_audio_rx *peer_rx)
{
  std::lock_guard<std::mutex> guard (mtx_3way);

  if (!is_3way)
  {
    return;
  }

  // Logging
  log_file->write_header("t_audio_tx::set_peer_rx_3way");
  log_file->write_raw("Audio tx line ");
  log_file->write_raw(get_line()->get_line_number()+1);
  if (peer_rx)
  {
    log_file->write_raw(": set peer receiver.\n");
  }
  else
  {
    log_file->write_raw(": erase peer receiver.\n");
  }
  if (is_3way_mixer)
  {
    log_file->write_raw("Role is: mixer.\n");
  }
  else
  {
    log_file->write_raw("Role is: non-mixing.\n");
  }
  log_file->write_footer();

  peer_rx_3way = peer_rx;
}

void t_audio_tx::set_mixer_3way(bool mixer)
{
  std::lock_guard<std::mutex> guard (mtx_3way);

  if (!is_3way)
  {
    return;
  }

  // Logging
  log_file->write_header("t_audio_tx::set_mixer_3way");
  log_file->write_raw("Audio tx line ");
  log_file->write_raw(get_line()->get_line_number()+1);
  if (mixer)
  {
    log_file->write_raw(": change role to: mixer.\n");
  }
  else
  {
    log_file->write_raw(": change role to: non-mixing.\n");
  }
  log_file->write_footer();

  is_3way_mixer = mixer;
}

void t_audio_tx::stop_3way()
{
  std::lock_guard<std::mutex> guard (mtx_3way);

  if (!is_3way)
  {
    log_file->write_header("t_audio_tx::stop_3way");
    log_file->write_raw("ERROR: audio tx line ");
    log_file->write_raw(get_line()->get_line_number()+1);
    log_file->write_raw(" - 3way is not active.\n");
    log_file->write_footer();
    return;
  }

  // Logging
  log_file->write_header("t_audio_tx::stop_3way");
  log_file->write_raw("Audio tx line ");
  log_file->write_raw(get_line()->get_line_number()+1);
  log_file->write_raw(": stop 3-way.\n");
  log_file->write_footer();

  is_3way = false;
  is_3way_mixer = false;

  media_3way_peer_tx.reset();
  mix_buf_3way.reset();
}

void t_audio_tx::post_media_peer_tx_3way(unsigned char *media, int len,
					 unsigned short peer_sample_rate)
{
  std::lock_guard<std::mutex> guard (mtx_3way);

  if (!is_3way || !is_3way_mixer)
  {
    return;
  }

  if (peer_sample_rate != sc_sample_rate)
  {
    // Resample media from peer to sample rate of this transmitter
    int output_len = (len / 2) * sc_sample_rate / peer_sample_rate;
    std::unique_ptr<short[]> output_buf(new short[output_len]);
    int resample_len = resample(reinterpret_cast<short *>(media), len / 2, peer_sample_rate,
				output_buf.get(), output_len, sc_sample_rate);
    media_3way_peer_tx->add(reinterpret_cast<unsigned char *>(output_buf.get()), resample_len * 2);
  }
  else
  {
    media_3way_peer_tx->add(media, len);
  }
}

bool t_audio_tx::get_is_3way_mixer() const
{
  return is_3way_mixer;
}
