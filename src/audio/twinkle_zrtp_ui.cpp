/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// Author: Werner Dittmann <Werner.Dittmann@t-online.de>, (C) 2006
//         Michel de Boer <michel@twinklephone.com>
#include "twinkle_zrtp_ui.h"

#ifdef HAVE_ZRTP

#include "phone.h"
#include "line.h"
#include "log.h"
#include "util.h"

using namespace GnuZrtpCodes;

extern t_phone *phone;

// Initialize static data
const std::map<int32, std::string> TwinkleZrtpUI::infoMap = {
  { InfoHelloReceived, "Hello received, preparing a Commit" },
  { InfoCommitDHGenerated, "Commit: Generated a public DH key" },
  { InfoRespCommitReceived, "Responder: Commit received, preparing DHPart1" },
  { InfoDH1DHGenerated, "DH1Part: Generated a public DH key" },
  { InfoInitDH1Received, "Initiator: DHPart1 received, preparing DHPart2" },
  { InfoRespDH2Received, "Responder: DHPart2 received, preparing Confirm1" },
  { InfoInitConf1Received, "Initiator: Confirm1 received, preparing Confirm2" },
  { InfoRespConf2Received, "Responder: Confirm2 received, preparing Conf2Ack" },
  { InfoRSMatchFound, "At least one retained secrets matches - security OK" },
  { InfoSecureStateOn, "Entered secure state" },
  { InfoSecureStateOff, "No more security for this session" }
};

const std::map<int32, std::string> TwinkleZrtpUI::warningMap = {
  { WarningDHAESmismatch, "Commit contains an AES256 cipher but does not offer a Diffie-Helman 4096" },
  { WarningGoClearReceived, "Received a GoClear message" },
  { WarningDHShort, "Hello offers an AES256 cipher but does not offer a Diffie-Helman 4096" },
  { WarningNoRSMatch, "No retained shared secrets available - must verify SAS" },
  { WarningCRCmismatch, "Internal ZRTP packet checksum mismatch - packet dropped" },
  { WarningSRTPauthError, "Dropping packet because SRTP authentication failed!" },
  { WarningSRTPreplayError, "Dropping packet because SRTP replay check failed!" },
  { WarningNoExpectedRSMatch, "Valid retained shared secrets availabe but no matches found - must verify SAS" }
};

const std::map<int32, std::string> TwinkleZrtpUI::severeMap = {
  { SevereHelloHMACFailed, "Hash HMAC check of Hello failed!" },
  { SevereCommitHMACFailed, "Hash HMAC check of Commit failed!" },
  { SevereDH1HMACFailed, "Hash HMAC check of DHPart1 failed!" },
  { SevereDH2HMACFailed, "Hash HMAC check of DHPart2 failed!" },
  { SevereCannotSend, "Cannot send data - connection or peer down?" },
  { SevereProtocolError, "Internal protocol error occured!" },
  { SevereNoTimer, "Cannot start a timer - internal resources exhausted?" },
  { SevereTooMuchRetries, "Too much retries during ZRTP negotiation - connection or peer down?" }
};

const std::map<int32, std::string> TwinkleZrtpUI::zrtpMap = {
  { MalformedPacket, "Malformed packet (CRC OK, but wrong structure)" },
  { CriticalSWError, "Critical software error" },
  { UnsuppZRTPVersion, "Unsupported ZRTP version" },
  { HelloCompMismatch, "Hello components mismatch" },
  { UnsuppHashType, "Hash type not supported" },
  { UnsuppCiphertype, "Cipher type not supported" },
  { UnsuppPKExchange, "Public key exchange not supported" },
  { UnsuppSRTPAuthTag, "SRTP auth. tag not supported" },
  { UnsuppSASScheme, "SAS scheme not supported" },
  { NoSharedSecret, "No shared secret available, DH mode required" },
  { DHErrorWrongPV, "DH Error: bad pvi or pvr ( == 1, 0, or p-1)" },
  { DHErrorWrongHVI, "DH Error: hvi != hashed data" },
  { SASuntrustedMiTM, "Received relayed SAS from untrusted MiTM" },
  { ConfirmHMACWrong, "Auth. Error: Bad Confirm pkt HMAC" },
  { NonceReused, "Nonce reuse" },
  { EqualZIDHello, "Equal ZIDs in Hello" },
  { GoCleatNotAllowed, "GoClear packet received, but not allowed" }
};

const std::string TwinkleZrtpUI::unknownCode = "Unknown error code";

namespace
{
  template<typename MapType>
  const typename MapType::value_type::second_type *
  findMapEntry(MapType const & m, typename MapType::value_type::first_type code)
  {
    auto const iter = m.find(code);
    return (iter != m.end()) ? &iter->second : nullptr;
  }
}

TwinkleZrtpUI::TwinkleZrtpUI(t_audio_session* session)
  : audioSession(session)
{ }

void TwinkleZrtpUI::secureOn(std::string cipher)
{
  audioSession->set_is_encrypted(true);
  audioSession->set_srtp_cipher_mode(cipher);

  t_line *line = audioSession->get_line();
  int lineno = line->get_line_number();

  log_file->write_header("TwinkleZrtpUI::secureOn");
  log_file->write_raw("Line ");
  log_file->write_raw(lineno + 1);
  log_file->write_raw(": audio encryption enabled: ");
  log_file->write_raw(cipher);
  log_file->write_endl();
  log_file->write_footer();

  ui->cb_line_encrypted(lineno, true);
  ui->cb_line_state_changed();
}

void TwinkleZrtpUI::secureOff()
{
  audioSession->set_is_encrypted(false);

  t_line *line = audioSession->get_line();
  int lineno = line->get_line_number();

  log_file->write_header("TwinkleZrtpUI::secureOff");
  log_file->write_raw("Line ");
  log_file->write_raw(lineno + 1);
  log_file->write_raw(": audio encryption disabled.\n");
  log_file->write_footer();

  ui->cb_line_encrypted(lineno, false);
  ui->cb_line_state_changed();
}

void TwinkleZrtpUI::showSAS(std::string sas, bool verified)
{
  audioSession->set_zrtp_sas(sas);
  audioSession->set_zrtp_sas_confirmed(verified);

  t_line *line = audioSession->get_line();
  int lineno = line->get_line_number();

  log_file->write_header("TwinkleZrtpUI::showSAS");
  log_file->write_raw("Line ");
  log_file->write_raw(lineno + 1);
  log_file->write_raw(": SAS =");
  log_file->write_raw(sas);
  log_file->write_endl();
  log_file->write_footer();

  if (!verified)
  {
    ui->cb_show_zrtp_sas(lineno, sas);
  }
  ui->cb_line_state_changed();
}

void TwinkleZrtpUI::confirmGoClear()
{
  t_line *line = audioSession->get_line();
  int lineno = line->get_line_number();

  ui->cb_zrtp_confirm_go_clear(lineno);
}

void TwinkleZrtpUI::showMessage(MessageSeverity sev, int subCode)
{
  t_line *line = audioSession->get_line();
  int lineno = line->get_line_number();

  std::string msg = "Line ";
  msg += int2str(lineno + 1);
  msg += ": ";
  msg += mapCodesToString(sev, subCode);

  switch (sev)
  {
  case Info:
   log_file->write_report(msg, "TwinkleZrtpUI::showMessage", LOG_NORMAL,
			  LOG_INFO);
   break;
  case Warning:
   log_file->write_report(msg, "TwinkleZrtpUI::showMessage", LOG_NORMAL,
			  LOG_WARNING);
   break;
  default:
   log_file->write_report(msg, "TwinkleZrtpUI::showMessage", LOG_NORMAL,
			  LOG_CRITICAL);
  }
}

void TwinkleZrtpUI::zrtpNegotiationFailed(MessageSeverity severity, int subCode)
{
  t_line *line = audioSession->get_line();
  int lineno = line->get_line_number();

  std::string m = "Line ";
  m += int2str(lineno + 1);
  m += ": ZRTP negotiation failed.\n";
  m += mapCodesToString(severity, subCode);

  switch (severity)
  {
  case Info:
   log_file->write_report(m, "TwinkleZrtpUI::zrtpNegotiationFailed", LOG_NORMAL,
			  LOG_INFO);
   break;
  case Warning:
   log_file->write_report(m, "TwinkleZrtpUI::zrtpNegotiationFailed", LOG_NORMAL,
			  LOG_WARNING);
   break;
  default:
   log_file->write_report(m, "TwinkleZrtpUI::zrtpNegotiationFailed", LOG_NORMAL,
			  LOG_CRITICAL);
  }
}

void TwinkleZrtpUI::zrtpNotSuppOther()
{
  t_line * const line(audioSession->get_line());
  const unsigned lineno(line->get_line_number());

  std::string msg = "Line ";
  msg += int2str(lineno + 1);
  msg += ": remote party does not support ZRTP.";
  log_file->write_report(msg, "TwinkleZrtpUI::zrtpNotSuppOther");
}

const std::string &
TwinkleZrtpUI::mapCodesToString(MessageSeverity severity, int subCode)
{
  const std::string *m = nullptr;

  switch (severity)
  {
  case Info:
   m = findMapEntry(infoMap, subCode);
   break;
  case Warning:
   m = findMapEntry(warningMap, subCode);
   break;
  case Severe:
   m = findMapEntry(severeMap, subCode);
   break;
  case ZrtpError:
   m = findMapEntry(zrtpMap, subCode < 0 ? -subCode : subCode);
   break;
  default:
   break;
  }

  return m ? *m : unknownCode;;
}

#endif
