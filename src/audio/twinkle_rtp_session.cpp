/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <unistd.h>
#include "twinkle_rtp_session.h"
#include "log.h"
#include "sys_settings.h"

#define TWINKLE_ZID_FILE	".twinkle.zid"

namespace
{
  class t_twinkle_ipv4_rtp_session
    : public t_twinkle_rtp_session,
#ifdef HAVE_ZRTP
      public ost::SymmetricZRTPSession
#else
      public ost::SymmetricRTPSession
#endif
  {
  public:
    explicit t_twinkle_ipv4_rtp_session(net::ip::endpoint const & endpoint);
    ~t_twinkle_ipv4_rtp_session() override;

    uint32 getFirstTimestamp(const ost::SyncSource *src = nullptr) const override;
    uint32 getCurrentTimestamp() const override;
    uint32 getLastTimestamp(const ost::SyncSource *src = nullptr) const override;
    const ost::AppDataUnit *getData(uint32 stamp, const ost::SyncSource *src = nullptr) override;
    void putData(uint32 stamp, const unsigned char *data, size_t len) override;
    bool isWaiting(const ost::SyncSource *src = nullptr) const override;

    void setExpireTimeout(ost::microtimeout_t to) override;
    void setMark(bool mark) override;
    bool addDestination(net::ip::endpoint const & endpoint) override;
    void startRunning() override;
    bool setPayloadFormat(const ost::PayloadFormat &pf) override;

#ifdef HAVE_ZRTP
    void init_zrtp() override;
    bool is_zrtp_initialized() const override;

    ost::ZrtpQueue &get_zrtp_queue() override;

  private:
    bool zrtp_initialized_;
#endif
  };

  class t_twinkle_ipv6_rtp_session
    : public t_twinkle_rtp_session,
#ifdef HAVE_ZRTP
      public ost::SymmetricZRTPSessionIPV6
#else
      public ost::SymmetricRTPSessionIPV6
#endif
  {
  public:
    explicit t_twinkle_ipv6_rtp_session(net::ip::endpoint const & endpoint);
    ~t_twinkle_ipv6_rtp_session() override;

    uint32 getFirstTimestamp(const ost::SyncSource *src = nullptr) const override;
    uint32 getCurrentTimestamp() const override;
    uint32 getLastTimestamp(const ost::SyncSource *src = nullptr) const override;
    const ost::AppDataUnit *getData(uint32 stamp, const ost::SyncSource *src = nullptr) override;
    void putData(uint32 stamp, const unsigned char *data, size_t len) override;
    bool isWaiting(const ost::SyncSource *src = nullptr) const override;

    void setExpireTimeout(ost::microtimeout_t to)  override;
    void setMark(bool mark) override;
    bool addDestination(net::ip::endpoint const & endpoint) override;
    void startRunning() override;
    bool setPayloadFormat(const ost::PayloadFormat &pf) override;

    // hack to work around lack of IPv6 support in ccrtp
    size_t recvData(unsigned char *buffer, size_t length, ost::InetHostAddress &host, ost::tpport_t &port) override;
    size_t recvControl(unsigned char *buffer, size_t length, ost::InetHostAddress &host, ost::tpport_t &port) override;

#ifdef HAVE_ZRTP
    void init_zrtp() override;
    bool is_zrtp_initialized() const override;

    ost::ZrtpQueue &get_zrtp_queue() override;

  private:
    bool zrtp_initialized_;
#endif
  };
}

t_twinkle_rtp_session::t_twinkle_rtp_session() = default;

t_twinkle_rtp_session::~t_twinkle_rtp_session() = default;

t_twinkle_rtp_session *t_twinkle_rtp_session::create(net::ip::endpoint const & endpoint)
{
  return endpoint.addr().is_v6()
    ? static_cast<t_twinkle_rtp_session *>(new t_twinkle_ipv6_rtp_session(endpoint))
    : static_cast<t_twinkle_rtp_session *>(new t_twinkle_ipv4_rtp_session(endpoint));
}


t_twinkle_ipv4_rtp_session::t_twinkle_ipv4_rtp_session(net::ip::endpoint const & endpoint)
#ifdef HAVE_ZRTP
  : ost::SymmetricZRTPSession(ost::IPV4Host(endpoint.addr().get_v4()),
			      endpoint.port()),
    zrtp_initialized_(false)
#else
  : ost::SymmetricRTPSession(ost::IPV4Host(endpoint.addr().get_v4()),
			     endpoint.port())
#endif
{ }

t_twinkle_ipv4_rtp_session::~t_twinkle_ipv4_rtp_session() = default;

uint32 t_twinkle_ipv4_rtp_session::getFirstTimestamp(const ost::SyncSource *src) const
{
  return SingleThreadRTPSession::getFirstTimestamp(src);
}

uint32 t_twinkle_ipv4_rtp_session::getCurrentTimestamp() const
{
  return SingleThreadRTPSession::getCurrentTimestamp();
}

uint32 t_twinkle_ipv4_rtp_session::getLastTimestamp(const ost::SyncSource *src) const
{
  if (src && !isMine(*src) ) return 0L;

  recvLock.readLock();

  uint32 ts = 0;
  if (src)
  {
    SyncSourceLink* srcm = getLink(*src);
    IncomingRTPPktLink* l = srcm->getFirst();

    while (l)
    {
      ts = l->getTimestamp();
      l = l->getSrcNext();
    }
  }
  else
  {
    IncomingRTPPktLink* l = recvFirst;

    while (l)
    {
      ts = l->getTimestamp();
      l = l->getNext();
    }
  }

  recvLock.unlock();
  return ts;
}

const ost::AppDataUnit *t_twinkle_ipv4_rtp_session::getData(uint32 stamp, const ost::SyncSource *src)
{
  return SingleThreadRTPSession::getData(stamp, src);
}

void t_twinkle_ipv4_rtp_session::putData(uint32 stamp, const unsigned char *data, size_t len)
{
  SingleThreadRTPSession::putData(stamp, data, len);
}

bool t_twinkle_ipv4_rtp_session::isWaiting(const ost::SyncSource *src) const
{
  return SingleThreadRTPSession::isWaiting(src);
}

void t_twinkle_ipv4_rtp_session::setExpireTimeout(ost::microtimeout_t to)
{
  return SingleThreadRTPSession::setExpireTimeout(to);
}

void t_twinkle_ipv4_rtp_session::setMark(bool mark)
{
  return SingleThreadRTPSession::setMark(mark);
}

bool t_twinkle_ipv4_rtp_session::addDestination(net::ip::endpoint const & endpoint)
{
  ost::IPV4Host const & host (ost::IPV4Host(endpoint.addr().get_v4()));

  const bool result = SingleThreadRTPSession::addDestination(host, endpoint.port());

  setDataPeer(host, endpoint.port());
  setControlPeer(host, endpoint.port() + 1);

  return result;
}

void t_twinkle_ipv4_rtp_session::startRunning()
{
  SingleThreadRTPSession::startRunning();
}

bool t_twinkle_ipv4_rtp_session::setPayloadFormat(const ost::PayloadFormat &pf)
{
  return SingleThreadRTPSession::setPayloadFormat(pf);
}

#ifdef HAVE_ZRTP
void t_twinkle_ipv4_rtp_session::init_zrtp()
{
  std::string zid_filename = sys_config->get_dir_user();
  zid_filename += '/';
  zid_filename += TWINKLE_ZID_FILE;

  if (initialize(zid_filename.c_str()) >= 0)
  {
    zrtp_initialized_ = true;
    return;
  }

  // ZID file initialization failed. Maybe the ZID file
  // is corrupt. Try to remove it
  if (unlink(zid_filename.c_str()) < 0)
  {
    std::string msg = "Failed to remove ";
    msg += zid_filename;
    log_file->write_report(msg,
			   "t_twinkle_rtp_session::init_zrtp",
			   LOG_NORMAL, LOG_CRITICAL);
    return;
  }

  // Try to initialize once more
  if (initialize(zid_filename.c_str()) >= 0)
  {
    zrtp_initialized_ = true;
  }
  else
  {
    std::string msg = "Failed to initialize ZRTP - ";
    msg += zid_filename;
    log_file->write_report(msg,
			   "t_twinkle_rtp_session::init_zrtp",
			   LOG_NORMAL, LOG_CRITICAL);
  }
}

bool t_twinkle_ipv4_rtp_session::is_zrtp_initialized() const
{
  return zrtp_initialized_;
}

ost::ZrtpQueue &t_twinkle_ipv4_rtp_session::get_zrtp_queue()
{
  return *this;
}
#endif


t_twinkle_ipv6_rtp_session::t_twinkle_ipv6_rtp_session(net::ip::endpoint const & endpoint)
#ifdef HAVE_ZRTP
  : ost::SymmetricZRTPSessionIPV6(ost::IPV6Host(endpoint.addr().get_v6()),
				  endpoint.port()),
    zrtp_initialized_(false)
#else
  : ost::SymmetricRTPSessionIPV6(ost::IPV6Host(endpoint.addr().get_v6()),
				 endpoint.port())
#endif
{ }

t_twinkle_ipv6_rtp_session::~t_twinkle_ipv6_rtp_session() = default;

uint32 t_twinkle_ipv6_rtp_session::getFirstTimestamp(const ost::SyncSource *src) const
{
  return SingleThreadRTPSessionIPV6::getFirstTimestamp(src);
}

uint32 t_twinkle_ipv6_rtp_session::getCurrentTimestamp() const
{
  return SingleThreadRTPSessionIPV6::getCurrentTimestamp();
}

uint32 t_twinkle_ipv6_rtp_session::getLastTimestamp(const ost::SyncSource *src) const
{
  if (src && !isMine(*src) ) return 0L;

  recvLock.readLock();

  uint32 ts = 0;
  if (src)
  {
    SyncSourceLink* srcm = getLink(*src);
    IncomingRTPPktLink* l = srcm->getFirst();

    while (l)
    {
      ts = l->getTimestamp();
      l = l->getSrcNext();
    }
  }
  else
  {
    IncomingRTPPktLink* l = recvFirst;

    while (l)
    {
      ts = l->getTimestamp();
      l = l->getNext();
    }
  }

  recvLock.unlock();
  return ts;
}

const ost::AppDataUnit *t_twinkle_ipv6_rtp_session::getData(uint32 stamp, const ost::SyncSource *src)
{
  return SingleThreadRTPSessionIPV6::getData(stamp, src);
}

void t_twinkle_ipv6_rtp_session::putData(uint32 stamp, const unsigned char *data, size_t len)
{
  SingleThreadRTPSessionIPV6::putData(stamp, data, len);
}

bool t_twinkle_ipv6_rtp_session::isWaiting(const ost::SyncSource *src) const
{
  return SingleThreadRTPSessionIPV6::isWaiting(src);
}

void t_twinkle_ipv6_rtp_session::setExpireTimeout(ost::microtimeout_t to)
{
  return SingleThreadRTPSessionIPV6::setExpireTimeout(to);
}

void t_twinkle_ipv6_rtp_session::setMark(bool mark)
{
  return SingleThreadRTPSessionIPV6::setMark(mark);
}

bool t_twinkle_ipv6_rtp_session::addDestination(net::ip::endpoint const & endpoint)
{
  ost::IPV6Host const & host (ost::IPV6Host(endpoint.addr().get_v6()));

  const bool result = SingleThreadRTPSessionIPV6::addDestination(host, endpoint.port());

  // should already have been called in addDestination, but for some
  // reason it doesn't appear to be happening... :-(
  setDataPeerIPV6(host, endpoint.port());
  setControlPeerIPV6(host, endpoint.port() + 1);

  return result;
}

void t_twinkle_ipv6_rtp_session::startRunning()
{
  SingleThreadRTPSessionIPV6::startRunning();
}

bool t_twinkle_ipv6_rtp_session::setPayloadFormat(const ost::PayloadFormat &pf)
{
  return SingleThreadRTPSessionIPV6::setPayloadFormat(pf);
}

size_t t_twinkle_ipv6_rtp_session::recvData(unsigned char *buffer, size_t length, ost::InetHostAddress &host, ost::tpport_t &port)
{
  ost::IPV6Host hostIpv6;
  const size_t res =
    SingleThreadRTPSessionIPV6::recvData(buffer, length, hostIpv6, port);

  return res;
}

size_t t_twinkle_ipv6_rtp_session::recvControl(unsigned char *buffer, size_t length, ost::InetHostAddress &host, ost::tpport_t &port)
{
  ost::IPV6Host hostIpv6;
  const size_t res =
    SingleThreadRTPSessionIPV6::recvControl(buffer, length, hostIpv6, port);

  return res;
}

#ifdef HAVE_ZRTP
void t_twinkle_ipv6_rtp_session::init_zrtp()
{
  std::string zid_filename = sys_config->get_dir_user();
  zid_filename += '/';
  zid_filename += TWINKLE_ZID_FILE;

  if (initialize(zid_filename.c_str()) >= 0)
  {
    zrtp_initialized_ = true;
    return;
  }

  // ZID file initialization failed. Maybe the ZID file
  // is corrupt. Try to remove it
  if (unlink(zid_filename.c_str()) < 0)
  {
    std::string msg = "Failed to remove ";
    msg += zid_filename;
    log_file->write_report(msg,
			   "t_twinkle_rtp_session::init_zrtp",
			   LOG_NORMAL, LOG_CRITICAL);
    return;
  }

  // Try to initialize once more
  if (initialize(zid_filename.c_str()) >= 0)
  {
    zrtp_initialized_ = true;
  }
  else
  {
    std::string msg = "Failed to initialize ZRTP - ";
    msg += zid_filename;
    log_file->write_report(msg,
			   "t_twinkle_rtp_session::init_zrtp",
			   LOG_NORMAL, LOG_CRITICAL);
  }
}

bool t_twinkle_ipv6_rtp_session::is_zrtp_initialized() const
{
  return zrtp_initialized_;
}

ost::ZrtpQueue &t_twinkle_ipv6_rtp_session::get_zrtp_queue()
{
  return *this;
}
#endif
