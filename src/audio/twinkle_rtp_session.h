/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef TWINKLE_RTP_SESSION_H
#define TWINKLE_RTP_SESSION_H

#include "gtwinkle_config.h"
#include "net/endpoint.h"

#include <string>

#ifdef HAVE_ZRTP
#include <libzrtpcpp/zrtpccrtp.h>
#else
#include <ccrtp/rtp.h>
#endif

class t_twinkle_rtp_session
{
public:
  t_twinkle_rtp_session();
  virtual ~t_twinkle_rtp_session();

  static t_twinkle_rtp_session *create(net::ip::endpoint const & endpoint);

  virtual uint32 getFirstTimestamp(const ost::SyncSource *src = nullptr) const = 0;
  virtual uint32 getCurrentTimestamp() const = 0;
  virtual uint32 getLastTimestamp(const ost::SyncSource *src = nullptr) const = 0;
  virtual const ost::AppDataUnit *getData(uint32 stamp, const ost::SyncSource *src = nullptr) = 0;
  virtual void putData(uint32 stamp, const unsigned char *data, size_t len) = 0;
  virtual bool isWaiting(const ost::SyncSource *src = nullptr) const = 0;

  virtual void setExpireTimeout(ost::microtimeout_t to) = 0;
  virtual void setMark(bool mark) = 0;

  virtual bool addDestination(net::ip::endpoint const & endpoint) = 0;
  virtual void startRunning() = 0;
  virtual bool setPayloadFormat(const ost::PayloadFormat &pf) = 0;

#ifdef HAVE_ZRTP
  virtual void init_zrtp() = 0;
  virtual bool is_zrtp_initialized() const = 0;

  virtual ost::ZrtpQueue &get_zrtp_queue() = 0;
#endif
};

// to disambiguate between std::swap and ucommon::swap :-(
inline void swap(t_twinkle_rtp_session *&a, t_twinkle_rtp_session *&b)
{
  std::swap(a, b);
}

#endif
