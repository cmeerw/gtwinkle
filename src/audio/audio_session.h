/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _AUDIO_SESSION_H
#define _AUDIO_SESSION_H

#include "audio_rx.h"
#include "audio_tx.h"
#include "session.h"
#include "twinkle_rtp_session.h"

#include "net/address.h"
#include "net/endpoint.h"

#include <atomic>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <unordered_map>
#include <vector>

#include <giomm.h>

#ifdef HAVE_SPEEX
#include <speex/speex_echo.h>
#endif


// Forward declarations
class t_session;
class t_line;

class t_audio_session
{
private:
  const std::string dev_speaker;
  const std::string dev_mic;

  // SIP session owning this audio session
  std::atomic<t_session *> session;

  // This flag indicates if the created audio session is valid.
  // It might be invalid because, the RTP session could not be created
  // or the soundcard could not be opened.
  bool valid;

  // file descriptor audio device
  std::shared_ptr<t_audio_io> speaker;
  std::shared_ptr<t_audio_io> mic;
  std::unique_ptr<t_twinkle_rtp_session> rtp_session;

  t_audio_codec	codec;
  unsigned short ptime;	// in milliseconds

  std::thread thr_audio_rx; // recording thread
  std::thread thr_audio_tx; // playing thread

  // ZRTP info
  mutable std::mutex mtx_zrtp_data;
  bool is_encrypted;
  std::string zrtp_sas;
  bool zrtp_sas_confirmed;
  std::string srtp_cipher_mode;

#ifdef HAVE_SPEEX
  // Indicator whether to use (Speex) AEC
  bool do_echo_cancellation;

  // Indicator whether the last operation of (Speex) AEC,
  // speex_echo_capture or speex_echo_playback, was the speex_echo_capture
  bool echo_captured_last;

  // speex AEC state
  std::unique_ptr<SpeexEchoState,
		  decltype(&speex_echo_state_destroy)> speex_echo_state;
#endif

  // 3-way conference data
  // Returns if this audio session is part of a 3-way conference
  bool is_3way() const;

  // Returns the peer audio session of a 3-way conference
  t_audio_session *get_peer_3way() const;

  // Open the sound card
  bool open_dsp();
  bool open_dsp_full_duplex();
  bool open_dsp_speaker();
  bool open_dsp_mic();

public:

  std::unique_ptr<t_audio_rx> audio_rx;
  std::unique_ptr<t_audio_tx> audio_tx;

  t_audio_session(Glib::RefPtr<const Gio::Settings> const &audio_settings,
		  t_session *_session,
		  net::ip::endpoint const &_recv_endpoint,
		  net::ip::endpoint const &_dst_endpoint,
		  t_audio_codec _codec, unsigned short _ptime,
		  const std::unordered_map<unsigned short, t_audio_codec> &recv_payload2ac,
		  const std::vector<unsigned short> &send_ac2payload,
		  bool encrypt);

  ~t_audio_session();

  void run();

  /**
   * Change the owning session.
   * @param _session New session owning this audio session.
   */
  void set_session(t_session *_session);

  // Set outgoing/incoming DTMF dynamic payload types
  void set_pt_out_dtmf(unsigned short pt);
  void set_pt_in_dtmf(unsigned short pt, unsigned short pt_alt);

  // Send DTMF digit
  void send_dtmf(char digit, bool inband);

  // Get the line that belongs to this audio session
  t_line *get_line() const;

  // Become the first session in a 3-way conference
  void start_3way();

  // Leave a 3-way conference
  void stop_3way();

  // Check if audio session is valid
  bool is_valid() const;

  // Get pointer for soundcard I/O object
  std::shared_ptr<t_audio_io> get_dsp_speaker() const;
  std::shared_ptr<t_audio_io> get_dsp_mic() const;

  // Check if sample rate from speaker and mic match with sample rate
  // from codec. The sample rates might not match due to 3-way conference
  // calls with mixed sample rate
  bool matching_sample_rates() const;

  // ZRTP actions
  void confirm_zrtp_sas();
  void reset_zrtp_sas_confirmation();
  void enable_zrtp();
  void zrtp_request_go_clear();
  void zrtp_go_clear_ok();

  // ZRTP data manipulations
  bool get_is_encrypted() const;
  std::string get_zrtp_sas() const;
  bool get_zrtp_sas_confirmed() const;
  std::string get_srtp_cipher_mode() const;

  void set_is_encrypted(bool on);
  void set_zrtp_sas(const std::string &sas);
  void set_zrtp_sas_confirmed(bool confirmed);
  void set_srtp_cipher_mode(const std::string &cipher_mode);

#ifdef HAVE_SPEEX
  // speex acoustic echo cancellation (AEC) manipulations
  bool get_do_echo_cancellation() const;
  bool get_echo_captured_last();
  void set_echo_captured_last(bool value);
  SpeexEchoState *get_speex_echo_state();
#endif
};

#endif
