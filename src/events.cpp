/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <iostream>
#include "events.h"
#include "log.h"
#include "userintf.h"
#include "util.h"

std::string event_type2str(t_event_type t)
{
  switch(t)
  {
  case EV_QUIT:		return "EV_QUIT";
  case EV_NETWORK:	return "EV_NETWORK";
  case EV_USER:		return "EV_USER";
  case EV_TIMEOUT:	return "EV_TIMEOUT";
  case EV_FAILURE:	return "EV_FAILURE";
  case EV_START_TIMER:	return "EV_START_TIMER";
  case EV_STOP_TIMER:	return "EV_STOP_TIMER";
  case EV_ABORT_TRANS:	return "EV_ABORT_TRANS";
  case EV_STUN_REQUEST:	return "EV_STUN_REQUEST";
  case EV_STUN_RESPONSE: return "EV_STUN_RESPONSE";
  case EV_NAT_KEEPALIVE: return "EV_NAT_KEEPALIVE";
  case EV_ICMP:		return "EV_ICMP";
  case EV_ASYNC_RESPONSE: return "EV_ASYNC_RESPONSE";
  case EV_BROKEN_CONNECTION: return "EV_BROKEN_CONNECTION";
  case EV_TCP_PING:	return "EV_TCP_PING";
  }

  return "UNKNOWN";
}

///////////////////////////////////////////////////////////
// class t_event_network
///////////////////////////////////////////////////////////

t_event_network::t_event_network(t_sip_message const &m)
  : t_event(), msg(m.copy())
{ }

t_event_network::~t_event_network() = default;

t_event_type t_event_network::get_type() const
{
  return EV_NETWORK;
}

t_sip_message &t_event_network::get_msg() const
{
  return *msg;
}

///////////////////////////////////////////////////////////
// class t_event_quit
///////////////////////////////////////////////////////////

t_event_quit::~t_event_quit() = default;

t_event_type t_event_quit::get_type() const
{
  return EV_QUIT;
}

///////////////////////////////////////////////////////////
// class t_event_user
///////////////////////////////////////////////////////////

t_event_user::t_event_user(t_user *u, t_sip_message const &m,
			   unsigned short _tuid, unsigned short _tid)
  : t_event(), msg(m.copy()), tuid(_tuid), tid(_tid), tid_cancel_target(0),
    user_config(u ? u->copy() : nullptr)
{ }

t_event_user::t_event_user(t_user *u, t_sip_message const &m,
			   unsigned short _tuid, unsigned short _tid,
			   unsigned short _tid_cancel_target)
  : t_event(), msg(m.copy()), tuid(_tuid), tid(_tid),
    tid_cancel_target(_tid_cancel_target),
    user_config(u ? u->copy() : nullptr)
{ }

t_event_user::~t_event_user() = default;

t_event_type t_event_user::get_type() const
{
  return EV_USER;
}

t_sip_message &t_event_user::get_msg() const
{
  return *msg;
}

unsigned short t_event_user::get_tuid() const
{
  return tuid;
}

unsigned short t_event_user::get_tid() const
{
  return tid;
}

unsigned short t_event_user::get_tid_cancel_target() const
{
  return tid_cancel_target;
}

t_user *t_event_user::get_user_config() const
{
  return user_config.get();
}

///////////////////////////////////////////////////////////
// class t_event_timeout
///////////////////////////////////////////////////////////

t_event_timeout::t_event_timeout(t_timer &t)
  : t_event(), timer(t.copy())
{ }

t_event_timeout::~t_event_timeout() = default;

t_event_type t_event_timeout::get_type() const
{
  return EV_TIMEOUT;
}

t_timer &t_event_timeout::get_timer() const
{
  return *timer;
}

///////////////////////////////////////////////////////////
// class t_event_failure
///////////////////////////////////////////////////////////
t_event_failure::t_event_failure(t_failure f, unsigned short _tid)
  : t_event(), failure(f), tid_populated(true), tid(_tid)
{ }

t_event_failure::t_event_failure(t_failure f, std::string _branch, const t_method &_cseq_method)
: failure(f),
  tid_populated(false),
  branch(std::move(_branch)),
  cseq_method(_cseq_method)
{ }

t_event_type t_event_failure::get_type() const
{
  return EV_FAILURE;
}

t_failure t_event_failure::get_failure() const
{
  return failure;
}

unsigned short t_event_failure::get_tid() const
{
  return tid;
}

std::string t_event_failure::get_branch() const
{
  return branch;
}

t_method t_event_failure::get_cseq_method() const
{
  return cseq_method;
}

bool t_event_failure::is_tid_populated() const
{
  return tid_populated;
}

///////////////////////////////////////////////////////////
// class t_event_start_timer
///////////////////////////////////////////////////////////
t_event_start_timer::t_event_start_timer(std::unique_ptr<t_timer> t)
  : t_event(), timer(std::move(t))
{ }

t_event_type t_event_start_timer::get_type() const
{
  return EV_START_TIMER;
}

std::unique_ptr<t_timer> t_event_start_timer::get_timer()
{
  return std::move(timer);
}

///////////////////////////////////////////////////////////
// class t_event_stop_timer
///////////////////////////////////////////////////////////
t_event_stop_timer::t_event_stop_timer(unsigned short id)
  : t_event(), timer_id(id)
{ }

t_event_type t_event_stop_timer::get_type() const
{
  return EV_STOP_TIMER;
}

unsigned short t_event_stop_timer::get_timer_id() const
{
  return timer_id;
}

///////////////////////////////////////////////////////////
// class t_event_abort_trans
///////////////////////////////////////////////////////////
t_event_abort_trans::t_event_abort_trans(unsigned short _tid)
  : t_event(), tid(_tid)
{ }

t_event_type t_event_abort_trans::get_type() const
{
  return EV_ABORT_TRANS;
}

unsigned short t_event_abort_trans::get_tid() const
{
  return tid;
}

///////////////////////////////////////////////////////////
// class t_event_stun_request
///////////////////////////////////////////////////////////

t_event_stun_request::t_event_stun_request(t_user &u,
					   StunMessage &m,
					   t_stun_event_type ev_type,
					   unsigned short _tuid,
					   unsigned short _tid)
  : t_event(), msg(new StunMessage(m)), tuid(_tuid), tid(_tid),
    stun_event_type(ev_type), user_config(u.copy()), src_port(0)
{ }

t_event_stun_request::~t_event_stun_request() = default;

t_event_type t_event_stun_request::get_type() const
{
  return EV_STUN_REQUEST;
}

StunMessage &t_event_stun_request::get_msg() const
{
  return *msg;
}

unsigned short t_event_stun_request::get_tuid() const
{
  return tuid;
}

unsigned short t_event_stun_request::get_tid() const
{
  return tid;
}

t_stun_event_type t_event_stun_request::get_stun_event_type() const
{
  return stun_event_type;
}

t_user *t_event_stun_request::get_user_config() const
{
  return user_config.get();
}

///////////////////////////////////////////////////////////
// class t_event_stun_response
///////////////////////////////////////////////////////////

t_event_stun_response::t_event_stun_response(StunMessage &m,
					     unsigned short _tuid,
					     unsigned short _tid)
  : t_event(), msg(new StunMessage(m)), tuid(_tuid), tid(_tid)
{ }

t_event_stun_response::~t_event_stun_response() = default;

t_event_type t_event_stun_response::get_type() const
{
  return EV_STUN_RESPONSE;
}

StunMessage &t_event_stun_response::get_msg() const
{
  return *msg;
}

unsigned short t_event_stun_response::get_tuid() const
{
  return tuid;
}

unsigned short t_event_stun_response::get_tid() const
{
  return tid;
}

///////////////////////////////////////////////////////////
// class t_event_nat_keepalive
///////////////////////////////////////////////////////////
t_event_type t_event_nat_keepalive::get_type() const
{
  return EV_NAT_KEEPALIVE;
}

///////////////////////////////////////////////////////////
// class t_event_icmp
///////////////////////////////////////////////////////////
t_event_icmp::t_event_icmp(const t_icmp_msg &m) : icmp(m)
{ }

t_event_type t_event_icmp::get_type() const
{
  return EV_ICMP;
}

t_icmp_msg t_event_icmp::get_icmp() const
{
  return icmp;
}

///////////////////////////////////////////////////////////
// class t_event_async_response
///////////////////////////////////////////////////////////

t_event_async_response::t_event_async_response(t_response_type type)
  : t_event(), response_type(type)
{ }

t_event_type t_event_async_response::get_type() const
{
  return EV_ASYNC_RESPONSE;
}

void t_event_async_response::set_bool_response(bool b)
{
  bool_response = b;
}

t_event_async_response::t_response_type t_event_async_response::get_response_type() const
{
  return response_type;
}

bool t_event_async_response::get_bool_response() const
{
  return bool_response;
}

///////////////////////////////////////////////////////////
// class t_event_broken_connection
///////////////////////////////////////////////////////////

t_event_broken_connection::t_event_broken_connection(t_url url)
  : t_event(), user_uri_(std::move(url))
{ }

t_event_type t_event_broken_connection::get_type() const
{
  return EV_BROKEN_CONNECTION;
}

t_url t_event_broken_connection::get_user_uri() const
{
  return user_uri_;
}

///////////////////////////////////////////////////////////
// class t_event_tcp_ping
///////////////////////////////////////////////////////////

t_event_tcp_ping::t_event_tcp_ping(t_url url, net::ip::endpoint dst)
  : t_event(), user_uri_(std::move(url)), dst_(dst)
{ }

t_event_type t_event_tcp_ping::get_type() const
{
  return EV_TCP_PING;
}

t_url t_event_tcp_ping::get_user_uri() const
{
  return user_uri_;
}

net::ip::endpoint const & t_event_tcp_ping::get_dst() const
{
  return dst_;
}

///////////////////////////////////////////////////////////
// class t_event_queue
///////////////////////////////////////////////////////////

t_event_queue::t_event_queue()
  : caught_interrupt(false)
{ }

t_event_queue::~t_event_queue()
{
  log_file->write_header("t_event_queue::~t_event_queue", LOG_NORMAL, LOG_INFO);
  log_file->write_raw("Clean up event queue.\n");

  while (!ev_queue.empty())
  {
    std::unique_ptr<t_event> e(std::move(ev_queue.front()));
    ev_queue.pop();
    log_file->write_raw("\nDeleting unprocessed event: \n");
    log_file->write_raw("Type: ");
    log_file->write_raw(event_type2str(e->get_type()));
    log_file->write_raw(", Pointer: ");
    log_file->write_raw(ptr2str(e.get()));
    log_file->write_endl();
  }

  log_file->write_footer();
}

void t_event_queue::push(std::unique_ptr<t_event> e)
{
  {
    std::lock_guard<std::mutex> guard(mutex_evq);
    ev_queue.push(std::move(e));
  }
  cond_evq.notify_one();
}

void t_event_queue::push_quit()
{
  std::unique_ptr<t_event_quit> event(new t_event_quit());
  push(std::move(event));
}

void t_event_queue::push_network(t_sip_message const &m,
				 const t_ip_port &ip_port)
{
  std::unique_ptr<t_event_network> event(new t_event_network(m));
  event->dst = ip_port.endpoint;
  event->transport = ip_port.transport;
  push(std::move(event));
}

void t_event_queue::push_user(t_user &user_config, t_sip_message const &m,
			      unsigned short tuid, unsigned short tid)
{
  std::unique_ptr<t_event_user> event(new t_event_user(&user_config, m, tuid, tid));
  push(std::move(event));
}

void t_event_queue::push_user(t_sip_message const &m, unsigned short tuid,
			      unsigned short tid)
{
  std::unique_ptr<t_event_user> event(new t_event_user(nullptr, m, tuid, tid));
  push(std::move(event));
}

void t_event_queue::push_user_cancel(t_user &user_config, t_sip_message const &m,
				     unsigned short tuid, unsigned short tid,
				     unsigned short target_tid)
{
  std::unique_ptr<t_event_user> event(new t_event_user(&user_config, m, tuid, tid, target_tid));
  push(std::move(event));
}

void t_event_queue::push_user_cancel(t_sip_message const &m, unsigned short tuid,
				     unsigned short tid,
				     unsigned short target_tid)
{
  std::unique_ptr<t_event_user> event(new t_event_user(nullptr, m, tuid, tid, target_tid));
  push(std::move(event));
}

void t_event_queue::push_timeout(t_timer &t)
{
  std::unique_ptr<t_event_timeout> event(new t_event_timeout(t));
  push(std::move(event));
}

void t_event_queue::push_failure(t_failure f, unsigned short tid)
{
  std::unique_ptr<t_event_failure> event(new t_event_failure(f, tid));
  push(std::move(event));
}

void t_event_queue::push_failure(t_failure f, const std::string &branch,
				 const t_method &cseq_method)
{
  std::unique_ptr<t_event_failure> event(new t_event_failure(f, branch, cseq_method));
  push(std::move(event));
}

void t_event_queue::push_start_timer(std::unique_ptr<t_timer> t)
{
  std::unique_ptr<t_event_start_timer> event(new t_event_start_timer(std::move(t)));
  push(std::move(event));
}

void t_event_queue::push_stop_timer(unsigned short timer_id)
{
  std::unique_ptr<t_event_stop_timer> event(new t_event_stop_timer(timer_id));
  push(std::move(event));
}

void t_event_queue::push_abort_trans(unsigned short tid)
{
  std::unique_ptr<t_event_abort_trans> event(new t_event_abort_trans(tid));
  push(std::move(event));
}

void t_event_queue::push_stun_request(t_user &user_config,
				      StunMessage &m, t_stun_event_type ev_type,
				      unsigned short tuid, unsigned short tid,
				      net::ip::endpoint const &dst, unsigned short src_port)
{
  std::unique_ptr<t_event_stun_request> event(
    new t_event_stun_request(user_config, m, ev_type, tuid, tid));
  event->dst = dst;
  event->src_port = src_port;

  push(std::move(event));
}

void t_event_queue::push_stun_response(StunMessage &m,
				       unsigned short tuid, unsigned short tid)
{
  std::unique_ptr<t_event_stun_response> event(new t_event_stun_response(m, tuid, tid));
  push(std::move(event));;
}

void t_event_queue::push_nat_keepalive(net::ip::endpoint const &dst)
{
  std::unique_ptr<t_event_nat_keepalive> event(new t_event_nat_keepalive());
  event->dst = dst;

  push(std::move(event));;
}

void t_event_queue::push_icmp(const t_icmp_msg &m)
{
  std::unique_ptr<t_event_icmp> event(new t_event_icmp(m));
  push(std::move(event));;
}

void t_event_queue::push_refer_permission_response(bool permission)
{
  std::unique_ptr<t_event_async_response> event(
    new t_event_async_response(t_event_async_response::RESP_REFER_PERMISSION));
  event->set_bool_response(permission);
  push(std::move(event));;
}

void t_event_queue::push_broken_connection(const t_url &user_uri)
{
  std::unique_ptr<t_event_broken_connection> event(
    new t_event_broken_connection(user_uri));
  push(std::move(event));;
}

void t_event_queue::push_tcp_ping(const t_url &user_uri, net::ip::endpoint const &dst)
{
  std::unique_ptr<t_event_tcp_ping> event(new t_event_tcp_ping(user_uri, dst));
  push(std::move(event));;
}

std::unique_ptr<t_event> t_event_queue::pop()
{
  std::unique_lock<std::mutex> l(mutex_evq);

  cond_evq.wait(l, [this] () { return !ev_queue.empty(); });

  // This pop is non-interruptable, so ignore the interrupt
  caught_interrupt = false;

  std::unique_ptr<t_event> e (std::move(ev_queue.front()));
  ev_queue.pop();
  return e;
}

std::unique_ptr<t_event> t_event_queue::pop(bool &interrupted)
{
  std::unique_lock<std::mutex> l(mutex_evq);

  cond_evq.wait(l, [this] () { return caught_interrupt || !ev_queue.empty(); });

  std::unique_ptr<t_event> e;
  if (caught_interrupt)
  {
    caught_interrupt = false;
    interrupted = true;
  }
  else
  {
    interrupted = false;
    e = std::move(ev_queue.front());
    ev_queue.pop();
  }

  return e;
}

void t_event_queue::interrupt()
{
  {
    std::lock_guard<std::mutex> l(mutex_evq);
    caught_interrupt = true;
  }

  cond_evq.notify_one();
}
