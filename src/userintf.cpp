/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "gtwinkle_config.h"

#include <iostream>
#include <cstdlib>
#if defined(HAVE_READLINE_H)
#include <readline.h>
#elif defined(HAVE_READLINE_READLINE_H)
#include <readline/readline.h>
#elif defined(HAVE_EDITLINE_READLINE_H)
#include <editline/readline.h>
#endif
#if defined(HAVE_HISTORY_H)
#include <history.h>
#elif defined(HAVE_READLINE_HISTORY_H)
#include <readline/history.h>
#elif defined(HAVE_EDITLINE_HISTORY_H)
#include <editline/history.h>
#endif
#include "address_book.h"
#include "events.h"
#include "line.h"
#include "log.h"
#include "sys_settings.h"
#include "translator.h"
#include "userintf.h"
#include "util.h"
#include "user.h"
#include "audio/rtp_telephone_event.h"
#include "parser/parse_ctrl.h"
#include "utils/file_utils.h"
#include "utils/mime_database.h"

#include <glibmm.h>

#include <algorithm>
#include <memory>
#include <sstream>

#define CLI_PROMPT              "Twinkle> "
#define CLI_MAX_HISTORY_LENGTH	1000

extern std::string user_host;
extern std::unique_ptr<t_event_queue> evq_trans_layer;

using namespace utils;


namespace
{
  Glib::RefPtr<Glib::MainLoop> loop;

  void readline_handler(char *command_line)
  {
    if (! command_line)
    {
      rl_callback_handler_remove();
    }
    else if (*command_line)
    {
      ui->exec_command(command_line);
      add_history(command_line);
      free(command_line);
    }
  }

  bool stdin_io_callback(Glib::IOCondition cond)
  {
    rl_callback_read_char();

    return true;
  }
}


////////////////////////
// GNU Readline helpers
////////////////////////

char ** tw_completion (const char *text, int start, int end);
char * tw_command_generator (const char *text, int state);

char ** tw_completion (const char *text, int start, int end)
{
  return (start == 0) ?
      rl_completion_matches (text, tw_command_generator) :
      nullptr;
}



char * tw_command_generator (const char *text, int state)
{
  static int len;
  static std::vector<std::string>::const_iterator i;

  if (!state){
    len = strlen(text);
    i = ui->get_all_commands().begin();
  }

  for (; i != ui->get_all_commands().end(); i++){
    const char * s = i->c_str();
    //std::cout << s << endl;
    if ( s && strncmp(s, text, len) == 0  ){
      i++;
      return strdup(s);
    }
  }

  /* If no names matched, then return NULL. */
  return ((char *)NULL);
}


/////////////////////////////
// Private
/////////////////////////////

std::string t_userintf::expand_destination(t_user &user_config, const std::string &dst, const std::string &scheme)
{
  std::string s = dst;

  // Apply number conversion rules if applicable
  // Add domain if it is missing from a sip-uri
  if (s.find('@') == std::string::npos)
  {
    bool is_tel_uri = (s.substr(0, 4) == "tel:");

    // Strip tel-scheme
    if (is_tel_uri) s = s.substr(4);

    // Remove white space
    s = remove_white_space(s);

    // Remove special phone symbols
    if (user_config.get_remove_special_phone_symbols() &&
	looks_like_phone(s, user_config.get_special_phone_symbols()))
    {
      s = remove_symbols(s, user_config.get_special_phone_symbols());
    }

    // Convert number according to the number conversion rules
    s = user_config.convert_number(s);

    if (is_tel_uri)
    {
      // Add tel-scheme again.
      s = "tel:" + s;
    }
    else if (s.substr(0, 4) != "sip:" &&
	     (user_config.get_use_tel_uri_for_phone() || scheme == "tel") &&
	     user_config.get_numerical_user_is_phone() &&
	     looks_like_phone(s, user_config.get_special_phone_symbols()))
    {
      // Add tel-scheme if a telephone number must be expanded
      // to a tel-uri according to user profile settings.
      s = "tel:" + s;
    }
    else
    {
      // Add domain
      s += '@';
      s += user_config.get_domain();
    }
  }

  // Add sip-scheme if a scheme is missing
  if (s.substr(0, 4) != "sip:" && s.substr(0, 4) != "tel:")
  {
    s = "sip:" + s;
  }

  // RFC 3261 19.1.1
  // Add user=phone for telehpone numbers in a SIP-URI
  // If the SIP-URI contains a telephone number it SHOULD contain
  // the user=phone parameter.
  if (user_config.get_numerical_user_is_phone() && s.substr(0, 4) == "sip:")
  {
    t_url u(s);
    if (u.get_user_param().empty() &&
	u.user_looks_like_phone(user_config.get_special_phone_symbols()))
    {
      s += ";user=phone";
    }
  }

  return s;
}

void t_userintf::expand_destination(t_user &user_config,
				    const std::string &dst, std::string &display, std::string &dst_url)
{
  display.clear();
  dst_url.clear();

  if (dst.empty())
  {
    return;
  }

  // If there is a display name then the url part is between angle
  // brackets.
  if (dst[dst.size() - 1] != '>')
  {
    dst_url = expand_destination(user_config, dst);
    return;
  }

  // Find start of url
  std::string::size_type i = dst.rfind('<');
  if (i == std::string::npos)
  {
    // It seems the string is invalid.
    return;
  }

  dst_url = expand_destination(user_config, dst.substr(i + 1, dst.size() - i - 2));

  if (i > 0)
  {
    display = unquote(trim(dst.substr(0, i)));
  }
}

void t_userintf::expand_destination(t_user &user_config,
				    const std::string &dst, t_display_url &display_url)
{
  std::string url_str;

  expand_destination(user_config, dst, display_url.display, url_str);
  display_url.url.set_url(url_str);
}

void t_userintf::expand_destination(t_user &user_config,
				    const std::string &dst, t_display_url &display_url, std::string &subject,
				    std::string &dst_no_headers)
{
  std::string headers;
  dst_no_headers = dst;
  t_url u(dst);

  // Split headers from URI
  if (u.is_valid())
  {
    // destination is a valid URI. Strip off the headers if any
    headers = u.get_headers();

    // Cut off headers
    // Note that a separator (?) will be in front of the
    // headers string
    if (!headers.empty())
    {
      std::string::size_type i = dst.find(headers);
      if (i != std::string::npos)
      {
	dst_no_headers = dst.substr(0, i - 1);
      }
    }

    expand_destination(user_config, dst_no_headers, display_url);
  }
  else
  {
    // destination may be a short URI.
    // Split at a '?' to find any headers.
    // NOTE: this is not fool proof. A user name may contain a '?'
    std::vector<std::string> l = split_on_first(dst, '?');
    dst_no_headers = l[0];
    expand_destination(user_config, dst_no_headers, display_url);
    if (display_url.is_valid() && l.size() == 2)
    {
      headers = l[1];
    }
  }

  // Parse headers to find subject header
  subject.clear();
  if (!headers.empty())
  {
    try
    {
      std::vector<std::string> parse_errors;
      std::unique_ptr<t_sip_message> m(t_parser::parse_headers(headers, parse_errors));
      if (m->hdr_subject.is_populated())
      {
	subject = m->hdr_subject.subject;
      }
    }
    catch (const std::runtime_error &)
    {
      // ignore invalid headers
    }
  }
}

bool t_userintf::parse_args(const std::vector<std::string> &command_list,
                            std::vector<t_command_arg> &al)
{
  t_command_arg	arg;
  bool parsed_flag = false;

  al.clear();
  arg.flag = 0;
  arg.value = "";

  for (auto i = command_list.begin(); i != command_list.end(); i++)
  {
    if (i == command_list.begin()) continue;

    const std::string &s = *i;
    if (s[0] == '-')
    {
      if (s.size() == 1) return false;
      if (parsed_flag) al.push_back(arg);

      arg.flag = s[1];

      if (s.size() > 2)
      {
	arg.value = unquote(s.substr(2));
	al.push_back(arg);
	arg.flag = 0;
	arg.value = "";
	parsed_flag = false;
      }
      else
      {
	arg.value = "";
	parsed_flag = true;
      }
    }
    else
    {
      if (parsed_flag)
      {
	arg.value = unquote(s);
      }
      else
      {
	arg.flag = 0;
	arg.value = unquote(s);
      }

      al.push_back(arg);
      parsed_flag = false;
      arg.flag = 0;
      arg.value = "";
    }
  }

  // Last parsed argument was a flag only
  if (parsed_flag) al.push_back(arg);

  return true;
}

bool t_userintf::exec_invite(const std::vector<std::string> &command_list)
{
  std::vector<t_command_arg> al;
  std::string display;
  std::string subject;
  std::string destination;
  bool hide_user = false;

  if (!parse_args(command_list, al))
  {
    exec_command("help call");
    return false;
  }

  for (auto const & arg : al)
  {
    switch (arg.flag)
    {
    case 'd':
     display = arg.value;
     break;
    case 's':
     subject = arg.value;
     break;
    case 'h':
     hide_user = true;
     break;
    case 0:
     destination = arg.value;
     break;
    default:
     exec_command("help call");
     return false;
     break;
    }
  }

  return do_invite(destination, display, subject, hide_user);
}

bool t_userintf::do_invite(const std::string &destination,
			   const std::string &display,
			   const std::string &subject, bool anonymous)
{
  t_url dest_url(expand_destination(*active_user, destination));

  if (!dest_url.is_valid())
  {
    exec_command("help call");
    return false;
  }

  t_url vm_url(expand_destination(*active_user, active_user->get_mwi_vm_address()));
  if (dest_url != vm_url)
  {
    // Keep call information for redial
    last_called_url = dest_url;
    last_called_display = display;
    last_called_subject = subject;
    last_called_profile = active_user->get_profile_name();
    last_called_hide_user = anonymous;
  }

  phone.pub_invite(active_user, dest_url, display, subject, anonymous);
  return true;
}

bool t_userintf::exec_redial(const std::vector<std::string> &command_list)
{
  if (can_redial())
  {
    do_redial();
    return true;
  }

  return false;
}

void t_userintf::do_redial()
{
  t_user *user_config = phone.ref_user_profile(last_called_profile);
  phone.pub_invite(user_config, last_called_url, last_called_display,
		   last_called_subject, last_called_hide_user);
}

bool t_userintf::exec_answer(const std::vector<std::string> &command_list)
{
  do_answer();
  return true;
}

void t_userintf::do_answer()
{
  do_answer(phone.get_active_line());
}

void t_userintf::do_answer(unsigned int line)
{
  cb_stop_call_notification(line);
  phone.pub_answer(line);
}

bool t_userintf::exec_answerbye(const std::vector<std::string> &command_list)
{
  do_answerbye();
  return true;
}

void t_userintf::do_answerbye()
{
  unsigned int line = phone.get_active_line();

  switch (phone.get_line_substate(line))
  {
  case LSSUB_INCOMING_PROGRESS:
   do_answer();
   break;
  case LSSUB_OUTGOING_PROGRESS:
  case LSSUB_ESTABLISHED:
   do_bye();
   break;
  default:
   break;
  }
}

bool t_userintf::exec_reject(const std::vector<std::string> &command_list)
{
  do_reject();
  return true;
}

void t_userintf::do_reject()
{
  do_reject(phone.get_active_line());
}

void t_userintf::do_reject(unsigned int line)
{
  cb_stop_call_notification(line);
  phone.pub_reject(line);
  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": call rejected.\n";
  std::cout << std::endl;
}

bool t_userintf::exec_redirect(const std::vector<std::string> &command_list)
{
  std::vector<t_command_arg> al;
  std::vector<std::string> dest_list;
  int num_redirections = 0;
  bool show_status = false;
  bool action_present = false;
  bool enable = true;
  bool type_present = false;
  t_cf_type cf_type = CF_ALWAYS;

  if (!parse_args(command_list, al))
  {
    exec_command("help redirect");
    return false;
  }

  for (auto const & arg : al)
  {
    switch (arg.flag)
    {
    case 's':
     show_status = true;
     break;
    case 't':
     if (arg.value == "always")
     {
       cf_type = CF_ALWAYS;
     }
     else if (arg.value == "busy")
     {
       cf_type = CF_BUSY;
     }
     else if (arg.value == "noanswer")
     {
       cf_type = CF_NOANSWER;
     }
     else
     {
       exec_command("help redirect");
       return false;
     }

     type_present = true;
     break;
    case 'a':
     if (arg.value == "on")
     {
       enable = true;
     }
     else if (arg.value == "off")
     {
       enable = false;
     }
     else
     {
       exec_command("help redirect");
       return false;
     }

     action_present = true;
     break;
    case 0:
     dest_list.push_back(arg.value);
     num_redirections++;
     break;
    default:
     exec_command("help redirect");
     return false;
     break;
    }
  }

  if (type_present && enable && (num_redirections == 0 || num_redirections > 5))
  {
    exec_command("help redirect");
    return false;
  }

  if (!type_present && action_present && enable)
  {
    exec_command("help redirect");
    return false;
  }

  if (!type_present && !action_present &&
      (num_redirections == 0 || num_redirections > 5))
  {
    exec_command("help redirect");
    return false;
  }

  do_redirect(show_status, type_present, cf_type, action_present, enable,
	      num_redirections, dest_list);
  return true;
}

void t_userintf::do_redirect(bool show_status, bool type_present, t_cf_type cf_type,
			     bool action_present, bool enable, int num_redirections,
			     const std::vector<std::string> &dest_strlist)
{
  std::vector<t_display_url> dest_list;
  for (auto const & dest : dest_strlist)
  {
    t_display_url du;
    du.url = expand_destination(*active_user, dest);
    du.display.clear();
    if (!du.is_valid()) return;
    dest_list.push_back(du);
  }

  if (show_status)
  {
    std::vector<t_display_url> cf_dest; // call forwarding destinations

    std::cout << '\n';

    std::cout << "Redirect always: ";
    if (phone.ref_service(active_user)->get_cf_active(CF_ALWAYS, cf_dest))
    {
      for (auto i = cf_dest.cbegin(); i != cf_dest.cend(); ++i)
      {
	if (i != cf_dest.cbegin()) std::cout << ", ";
	std::cout << i->encode();
      }
    }
    else
    {
      std::cout << "not active";
    }
    std::cout << '\n';

    std::cout << "Redirect busy: ";
    if (phone.ref_service(active_user)->get_cf_active(CF_BUSY, cf_dest))
    {
      for (auto i = cf_dest.cbegin(); i != cf_dest.cend(); ++i)
      {
	if (i != cf_dest.cbegin()) std::cout << ", ";
	std::cout << i->encode();
      }
    }
    else
    {
      std::cout << "not active";
    }
    std::cout << '\n';

    std::cout << "Redirect noanswer: ";
    if (phone.ref_service(active_user)->get_cf_active(CF_NOANSWER, cf_dest))
    {
      for (auto i = cf_dest.cbegin(); i != cf_dest.cend(); i++)
      {
	if (i != cf_dest.cbegin()) std::cout << ", ";
	std::cout << i->encode();
      }
    }
    else
    {
      std::cout << "not active";
    }
    std::cout << '\n';

    std::cout << std::endl;
    return;
  }

  // Enable/disable permanent redirections
  if (type_present)
  {
    if (enable)
    {
      phone.ref_service(active_user)->enable_cf(cf_type, dest_list);
      std::cout << "Redirection enabled.\n\n";
    }
    else
    {
      phone.ref_service(active_user)->disable_cf(cf_type);
      std::cout << "Redirection disabled.\n\n";
    }

    return;
  }
  else
  {
    if (action_present)
    {
      if (!enable)
      {
	phone.ref_service(active_user)->disable_cf(CF_ALWAYS);
	phone.ref_service(active_user)->disable_cf(CF_BUSY);
	phone.ref_service(active_user)->disable_cf(CF_NOANSWER);
	std::cout << "All redirections disabled.\n\n";
	return;
      }

      return;
    }
  }

  // Redirect current call
  cb_stop_call_notification(phone.get_active_line());
  phone.pub_redirect(dest_list, 302);
  std::cout << '\n';
  std::cout << "Line " << phone.get_active_line() + 1 << ": call redirected.\n";
  std::cout << std::endl;
}

bool t_userintf::exec_dnd(const std::vector<std::string> &command_list)
{
  std::vector<t_command_arg> al;
  bool show_status = false;
  bool toggle = true;
  bool enable = false;

  if (!parse_args(command_list, al))
  {
    exec_command("help dnd");
    return false;
  }

  for (auto const & arg : al)
  {
    switch (arg.flag)
    {
    case 's':
     show_status = true;
     break;
    case 'a':
     if (arg.value == "on")
     {
       enable = true;
     }
     else if (arg.value == "off")
     {
       enable = false;
     }
     else
     {
       exec_command("help dnd");
       return false;
     }
     toggle = false;
     break;
    default:
     exec_command("help dnd");
     return false;
     break;
    }
  }

  do_dnd(show_status, toggle, enable);
  return true;
}

void t_userintf::do_dnd(bool show_status, bool toggle, bool enable)
{
  if (show_status)
  {
    std::cout << '\n';
    std::cout << "Do not disturb: ";
    if (phone.ref_service(active_user)->is_dnd_active())
    {
      std::cout << "active";
    }
    else
    {
      std::cout << "not active";
    }
    std::cout << std::endl;
    return;
  }

  if (toggle)
  {
    enable = !phone.ref_service(active_user)->is_dnd_active();
  }

  if (enable)
  {
    phone.ref_service(active_user)->enable_dnd();
    std::cout << "Do not disturb enabled.\n\n";
    return;
  }
  else
  {
    phone.ref_service(active_user)->disable_dnd();
    std::cout << "Do not disturb disabled.\n\n";
    return;
  }
}

bool t_userintf::exec_auto_answer(const std::vector<std::string> &command_list)
{
  std::vector<t_command_arg> al;
  bool show_status = false;
  bool toggle = true;
  bool enable = false;

  if (!parse_args(command_list, al))
  {
    exec_command("help auto_answer");
    return false;
  }

  for (auto const & arg : al)
  {
    switch (arg.flag)
    {
    case 's':
     show_status = true;
     break;
    case 'a':
     if (arg.value == "on")
     {
       enable = true;
     }
     else if (arg.value == "off")
     {
       enable = false;
     }
     else
     {
       exec_command("help auto_answer");
       return false;
     }
     toggle = false;
     break;
    default:
     exec_command("help auto_answer");
     return false;
     break;
    }
  }

  do_auto_answer(show_status, toggle, enable);
  return true;
}

void t_userintf::do_auto_answer(bool show_status, bool toggle, bool enable)
{
  if (show_status)
  {
    std::cout << '\n';
    std::cout << "Auto answer: ";
    if (phone.ref_service(active_user)->is_auto_answer_active())
    {
      std::cout << "active";
    }
    else
    {
      std::cout << "not active";
    }
    std::cout << std::endl;
    return;
  }

  if (toggle)
  {
    enable = !phone.ref_service(active_user)->is_auto_answer_active();
  }

  if (enable)
  {
    phone.ref_service(active_user)->enable_auto_answer(true);
    std::cout << "Auto answer enabled.\n\n";
    return;
  }
  else
  {
    phone.ref_service(active_user)->enable_auto_answer(false);
    std::cout << "Auto answer disabled.\n\n";
    return;
  }
}

bool t_userintf::exec_bye(const std::vector<std::string> &command_list)
{
  do_bye();
  return true;
}

void t_userintf::do_bye()
{
  phone.pub_end_call();
}

bool t_userintf::exec_hold(const std::vector<std::string> &command_list)
{
  do_hold();
  return true;
}

void t_userintf::do_hold()
{
  phone.pub_hold();
}

bool t_userintf::exec_retrieve(const std::vector<std::string> &command_list)
{
  do_retrieve();
  return true;
}

void t_userintf::do_retrieve()
{
  phone.pub_retrieve();
}

bool t_userintf::exec_refer(const std::vector<std::string> &command_list)
{
  std::vector<t_command_arg> al;
  std::string destination;
  bool dest_set = false;
  t_transfer_type transfer_type = TRANSFER_BASIC;

  if (!parse_args(command_list, al))
  {
    exec_command("help transfer");
    return false;
  }

  for (auto const & arg : al)
  {
    switch (arg.flag)
    {
    case 'c':
     if (transfer_type != TRANSFER_BASIC)
     {
       exec_command("help transfer");
       return false;
     }
     transfer_type = TRANSFER_CONSULT;
     if (!arg.value.empty())
     {
       destination = arg.value;
       dest_set = true;
     }
     break;
    case 'l':
     if (transfer_type != TRANSFER_BASIC)
     {
       exec_command("help transfer");
       return false;
     }
     transfer_type = TRANSFER_OTHER_LINE;
     break;
    case 0:
     destination = arg.value;
     dest_set = true;
     break;
    default:
     exec_command("help transfer");
     return false;
     break;
    }
  }

  if (!dest_set && transfer_type == TRANSFER_BASIC)
  {
    exec_command("help transfer");
    return false;
  }

  return do_refer(destination, transfer_type);
}

bool t_userintf::do_refer(const std::string &destination, t_transfer_type transfer_type)
{
  t_url dest_url;

  if (transfer_type == TRANSFER_BASIC ||
      (transfer_type == TRANSFER_CONSULT && !destination.empty()))
  {
    dest_url.set_url(expand_destination(*active_user, destination));

    if (!dest_url.is_valid())
    {
      exec_command("help transfer");
      return false;
    }
  }

  unsigned int active_line;
  unsigned int other_line;
  unsigned int line_to_be_transferred;

  switch (transfer_type)
  {
  case TRANSFER_BASIC:
   phone.pub_refer(dest_url, "");
   break;
  case TRANSFER_CONSULT:
   if (destination.empty())
   {
     active_line = phone.get_active_line();
     if (!phone.is_line_transfer_consult(active_line,
					 line_to_be_transferred))
     {
       // There is no call to transfer
       return false;
     }
     phone.pub_refer(line_to_be_transferred, active_line);
   }
   else
   {
     phone.pub_setup_consultation_call(dest_url, "");
   }
   break;
  case TRANSFER_OTHER_LINE:
   active_line = phone.get_active_line();
   other_line = (active_line == 0 ? 1 : 0);
   phone.pub_refer(active_line, other_line);
   break;
  }

  return true;
}


bool t_userintf::exec_conference(const std::vector<std::string> &command_list)
{
  do_conference();
  return true;
}

void t_userintf::do_conference()
{
  if (phone.join_3way(0, 1))
  {
    std::cout << '\n';
    std::cout << "Started 3-way conference.\n";
    std::cout << std::endl;
  }
  else
  {
    std::cout << '\n';
    std::cout << "Failed to start 3-way conference.\n";
    std::cout << std::endl;
  }
}

bool t_userintf::exec_mute(const std::vector<std::string> &command_list)
{
  std::vector<t_command_arg> al;
  bool show_status = false;
  bool toggle = true;
  bool enable = true;

  if (!parse_args(command_list, al))
  {
    exec_command("help mute");
    return false;
  }

  for (auto const & arg : al)
  {
    switch (arg.flag)
    {
    case 's':
     show_status = true;
     break;
    case 'a':
     if (arg.value == "on")
     {
       enable = true;
     }
     else if (arg.value == "off")
     {
       enable = false;
     }
     else
     {
       exec_command("help mute");
       return false;
     }
     toggle = false;
     break;
    default:
     exec_command("help mute");
     return false;
     break;
    }
  }

  do_mute(show_status, toggle, enable);
  return true;
}

void t_userintf::do_mute(bool show_status, bool toggle, bool enable)
{
  if (show_status)
  {
    std::cout << '\n';
    std::cout << "Line is ";
    if (phone.is_line_muted(phone.get_active_line()))
    {
      std::cout << "muted.";
    }
    else
    {
      std::cout << "not muted.";
    }
    std::cout << std::endl;
    return;
  }

  if (toggle) enable = !phone.is_line_muted(phone.get_active_line());
  if (enable)
  {
    phone.mute(enable);
    std::cout << "Line muted.\n\n";
    return;
  }
  else
  {
    phone.mute(enable);
    std::cout << "Line unmuted.\n\n";
    return;
  }
}

bool t_userintf::exec_dtmf(const std::vector<std::string> &command_list)
{
  std::vector<t_command_arg> al;
  std::string digits;
  bool raw_mode = false;

  if (phone.get_line_state(phone.get_active_line()) == LS_IDLE)
  {
    return false;
  }

  if (!parse_args(command_list, al))
  {
    exec_command("help dtmf");
    return false;
  }

  for (auto const & arg : al)
  {
    switch (arg.flag)
    {
    case 'r':
     raw_mode = true;
     if (!arg.value.empty())
     {
       digits = arg.value;
     }
     break;

    case 0:
     digits = arg.value;
     break;

    default:
     exec_command("help dtmf");
     return false;
     break;
    }
  }

  if (!raw_mode)
  {
    digits = str2dtmf(digits);
  }

  if (digits == "")
  {
    exec_command("help dtmf");
    return false;
  }

  do_dtmf(digits);
  return true;
}

void t_userintf::do_dtmf(const std::string &digits)
{
  const t_call_info call_info = phone.get_call_info(phone.get_active_line());
  throttle_dtmf_not_supported = false;

  if (!call_info.dtmf_supported) return;

  for (char c : digits)
  {
    if (VALID_DTMF_SYM(c))
    {
      phone.pub_send_dtmf(c, call_info.dtmf_inband, call_info.dtmf_info);
    }
  }
}

bool t_userintf::exec_register(const std::vector<std::string> &command_list)
{
  std::vector<t_command_arg> al;
  bool reg_all_profiles = false;

  if (!parse_args(command_list, al))
  {
    exec_command("help register");
    return false;
  }

  for (auto const & arg : al)
  {
    switch (arg.flag)
    {
    case 'a':
     reg_all_profiles = true;
     break;
    default:
     exec_command("help register");
     return false;
     break;
    }
  }

  do_register(reg_all_profiles);
  return true;
}

void t_userintf::do_register(bool reg_all_profiles)
{
  if (reg_all_profiles)
  {
    for (auto * user : phone.ref_users())
    {
      phone.pub_registration(user, REG_REGISTER,
			     DUR_REGISTRATION(*user));
    }
  }
  else
  {
    phone.pub_registration(active_user, REG_REGISTER,
			   DUR_REGISTRATION(*active_user));
  }
}

bool t_userintf::exec_deregister(const std::vector<std::string> &command_list)
{
  std::vector<t_command_arg> al;
  bool dereg_all_devices = false;
  bool dereg_all_profiles = false;

  if (!parse_args(command_list, al))
  {
    exec_command("help deregister");
    return false;
  }

  for (auto const & arg : al)
  {
    switch (arg.flag)
    {
    case 'a':
     dereg_all_profiles = true;
     break;
    case 'd':
     dereg_all_devices = true;
     break;
    default:
     exec_command("help deregister");
     return false;
     break;
    }
  }

  do_deregister(dereg_all_profiles, dereg_all_devices);
  return true;
}

void t_userintf::do_deregister(bool dereg_all_profiles, bool dereg_all_devices)
{
  t_register_type dereg_type = REG_DEREGISTER;

  if (dereg_all_devices)
  {
    dereg_type = REG_DEREGISTER_ALL;
  }

  if (dereg_all_profiles)
  {
    for (auto * user : phone.ref_users())
    {
      phone.pub_registration(user, dereg_type);
    }
  }
  else
  {
    phone.pub_registration(active_user, dereg_type);
  }
}

bool t_userintf::exec_fetch_registrations(const std::vector<std::string> &command_list)
{
  do_fetch_registrations();
  return true;
}

void t_userintf::do_fetch_registrations()
{
  phone.pub_registration(active_user, REG_QUERY);
}

bool t_userintf::exec_options(const std::vector<std::string> &command_list)
{
  std::vector<t_command_arg> al;
  std::string destination;
  bool dest_set = false;

  if (!parse_args(command_list, al))
  {
    exec_command("help options");
    return false;
  }

  for (auto const & arg : al)
  {
    switch (arg.flag)
    {
    case 0:
     destination = arg.value;
     dest_set = true;
     break;
    default:
     exec_command("help options");
     return false;
     break;
    }
  }

  if (!dest_set)
  {
    if (phone.get_line_state(phone.get_active_line()) == LS_IDLE)
    {
      exec_command("help options");
      return false;
    }
  }

  return do_options(dest_set, destination);
}

bool t_userintf::do_options(bool dest_set, const std::string &destination)
{
  if (!dest_set)
  {
    phone.pub_options();
  }
  else
  {
    t_url dest_url;
    dest_url.set_url(expand_destination(*active_user, destination));

    if (!dest_url.is_valid())
    {
      exec_command("help options");
      return false;
    }

    phone.pub_options(active_user, dest_url);
  }

  return true;
}

bool t_userintf::exec_line(const std::vector<std::string> &command_list)
{
  std::vector<t_command_arg> al;
  unsigned int line = 0;

  if (!parse_args(command_list, al))
  {
    exec_command("help line");
    return false;
  }

  for (auto const & arg : al)
  {
    switch (arg.flag)
    {
    case 0:
     line = atoi(arg.value.c_str());
     break;
    default:
     exec_command("help line");
     return false;
     break;
    }
  }

  if (line < 0 || line > 2)
  {
    exec_command("help line");
    return false;
  }

  do_line(line);
  return true;
}

void t_userintf::do_line(unsigned int line)
{
  if (line == 0)
  {
    std::cout << '\n';
    std::cout << "Active line is: " << phone.get_active_line()+1 << '\n';
    std::cout << std::endl;
    return;
  }

  unsigned int current = phone.get_active_line();

  if (line == current + 1)
  {
    std::cout << '\n';
    std::cout << "Line " << current + 1 << " is already active.\n";
    std::cout << std::endl;
    return;
  }

  phone.pub_activate_line(line - 1);
  if (phone.get_active_line() == current)
  {
    std::cout << '\n';
    std::cout << "Current call cannot be put on-hold.\n";
    std::cout << "Cannot switch to another line now.\n";
    std::cout << std::endl;
  }
  else
  {
    std::cout << '\n';
    std::cout << "Line " << phone.get_active_line()+1 << " is now active.\n";
    std::cout << std::endl;
  }
}

bool t_userintf::exec_user(const std::vector<std::string> &command_list)
{
  std::vector<t_command_arg> al;
  std::string profile_name;

  if (!parse_args(command_list, al))
  {
    exec_command("help user");
    return false;
  }

  for (auto const & arg : al)
  {
    switch (arg.flag)
    {
    case 0:
     profile_name = arg.value;
     break;
    default:
     exec_command("help user");
     return false;
     break;
    }
  }

  do_user(profile_name);
  return true;
}

void t_userintf::do_user(const std::string &profile_name)
{
  auto user_list(phone.ref_users());
  if (profile_name.empty())
  {
    // Show all users
    std::cout << '\n';
    for (auto * user : user_list)
    {
      if (user == active_user)
      {
	std::cout << "* ";
      }
      else
      {
	std::cout << "  ";
      }

      std::cout << user->get_profile_name();
      std::cout << "\n    ";
      std::cout << user->get_display(false);
      std::cout << " <sip:" << user->get_name();
      std::cout << "@" << user->get_domain() << ">\n";
    }
    std::cout << std::endl;
    return;
  }

  for (auto * user : user_list)
  {
    if (user->get_profile_name() == profile_name)
    {
      active_user = user;
      std::cout << '\n';
      std::cout << profile_name;
      std::cout << " activated.\n";
      std::cout << std::endl;

      return;
    }
  }

  std::cout << '\n';
  std::cout << "Unknown user profile: ";
  std::cout << profile_name;
  std::cout << '\n' << std::endl;
}

bool t_userintf::exec_zrtp(const std::vector<std::string> &command_list)
{
  std::vector<t_command_arg> al;
  t_zrtp_cmd zrtp_cmd = ZRTP_ENCRYPT;

  if (!parse_args(command_list, al))
  {
    exec_command("help zrtp");
    return false;
  }

  if (al.size() != 1)
  {
    exec_command("help zrtp");
    return false;
  }

  for (auto const & arg : al)
  {
    switch (arg.flag)
    {
    case 0:
     if (arg.value == "encrypt")
     {
       zrtp_cmd = ZRTP_ENCRYPT;
     }
     else if (arg.value == "go-clear")
     {
       zrtp_cmd = ZRTP_GO_CLEAR;
     }
     else if (arg.value == "confirm-sas")
     {
       zrtp_cmd = ZRTP_CONFIRM_SAS;
     }
     else if (arg.value == "reset-sas")
     {
       zrtp_cmd = ZRTP_RESET_SAS;
     }
     else
     {
       exec_command("help zrtp");
       return false;
     }
     break;
    default:
     exec_command("help zrtp");
     return false;
     break;
    }
  }

  do_zrtp(zrtp_cmd);
  return true;
}

void t_userintf::do_zrtp(t_zrtp_cmd zrtp_cmd)
{
  switch (zrtp_cmd)
  {
  case ZRTP_ENCRYPT:
   phone.pub_enable_zrtp();
   break;
  case ZRTP_GO_CLEAR:
   phone.pub_zrtp_request_go_clear();
   break;
  case ZRTP_CONFIRM_SAS:
   phone.pub_confirm_zrtp_sas();
   break;
  case ZRTP_RESET_SAS:
   phone.pub_reset_zrtp_sas_confirmation();
   break;
  default:
   assert(false);
  }
}

bool t_userintf::exec_message(const std::vector<std::string> &command_list)
{
  std::vector<t_command_arg> al;
  std::string display;
  std::string subject;
  std::string filename;
  std::string destination;
  std::string text;

  if (!parse_args(command_list, al))
  {
    exec_command("help message");
    return false;
  }

  for (auto const & arg : al)
  {
    switch (arg.flag)
    {
    case 's':
     subject = arg.value;
     break;
    case 'f':
     filename = arg.value;
     break;
    case 'd':
     display = arg.value;
     break;
    case 0:
     if (destination.empty())
     {
       destination = arg.value;
     }
     else
     {
       text = arg.value;
     }
     break;
    default:
     exec_command("help message");
     return false;
     break;
    }
  }

  if (destination.empty() || (text.empty() && filename.empty()))
  {
    exec_command("help message");
    return false;
  }

  im::t_msg msg(text, im::MSG_DIR_OUT, im::TXT_PLAIN);
  msg.subject = subject;

  if (!filename.empty())
  {
    t_media media("application/octet-stream");
    std::string mime_type = mime_database->get_mimetype(filename);

    if (!mime_type.empty())
    {
      media = t_media(mime_type);
    }

    msg.set_attachment(filename, media, strip_path_from_filename(filename));
  }

  return do_message(destination, display, msg);
}

bool t_userintf::do_message(const std::string &destination, const std::string &display,
			    const im::t_msg &msg)
{
  t_url dest_url(expand_destination(*active_user, destination));

  if (!dest_url.is_valid())
  {
    exec_command("help message");
    return false;
  }

  (void)phone.pub_send_message(active_user, dest_url, display, msg);
  return true;
}

bool t_userintf::exec_presence(const std::vector<std::string> &command_list)
{
  std::vector<t_command_arg> al;
  t_presence_state::t_basic_state basic_state = t_presence_state::ST_BASIC_OPEN;

  if (!parse_args(command_list, al))
  {
    exec_command("help presence");
    return false;
  }

  for (auto const & arg : al)
  {
    switch (arg.flag)
    {
    case 'b':
     if (arg.value == "online")
     {
       basic_state = t_presence_state::ST_BASIC_OPEN;
     }
     else if (arg.value == "offline")
     {
       basic_state = t_presence_state::ST_BASIC_CLOSED;
     }
     else
     {
       exec_command("help presence");
       return false;
     }
     break;
    default:
     exec_command("help presence");
     return false;
     break;
    }
  }

  do_presence(basic_state);
  return true;
}

void t_userintf::do_presence(t_presence_state::t_basic_state basic_state)
{
  phone.pub_publish_presence(active_user, basic_state);
}

bool t_userintf::exec_quit(const std::vector<std::string> &command_list)
{
  do_quit();
  return true;
}

void t_userintf::do_quit()
{
  loop->quit();
}

bool t_userintf::exec_help(const std::vector<std::string> &command_list)
{
  std::vector<t_command_arg> al;

  if (!parse_args(command_list, al) ||
      (al.size() > 1))
  {
    exec_command("help help");
    return false;
  }

  do_help(al);
  return true;
}

void t_userintf::do_help(const std::vector<t_command_arg> &al)
{
  if (al.empty())
  {
    std::cout << '\n';
    std::cout << "call		Call someone\n";
    std::cout << "answer		Answer an incoming call\n";
    std::cout << "answerbye	Answer an incoming call or end a call\n";
    std::cout << "reject		Reject an incoming call\n";
    std::cout << "redirect	Redirect an incoming call\n";
    std::cout << "transfer	Transfer a standing call\n";
    std::cout << "bye		End a call\n";
    std::cout << "hold		Put a call on-hold\n";
    std::cout << "retrieve	Retrieve a held call\n";
    std::cout << "conference	Join 2 calls in a 3-way conference\n";
    std::cout << "mute		Mute a line\n";
    std::cout << "dtmf		Send DTMF\n";
    std::cout << "redial		Repeat last call\n";
    std::cout << "register	Register your phone at a registrar\n";
    std::cout << "deregister	De-register your phone at a registrar\n";
    std::cout << "fetch_reg	Fetch registrations from registrar\n";
    std::cout << "options\t\tGet capabilities of another SIP endpoint\n";
    std::cout << "line		Toggle between phone lines\n";
    std::cout << "dnd		Do not disturb\n";
    std::cout << "auto_answer	Auto answer\n";
    std::cout << "user		Show users / set active user\n";
#ifdef HAVE_ZRTP
    std::cout << "zrtp		ZRTP command for voice encryption\n";
#endif
    std::cout << "message\t\tSend an instant message\n";
    std::cout << "presence	Publish your presence state\n";
    std::cout << "quit		Quit\n";
    std::cout << "help		Get help on a command\n";
    std::cout << std::endl;

    return;
  }

  bool ambiguous;
  std::string const & c (complete_command(tolower(al.front().value), ambiguous));

  if (c == "call")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\tcall [-s subject] [-d display] [-h] dst\n";
    std::cout << "Description:\n";
    std::cout << "\tCall someone.\n";
    std::cout << "Arguments:\n";
    std::cout << "\t-s subject	Add a subject header to the INVITE\n";
    std::cout << "\t-d display	Add display name to To-header\n";
    std::cout << "\t-h		Hide your identity\n";
    std::cout << "\tdst		SIP uri of party to invite\n";
    std::cout << std::endl;

    return;
  }

  if (c == "answer")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\tanswer\n";
    std::cout << "Description:\n";
    std::cout << "\tAnswer an incoming call.\n";
    std::cout << std::endl;

    return;
  }

  if (c == "answerbye")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\tanswerbye\n";
    std::cout << "Description:\n";
    std::cout << "\tWith this command you can answer an incoming call or\n";
    std::cout << "\tend an established call.\n";
    std::cout << std::endl;

    return;
  }

  if (c == "reject")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\treject\n";
    std::cout << "Description:\n";
    std::cout << "\tReject an incoming call. A 486 Busy Here response\n";
    std::cout << "\twill be sent.\n";
    std::cout << std::endl;

    return;
  }

  if (c == "redirect")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\tredirect [-s] [-t type] [-a on|off] [dst ... dst]\n";
    std::cout << "Description:\n";
    std::cout << "\tRedirect an incoming call. A 302 Moved Temporarily\n";
    std::cout << "\tresponse will be sent.\n";
    std::cout << "\tYou can redirect the current incoming call by specifying\n";
    std::cout << "\tone or more destinations without any other arguments.\n";
    std::cout << "Arguments:\n";
    std::cout << "\t-s		Show which redirections are active.\n";
    std::cout << "\t-t type\t	Type for permanent redirection of calls.\n";
    std::cout << "\t		Values: always, busy, noanswer.\n";
    std::cout << "\t-a on|off	Enable/disable permanent redirection.\n";
    std::cout << "\t		The default action is 'on'.\n";
    std::cout << "\t		You can disable all redirections with the\n";
    std::cout << "\t		'off' action and no type.\n";
    std::cout << "\tdst		SIP uri where the call should be redirected.\n";
    std::cout << "\t		You can specify up to 5 destinations.\n";
    std::cout << "\t		The destinations will be tried in sequence.\n";
    std::cout << "Examples:\n";
    std::cout << "\tRedirect current incoming call to michel@twinklephone.com\n";
    std::cout << "\tredirect michel@twinklephone.com\n";
    std::cout << '\n';
    std::cout << "\tRedirect busy calls permanently to michel@twinklephone.com\n";
    std::cout << "\tredirect -t busy michel@twinklephone.com\n";
    std::cout << '\n';
    std::cout << "\tDisable redirection of busy calls.\n";
    std::cout << "\tredirect -t busy -a off\n";
    std::cout << std::endl;

    return;
  }

  if (c == "transfer")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\ttransfer [-c] [-l] [dst]\n";
    std::cout << "Description:\n";
    std::cout << "\tTransfer a standing call to another destination.\n";
    std::cout << "\tFor a transfer with consultation, first use the -c flag with a\n";
    std::cout << "\tdestination. This sets up the consultation call. When the\n";
    std::cout << "\tconsulted party agrees, give the command with the -c flag once\n";
    std::cout << "\tmore, but now without a destination. This transfers the call.\n";
    std::cout << "Arguments:\n";
    std::cout << "\t-c	Consult destination before transferring call.\n";
    std::cout << "\t-l	Transfer call to party on other line.\n";
    std::cout << "\tdst	SIP uri of transfer destination\n";
    std::cout << std::endl;

    return;
  }

  if (c == "bye")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\tbye\n";
    std::cout << "Description:\n";
    std::cout << "\tEnd a call.\n";
    std::cout << "\tFor a stable call a BYE will be sent.\n";
    std::cout << "\tIf the invited party did not yet sent a final answer,\n";
    std::cout << "\tthen a CANCEL will be sent.\n";
    std::cout << std::endl;

    return;
  }

  if (c == "hold")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\thold\n";
    std::cout << "Description:\n";
    std::cout << "\tPut the current call on the acitve line on-hold.\n";
    std::cout << std::endl;

    return;
  }

  if (c == "retrieve")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\tretrieve\n";
    std::cout << "Description:\n";
    std::cout << "\tRetrieve a held call on the active line.\n";
    std::cout << std::endl;

    return;
  }

  if (c == "conference")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\tconference\n";
    std::cout << "Description:\n";
    std::cout << "\tJoin 2 calls in a 3-way conference. Before you give this\n";
    std::cout << "\tcommand you must have a call on each line.\n";
    std::cout << std::endl;

    return;
  }

  if (c == "mute")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\tmute [-s] [-a on|off]\n";
    std::cout << "Description:\n";
    std::cout << "\tMute/unmute the active line.\n";
    std::cout << "\tYou can hear the other side of the line, but they cannot\n";
    std::cout << "\thear you.\n";
    std::cout << "Arguments:\n";
    std::cout << "\t-s		Show if line is muted.\n";
    std::cout << "\t-a on|off	Mute/unmute.\n";
    std::cout << "Notes:\n";
    std::cout << "\tWithout any arguments you can toggle the status.\n";
    std::cout << std::endl;

    return;
  }

  if (c == "dtmf")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\tdtmf digits\n";
    std::cout << "Description:\n";
    std::cout << "\tSend the digits as out-of-band DTMF telephone events ";
    std::cout << "(RFC 2833).\n";
    std::cout << "\tThis command can only be given when a call is ";
    std::cout << "established.\n";
    std::cout << "Arguments:\n";
    std::cout << "\t-r	Raw mode: do not convert letters to digits.\n";
    std::cout << "\tdigits	0-9 | A-D | * | #\n";
    std::cout << "Example:\n";
    std::cout << "\tdtmf 1234#\n";
    std::cout << "\tdmtf movies\n";
    std::cout << "Notes:\n";
    std::cout << "\tThe overdecadic digits A-D can only be sent in raw mode.\n";
    std::cout << std::endl;

    return;
  }

  if (c == "redial")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\tredial\n";
    std::cout << "Description:\n";
    std::cout << "\tRepeat last call.\n";
    std::cout << std::endl;

    return;
  }

  if (c == "register")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\tregister\n";
    std::cout << "Description:\n";
    std::cout << "\tRegister your phone at a registrar.\n";
    std::cout << "Arguments:\n";
    std::cout << "\t-a	Register all enabled user profiles.\n";
    std::cout << std::endl;

    return;
  }

  if (c == "deregister")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\tderegister [-a]\n";
    std::cout << "Description:\n";
    std::cout << "\tDe-register your phone at a registrar.\n";
    std::cout << "Arguments:\n";
    std::cout << "\t-a	De-register all enabled user profiles.\n";
    std::cout << "\t-d	De-register all devices.\n";
    std::cout << std::endl;

    return;
  }

  if (c == "fetch_reg")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\tfetch_reg\n";
    std::cout << "Description:\n";
    std::cout << "\tFetch current registrations from registrar.\n";
    std::cout << std::endl;

    return;
  }

  if (c == "options")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\toptions [dst]\n";
    std::cout << "Description:\n";
    std::cout << "\tGet capabilities of another SIP endpoint.\n";
    std::cout << "\tIf no destination is passed as an argument, then\n";
    std::cout << "\tthe capabilities of the far-end in the current call\n";
    std::cout << "\ton the active line are requested.\n";
    std::cout << "Arguments:\n";
    std::cout << "\tdst		SIP uri of end-point\n";
    std::cout << std::endl;

    return;
  }

  if (c == "line")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\tline [lineno]\n";
    std::cout << "Description:\n";
    std::cout << "\tIf no argument is passed then the current active ";
    std::cout << "line is shown\n";
    std::cout << "\tOtherwise switch to another line. If the current active\n";
    std::cout << "\thas a call, then this call will be put on-hold.\n";
    std::cout << "\tIf the new active line has a held call, then this call\n";
    std::cout << "\twill be retrieved.\n";
    std::cout << "Arguments:\n";
    std::cout << "\tlineno		Switch to another line (values = ";
    std::cout << "1,2)\n";
    std::cout << std::endl;

    return;
  }

  if (c == "dnd")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\tdnd [-s] [-a on|off]\n";
    std::cout << "Description:\n";
    std::cout << "\tEnable/disable the do not disturb service.\n";
    std::cout << "\tIf dnd is enabled then a 480 Temporarily Unavailable ";
    std::cout << "response is given\n";
    std::cout << "\ton incoming calls.\n";
    std::cout << "Arguments:\n";
    std::cout << "\t-s		Show if dnd is active.\n";
    std::cout << "\t-a on|off	Enable/disable dnd.\n";
    std::cout << "Notes:\n";
    std::cout << "\tWithout any arguments you can toggle the status.\n";
    std::cout << std::endl;

    return;
  }

  if (c == "auto_answer")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\tauto_answer [-s] [-a on|off]\n";
    std::cout << "Description:\n";
    std::cout << "\tEnable/disable the auto answer service.\n";
    std::cout << "Arguments:\n";
    std::cout << "\t-s		Show if auto answer is active.\n";
    std::cout << "\t-a on|off	Enable/disable auto answer.\n";
    std::cout << "Notes:\n";
    std::cout << "\tWithout any arguments you can toggle the status.\n";
    std::cout << std::endl;

    return;
  }

  if (c == "user")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\tuser [profile name]\n";
    std::cout << "Description:\n";
    std::cout << "\tMake a user profile the active profile.\n";
    std::cout << "\tCommands like 'invite' are executed for the active profile.\n";
    std::cout << "\tWithout an argument this command lists all users. The active\n";
    std::cout << "\tuser will be marked with '*'.\n";
    std::cout << "Arguments:\n";
    std::cout << "\tprofile name	The user profile to activate.\n";
    std::cout << std::endl;

    return;
  }

#ifdef HAVE_ZRTP
  if (c == "zrtp")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\tzrtp <zrtp-command>\n";
    std::cout << "Description:\n";
    std::cout << "\tExecute a ZRTP command.\n";
    std::cout << "ZRTP commands:\n";
    std::cout << "\tencrypt      Start ZRTP negotiation for encryption.\n";
    std::cout << "\tgo-clear     Send ZRTP go-clear request.\n";
    std::cout << "\tconfirm-sas  Confirm the SAS value.\n";
    std::cout << "\treset-sas    Reset SAS confirmation.\n";
    std::cout << std::endl;

    return;
  }
#endif

  if (c == "message")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\tmessage [-s subject] [-f file name] [-d display] dst [text]\n";
    std::cout << "Description:\n";
    std::cout << "\tSend an instant message.\n";
    std::cout << "Arguments:\n";
    std::cout << "\t-s subject	Subject of the message.\n";
    std::cout << "\t-f file name	File name of the file to send.\n";
    std::cout << "\t-d display	Add display name to To-header\n";
    std::cout << "\tdst		SIP uri of party to message\n";
    std::cout << "\ttext		Message text to send. Surround with double quotes\n";
    std::cout << "\t\t\twhen your text contains whitespace.\n";
    std::cout << "\t\t\tWhen you send a file, then the text is ignored.\n";
    std::cout << std::endl;

    return;
  }

  if (c == "presence")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\tpresence -b [online|offline]\n";
    std::cout << "Description:\n";
    std::cout << "\tPublish your presence state to a presence agent\n";
    std::cout << "Arguments:\n";
    std::cout << "\t-b		A basic presence state: online or offline\n";
    std::cout << std::endl;

    return;
  }

  if (c == "quit")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\tquit\n";
    std::cout << "Description:\n";
    std::cout << "\tQuit.\n";
    std::cout << std::endl;

    return;
  }

  if (c == "help")
  {
    std::cout << '\n';
    std::cout << "Usage:\n";
    std::cout << "\thelp [command]\n";
    std::cout << "Description:\n";
    std::cout << "\tShow help on a command.\n";
    std::cout << "Arguments:\n";
    std::cout << "\tcommand		Command you want help with\n";
    std::cout << std::endl;

    return;
  }

  std::cout << '\n';
  std::cout << "\nUnknown command\n\n";
  std::cout << std::endl;
}


/////////////////////////////
// Public
/////////////////////////////

t_userintf::t_userintf(t_phone &_phone)
  : active_user (nullptr),
    phone (_phone),
    use_stdout (true),
    throttle_dtmf_not_supported (false)
{
  all_commands.emplace_back("invite");
  all_commands.emplace_back("call");
  all_commands.emplace_back("answer");
  all_commands.emplace_back("answerbye");
  all_commands.emplace_back("reject");
  all_commands.emplace_back("redirect");
  all_commands.emplace_back("bye");
  all_commands.emplace_back("hold");
  all_commands.emplace_back("retrieve");
  all_commands.emplace_back("refer");
  all_commands.emplace_back("transfer");
  all_commands.emplace_back("conference");
  all_commands.emplace_back("mute");
  all_commands.emplace_back("dtmf");
  all_commands.emplace_back("redial");
  all_commands.emplace_back("register");
  all_commands.emplace_back("deregister");
  all_commands.emplace_back("fetch_reg");
  all_commands.emplace_back("options");
  all_commands.emplace_back("line");
  all_commands.emplace_back("dnd");
  all_commands.emplace_back("auto_answer");
  all_commands.emplace_back("user");
#ifdef HAVE_ZRTP
  all_commands.emplace_back("zrtp");
#endif
  all_commands.emplace_back("message");
  all_commands.emplace_back("presence");
  all_commands.emplace_back("quit");
  all_commands.emplace_back("exit");
  all_commands.emplace_back("q");
  all_commands.emplace_back("x");
  all_commands.emplace_back("help");
  all_commands.emplace_back("h");
  all_commands.emplace_back("?");
}

t_userintf::~t_userintf()
{ }

std::string t_userintf::complete_command(const std::string &c, bool &ambiguous)
{
  ambiguous = false;
  std::string full_command;

  for (auto const & cmd : all_commands)
  {

    // If there is an exact match, then this is the command.
    // This allows a one command to be a prefix of another command.
    if (c == cmd)
    {
      ambiguous = false;
      return c;
    }

    if (c.size() < cmd.size() && c == cmd.substr(0, c.size()))
    {
      if (full_command != "")
      {
	ambiguous = true;
	// Do not return here, as there might still be
	// an exact match.
      }

      full_command = cmd;
    }
  }

  return (ambiguous ? "" : std::string(std::move(full_command)));
}

bool t_userintf::exec_command(const std::string &command_line)
{
  std::vector<std::string> l(split_ws(command_line, true));
  if (l.empty())
  {
    return false;
  }

  bool ambiguous;
  std::string command = complete_command(tolower(l[0]), ambiguous);

  if (ambiguous)
  {
    if (use_stdout)
    {
      std::cout << '\n';
      std::cout << "Ambiguous command\n";
      std::cout << std::endl;
    }

    return false;
  }

  if (command == "invite") return exec_invite(l);
  if (command == "call") return exec_invite(l);
  if (command == "answer") return exec_answer(l);
  if (command == "answerbye") return exec_answerbye(l);
  if (command == "reject") return exec_reject(l);
  if (command == "redirect") return exec_redirect(l);
  if (command == "bye") return exec_bye(l);
  if (command == "hold") return exec_hold(l);
  if (command == "retrieve") return exec_retrieve(l);
  if (command == "refer") return exec_refer(l);
  if (command == "transfer") return exec_refer(l);
  if (command == "conference") return exec_conference(l);
  if (command == "mute") return exec_mute(l);
  if (command == "dtmf") return exec_dtmf(l);
  if (command == "redial") return exec_redial(l);
  if (command == "register") return exec_register(l);
  if (command == "deregister") return exec_deregister(l);
  if (command == "fetch_reg") return exec_fetch_registrations(l);
  if (command == "options") return exec_options(l);
  if (command == "line") return exec_line(l);
  if (command == "dnd") return exec_dnd(l);
  if (command == "auto_answer") return exec_auto_answer(l);
  if (command == "user") return exec_user(l);
#ifdef HAVE_ZRTP
  if (command == "zrtp") return exec_zrtp(l);
#endif
  if (command == "message") return exec_message(l);
  if (command == "presence") return exec_presence(l);
  if (command == "quit") return exec_quit(l);
  if (command == "exit") return exec_quit(l);
  if (command == "x") return exec_quit(l);
  if (command == "q") return exec_quit(l);
  if (command == "help") return exec_help(l);
  if (command == "h") return exec_help(l);
  if (command == "?") return exec_help(l);

  if (use_stdout)
  {
    std::cout << '\n';
    std::cout << "Unknown command\n";
    std::cout << std::endl;
  }

  return false;
}

std::string t_userintf::format_sip_address(t_user &user_config, const std::string &display,
					   const t_url &uri) const
{
  std::string s;

  if (uri.encode() == ANONYMOUS_URI)
  {
    return TRANSLATE("Anonymous");
  }

  const std::string &number { (uri.get_scheme() == "tel") ?
      uri.get_host() :
      uri.get_user() };

  const bool use_display = (!display.empty() && display != number);
  if (use_display)
  {
    s = display + " <";
  }

  if (user_config.get_display_useronly_phone() &&
      uri.is_phone(user_config.get_numerical_user_is_phone(),
		   user_config.get_special_phone_symbols()))
  {
    // Display telephone number only
    s += user_config.convert_number(number);
  }
  else
  {
    // Display full URI
    // Convert the username according to the number conversion
    // rules.
    t_url u(uri);
    std::string const &username = user_config.convert_number(number);
    if (username != number)
    {
      if (uri.get_scheme() == "tel")
      {
	u.set_host(username);
      }
      else
      {
	u.set_user(username);
      }
    }
    s += u.encode_no_params_hdrs(false);
  }

  if (use_display)
  {
    s += ">";
  }

  return s;
}

std::vector<std::string> t_userintf::format_warnings(const t_hdr_warning &hdr_warning) const
{
  std::string s;
  std::vector<std::string> l;

  for (auto const & warn : hdr_warning.warnings)
  {
    std::ostringstream s;
    s << TRANSLATE("Warning:") << ' '
      << warn.code << ' ' << warn.text << " (" << warn.host;
    if (warn.port > 0)
    {
      s << ':' << warn.port;
    }
    s << ')';
    l.push_back(s.str());
  }

  return l;
}

std::string t_userintf::format_codec(t_audio_codec codec) const
{
  switch (codec)
  {
  case CODEC_NULL:		return "null";
  case CODEC_UNSUPPORTED:	return "???";
  case CODEC_G711_ALAW:		return "g711a";
  case CODEC_G711_ULAW:		return "g711u";
  case CODEC_GSM:		return "gsm";
  case CODEC_G722:		return "g722";
  case CODEC_SPEEX_NB:		return "spx-nb";
  case CODEC_SPEEX_WB:		return "spx-wb";
  case CODEC_SPEEX_UWB:		return "spx-uwb";
  case CODEC_OPUS:		return "opus";
  case CODEC_ILBC:		return "ilbc";
  case CODEC_G726_16:		return "g726-16";
  case CODEC_G726_24:		return "g726-24";
  case CODEC_G726_32:		return "g726-32";
  case CODEC_G726_40:		return "g726-40";
  default:			return "???";
  }
}

void t_userintf::run()
{
  active_user = !phone.ref_users().empty() ? phone.ref_users().front() : nullptr;

  std::cout << PRODUCT_NAME << " " << PRODUCT_VERSION << ", " << PRODUCT_DATE;
  std::cout << '\n';
  std::cout << "Copyright (C) 2005-2009  " << PRODUCT_AUTHOR << '\n';
  std::cout << '\n';

  std::cout << "Users:";
  exec_command("user");

  std::cout << std::endl;

  // Initialize phone functions
  phone.init();

  //Initialize GNU readline functions
  rl_attempted_completion_function = tw_completion;
  using_history();
  read_history(sys_config->get_history_file().c_str());
  stifle_history(CLI_MAX_HISTORY_LENGTH);


  rl_callback_handler_install (CLI_PROMPT, &readline_handler);

  Glib::RefPtr<Glib::IOSource> stdin_source (Glib::IOSource::create (0, Glib::IO_IN));
  stdin_source->connect(sigc::ptr_fun(stdin_io_callback));
  stdin_source->attach();

  // Keep the service running until the process is killed:
  loop = Glib::MainLoop::create();
  loop->run();

  rl_callback_handler_remove();

  // Terminate phone functions
  write_history(sys_config->get_history_file().c_str());
  phone.terminate();

  std::cout << std::endl;
}

void t_userintf::lock()
{
  assert(!is_prohibited_thread());
  // TODO: lock for CLI
}

void t_userintf::unlock()
{
  // TODO: lock for CLI
}

bool t_userintf::select_user_config(std::vector<std::string> &config_files)
{
  // In CLI mode, simply select the default config file
  config_files.clear();
  config_files.push_back(USER_CONFIG_FILE);
  return true;
}

void t_userintf::cb_outgoing_call(t_user &user_config, unsigned int line,
				  const t_request &r)
{
}

void t_userintf::cb_incoming_call(t_user &user_config, unsigned int line,
				  const t_request &r)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": ";
  std::cout << "incoming call\n";
  std::cout << "From:\t\t";

  std::string from_party = format_sip_address(user_config,
					      r.hdr_from.get_display_presentation(), r.hdr_from.uri);
  std::cout << from_party << '\n';

  if (r.hdr_organization.is_populated())
  {
    std::cout << "Organization:\t" << r.hdr_organization.name << '\n';
  }

  std::cout << "To:\t\t";
  std::cout << format_sip_address(user_config, r.hdr_to.display, r.hdr_to.uri) << '\n';

  if (r.hdr_referred_by.is_populated())
  {
    std::cout << "Referred-by:\t";
    std::cout << format_sip_address(user_config, r.hdr_referred_by.display,
				    r.hdr_referred_by.uri);
    std::cout << '\n';
  }

  if (r.hdr_subject.is_populated())
  {
    std::cout << "Subject:\t" << r.hdr_subject.subject << '\n';
  }

  std::cout << std::endl;

  cb_notify_call(line, user_config.get_profile_name(), std::move(from_party));
}

void t_userintf::cb_call_cancelled(unsigned int line)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": ";
  std::cout << "far end cancelled call.\n";
  std::cout << '\n';
  std::cout.flush();

  cb_stop_call_notification(line);
}

void t_userintf::cb_far_end_hung_up(unsigned int line)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": ";
  std::cout << "far end ended call.\n";
  std::cout << '\n';
  std::cout.flush();

  cb_stop_call_notification(line);
}

void t_userintf::cb_answer_timeout(unsigned int line)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": ";
  std::cout << "answer timeout.\n";
  std::cout << '\n';
  std::cout.flush();

  cb_stop_call_notification(line);
}

void t_userintf::cb_sdp_answer_not_supported(unsigned int line, const std::string &reason)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": ";
  std::cout << "SDP answer from far end not supported.\n";
  std::cout << reason << '\n';
  std::cout << '\n';
  std::cout.flush();

  cb_stop_call_notification(line);
}

void t_userintf::cb_sdp_answer_missing(unsigned int line)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": ";
  std::cout << "SDP answer from far end missing.\n";
  std::cout << '\n';
  std::cout.flush();

  cb_stop_call_notification(line);
}

void t_userintf::cb_unsupported_content_type(unsigned int line, const t_sip_message &r)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": ";
  std::cout << "Unsupported content type in answer from far end.\n";
  std::cout << r.hdr_content_type.media.type << "/";
  std::cout << r.hdr_content_type.media.subtype << '\n';
  std::cout << '\n';
  std::cout.flush();

  cb_stop_call_notification(line);
}

void t_userintf::cb_ack_timeout(unsigned int line)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": ";
  std::cout << "no ACK received, call will be terminated.\n";
  std::cout << '\n';
  std::cout.flush();

  cb_stop_call_notification(line);
}

void t_userintf::cb_100rel_timeout(unsigned int line)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": ";
  std::cout << "no PRACK received, call will be terminated.\n";
  std::cout << '\n';
  std::cout.flush();

  cb_stop_call_notification(line);
}

void t_userintf::cb_prack_failed(unsigned int line, const t_response &r)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": PRACK failed.\n";
  std::cout << r.code << ' ' << r.reason << '\n';
  std::cout << '\n';
  std::cout.flush();

  cb_stop_call_notification(line);
}

void t_userintf::cb_provisional_resp_invite(unsigned int line, const t_response &r)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": received ";
  std::cout << r.code << ' ' << r.reason << '\n';
  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_cancel_failed(unsigned int line, const t_response &r)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": cancel failed.\n";
  std::cout << r.code << ' ' << r.reason << '\n';
  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_call_answered(t_user &user_config, unsigned int line, const t_response &r)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": far end answered call.\n";
  std::cout << r.code << ' ' << r.reason << '\n';

  std::cout << "To: ";
  std::cout << format_sip_address(user_config, r.hdr_to.display, r.hdr_to.uri) << '\n';

  if (r.hdr_organization.is_populated())
  {
    std::cout << "Organization: " << r.hdr_organization.name << '\n';
  }

  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_call_failed(t_user &user_config, unsigned int line, const t_response &r)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": call failed.\n";
  std::cout << r.code << ' ' << r.reason << '\n';

  // Warnings
  if (r.hdr_warning.is_populated())
  {
    std::vector<std::string> l = format_warnings(r.hdr_warning);
    for (const std::string &s : l)
    {
      std::cout << s << '\n';
    }
  }

  // Redirection response
  if (r.get_class() == R_3XX && r.hdr_contact.is_populated())
  {
    auto l = r.hdr_contact.contact_list;
    std::sort(l.begin(), l.end());
    std::cout << "You can try the following contacts:\n";
    for (auto const & contact : l)
    {
      std::cout << format_sip_address(user_config, contact.display, contact.uri) << '\n';
    }
  }

  // Unsupported extensions
  if (r.code == R_420_BAD_EXTENSION)
  {
    std::cout << r.hdr_unsupported.encode();
  }

  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_stun_failed_call_ended(unsigned int line)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": call failed.\n";
  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_call_ended(unsigned int line)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": call ended.\n";
  std::cout.flush();
}

void t_userintf::cb_call_established(unsigned int line)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": call established.\n";
  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_options_response(const t_response &r)
{
  std::cout << '\n';
  std::cout << "OPTIONS response received: ";
  std::cout << r.code << ' ' << r.reason << '\n';

  std::cout << "Capabilities of " << r.hdr_to.uri.encode() << '\n';

  std::cout << "Accepted body types\n";
  if (r.hdr_accept.is_populated())
  {
    std::cout << "\t" << r.hdr_accept.encode();
  }
  else
  {
    std::cout << "\tUnknown\n";
  }

  std::cout << "Accepted encodings\n";
  if (r.hdr_accept_encoding.is_populated())
  {
    std::cout << "\t" << r.hdr_accept_encoding.encode();
  }
  else
  {
    std::cout << "\tUnknown\n";
  }

  std::cout << "Accepted languages\n";
  if (r.hdr_accept_language.is_populated())
  {
    std::cout << "\t" << r.hdr_accept_language.encode();
  }
  else
  {
    std::cout << "\tUnknown\n";
  }

  std::cout << "Allowed requests\n";
  if (r.hdr_allow.is_populated())
  {
    std::cout << "\t" << r.hdr_allow.encode();
  }
  else
  {
    std::cout << "\tUnknown\n";
  }

  std::cout << "Supported extensions\n";
  if (r.hdr_supported.is_populated())
  {
    if (r.hdr_supported.features.empty())
    {
      std::cout << "\tNone\n";
    }
    else
    {
      std::cout << "\t" << r.hdr_supported.encode();
    }
  }
  else
  {
    std::cout << "\tUnknown\n";
  }

  std::cout << "End point type\n";
  bool endpoint_known = false;
  if (r.hdr_server.is_populated())
  {
    std::cout << "\t" << r.hdr_server.encode();
    endpoint_known = true;
  }

  if (r.hdr_user_agent.is_populated())
  {
    // Some end-point put a User-Agent header in the response
    // instead of a Server header.
    std::cout << "\t" << r.hdr_user_agent.encode();
    endpoint_known = true;
  }

  if (!endpoint_known)
  {
    std::cout << "\tUnknown\n";
  }

  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_reinvite_success(unsigned int line, const t_response &r)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": re-INVITE successful.\n";
  std::cout << r.code << ' ' << r.reason << '\n';
  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_reinvite_failed(unsigned int line, const t_response &r)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": re-INVITE failed.\n";
  std::cout << r.code << ' ' << r.reason << '\n';
  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_retrieve_failed(unsigned int line, const t_response &r)
{
  if (line >= NUM_USER_LINES) return;

  // The status code from the response has already been reported
  // by cb_reinvite_failed.

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": retrieve failed.\n";
  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_invalid_reg_resp(t_user &user_config,
				     const t_response &r, const std::string &reason)
{
  std::cout << '\n';
  std::cout << user_config.get_profile_name();
  std::cout << ", registration failed: " << r.code << ' ' << r.reason << '\n';
  std::cout << reason << '\n';
  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_register_success(t_user &user_config,
				     const t_response &r, unsigned long expires, bool first_success)
{
  // Only report success if this is the first success in a sequence
  if (!first_success) return;

  std::cout << '\n';
  std::cout << user_config.get_profile_name();
  std::cout << ": registration succeeded (expires = " << expires << " seconds)\n";

  // Date at registrar
  if (r.hdr_date.is_populated())
  {
    std::cout << "Registrar ";
    std::cout << r.hdr_date.encode() << '\n';
  }

  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_register_failed(t_user &user_config,
				    const t_response &r, bool first_failure)
{
  // Only report the first failure in a sequence of failures
  if (!first_failure) return;

  std::cout << '\n';
  std::cout << user_config.get_profile_name();
  std::cout << ", registration failed: " << r.code << ' ' << r.reason << '\n';
  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_register_stun_failed(t_user &user_config, bool first_failure)
{
  // Only report the first failure in a sequence of failures
  if (!first_failure) return;

  std::cout << '\n';
  std::cout << user_config.get_profile_name();
  std::cout << ", registration failed: STUN failure";
  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_deregister_success(t_user &user_config, const t_response &r)
{
  std::cout << '\n';
  std::cout << user_config.get_profile_name();
  std::cout << ", de-registration succeeded: " << r.code << ' ' << r.reason << '\n';
  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_deregister_failed(t_user &user_config,  const t_response &r)
{
  std::cout << '\n';
  std::cout << user_config.get_profile_name();
  std::cout << ", de-registration failed: " << r.code << ' ' << r.reason << '\n';
  std::cout << '\n';
  std::cout.flush();
}
void t_userintf::cb_fetch_reg_failed(t_user &user_config, const t_response &r)
{
  std::cout << '\n';
  std::cout << user_config.get_profile_name();
  std::cout << ", fetch registrations failed: " << r.code << ' ' << r.reason << '\n';
  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_fetch_reg_result(t_user &user_config, const t_response &r)
{
  std::cout << '\n';

  std::cout << user_config.get_profile_name();
  const auto &l = r.hdr_contact.contact_list;
  if (l.empty())
  {
    std::cout << ": you are not registered\n";
  }
  else
  {
    std::cout << ": you have the following registrations\n";
    for (auto const & contact : l)
    {
      std::cout << contact.encode() << '\n';
    }
  }

  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_register_inprog(t_user &user_config, t_register_type register_type)
{
  switch (register_type)
  {
  case REG_REGISTER:
   // Do not report a register refreshment
   if (phone.get_is_registered(&user_config)) return;

   // Do not report an automatic register re-attempt
   if (phone.get_last_reg_failed(&user_config)) return;

   std::cout << '\n';
   std::cout << user_config.get_profile_name();
   std::cout << ": registering phone...\n";
   break;
  case REG_DEREGISTER:
   std::cout << '\n';
   std::cout << user_config.get_profile_name();
   std::cout << ": deregistering phone...\n";
   break;
  case REG_DEREGISTER_ALL:
   std::cout << '\n';
   std::cout << user_config.get_profile_name();
   std::cout << ": deregistering all phones...";
   break;
  case REG_QUERY:
   std::cout << '\n';
   std::cout << user_config.get_profile_name();
   std::cout << ": fetching registrations...";
   break;
  default:
   assert(false);
  }

  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_redirecting_request(t_user &user_config,
					unsigned int line, const t_contact_param &contact)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": redirecting request to:\n";

  std::cout << format_sip_address(user_config, contact.display, contact.uri) << '\n';

  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_redirecting_request(t_user &user_config, const t_contact_param &contact)
{
  std::cout << '\n';
  std::cout << "Redirecting request to: ";

  std::cout << format_sip_address(user_config, contact.display, contact.uri) << '\n';

  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_play_ringtone(unsigned int line)
{
  auto const &ringtone_settings(settings->get_child("ringtone"));
  if (!ringtone_settings->get_boolean("play-ringtone"))
  {
    return;
  }

  if (tone_gen)
  {
    tone_gen->stop();
    tone_gen.reset();
  }

  // Determine ring tone
  std::string ringtone_file = phone.get_ringtone(line);
  auto const &audio_settings(settings->get_child("audio"));
  const std::string &dev_ringtone(audio_settings->get_string("dev-ringtone"));

  tone_gen = std::make_unique<t_tone_gen>(ringtone_file, dev_ringtone);

  // If ring tone does not exist, then fall back to system default.
  if (!tone_gen->is_valid() && ringtone_file != FILE_RINGTONE)
  {
    tone_gen = std::make_unique<t_tone_gen>(FILE_RINGTONE, dev_ringtone);
  }

  // Play ring tone
  tone_gen->start_play_thread(true, INTERVAL_RINGTONE);
}

void t_userintf::cb_play_ringback(t_user &user_config)
{
  auto const &ringtone_settings(settings->get_child("ringtone"));
  if (!ringtone_settings->get_boolean("play-ringback"))
  {
    return;
  }

  if (tone_gen)
  {
    tone_gen->stop();
    tone_gen.reset();
  }

  // Determine ring back tone
  std::string ringback_file;
  if (!user_config.get_ringback_file().empty())
  {
    ringback_file = user_config.get_ringback_file();
  }
  else if (!ringtone_settings->get_string("ringback-file").empty())
  {
    ringback_file = ringtone_settings->get_string("ringback-file");
  }
  else
  {
    // System default
    ringback_file = FILE_RINGBACK;
  }

  auto const &audio_settings(settings->get_child("audio"));
  const std::string &dev_speaker(audio_settings->get_string("dev-speaker"));

  tone_gen = std::make_unique<t_tone_gen>(ringback_file, dev_speaker);

  // If ring back tone does not exist, then fall back to system default.
  if (!tone_gen->is_valid() && ringback_file != FILE_RINGBACK)
  {
    tone_gen = std::make_unique<t_tone_gen>(FILE_RINGBACK, dev_speaker);
  }

  // Play ring back tone
  tone_gen->start_play_thread(true, INTERVAL_RINGBACK);
}

void t_userintf::cb_stop_tone(unsigned int line)
{
  // Only stop the tone if the current line is the active line
  if (tone_gen && line == phone.get_active_line())
  {
    tone_gen->stop();
    tone_gen.reset();
  }
}

void t_userintf::cb_notify_call(unsigned int line, std::string profile_name, std::string from_party)
{
  // Play ringtone if the call is received on the active line
  if (line == phone.get_active_line() &&
      !phone.is_line_auto_answered(line))
  {
    cb_play_ringtone(line);
  }
}

void t_userintf::cb_stop_call_notification(unsigned int line)
{
  cb_stop_tone(line);
}

void t_userintf::cb_dtmf_detected(unsigned int line, char dtmf_event)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": DTMF detected: ";

  if (VALID_DTMF_EV(dtmf_event))
  {
    std::cout << dtmf_ev2char(dtmf_event) << '\n';
  }
  else
  {
    std::cout << "invalid DTMF telephone event (" << (int)dtmf_event << '\n';
  }

  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_send_dtmf(unsigned int line, char dtmf_event)
{
  // No feed back in CLI
}

void t_userintf::cb_dtmf_not_supported(unsigned int line)
{
  if (line >= NUM_USER_LINES) return;

  if (throttle_dtmf_not_supported) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": far end does not support DTMF events.\n";
  std::cout << '\n';
  std::cout.flush();

  // Throttle subsequent call backs
  throttle_dtmf_not_supported = true;
}

void t_userintf::cb_dtmf_supported(unsigned int line)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": far end supports DTMF telephone event.\n";
  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_line_state_changed()
{
  // Nothing to do for CLI
}

void t_userintf::cb_send_codec_changed(unsigned int line, t_audio_codec codec)
{
  // No feedback in CLI
}

void t_userintf::cb_recv_codec_changed(unsigned int line, t_audio_codec codec)
{
  // No feedback in CLI
}

void t_userintf::cb_notify_recvd(unsigned int line, const t_request &r)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 <<  ": received notification.\n";
  std::cout << "Event:    " << r.hdr_event.event_type << '\n';
  std::cout << "State:    " << r.hdr_subscription_state.substate << '\n';

  if (r.hdr_subscription_state.substate == SUBSTATE_TERMINATED)
  {
    std::cout << "Reason:   " << r.hdr_subscription_state.reason << '\n';
  }

  auto *sipfrag = (t_response *)((t_sip_body_sipfrag *)r.body.get())->sipfrag.get();
  std::cout << "Progress: " << sipfrag->code << ' ' << sipfrag->reason << '\n';

  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_refer_failed(unsigned int line, const t_response &r)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": refer request failed.\n";
  std::cout << r.code << ' ' << r.reason << '\n';
  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_refer_result_success(unsigned int line)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": call succesfully referred.\n";
  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_refer_result_failed(unsigned int line)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": call refer failed.\n";
  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_refer_result_inprog(unsigned int line)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": call refer in progress.\n";
  std::cout << "No further notifications will be received.\n";
  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_call_referred(t_user &user_config, unsigned int line, t_request &r)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": transferring call to ";
  std::cout << format_sip_address(user_config, r.hdr_refer_to.display,
				  r.hdr_refer_to.uri);
  std::cout << '\n';

  if (r.hdr_referred_by.is_populated())
  {
    std::cout << "Tranfer requested by ";
    std::cout << format_sip_address(user_config, r.hdr_referred_by.display,
				    r.hdr_referred_by.uri);
    std::cout << '\n';
  }

  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_retrieve_referrer(t_user &user_config, unsigned int line)
{
  if (line >= NUM_USER_LINES) return;

  const t_call_info call_info = phone.get_call_info(line);

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": call transfer failed.\n";
  std::cout << "Retrieving call: \n";
  std::cout << "From:    ";
  std::cout << format_sip_address(user_config, call_info.from_display, call_info.from_uri);
  std::cout << '\n';
  if (!call_info.from_organization.empty())
  {
    std::cout << "         " << call_info.from_organization;
    std::cout << '\n';
  }
  std::cout << "To:      ";
  std::cout << format_sip_address(user_config, call_info.to_display, call_info.to_uri);
  std::cout << '\n';
  if (!call_info.to_organization.empty())
  {
    std::cout << "         " << call_info.to_organization;
    std::cout << '\n';
  }
  std::cout << "Subject: ";
  std::cout << call_info.subject;
  std::cout << '\n' << '\n';
  std::cout.flush();
}

void t_userintf::cb_consultation_call_setup(t_user &user_config, unsigned int line)
{
  if (line >= NUM_USER_LINES) return;

  const t_call_info call_info = phone.get_call_info(line);

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": setup consultation call.\n";
  std::cout << "From:    ";
  std::cout << format_sip_address(user_config, call_info.from_display, call_info.from_uri);
  std::cout << '\n';
  if (!call_info.from_organization.empty())
  {
    std::cout << "         " << call_info.from_organization;
    std::cout << '\n';
  }
  std::cout << "To:      ";
  std::cout << format_sip_address(user_config, call_info.to_display, call_info.to_uri);
  std::cout << '\n';
  if (!call_info.to_organization.empty())
  {
    std::cout << "         " << call_info.to_organization;
    std::cout << '\n';
  }
  std::cout << "Subject: ";
  std::cout << call_info.subject;
  std::cout << '\n' << '\n';
  std::cout.flush();
}

void t_userintf::cb_stun_failed(t_user &user_config, int err_code, const std::string &err_reason)
{
  std::cout << '\n';
  std::cout << user_config.get_profile_name();
  std::cout << ", STUN request failed: ";
  std::cout << err_code << " " << err_reason << '\n';
  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_stun_failed(t_user &user_config)
{
  std::cout << '\n';
  std::cout << user_config.get_profile_name();
  std::cout << ", STUN request failed.\n";
  std::cout << '\n';
  std::cout.flush();
}


bool t_userintf::cb_ask_user_to_redirect_invite(t_user &user_config,
						const t_url &destination, const std::string &display)
{
  // Cannot ask user for permission in CLI, so deny redirection.
  return false;
}

bool t_userintf::cb_ask_user_to_redirect_request(t_user &user_config,
						 const t_url &destination, const std::string &display, t_method method)
{
  // Cannot ask user for permission in CLI, so deny redirection.
  return false;
}

bool t_userintf::cb_ask_credentials(t_user &user_config,
				    const std::string &realm, std::string &username, std::string &password)
{
  // Cannot ask user for username/password in CLI
  return false;
}

void t_userintf::cb_ask_user_to_refer(t_user &user_config,
				      const t_url &refer_to_uri,
				      const std::string &refer_to_display,
				      const t_url &referred_by_uri,
				      const std::string &referred_by_display)
{
  // Cannot ask user for permission in CLI, so deny REFER
  send_refer_permission(false);
}

void t_userintf::send_refer_permission(bool permission)
{
  evq_trans_layer->push_refer_permission_response(permission);
}

void t_userintf::cb_show_msg(const std::string &msg, t_msg_priority prio)
{
  std::cout << '\n';

  switch (prio)
  {
  case MSG_NO_PRIO:
   break;
  case MSG_INFO:
   std::cout << "Info: ";
   break;
  case MSG_WARNING:
   std::cout << "Warning: ";
   break;
  case MSG_CRITICAL:
   std::cout << "Critical: ";
   break;
  default:
   std::cout << "???: ";
  }

  std::cout << msg << '\n';

  std::cout << '\n';
  std::cout.flush();
}

bool t_userintf::cb_ask_msg(const std::string &msg, t_msg_priority prio)
{
  // Cannot ask questions in CLI mode.
  // Print message and return false
  cb_show_msg(msg, prio);
  return false;
}

void t_userintf::cb_display_msg(const std::string &msg, t_msg_priority prio)
{
  // In CLI mode this is the same as cb_show_msg
  cb_show_msg(msg, prio);
}

void t_userintf::cb_log_updated(bool log_zapped)
{
  // In CLI mode there is no log viewer.
}

void t_userintf::cb_call_history_updated()
{
  // In CLI mode there is no call history viewer.
}

void t_userintf::cb_missed_call(int num_missed_calls)
{
  // In CLI mode there is no missed call indication.
}

void t_userintf::cb_nat_discovery_progress_start(int num_steps)
{
  std::cout << '\n';
  std::cout << "Firewall/NAT discovery in progress.\n";
  std::cout << "Please wait.\n";
  std::cout << std::endl;
}

void t_userintf::cb_nat_discovery_finished()
{
  // Nothing to do in CLI mode.
}

void t_userintf::cb_nat_discovery_progress_step(int step)
{
  // Nothing to do in CLI mode.
}

bool t_userintf::cb_nat_discovery_cancelled()
{
  // User cannot cancel NAT discovery in CLI mode.
  return false;
}

void t_userintf::cb_line_encrypted(unsigned int line, bool encrypted, const std::string &cipher_mode)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  if (encrypted)
  {
    std::cout << "Line " << line + 1 << ": audio encryption enabled (";
    std::cout << cipher_mode << ").\n";
  }
  else
  {
    std::cout << "Line " << line + 1 << ": audio encryption disabled.\n";
  }
  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_show_zrtp_sas(unsigned int line, const std::string &sas)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": ZRTP SAS = " << sas << '\n';
  std::cout << "Confirm the SAS if it is correct.\n";
  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_zrtp_confirm_go_clear(unsigned int line)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": remote user disabled encryption.\n";
  std::cout << '\n';
  std::cout.flush();

  phone.pub_zrtp_go_clear_ok(line);
}

void t_userintf::cb_zrtp_sas_confirmed(unsigned int line)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": SAS confirmed.\n";
  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_zrtp_sas_confirmation_reset(unsigned int line)
{
  if (line >= NUM_USER_LINES) return;

  std::cout << '\n';
  std::cout << "Line " << line + 1 << ": SAS confirmation reset.\n";
  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_update_mwi()
{
  // Nothing to do in CLI mode.
}

void t_userintf::cb_mwi_subscribe_failed(t_user &user_config, t_response &r, bool first_failure)
{
  // Only report the first failure in a sequence of failures
  if (!first_failure) return;

  std::cout << '\n';
  std::cout << user_config.get_profile_name();
  std::cout << ", MWI subscription failed: " << r.code << ' ' << r.reason << '\n';
  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_mwi_terminated(t_user &user_config, const std::string &reason)
{
  std::cout << '\n';
  std::cout << user_config.get_profile_name();
  std::cout << ", MWI subscription terminated: " << reason << '\n';
  std::cout << '\n';
  std::cout.flush();
}

bool t_userintf::cb_message_request(t_user &user_config, t_request &r)
{
  std::cout << '\n';
  std::cout << "Received message\n";
  std::cout << "From:\t\t";

  std::string from_party = format_sip_address(user_config,
					      r.hdr_from.get_display_presentation(), r.hdr_from.uri);
  std::cout << from_party << '\n';

  if (r.hdr_organization.is_populated())
  {
    std::cout << "Organization:\t" << r.hdr_organization.name << '\n';
  }

  std::cout << "To:\t\t";
  std::cout << format_sip_address(user_config, r.hdr_to.display, r.hdr_to.uri) << '\n';

  if (r.hdr_subject.is_populated())
  {
    std::cout << "Subject:\t" << r.hdr_subject.subject << '\n';
  }

  std::cout << '\n';
  if (r.body && r.body->get_type() == BODY_PLAIN_TEXT)
  {
    t_sip_body_plain_text *sb = static_cast<t_sip_body_plain_text *>(r.body.get());
    std::cout << sb->text << '\n';
  }
  else if (r.body && r.body->get_type() == BODY_HTML_TEXT)
  {
    t_sip_body_html_text *sb = static_cast<t_sip_body_html_text *>(r.body.get());
    std::cout << sb->text << '\n';
  }
  else
  {
    std::cout << "Unsupported content type.\n";
  }

  std::cout << '\n';
  std::cout.flush();

  // There are no session in CLI mode, so all messages are accepted.
  return true;
}

void t_userintf::cb_message_response(t_user &user_config, t_response &r, t_request &req)
{
  if (r.is_success()) return;

  std::cout << '\n';
  std::cout << "Failed to send MESSAGE.\n";
  std::cout << r.code << " " << r.reason << '\n';

  std::cout << '\n';
  std::cout.flush();
}

void t_userintf::cb_im_iscomposing_request(t_user &user_config, t_request &r,
					   im::t_composing_state state, time_t refresh)
{
  // Nothing to do in CLI mode
  return;
}

void t_userintf::cb_im_iscomposing_not_supported(t_user &user_config, t_response &r)
{
  // Nothing to do in CLI mode
  return;
}

bool t_userintf::get_last_call_info(t_url &url, std::string &display,
				    std::string &subject, t_user **user_config, bool &hide_user) const
{
  if (!last_called_url.is_valid()) return false;

  url = last_called_url;
  display = last_called_display;
  subject = last_called_subject;
  *user_config = phone.ref_user_profile(last_called_profile);
  hide_user = last_called_hide_user;

  return *user_config != NULL;
}

bool t_userintf::can_redial() const
{
  return last_called_url.is_valid() &&
    phone.ref_user_profile(last_called_profile) != NULL;
}

void t_userintf::cmd_call(const std::string &destination)
{
  std::string s = "invite ";
  s += destination;
  exec_command(s);
}

void t_userintf::cmd_quit()
{
  exec_command("quit");
}

void t_userintf::cmd_cli(const std::string &command)
{
  exec_command(command);
}

void t_userintf::cmd_show()
{
  // Do nothing in CLI mode.
}

void t_userintf::cmd_hide()
{
  // Do nothing in CLI mode.
}

std::string t_userintf::get_name_from_abook(t_user &user_config, const t_url &u)
{
  return ab_local->find_name(&user_config, u);
}

const std::vector<std::string>& t_userintf::get_all_commands()
{
  return all_commands;
}
