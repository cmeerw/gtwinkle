/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "address_book.h"

#include "sys_settings.h"
#include "translator.h"
#include "userintf.h"
#include "util.h"

namespace
{
  // Call history file
  const std::string ADDRESS_BOOK_FILE = "twinkle.ab";

  // Field seperator in call history file
  const char REC_SEPARATOR = '|';
}

////////////////////////////
// class t_address_card
////////////////////////////

std::string t_address_card::get_display_name() const
{
  std::string s;

  if (!name_first.empty())
  {
    s = name_first;
  }

  if (!name_infix.empty())
  {
    if (!s.empty()) s += ' ';
    s += name_infix;
  }

  if (!name_last.empty())
  {
    if (!s.empty()) s += ' ';
    s += name_last;
  }

  return s;
}

bool t_address_card::create_file_record(std::vector<std::string> &v) const
{
  v.clear();

  v.push_back(name_first);
  v.push_back(name_infix);
  v.push_back(name_last);
  v.push_back(sip_address);
  v.push_back(remark);

  return true;
}

bool t_address_card::populate_from_file_record(const std::vector<std::string> &v)
{
  // Check number of fields
  if (v.size() != 5) return false;

  name_first = v[0];
  name_infix = v[1];
  name_last = v[2];
  sip_address = v[3];
  remark = v[4];

  return true;
}


////////////////////////////
// class t_address_book
////////////////////////////

// Private

void t_address_book::find_address(t_user *user_config, const t_url &u) const
{
  if (u == last_url) return;

  last_url = u;
  last_name.clear();

  // Normalize url using number conversion rules
  t_url u_normalized(u);
  u_normalized.apply_conversion_rules(user_config);

  for (auto const & card : records)
  {
    std::string full_address =
      ui->expand_destination(*user_config, card.sip_address,
			     u_normalized.get_scheme());
    t_url url_phone(full_address);
    if (!url_phone.is_valid()) continue;

    if (u_normalized.user_host_match(url_phone,
				     user_config->get_remove_special_phone_symbols(),
				     user_config->get_special_phone_symbols()))
    {
      last_name = card.get_display_name();
      return;
    }
  }
}


// Public

t_address_book::t_address_book()
  : utils::t_record_file<t_address_card>()
{
  set_header("first_name|infix_name|last_name|sip_address|remark");
  set_separator(REC_SEPARATOR);

  std::string s(DIR_HOME);
  s += "/";
  s += USER_DIR;
  s += "/";
  s += ADDRESS_BOOK_FILE;
  set_filename(s);
}

void t_address_book::add_address(const t_address_card &address)
{
  std::lock_guard<std::recursive_mutex> g (mtx_records);
  records.push_back(address);
}

bool t_address_book::del_address(const t_address_card &address)
{
  std::lock_guard<std::recursive_mutex> g (mtx_records);

  auto it = find(records.begin(), records.end(), address);
  if (it == records.end())
  {
    return false;
  }

  records.erase(it);

  // Invalidate the cache for the address finder
  last_url.set_url("");

  return true;
}

bool t_address_book::update_address(const t_address_card &old_address,
				    const t_address_card &new_address)
{
  std::lock_guard<std::recursive_mutex> g (mtx_records);

  if (!del_address(old_address))
  {
    return false;
  }

  records.push_back(new_address);

  return true;
}

std::string t_address_book::find_name(t_user *user_config, const t_url &u) const
{
  std::lock_guard<std::recursive_mutex> g (mtx_records);
  find_address(user_config, u);
  std::string name = last_name;

  return name;
}

const std::deque<t_address_card> &t_address_book::get_address_list() const
{
  return records;
}
