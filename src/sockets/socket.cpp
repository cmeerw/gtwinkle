/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <cstdio>
#include <cerrno>
#include <cstring>
#include <sys/un.h>
#include "gtwinkle_config.h"
#include "socket.h"

#if HAVE_UNISTD_H
#include <unistd.h>
#endif

#if HAVE_LINUX_TYPES_H
#include <linux/types.h>
#endif

#if HAVE_LINUX_ERRQUEUE_H
#include <linux/errqueue.h>
#endif

/////////////////
// t_icmp_msg
/////////////////

t_icmp_msg::t_icmp_msg(short _type, short _code,
		       net::ip::address const &_icmp_src_ipaddr,
		       net::ip::endpoint const &_dest)
  : type(_type), code(_code), icmp_src_ipaddr(_icmp_src_ipaddr),
    dest(_dest)
{ }

/////////////////
// t_socket
/////////////////

t_socket::~t_socket() = default;

t_socket::t_socket() = default;


/////////////////
// t_socket_udp
/////////////////


t_socket_udp::t_socket_udp(net::ip::address::addr_type type)
  : net::ip::udp_socket(type)
{ }

bool t_socket_udp::select_read(unsigned long timeout, std::error_code &ec)
{
  fd_set fds;

  FD_ZERO(&fds);
  FD_SET(native_handle(), &fds);

  struct timeval t { static_cast<long>(timeout) / 1000,
		     (static_cast<long>(timeout) % 1000) * 1000 };
  int const ret = select(native_handle() + 1, &fds, nullptr, nullptr, &t);

  if (ret < 0)
  {
    ec = std::make_error_code(static_cast<std::errc>(errno));
  }

  return ret > 0;
}

bool t_socket_udp::enable_icmp()
{
#if HAVE_LINUX_ERRQUEUE_H
  std::error_code ec;
  int enable = 1;
  setsockopt(SOL_IP, IP_RECVERR, &enable, sizeof(int), ec);
  return !ec;
#else
  return false;
#endif
}

bool t_socket_udp::get_icmp(t_icmp_msg &icmp)
{
#if HAVE_LINUX_ERRQUEUE_H
  int ret;
  char buf[256];

  // The destination address of the packet causing the ICMP
  struct sockaddr dest_addr;

  struct msghdr msgh;
  struct cmsghdr *cmsg;

  // Initialize message header to receive the ancillary data for
  // an ICMP message.
  memset(&msgh, 0, sizeof(struct msghdr));
  msgh.msg_control = buf;
  msgh.msg_controllen = 256;
  msgh.msg_name = &dest_addr;
  msgh.msg_namelen = sizeof(struct sockaddr);

  // Get error from the socket error queue
  ret = ::recvmsg(native_handle(), &msgh, MSG_ERRQUEUE);
  if (ret < 0) return false;

  // Find ICMP message in returned controll messages
  for (cmsg = CMSG_FIRSTHDR(&msgh); cmsg != NULL;
       cmsg = CMSG_NXTHDR(&msgh, cmsg))
  {
    if (cmsg->cmsg_level == SOL_IP &&
	cmsg->cmsg_type == IP_RECVERR)
    {
      // ICMP message found
      sock_extended_err *err = (sock_extended_err *)CMSG_DATA(cmsg);
      icmp.type = err->ee_type;
      icmp.code = err->ee_code;

      // Get IP address of host that has sent the ICMP error
      sockaddr *sa_src_icmp = SO_EE_OFFENDER(err);
      if (sa_src_icmp->sa_family == AF_INET)
      {
	sockaddr_in *addr = (sockaddr_in *)sa_src_icmp;
	icmp.icmp_src_ipaddr = net::ip::v4_address(addr->sin_addr.s_addr);
      }
      else
      {
	// Non supported address type
	icmp.icmp_src_ipaddr = net::ip::address();
      }

      // Get destinnation address/port of packet causing the error.
      if (dest_addr.sa_family == AF_INET)
      {
	sockaddr_in *addr = (sockaddr_in *)&dest_addr;
	icmp.dest = net::ip::endpoint(net::ip::v4_address(addr->sin_addr.s_addr),
				      ntohs(addr->sin_port));
	return true;
      }
      else
      {
	// Non supported address type
	continue;
      }
    }
  }
#endif
  return false;
}

std::string h_ip2str(net::ip::address const &ipaddr)
{
  std::string result;

  if (ipaddr.is_v6())
  {
    result += '[';
    result += ipaddr.to_string();
    result += ']';
  }
  else
  {
    result = ipaddr.to_string();
  }

  return result;
}

/////////////////
// t_socket_tcp
/////////////////

t_socket_tcp::t_socket_tcp(net::ip::address::addr_type type)
  : net::ip::tcp_socket(type)
{ }

t_socket_tcp::t_socket_tcp(net::ip::tcp_socket && sock)
  : net::ip::tcp_socket(std::move(sock))
{ }

std::unique_ptr<t_socket_tcp> t_socket_tcp::accept(net::ip::endpoint &ep)
{
  std::unique_ptr<t_socket_tcp> sock(new t_socket_tcp(net::ip::tcp_socket::accept(ep)));
  return sock;
}

/////////////////
// t_socket_local
/////////////////

t_socket_local::t_socket_local()
{
  if (native_handle() < 0)
  {
    throw std::system_error(std::make_error_code(static_cast<std::errc>(errno)));
  }
}

t_socket_local::t_socket_local(net::posix::unix_domain_stream_socket && sock)
  : net::posix::unix_domain_stream_socket(std::move(sock))
{ }
