/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _H_URL
#define _H_URL

#include "parser/header.h"

#include "net/address.h"
#include "net/endpoint.h"

#include <deque>
#include <string>

/** @name Forward declarations */
//@{
class t_user;
//@}

class t_ip_port
{
public:
  std::string transport;
  net::ip::endpoint endpoint;

  t_ip_port()
   : transport("udp")
  { }

  t_ip_port(net::ip::endpoint const &_endpoint);
  t_ip_port(std::string proto, net::ip::endpoint const &_endpoint);

  void clear();

  bool is_null() const noexcept;
  explicit operator bool() const noexcept;

  friend bool operator==(const t_ip_port &lhs, const t_ip_port &rhs) noexcept
  {
    return lhs.transport == rhs.transport && lhs.endpoint == rhs.endpoint;
  }
  friend bool operator!=(const t_ip_port &lhs, const t_ip_port &rhs) noexcept
  {
    return !(lhs == rhs);
  }

  std::string tostring() const;
};

// Return the default port for a protocol (host order)
unsigned short get_default_port(const std::string &protocol);

// Return the first IP address of host name.
// Return 0 if no IP address can be found.
net::ip::address gethostbyname(net::ip::address::addr_type type, const std::string &name);

// Return all IP address of host name
std::deque<net::ip::address> gethostbyname_all(const std::string &name);

/**
 * Get local host name.
 * @return Local host name.
 */
std::string get_local_hostname();

/**
 * Get the source IP address that will be used for sending
 * a packet to a certain destination.
 * @param dst_ip4 [in] The destination IPv4 address.
 * @return The source IPv4 address.
 * @return 0 if the source address cannot be determined.
 */
net::ip::address get_src_address_for_dst(net::ip::address const &dst);

class t_url
{
private:
  /**
   * A t_url object is created with a string represnetation of
   * the URL. The encode method just returns this string.
   * If one of the components of the t_url object is modified
   * however, then encode will build a new string representation.
   * The modified flag indicates if the object was modified after
   * construction.
   */
  bool modified;

  /** URL scheme. */
  std::string scheme;

  /** The user part of a URL. For a tel URL this is empty. */
  std::string user;

  /** The user password. */
  std::string password;

  /** 
   * The host part of a URL. For a tel URL, it contains the part before
   * the first semi-colon.
   */
  std::string host;

  unsigned short port; // host order

  // parameters
  std::string transport;
  std::string maddr;
  bool lr;
  std::string user_param;
  std::string method;
  int ttl;
  std::string other_params; // unparsed other parameters
  // starting with a semi-colon

  // headers
  std::string headers; // unparsed headers

  bool user_url; // true -> user url
  // false -> machine
  bool valid; // indicates if the url is valid

  std::string text_format; // url in string format

  void construct_user_url(const std::string &s);    // eg sip:, mailto:
  void construct_machine_url(const std::string &s); // eg http:, ftp:

  // Parse uri parameters and headers. Returns false if parsing
  // fails.
  bool parse_params_headers(const std::string &s);

public:
  // Escape reserved symbols in a user value
  static std::string escape_user_value(const std::string &user_value);

  // Escape reserved symbols in a password value
  static std::string escape_passwd_value(const std::string &passwd_value);

  // Escape reserved symbols in a header name or value
  static std::string escape_hnv(const std::string &hnv);

public:
  t_url();
  t_url(const std::string &s);

  // Return a copy of the URI without headers.
  // If the URI does not contain any headers, then the copy is
  // identical to the URI.
  t_url copy_without_headers() const;

  void set_url(const std::string &s);

  // Returns "" or 0 if item cannot be found
  std::string get_scheme() const;
  std::string get_user() const;
  std::string get_password() const;
  std::string get_host() const;

  // The following methods will return the default port if
  // no port is present in the url.
  int get_nport() const; // Get port in network order.
  int get_hport() const; // get port in host order.

  // The following method returns 0 if no port is present
  // in the url.
  int get_port() const;

  // ip address host order. Return 0 if address not found
  // DNS A RR lookup
  net::ip::address get_h_ip() const;
  std::deque<net::ip::address> get_h_ip_all() const;

  // Get list op IP address/ports in host order.
  // First do DNS SRV lookup. If no SRV RR's are found, then
  // do a DNS A RR lookup.
  // transport = the transport protocol for the service
  std::deque<t_ip_port> get_h_ip_srv(const std::string &transport) const;

  /** @name Getters */
  //@{
  std::string get_transport() const;
  std::string get_maddr() const;
  bool get_lr() const;
  std::string get_user_param() const;
  std::string get_method() const;
  int get_ttl() const;
  std::string get_other_params() const;
  std::string get_headers() const;
  //@}

  /** @name Setters */
  //@{
  void set_user(const std::string &u);
  void set_host(const std::string &h);
  //@}

  /**
   * Add a header to the URI.
   * The encoded header will be concatenated to the headers field.
   * @param hdr [in] Header to be added.
   */
  void add_header(const t_header &hdr);

  /** Remove headers from the URI. */
  void clear_headers();

  /**
   * Check if the URI is valid.
   * @return True if valid, otherwise false.
   */
  bool is_valid() const;

  // Check if 2 sip or sips url's are equivalent
  bool sip_match(const t_url &u) const;
  friend bool operator==(const t_url &lhs, const t_url &rhs) noexcept
  {
    return lhs.sip_match(rhs);
  }
  friend bool operator!=(const t_url &lhs, const t_url &rhs) noexcept
  {
    return !(lhs == rhs);
  }

  /**
   * Check if the user-host part of 2 url's are equal.
   * If the user-part is a phone number, then only compare
   * the user parts. If the url is a tel-url then the host
   * contains a telephone number for comparison.
   * @param u [in] Other URL to compare with.
   * @param looks_like_phone [in] Flag to indicate is a SIP URL
   *        that looks like a phone number must be treated as such.
   * @param special_symbols [in] Interpuction symbols in a phone number.
   * @return true if the URLs match, false otherwise.
   */
  bool user_host_match(const t_url &u, bool looks_like_phone, 
		       const std::string &special_symbols) const;

  // Return true if the user part looks like a phone number, i.e.
  // consists of digits, *, # and special symbols
  bool user_looks_like_phone(const std::string &special_symbols) const;

  // Return true if the URI indicates a phone number, i.e.
  // - the user=phone parameter is present
  // or
  // - if looks_like_phone == true and the user part looks like
  //   a phone number
  bool is_phone(bool looks_like_phone, const std::string &special_symbols) const;

  // Return string encoding of url
  std::string encode() const;

  // Return string encoding of url without scheme information
  std::string encode_noscheme() const;

  // Return string encoding of url without parameters/headers
  std::string encode_no_params_hdrs(bool escape = true) const;

  /**
   * Apply number conversion rules to modify the URL.
   * @param user_config [in] The user profile having the conversion
   *        rules to apply.
   */
  void apply_conversion_rules(t_user *user_config);
};

// Display name and url combined

class t_display_url
{
public:
  t_url url;
  std::string display;

  t_display_url();
  t_display_url(t_url _url, std::string _display);

  bool is_valid() const;
  std::string encode() const;
};

#endif
