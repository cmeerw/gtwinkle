/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/**
 * @file
 * Socket operations
 */

#ifndef _H_SOCKET
#define _H_SOCKET

#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

#include "net/socket.h"

#include <memory>
#include <string>


// ports and addresses should be in host order

/** ICMP message */
class t_icmp_msg
{
public:
  short type;
  short code;

  // ICMP source IP address
  net::ip::address icmp_src_ipaddr;

  // Destination IP address/port of packet causing the ICMP message.
  net::ip::endpoint dest;

  t_icmp_msg() {};
  t_icmp_msg(short _type, short _code,
	     net::ip::address const & _icmp_src_ipaddr,
	     net::ip::endpoint const & _dest);
};

/** Abstract socket */
class t_socket
{
protected:
  t_socket();

public:
  /** Destructor */
  virtual ~t_socket();
};

/** UDP socket */
class t_socket_udp : public t_socket, public net::ip::udp_socket
{
public:
  // Create a socket and bind it to any port.
  // Throws an int exception if it fails. The int thrown is the value
  // of errno as set by 'socket' or 'bind'.
  t_socket_udp(net::ip::address::addr_type = net::ip::address::v4_addr);

  // Do a select on the socket in read mode. timeout is in ms.
  // Returns true if the socket becomes unblocked. Returns false
  // on time out. Throws an int exception if select fails
  // (errno as set by 'select')
  bool select_read(unsigned long timeout, std::error_code &ec);

  // Enable reception of ICMP errors on this socket.
  // Returns false if ICMP reception cannot be enabled.
  bool enable_icmp();

  // Get an ICMP message that was received on this socket.
  // Returns false if no ICMP message can be retrieved.
  bool get_icmp(t_icmp_msg &icmp);
};

/** TCP socket */
class t_socket_tcp : public t_socket, public net::ip::tcp_socket
{
public:
  /**
   * Constructor. Create a socket.
   * @throw int The errno value
   */
  t_socket_tcp(net::ip::address::addr_type = net::ip::address::v4_addr);

  /**
   * Constructor. Create a socket from an existing descriptor.
   */
  t_socket_tcp(net::ip::tcp_socket && sock);

  /**
   * Accept a connection
   * @param src_addr [out] Source IPv4 address of the connection.
   * @param src_port [out] Source port of the connection.
   * @return A socket for the new connection
   * @throw int Errno
   */
  std::unique_ptr<t_socket_tcp> accept(net::ip::endpoint &ep);
};

/** Local socket */
class t_socket_local : public t_socket, public net::posix::unix_domain_stream_socket
{
public:
  // Throws an int exception if it fails. The int thrown is the value
  // of errno as set by 'socket'
  t_socket_local();
  t_socket_local(net::posix::unix_domain_stream_socket && sock);
};

// Convert an IP address in host order to a string.
std::string h_ip2str(net::ip::address const &ipaddr);

#endif
