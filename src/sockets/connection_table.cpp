/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "connection_table.h"

#include <algorithm>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/time.h>
#include <cerrno>
#include <iostream>
#include <cstdlib>
#include <fcntl.h>

#include <vector>

#include "log.h"
#include "protocol.h"
#include "util.h"


extern t_connection_table *connection_table;

void t_connection_table::create_pipe(int p[2])
{
  if (pipe(p) == -1)
  {
    std::string err = get_error_str(errno);
    std::cerr << "FATAL: t_connection_table - Cannot create pipe.\n";
    std::cerr << err << std::endl;
    exit(-1);
  }

  if (fcntl(p[0], F_SETFL, O_NONBLOCK) == -1)
  {
    std::string err = get_error_str(errno);
    std::cerr << "FATAL: t_connection_table - fcntl fails on read side of pipe.\n";
    std::cerr << err << std::endl;
    exit(-1);
  }

  if (fcntl(p[1], F_SETFL, O_NONBLOCK) == -1)
  {
    std::string err = get_error_str(errno);
    std::cerr << "FATAL: t_connection_table - fcntl fails on write side of pipe.\n";
    std::cerr << err << std::endl;
    exit(-1);
  }
}

void t_connection_table::signal_modification_read()
{
  std::lock_guard<std::recursive_mutex> guard(mtx_connections_);

  // Write a byte to the modified pipe, so a select can be retried.
  char x = 'x';
  (void)write(fd_pipe_modified_read_[1], &x, 1);
}

void t_connection_table::signal_modification_write()
{
  std::lock_guard<std::recursive_mutex> guard(mtx_connections_);

  // Write a byte to the modified pipe, so a select can be retried.
  char x = 'x';
  (void)write(fd_pipe_modified_write_[1], &x, 1);
}

void t_connection_table::signal_quit()
{
  std::lock_guard<std::recursive_mutex> guard(mtx_connections_);

  // Write a byte to the quit pipe, so a select can be halted.
  char x = 'x';
  (void)write(fd_pipe_quit_read_[1], &x, 1);
  (void)write(fd_pipe_quit_write_[1], &x, 1);

  terminated_ = true;
}

std::recursive_mutex t_connection_table::mtx_connections_;

t_connection_table::t_connection_table()
  : terminated_(false)
{
  create_pipe(fd_pipe_modified_read_);
  create_pipe(fd_pipe_modified_write_);
  create_pipe(fd_pipe_quit_read_);
  create_pipe(fd_pipe_quit_write_);
}

t_connection_table::~t_connection_table() = default;

void t_connection_table::unlock() const
{
  mtx_connections_.unlock();
}

bool t_connection_table::empty() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_connections_);
  return connections_.empty();
}

t_connection_table::size_type t_connection_table::size() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_connections_);
  return connections_.size();
}

void t_connection_table::add_connection(std::unique_ptr<t_connection> connection)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_connections_);
  connections_.push_back(std::move(connection));
  signal_modification_read();
  signal_modification_write();
}

void t_connection_table::remove_connection(t_connection *connection)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_connections_);

  connections_.remove_if(
    [connection] (std::unique_ptr<t_connection> const &other) {
      return other.get() == connection;
    });

  signal_modification_read();
  signal_modification_write();
}

t_connection *t_connection_table::get_connection(net::ip::endpoint const & remote)
{
  mtx_connections_.lock();

  t_connection *found_connection = nullptr;
  std::vector<t_connection *> broken_connections;

  for (auto const & conn : connections_)
  {
    net::ip::endpoint ep;

    if (conn->may_reuse())
    {
      t_socket_tcp &socket(conn->get_socket());

      std::error_code ec;
      ep = socket.getpeername(ec);
      if (ec)
      {
	// This should never happen.
	std::cerr << "Cannot get remote address of socket." << std::endl;

	// Destroy and remove connection as it is probably broken.
	broken_connections.push_back(conn.get());
      }
      else if (ep == remote)
      {
	found_connection = conn.get();
	break;
      }
    }
  }

  // Clear broken connections
  for (auto conn : broken_connections)
  {
    remove_connection(conn);
  }

  if (!found_connection) mtx_connections_.unlock();
  return found_connection;
}

std::vector<t_connection *> t_connection_table::select_read(struct timeval *timeout) const
{
  fd_set read_fds;
  int nfds = 0;
  bool retry = true;
  std::vector<t_connection *> result;

  // Empty modification pipe
  char pipe_buf;
  while (read(fd_pipe_modified_read_[0], &pipe_buf, 1) > 0);

  while (retry)
  {
    FD_ZERO(&read_fds);

    // Add modification pipe so select can be restarted when the
    // connection table modifies.
    FD_SET(fd_pipe_modified_read_[0], &read_fds);
    nfds = fd_pipe_modified_read_[0];

    // Add quit pipe so select can quit on demand.
    FD_SET(fd_pipe_quit_read_[0], &read_fds);
    nfds = std::max(nfds, fd_pipe_quit_read_[0]);

    {
      std::lock_guard<std::recursive_mutex> g (mtx_connections_);

      for (auto const & conn : connections_)
      {
	t_socket_tcp &socket(conn->get_socket());
	int fd = socket.native_handle();
	FD_SET(fd, &read_fds);
	nfds = std::max(nfds, fd);
      }
    }

    int ret = select(nfds + 1, &read_fds, nullptr, nullptr, timeout);
    if (ret < 0)
    {
      throw std::system_error(std::make_error_code(static_cast<std::errc>(errno)));
    }

    if (FD_ISSET(fd_pipe_quit_read_[0], &read_fds))
    {
      // Quit was signalled, so stop immediately.
      break;
    }

    mtx_connections_.lock();

    // Determine which sockets have become readable
    for (auto const & conn : connections_)
    {
      t_socket_tcp &socket(conn->get_socket());
      int fd = socket.native_handle();
      if (FD_ISSET(fd, &read_fds))
      {
	result.push_back(conn.get());
      }
    }

    if (!result.empty())
    {
      // Connections have become readable, so return to the caller.
      retry = false;
    }
    else
    {
      mtx_connections_.unlock();

      // No connections have become readable. Check signal descriptors
      if (FD_ISSET(fd_pipe_modified_read_[0], &read_fds))
      {
	// The connection table is modified. Retry select.
	read(fd_pipe_modified_read_[0], &pipe_buf, 1);
      }
      else
      {
	// This should never happen.
	std::cerr << "ERROR: select_read returned without any file descriptor." << std::endl;
      }
    }
  }

  return result;
}

std::vector<t_connection *> t_connection_table::select_write(struct timeval *timeout) const
{
  fd_set read_fds;
  fd_set write_fds;
  int nfds = 0;
  bool retry = true;
  std::vector<t_connection *> result;

  // Empty modification pipe
  char pipe_buf;
  while (read(fd_pipe_modified_write_[0], &pipe_buf, 1) > 0);

  while (retry)
  {
    FD_ZERO(&read_fds);
    FD_ZERO(&write_fds);

    // Add modification pipe so select can be restarted when the
    // connection table modifies.
    FD_SET(fd_pipe_modified_write_[0], &read_fds);
    nfds = fd_pipe_modified_write_[0];

    // Add quit pipe so select can quit on demand.
    FD_SET(fd_pipe_quit_write_[0], &read_fds);
    nfds = std::max(nfds, fd_pipe_quit_write_[0]);

    {
      std::lock_guard<std::recursive_mutex> g (mtx_connections_);

      for (auto const & conn : connections_)
      {
	if (conn->has_data_to_send())
	{
	  t_socket_tcp &socket(conn->get_socket());
	  int fd = socket.native_handle();
	  FD_SET(fd, &write_fds);
	  nfds = std::max(nfds, fd);
	}
      }
    }

    int ret = select(nfds + 1, &read_fds, &write_fds, nullptr, timeout);
    if (ret < 0)
    {
      throw std::system_error(std::make_error_code(static_cast<std::errc>(errno)));
    }

    if (FD_ISSET(fd_pipe_quit_write_[0], &read_fds))
    {
      // Quit was signalled, so stop immediately.
      break;
    }

    mtx_connections_.lock();

    // Determine which sockets have become writable
    for (auto const & conn : connections_)
    {
      t_socket_tcp &socket(conn->get_socket());
      int fd = socket.native_handle();
      if (FD_ISSET(fd, &write_fds))
      {
	result.push_back(conn.get());
      }
    }

    if (!result.empty())
    {
      // Connections have become writable, so return to the caller.
      retry = false;
    }
    else
    {
      mtx_connections_.unlock();

      // No connections have become writable. Check signal descriptors
      if (FD_ISSET(fd_pipe_modified_write_[0], &read_fds))
      {
	// The connection table is modified. Retry select.
	read(fd_pipe_modified_write_[0], &pipe_buf, 1);
      }
      else
      {
	// This should never happen.
	std::cerr << "ERROR: select_write returned without any file descriptor." << std::endl;
      }
    }
  }

  return result;
}

void t_connection_table::cancel_select()
{
  signal_quit();
}

void t_connection_table::restart_write_select()
{
  signal_modification_write();
}

void t_connection_table::close_idle_connections(unsigned long interval, bool &terminated)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_connections_);

  terminated = terminated_;

  std::vector<t_connection *> expired_connections;

  // Update idle times and find expired connections.
  for (const std::unique_ptr<t_connection> &conn : connections_)
  {
    unsigned long idle_time = conn->increment_idle_time(interval);
    if (idle_time >= DUR_IDLE_CONNECTION || terminated)
    {
      // If a registered URI is associated with the connection, then
      // it is persistent and it should not be closed.
      if (!conn->has_registered_uri())
      {
	expired_connections.push_back(conn.get());
      }
    }
  }

  // Close expired connections.
  for (t_connection * const conn : expired_connections)
  {
    net::ip::endpoint ep (conn->get_remote_address());
    log_file->write_header("t_connection_table::close_idle_connections", LOG_NORMAL, LOG_DEBUG);
    log_file->write_raw("Close connection to ");
    log_file->write_raw(h_ip2str(ep.addr()));
    log_file->write_raw(":");
    log_file->write_raw(int2str(ep.port()));
    log_file->write_endl();
    log_file->write_footer();

    remove_connection(conn);
  }
}

void connection_timeout_main()
{
  bool terminated = false;

  while (!terminated)
  {
    struct timespec sleep_timer;

    sleep_timer.tv_sec = 1;
    sleep_timer.tv_nsec = 0;
    nanosleep(&sleep_timer, nullptr);
    connection_table->close_idle_connections(1000, terminated);
  }

  log_file->write_report("Connection timeout handler terminated.",
			 "::connection_timeout_main");
};
