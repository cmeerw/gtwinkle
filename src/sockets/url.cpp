/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "dnssrv.h"
#include "log.h"
#include "socket.h"
#include "url.h"
#include "user.h"
#include "util.h"

#include <vector>

#include "net/addrinfo.h"
#include "net/socket.h"
#include "sys_settings.h"


unsigned short get_default_port(const std::string &protocol)
{
  if (protocol == "mailto") return 25;
  if (protocol == "http") return 80;
  if (protocol == "sip") return 5060;
  if (protocol == "sips") return 5061;
  if (protocol == "stun") return 3478;

  return 0;
}

net::ip::address gethostbyname(net::ip::address::addr_type type, const std::string &name)
{
  net::ip::address addr;

  for (auto const & ai :
	 net::addrinfo(name.c_str(), ""
		       , type == net::ip::address::v6_addr ? AF_INET6
		       : type == net::ip::address::v4_addr ? AF_INET
		       : AF_UNSPEC))
  {
    if (ai.ai_family == AF_INET)
    {
      addr = net::ip::v4_address(reinterpret_cast<struct sockaddr_in *>(ai.ai_addr)->sin_addr.s_addr);
      break;
    }
    else if (ai.ai_family == AF_INET6)
    {
      addr = net::ip::v6_address(reinterpret_cast<struct sockaddr_in6 *>(ai.ai_addr)->sin6_addr);
      break;
    }
  }

  return addr;
}

std::deque<net::ip::address> gethostbyname_all(const std::string &name)
{
  auto const &network_settings(settings->get_child("network"));

  std::deque<net::ip::address> addrs;
  for (auto const & ai :
	 net::addrinfo(name.c_str(), "",
		       (! network_settings->get_boolean("use-ipv6") ?
			AF_INET : AF_UNSPEC)))
  {
    if (ai.ai_family == AF_INET)
    {
      addrs.push_back(net::ip::v4_address(reinterpret_cast<struct sockaddr_in *>(ai.ai_addr)->sin_addr.s_addr));
    }
    else if (ai.ai_family == AF_INET6)
    {
      addrs.push_back(net::ip::v6_address(reinterpret_cast<struct sockaddr_in6 *>(ai.ai_addr)->sin6_addr));
    }
  }

  return addrs;
}

std::string get_local_hostname()
{
  char name[256];
  int const rc = gethostname(name, 256);

  if (rc < 0)
  {
    return "localhost";
  }

  if (struct hostent *h = ::gethostbyname(name))
  {
    return h->h_name;
  }

  return "localhost";
}

net::ip::address get_src_address_for_dst(net::ip::address const & dst)
{
  try
  {
    net::ip::udp_socket sock(dst.type());
    sock.connect(net::ip::endpoint(dst, 5060));
    net::ip::endpoint ep(sock.getsockname());

    return ep.addr();
  }
  catch (const std::system_error &err)
  {
    std::string log_msg ("Cannot connect socket: ");
    log_msg += err.what();
    log_file->write_report(log_msg, "::get_src_address_for_dst",
			   LOG_NORMAL, LOG_CRITICAL);
  }

  return net::ip::address();
}

std::string display_and_url2str(const std::string &display, const std::string &url)
{
  std::string s;

  if (!display.empty())
  {
    if (must_quote(display)) s += '"';
    s += display;
    if (must_quote(display)) s += '"';
    s += " <";
  }

  s += url;

  if (!display.empty()) s += '>';

  return s;
}

// t_ip_port

t_ip_port::t_ip_port(net::ip::endpoint const &_endpoint)
  : transport("udp"), endpoint(_endpoint)
{ }

t_ip_port::t_ip_port(std::string proto,
		     net::ip::endpoint const &_endpoint)
  : transport(std::move(proto)), endpoint(_endpoint)
{ }

void t_ip_port::clear()
{
  transport.clear();
  endpoint = net::ip::endpoint();
}

bool t_ip_port::is_null() const noexcept
{
  return endpoint.addr().is_unspec() && (endpoint.port() == 0);
}

t_ip_port::operator bool() const noexcept
{
  return !endpoint.addr().is_unspec();
}

std::string t_ip_port::tostring() const
{
  std::string s;
  s = transport;
  s += ":";
  s += h_ip2str(endpoint.addr());
  s += ":";
  s += int2str(endpoint.port());

  return s;
}

// Private

void t_url::construct_user_url(const std::string &s)
{
  std::string::size_type i;
  std::string r;

  // Determine user/password (both are optional)
  i = s.find('@');
  if (i != std::string::npos)
  {
    if (i == 0 || i == s.size()-1) return;
    std::string userpass = s.substr(0, i);
    r = s.substr(i+1);
    i = userpass.find(':');
    if (i != std::string::npos)
    {
      if (i == 0 || i == userpass.size()-1) return;
      user = unescape_hex(userpass.substr(0, i));
      password = unescape_hex(userpass.substr(i+1));

      if (escape_passwd_value(password) != password)
      {
	modified = true;
      }
    }
    else
    {
      user = unescape_hex(userpass);
    }

    // Set modified flag if user contains reserved symbols.
    // This enforces escaping these symbols when the url gets
    // encoded.
    if (escape_user_value(user) != user)
    {
      modified = true;
    }
  }
  else
  {
    r = s;
  }

  // Determine host/port
  std::string hostport;

  i = r.find_first_of(";?");
  if (i != std::string::npos)
  {
    hostport = r.substr(0, i);
    if (!parse_params_headers(r.substr(i))) return;
  }
  else
  {
    hostport = r;
  }

  if (hostport.empty()) return;

  if (hostport.at(0) == '[')
  {
    // Host contains an IPv6 reference
    i = hostport.find(']');
    if (i == std::string::npos) return;
    // TODO: check format of an IPv6 address
    host = hostport.substr(0, i+1);
    if (i < hostport.size()-1)
    {
      if (hostport.at(i+1) != ':') return; // wrong port separator
      if (i+1 == hostport.size()-1) return; // port missing
      unsigned long p = atol(hostport.substr(i+2).c_str());
      if (p > 65535) return; // illegal port value
      port = (unsigned short)p;
    }
  }
  else
  {
    // Host contains a host name or IPv4 address
    i = hostport.find(':');
    if (i != std::string::npos)
    {
      if (i == 0 || i == hostport.size()-1) return;
      host = hostport.substr(0, i);
      unsigned long p = atol(hostport.substr(i+1).c_str());
      if (p > 65535) return; // illegal port value
      port = (unsigned short)p;
    }
    else
    {
      host = hostport;
    }
  }

  user_url = true;
  valid = true;
}


void t_url::construct_machine_url(const std::string &s)
{
  std::string::size_type i;

  // Determine host
  std::string hostport;
  i = s.find_first_of("/?;");
  if ( i != std::string::npos)
  {
    hostport = s.substr(0, i);
    if (!parse_params_headers(s.substr(i))) return;
  }
  else
  {
    hostport = s;
  }

  if (hostport.empty()) return;

  if (hostport.at(0) == '[')
  {
    // Host contains an IPv6 reference
    i = hostport.find(']');
    if (i == std::string::npos) return;
    // TODO: check format of an IPv6 address
    host = hostport.substr(0, i+1);
    if (i < hostport.size()-1)
    {
      if (hostport.at(i+1) != ':') return; // wrong port separator
      if (i+1 == hostport.size()-1) return; // port missing
      unsigned long p = atol(hostport.substr(i+2).c_str());
      if (p > 65535) return; // illegal port value
      port = (unsigned short)p;
    }
  }
  else
  {
    // Host contains a host name or IPv4 address
    i = hostport.find(':');
    if (i != std::string::npos)
    {
      if (i == 0 || i == hostport.size()-1) return;
      host = hostport.substr(0, i);
      unsigned long p = atol(hostport.substr(i+1).c_str());
      if (p > 65535) return; // illegal port value
      port = (unsigned short)p;
    }
    else
    {
      host = hostport;
    }
  }

  user_url = false;
  valid = true;
}

bool t_url::parse_params_headers(const std::string &s)
{
  std::string param_str;

  // Find start of headers
  // Note: parameters will not contain / or ?-symbol
  std::string::size_type header_start = s.find_first_of("/?");
  if (header_start != std::string::npos)
  {
    headers = s.substr(header_start + 1);

    if (s[0] == ';')
    {
      // The first symbol of the parameter list is ;
      // Remove this.
      param_str = s.substr(1, header_start - 1);
    }
  }
  else
  {
    // There are no headers
    // The first symbol of the parameter list is ;
    // Remove this.
    param_str = s.substr(1);
  }

  if (param_str.empty()) return true;

  // Create a list of single parameters. Parameters are
  // seperated by semi-colons.
  // Note: parameters will not contain a semi-colon in the
  //       name or value.
  // Parse the parameters
  for (const std::string & s : split(param_str, ';'))
  {
    std::string pname;
    std::string pvalue;

    auto const param = split(s, '=');
    if (param.size() > 2) return false;

    pname = tolower(unescape_hex(trim(param.front())));
    if (param.size() == 2)
    {
      pvalue = tolower(unescape_hex(trim(param.back())));
    }

    if (pname == "transport")
    {
      transport = pvalue;
    }
    else if (pname == "maddr")
    {
      maddr = pvalue;
    }
    else if (pname == "lr")
    {
      lr = true;
    }
    else if (pname == "user")
    {
      user_param = pvalue;
    }
    else if (pname == "method")
    {
      method = pvalue;
    }
    else if (pname == "ttl")
    {
      ttl = atoi(pvalue.c_str());
    }
    else
    {
      other_params += ';';
      other_params += s;
    }
  }

  return true;
}

// Public static

std::string t_url::escape_user_value(const std::string &user_value)
{
  // RFC 3261
  // user             =  1*( unreserved / escaped / user-unreserved )
  // user-unreserved  =  "&" / "=" / "+" / "$" / "," / ";" / "?" / "/"
  // unreserved       =  alphanum / mark
  // mark             =  "-" / "_" / "." / "!" / "~" / "*" / "'" / "(" / ")"

  return escape_hex(user_value,
		    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYX0123456789"\
		    "-_.!~*'()&=+$,;?/");
}

std::string t_url::escape_passwd_value(const std::string &passwd_value)
{
  // RFC 3261
  // password         = *( unreserved / escaped / "&" / "=" / "+" / "$" / "," )
  // unreserved       =  alphanum / mark
  // mark             =  "-" / "_" / "." / "!" / "~" / "*" / "'" / "(" / ")"

  return escape_hex(passwd_value,
		    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYX0123456789"\
		    "-_.!~*'()&=+$,");
}

std::string t_url::escape_hnv(const std::string &hnv)
{
  // RFC 3261
  // hname           =  1*( hnv-unreserved / unreserved / escaped )
  // hvalue          =  *( hnv-unreserved / unreserved / escaped )
  // hnv-unreserved  =  "[" / "]" / "/" / "?" / ":" / "+" / "$"
  // unreserved       =  alphanum / mark
  // mark             =  "-" / "_" / "." / "!" / "~" / "*" / "'" / "(" / ")"

  return escape_hex(hnv,
		    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYX0123456789"\
		    "-_.!~*'()[]/?:+$");
}

// Public

t_url::t_url()
  : modified(false), port(0), lr(false), ttl(0), valid(false)
{ }

t_url::t_url(const std::string &s)
{
  set_url(s);
}

t_url t_url::copy_without_headers() const
{
  t_url u(*this);
  u.clear_headers();
  return u;
}

void t_url::set_url(const std::string &s)
{
  std::string::size_type i;
  std::string r;

  modified = false;
  valid = false;
  scheme.clear();
  user.clear();
  password.clear();
  host.clear();
  port = 0;
  transport.clear();
  maddr.clear();
  lr = false;
  user_param.clear();
  method.clear();
  ttl = 0;
  other_params.clear();
  headers.clear();
  user_url = false;

  text_format = s;

  // Determine scheme. A scheme is mandatory. There should
  // be text following the scheme.
  i = s.find(':');
  if (i == std::string::npos || i == 0 || i == s.size()-1) return;
  scheme = tolower(s.substr(0, i));
  r = s.substr(i+1);

  if (r[0] == '/')
  {
    if (r.size() == 1) return;
    if (r[1] != '/') return;
    construct_machine_url(r.substr(2));
  }
  else
  {
    construct_user_url(r);
  }
}

std::string t_url::get_scheme() const
{
  return scheme;
}

std::string t_url::get_user() const
{
  return user;
}

std::string t_url::get_password() const
{
  return password;
}

std::string t_url::get_host() const
{
  return host;
}

int t_url::get_nport() const
{
  return htons(get_hport());
}

int t_url::get_hport() const
{
  if (port != 0) return port;
  return get_default_port(scheme);
}

int t_url::get_port() const
{
  return port;
}

net::ip::address t_url::get_h_ip() const
{
  net::ip::address addr;
  if (scheme != "tel")
  {
    if (!host.empty() && (host.front() == '[') && (host.back() == ']'))
    {
      // IPv6 address literal
      addr = net::ip::v6_address(host.substr(1, host.length() - 2));
    }
    else
    {
      addr = gethostbyname(net::ip::address::v4_addr, host);
    }
  }

  return addr;
}

std::deque<net::ip::address> t_url::get_h_ip_all() const
{
  std::deque<net::ip::address> addrs;

  if (scheme != "tel")
  {
    if (!host.empty() && (host.front() == '[') && (host.back() == ']'))
    {
      // IPv6 address literal
      addrs.push_back(net::ip::v6_address(host.substr(1, host.length() - 2)));
    }
    else
    {
      addrs = gethostbyname_all(host);
    }
  }

  return addrs;
}

std::deque<t_ip_port> t_url::get_h_ip_srv(const std::string &transport) const
{
  std::deque<t_ip_port> ip_list;

  if (scheme != "tel")
  {
    // RFC 3263 4.2
    // Only do an SRV lookup if host is a hostname and no port is specified.
    if (!host.empty() &&
	((host.front() != '[') || (host.back() != ']')) &&
	!is_ipaddr(host) && port == 0)
    {
      std::vector<t_dns_result> srv_list;
      int const ret = insrv_lookup(scheme.c_str(), transport.c_str(),
				   host.c_str(), srv_list);

      if (ret >= 0 && !srv_list.empty())
      {
	// SRV RR's found
	for (auto const & srv : srv_list)
	{
	  // Get A RR's
	  for (auto const & ipaddr : gethostbyname_all(srv.hostname))
	  {
	    ip_list.push_back(t_ip_port(transport,
					net::ip::endpoint(ipaddr, srv.port)));
	  }
	}

	return ip_list;
      }
    }

    // No SRV RR's found, do an A RR lookup
    for (auto const & ipaddr : get_h_ip_all())
    {
      ip_list.push_back(t_ip_port(transport,
				  net::ip::endpoint(ipaddr, get_hport())));
    }
  }

  return ip_list;
}

std::string t_url::get_transport() const
{
  return transport;
}

std::string t_url::get_maddr() const
{
  return maddr;
}

bool t_url::get_lr() const
{
  return lr;
}

std::string t_url::get_user_param() const
{
  return user_param;
}

std::string t_url::get_method() const
{
  return method;
}

int t_url::get_ttl() const
{
  return ttl;
}

std::string t_url::get_other_params() const
{
  return other_params;
}

std::string t_url::get_headers() const
{
  return headers;
}

void t_url::set_user(const std::string &u)
{
  modified = true;
  user = u;
}

void t_url::set_host(const std::string &h)
{
  modified = true;
  host = h;
}

void t_url::add_header(const t_header &hdr)
{
  if (!hdr.is_populated()) return;

  modified = true;

  if (!headers.empty()) headers += ';';
  headers += escape_hnv(hdr.get_name());
  headers += '=';
  headers += escape_hnv(hdr.get_value());
}

void t_url::clear_headers()
{
  if (headers.empty())
  {
    // No headers to clear
    return;
  }

  modified = true;
  headers.clear();
}

bool t_url::is_valid() const
{
  return valid;
}

// RCF 3261 19.1.4
bool t_url::sip_match(const t_url &u) const
{
  if (!u.is_valid() || !is_valid()) return false;

  // Compare schemes
  if (scheme != "sip" && scheme != "sips") return false;
  if (u.get_scheme() != "sip" && u.get_scheme() != "sips")
  {
    return false;
  }
  if (scheme != u.get_scheme()) return false;

  // Compare user info
  if (user != u.get_user()) return false;
  if (password != u.get_password()) return false;

  // Compare host/port
  if (cmp_nocase(host, u.get_host()) != 0) return false;
  if (port != u.get_port()) return false;

  // Compare parameters
  if (!transport.empty() && !u.get_transport().empty() &&
      cmp_nocase(transport, u.get_transport()) != 0)
  {
    return false;
  }

  if (maddr != u.get_maddr()) return false;
  if (cmp_nocase(user_param, u.get_user_param()) != 0) return false;
  if (cmp_nocase(method, u.get_method()) != 0) return false;
  if (ttl != u.get_ttl()) return false;

  // TODO: compare other params and headers

  return true;
}

bool t_url::user_host_match(const t_url &u, bool looks_like_phone,
			    const std::string &special_symbols) const
{
  std::string u1 = get_user();
  std::string u2 = u.get_user();

  // For tel-URIs the phone number is in the host part.
  if (scheme == "tel") u1 = get_host();
  if (u.scheme == "tel") u2 = u.get_host();

  bool u1_is_phone = false;
  bool u2_is_phone = false;

  if (is_phone(looks_like_phone, special_symbols))
  {
    u1 = remove_symbols(u1, special_symbols);
    u1_is_phone = true;
  }

  if (u.is_phone(looks_like_phone, special_symbols))
  {
    u2 = remove_symbols(u2, special_symbols);
    u2_is_phone = true;
  }

  if (u1 != u2) return false;

  if (u1_is_phone && u2_is_phone)
  {
    // Both URLs are phone numbers. Do not compare
    // the host-part.
    return true;
  }

  return (get_host() == u.get_host());
}

bool t_url::user_looks_like_phone(const std::string &special_symbols) const
{
  return looks_like_phone(user, special_symbols);
}

bool t_url::is_phone(bool looks_like_phone, const std::string &special_symbols) const
{
  if (scheme == "tel") return true;

  // RFC 3261 19.1.1
  if (user_param == "phone") return true;
  return (looks_like_phone && user_looks_like_phone(special_symbols));
}

std::string t_url::encode() const
{
  if (modified)
  {
    if (!user_url)
    {
      // TODO: machine URL's are currently not used
      return text_format;
    }

    std::string s;

    s = scheme;
    s += ':';

    if (!user.empty())
    {
      s += escape_user_value(user);

      if (!password.empty())
      {
	s += ':';
	s += escape_passwd_value(password);
      }

      s += '@';
    }

    s += host;

    if (port > 0)
    {
      s += ':';
      s += int2str(port);
    }

    if (!transport.empty())
    {
      s += ";transport=";
      s += transport;
    }

    if (!maddr.empty())
    {
      s += ";maddr=";
      s += maddr;
    }

    if (lr)
    {
      s += ";lr";
    }

    if (!user_param.empty())
    {
      s += ";user=";
      s += user_param;
    }

    if (!method.empty())
    {
      s += ";method=";
      s += method;
    }

    if (ttl > 0)
    {
      s += ";ttl=";
      s += int2str(ttl);
    }

    if (!other_params.empty())
    {
      s += other_params;
    }

    if (!headers.empty())
    {
      s += "?";
      s += headers;
    }

    return s;
  }
  else
  {
    return text_format;
  }
}

std::string t_url::encode_noscheme() const
{
  std::string s = encode();
  std::string::size_type i = s.find(':');

  if (i != std::string::npos && i < s.size())
  {
    s = s.substr(i + 1);
  }

  return s;
}

std::string t_url::encode_no_params_hdrs(bool escape) const
{
  if (!user_url)
  {
    // TODO: machine URL's are currently not used
    return text_format;
  }

  std::string s;

  s = scheme;
  s += ':';

  if (!user.empty())
  {
    if (escape)
    {
      s += escape_user_value(user);
    }
    else
    {
      s += user;
    }

    if (!password.empty())
    {
      s += ':';
      if (escape)
      {
	s += escape_passwd_value(password);
      }
      else
      {
	s += password;
      }
    }

    s += '@';
  }

  s += host;

  if (port > 0)
  {
    s += ':';
    s += int2str(port);
  }

  return s;
}

void t_url::apply_conversion_rules(t_user *user_config)
{
  if (scheme == "tel")
  {
    host = user_config->convert_number(host);
  }
  else
  {
    // Convert user part for all other schemes
    user = user_config->convert_number(user);
  }

  modified = true;
}


t_display_url::t_display_url() = default;

t_display_url::t_display_url(t_url _url, std::string _display)
  : url(std::move(_url)), display(std::move(_display))
{ }

bool t_display_url::is_valid() const
{
  return url.is_valid();
}

std::string t_display_url::encode() const
{
  std::string s;

  if (!display.empty())
  {
    if (must_quote(display)) s += '"';
    s += display;
    if (must_quote(display)) s += '"';
    s += " <";
  }

  s += url.encode();

  if (!display.empty()) s += '>';

  return s;
}
