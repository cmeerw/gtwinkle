/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/**
 * @file
 * Exceptions.
 */
 
#ifndef _EXCEPTIONS_H
#define _EXCEPTIONS_H

#include <stdexcept>

class t_dialog_error
  : std::runtime_error
{
public:
  using std::runtime_error::runtime_error;
};

class t_dialog_already_established
  : t_dialog_error
{
public:
  t_dialog_already_established()
    : t_dialog_error("dialog already established")
  { }
};

class t_wrong_state
  : t_dialog_error
{
public:
  t_wrong_state()
    : t_dialog_error("wrong state")
  { }
};

class empty_list_exception
  : public std::exception
{ };

#endif
