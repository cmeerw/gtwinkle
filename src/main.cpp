/*
  Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include "address_book.h"
#include "call_history.h"
#include "events.h"
#include "line.h"
#include "listener.h"
#include "log.h"
#include "phone.h"
#include "protocol.h"
#include "sender.h"
#include "sys_settings.h"
#include "transaction_mgr.h"
#include "translator.h"
#include "user.h"
#include "userintf.h"
#include "util.h"
#include "dbus/dbus_intf.h"
#include "sockets/connection_table.h"
#include "sockets/socket.h"
#include "threads/thread.h"
#include "utils/mime_database.h"

#include <giomm.h>

#include <algorithm>
#include <deque>
#include <iterator>
#include <memory>
#include <system_error>

using namespace utils;

// Initialize the random generator before objects of other classes are
// created. Initializing just from the main function is too late.
const bool rand_initialized {(srand(time(nullptr)), true)};

// Indicates if application is ending (because user pressed Quit)
bool end_app = false;

// top-level settings
Glib::RefPtr<Gio::Settings> settings;

// Language translator
std::unique_ptr<t_translator> translator;

// IP address on which the phone is running
net::ip::address user_host;

// Local host name
std::string local_hostname;

// SIP UDP socket for sending and receiving signaling
std::unique_ptr<t_socket_udp> sip_socket;
std::unique_ptr<t_socket_udp> sip_socket_v6;

// SIP TCP socket for sending and receiving signaling
std::unique_ptr<t_socket_tcp> sip_socket_tcp;
std::unique_ptr<t_socket_tcp> sip_socket_tcp_v6;

// SIP connection table for connection oriented transport
std::unique_ptr<t_connection_table> connection_table;

// Event queue that is handled by the transaction manager thread
// The following threads write to this queue
// - UDP listener
// - transaction layer
// - timekeeper
std::unique_ptr<t_event_queue> evq_trans_mgr;

// Event queue that is handled by the sender thread
// The following threads write to this queue:
// - phone UAS
// - phone UAC
// - transaction manager
std::unique_ptr<t_event_queue> evq_sender;

// Event queue that is handled by the transaction layer thread
// The following threads write to this queue
// - transaction manager
// - timekeeper
std::unique_ptr<t_event_queue> evq_trans_layer;

// Event queue that is handled by the phone timekeeper thread
// The following threads write into this queue
// - phone UAS
// - phone UAC
// - transaction manager
std::unique_ptr<t_event_queue> evq_timekeeper;

// The timekeeper
std::unique_ptr<t_timekeeper> timekeeper;

// The transaction manager
std::unique_ptr<t_transaction_mgr> transaction_mgr;

// The phone
std::unique_ptr<t_phone> phone;

// User interface
std::unique_ptr<t_userintf> ui;

// Log file
std::unique_ptr<t_log> log_file;

// System config
std::unique_ptr<t_sys_settings> sys_config;

// Call history
std::unique_ptr<t_call_history> call_history;

// Local address book
std::unique_ptr<t_address_book> ab_local;

// Mime database
std::unique_ptr<t_mime_database> mime_database;

// If a port number is passed by the user on the command line, then
// that port number overrides the port from the system settings.
unsigned short g_override_sip_port = 0;
unsigned short g_override_rtp_port = 0;


int main(int argc, char *argv[])
{
  std::string error_msg;

  Gio::init();

  // Dedicated thread will catch SIGALRM, SIGINT, SIGTERM, SIGCHLD signals,
  // therefore all threads must block these signals. Block now, then all
  // created threads will inherit the signal mask.
  // In LinuxThreads the sigwait does not work very well, so
  // in LinuxThreads a signal handler is used instead.
  sigset_t sigset;
  sigemptyset(&sigset);
  sigaddset(&sigset, SIGALRM);
  sigaddset(&sigset, SIGINT);
  sigaddset(&sigset, SIGTERM);
  sigaddset(&sigset, SIGCHLD);
  sigprocmask(SIG_BLOCK, &sigset, nullptr);

  // Ignore SIGPIPE so read from broken sockets will not cause
  // the process to terminate.
  (void)signal(SIGPIPE, SIG_IGN);

  settings = Gio::Settings::create("net.cmeerw.GTwinkle");

  translator = std::make_unique<t_translator>();
  connection_table = std::make_unique<t_connection_table>();
  evq_trans_mgr = std::make_unique<t_event_queue>();
  evq_sender = std::make_unique<t_event_queue>();
  evq_trans_layer = std::make_unique<t_event_queue>();
  evq_timekeeper = std::make_unique<t_event_queue>();
  timekeeper = std::make_unique<t_timekeeper>();
  transaction_mgr = std::make_unique<t_transaction_mgr>();
  phone = std::make_unique<t_phone>();

  sys_config = std::make_unique<t_sys_settings>();
  ui = std::make_unique<t_userintf>(*phone);

  // Check requirements on environment
  if (!sys_config->check_environment(error_msg))
  {
    // Environment is not good
    ui->cb_show_msg(error_msg, MSG_CRITICAL);
    exit(1);
  }

  local_hostname = get_local_hostname();

  // Create a lock file to guarantee that the application
  // runs only once.
  bool already_running;
  if (!sys_config->create_lock_file(false, error_msg, already_running))
  {
    ui->cb_show_msg(error_msg, MSG_CRITICAL);
    exit(1);
  }

  log_file = std::make_unique<t_log>(settings->get_child("log"));

  call_history = std::make_unique<t_call_history>();

  // Activate users
  auto const &accounts_settings(settings->get_child("accounts"));
  const auto &account_list(accounts_settings->get_string_array("account-list"));

  for (auto const & account_name : account_list)
  {
    std::unique_ptr<t_user> user_config(new t_user(*phone, accounts_settings,
						   account_name));

    t_user *dup_user(nullptr);
    if (!phone->add_phone_user(*user_config, dup_user))
    {
      error_msg = "The following profiles are both for user ";
      error_msg += user_config->get_name();
      error_msg += '@';
      error_msg += user_config->get_domain();
      error_msg += ":\n\n";
      error_msg += user_config->get_profile_name();
      error_msg += "\n";
      error_msg += dup_user->get_profile_name();
      error_msg += "\n\n";
      error_msg += "You can only run multiple profiles ";
      error_msg += "for different users.";
      ui->cb_show_msg(error_msg, MSG_CRITICAL);
      sys_config->delete_lock_file();
      exit(1);
    }
  }

  // Read call history
  if (!call_history->load(error_msg))
  {
    log_file->write_report(error_msg, "::main", LOG_NORMAL, LOG_WARNING);
  }

  // Create local address book
  ab_local = std::make_unique<t_address_book>();

  // Read local address book
  if (!ab_local->load(error_msg))
  {
    log_file->write_report(error_msg, "::main", LOG_NORMAL, LOG_WARNING);
    ui->cb_show_msg(error_msg, MSG_WARNING);
  }

  // Create mime database
  mime_database = std::make_unique<t_mime_database>();
  if (!mime_database->load(error_msg))
  {
    log_file->write_report(error_msg, "::main", LOG_NORMAL, LOG_WARNING);
  }

  auto const &network_settings(settings->get_child("network"));
  const unsigned short sip_port(network_settings->get_uint("sip-port"));

  // Open UDP socket for SIP signaling
  try
  {
    sip_socket = std::make_unique<t_socket_udp>(net::ip::address::v4_addr);
    sip_socket->bind(net::ip::endpoint(net::ip::v4_address::any(), sip_port));

    if (sip_socket->enable_icmp())
    {
      log_file->write_report("ICMP processing enabled.", "::main");
    }
    else
    {
      log_file->write_report("ICMP processing disabled.", "::main");
    }
  }
  catch (const std::system_error & err)
  {
    std::string msg("Failed to create a UDP socket (SIP) on port ");
    msg += int2str(sip_port);
    msg += "\n";
    msg += err.what();
    log_file->write_report(msg, "::main", LOG_NORMAL, LOG_CRITICAL);
    ui->cb_show_msg(msg, MSG_CRITICAL);
    sys_config->delete_lock_file();
    exit(1);
  }

  // Open UDP socket for SIP signaling
  if (network_settings->get_boolean("use-ipv6"))
  {
    try
    {
      sip_socket_v6 = std::make_unique<t_socket_udp>(net::ip::address::v6_addr);

      int enable = 1;
      sip_socket_v6->setsockopt(IPPROTO_IPV6, IPV6_V6ONLY, &enable, sizeof(enable));

      sip_socket_v6->bind(net::ip::endpoint(net::ip::v6_address::any(),
					    sip_port));

      if (sip_socket_v6->enable_icmp())
      {
	log_file->write_report("ICMP processing enabled.", "::main");
      }
      else
      {
	log_file->write_report("ICMP processing disabled.", "::main");
      }
    }
    catch (const std::system_error & err)
    {
      std::string msg("Failed to create a UDPv6 socket (SIP) on port ");
      msg += int2str(sip_port);
      msg += "\n";
      msg += err.what();
      log_file->write_report(msg, "::main", LOG_NORMAL, LOG_CRITICAL);

      sip_socket_v6.reset();
    }
  }

  // Open TCP socket for SIP signaling
  try
  {
    sip_socket_tcp = std::make_unique<t_socket_tcp>(net::ip::address::v4_addr);

    int enable = 1;
    // Allow server to connect to the socket immediately (disable TIME_WAIT)
    sip_socket_tcp->setsockopt(SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(enable));

    enable = 1;
    // Disable Nagle algorithm
    sip_socket_tcp->setsockopt(IPPROTO_TCP, TCP_NODELAY, &enable, sizeof(enable));

    sip_socket_tcp->bind(net::ip::endpoint(net::ip::v4_address::any(),
					   sip_port));
  }
  catch (const std::system_error & err)
  {
    std::string msg("Failed to create a TCP socket (SIP) on port ");
    msg += int2str(sip_port);
    msg += "\n";
    msg += err.what();
    log_file->write_report(msg, "::main", LOG_NORMAL, LOG_CRITICAL);
    ui->cb_show_msg(msg, MSG_CRITICAL);
    sys_config->delete_lock_file();
    exit(1);
  }

  // Open TCP socket for SIP signaling
  if (network_settings->get_boolean("use-ipv6"))
  {
    try
    {
      sip_socket_tcp_v6 = std::make_unique<t_socket_tcp>(net::ip::address::v6_addr);

      int enable = 1;
      sip_socket_tcp_v6->setsockopt(IPPROTO_IPV6, IPV6_V6ONLY, &enable, sizeof(enable));

      enable = 1;
      // Allow server to connect to the socket immediately (disable TIME_WAIT)
      sip_socket_tcp_v6->setsockopt(SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(enable));

      enable = 1;
      // Disable Nagle algorithm
      sip_socket_tcp_v6->setsockopt(IPPROTO_TCP, TCP_NODELAY, &enable, sizeof(enable));

      sip_socket_tcp_v6->bind(net::ip::endpoint(net::ip::v6_address::any(),
						sip_port));
    }
    catch (const std::system_error & err)
    {
      std::string msg("Failed to create a TCPv6 socket (SIP) on port ");
      msg += int2str(sip_port);
      msg += "\n";
      msg += err.what();
      log_file->write_report(msg, "::main", LOG_NORMAL, LOG_CRITICAL);
      sip_socket_tcp_v6.reset();
    }
  }

  // Discover NAT type if STUN is enabled
  std::vector<std::string> msg_list;
  if (!phone->stun_discover_nat(msg_list))
  {
    std::for_each(msg_list.cbegin(), msg_list.cend(),
		  [] (const std::string &msg) {
		    ui->cb_show_msg(msg, MSG_WARNING);
		  });
  }

  // Create threads
  std::thread thr_sender;
  std::thread thr_tcp_sender;
  std::thread thr_listen_udp;
  std::thread thr_listen_udp_v6;
  std::thread thr_listen_data_tcp;
  std::thread thr_listen_conn_tcp;
  std::thread thr_listen_conn_tcp_v6;
  std::thread thr_conn_timeout_handler;
  std::thread thr_timekeeper;
  std::thread thr_alarm_catcher;
  std::thread thr_sig_catcher;
  std::thread thr_trans_mgr;
  std::thread thr_phone_uas;

  try
  {
    // SIP sender thread
    thr_sender = std::thread(sender_loop,
			     sip_socket.get(), sip_socket_v6.get());

    // SIP TCP sender thread
    thr_tcp_sender = std::thread(tcp_sender_loop);

    // UDP listener thread
    thr_listen_udp = std::thread(listen_udp, std::ref(*sip_socket));
    if (sip_socket_v6)
    {
      thr_listen_udp_v6 = std::thread(listen_udp, std::ref(*sip_socket_v6));
    }

    // TCP data listener thread
    thr_listen_data_tcp = std::thread(listen_for_data_tcp);

    // TCP connection listener thread
    thr_listen_conn_tcp = std::thread(listen_for_conn_requests_tcp,
				      std::ref(*sip_socket_tcp));
    if (sip_socket_tcp_v6)
    {
      thr_listen_conn_tcp_v6 = std::thread(listen_for_conn_requests_tcp,
					   std::ref(*sip_socket_tcp_v6));
    }

    // Connection timeout handler thread
    thr_conn_timeout_handler = std::thread(connection_timeout_main);

    // Timekeeper thread
    thr_timekeeper = std::thread(timekeeper_main);

    // Alarm catcher thread
    thr_alarm_catcher = std::thread(timekeeper_sigwait);

    // Signal catcher thread
    thr_sig_catcher = std::thread(phone_sigwait);

    // Transaction manager thread
    thr_trans_mgr = std::thread(transaction_mgr_main);

    // Phone thread (UAS)
    thr_phone_uas = std::thread(phone_uas_main, std::ref(*phone));
  }
  catch (const std::system_error &)
  {
    std::string msg = "Failed to create threads.";
    log_file->write_report(msg, "::main", LOG_NORMAL, LOG_CRITICAL);
    ui->cb_show_msg(msg, MSG_CRITICAL);
    sys_config->delete_lock_file();
    exit(1);
  }

  // Validate sound devices
  if (!sys_config->exec_audio_validation(true, true, true, error_msg))
  {
    ui->cb_show_msg(error_msg, MSG_WARNING);
  }

  bool const dbus_initialized = dbus_intf_init();

  try
  {
    ui->run();
  }
  catch (std::string const &e)
  {
    std::string msg = "Exception: ";
    msg += e;
    log_file->write_report(msg, "::main", LOG_NORMAL, LOG_CRITICAL);
    ui->cb_show_msg(msg, MSG_CRITICAL);
    sys_config->delete_lock_file();
    exit(1);
  }
  catch (...)
  {
    std::string msg = "Unknown exception";
    log_file->write_report(msg, "::main", LOG_NORMAL, LOG_CRITICAL);
    ui->cb_show_msg(msg, MSG_CRITICAL);
    sys_config->delete_lock_file();
    exit(1);
  }

  // Application is ending
  end_app = true;

  // Kill the threads getting receiving input from the outside world first,
  // so no new inputs come in during termination.
  if (dbus_initialized)
  {
    dbus_intf_destroy();
  }

  if (thr_listen_udp_v6.joinable())
  {
    thread_cancel(thr_listen_udp_v6);
    thr_listen_udp_v6.join();
  }
  thread_cancel(thr_listen_udp);
  thr_listen_udp.join();

  if (thr_listen_conn_tcp_v6.joinable())
  {
    thread_cancel(thr_listen_conn_tcp_v6);
    thr_listen_conn_tcp_v6.join();
  }
  thread_cancel(thr_listen_conn_tcp);
  thr_listen_conn_tcp.join();

  connection_table->cancel_select();
  thr_listen_data_tcp.join();
  thr_conn_timeout_handler.join();
  thr_tcp_sender.join();

  evq_trans_layer->push_quit();
  thr_phone_uas.join();

  evq_trans_mgr->push_quit();
  thr_trans_mgr.join();

  try
  {
    thread_cancel(thr_sig_catcher);
  }
  catch (const std::system_error &)
  {
    // Thread terminated already by itself
  }
  thr_sig_catcher.join();

  thread_cancel(thr_alarm_catcher);
  thr_alarm_catcher.join();

  evq_timekeeper->push_quit();
  thr_timekeeper.join();

  evq_sender->push_quit();
  thr_sender.join();

  sys_config->remove_all_tmp_files();

  mime_database.reset();
  ab_local.reset();
  call_history.reset();

  ui.reset();

  connection_table.reset();

  phone.reset();
  transaction_mgr.reset();
  timekeeper.reset();
  evq_trans_mgr.reset();
  evq_sender.reset();
  evq_trans_layer.reset();
  evq_timekeeper.reset();

  translator.reset();

  log_file.reset();

  sys_config->delete_lock_file();
  sys_config.reset();
}
