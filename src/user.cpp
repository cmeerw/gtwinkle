/*
  Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "user.h"

#include "log.h"
#include "phone.h"
#include "gtwinkle_config.h"
#include "userintf.h"
#include "util.h"
#include "protocol.h"
#include "sys_settings.h"
#include "sdp/sdp.h"
#include "parser/parse_ctrl.h"
#include "parser/request.h"

#include <cassert>
#include <cstdlib>
#include <cstring>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fstream>
#include <iostream>
#include <vector>

#if HAVE_LIBSECRET
#include <libsecret/secret.h>
#endif

namespace
{
#if HAVE_LIBSECRET
  const SecretSchema *get_schema()
  {
    static const SecretSchema schema = {
      "org.freedesktop.Secret.Generic", SECRET_SCHEMA_NONE,
      {
	{ "user", SECRET_SCHEMA_ATTRIBUTE_STRING },
	{ "realm", SECRET_SCHEMA_ATTRIBUTE_STRING },
	{ "domain", SECRET_SCHEMA_ATTRIBUTE_STRING },
	{ "protocol", SECRET_SCHEMA_ATTRIBUTE_STRING },
	{ nullptr, SecretSchemaAttributeType() }
      }
    };

    return &schema;
  }
#endif

  std::string get_password(std::string const &domain,
			   std::string const &auth_name,
			   std::string const &auth_realm,
			   std::string const &config_password)
  {
    if (! config_password.empty())
    {
      return config_password;
    }
#if HAVE_LIBSECRET
    else if (! auth_name.empty() && ! domain.empty())
    {
      GError *error = nullptr;
      gchar *pw = secret_password_lookup_sync(get_schema(),
					      nullptr,
					      &error,
					      "user", auth_name.c_str(),
					      "domain", domain.c_str(),
					      "realm", auth_realm.c_str(),
					      "protocol", "sip",
					      nullptr);
      if (error)
      {
	g_critical("could not connect to Secret Service: %s", error->message);
	g_error_free(error);
	return { };
      }
      else if (pw)
      {
	std::string password = pw;
	secret_password_free(pw);
	return password;
      }
    }
#endif

    return { };
  }
}

/////////////////////////
// class t_user
/////////////////////////

////////////////////
// Private
////////////////////

std::string t_user::expand_filename(const std::string &filename)
{
  std::string f;

  if (filename[0] == '/')
  {
    f = filename;
  }
  else
  {
    f = std::string(DIR_HOME);
    f += "/";
    f += USER_DIR;
    f += "/";
    f += filename;
  }

  return f;
}

bool t_user::parse_num_conversion(const std::string &value,
				  t_number_conversion &c)
{
  std::vector<std::string> l (split_escaped(value, ','));

  if (l.size() != 2)
  {
    // Invalid conversion rule
    return false;
  }

  try
  {
    c.re.assign(l.front());
    c.re_str = l.front();
    c.fmt = l.back();
  }
  catch (const std::regex_error &)
  {
    // Invalid regular expression
    log_file->write_header("t_user::parse_num_conversion",
			   LOG_NORMAL, LOG_WARNING);
    log_file->write_raw("Bad number conversion:\n");
    log_file->write_raw(l.front());
    log_file->write_raw(" --> ");
    log_file->write_raw(l.back());
    log_file->write_endl();
    log_file->write_footer();

    return false;
  }

  return true;
}

bool t_user::set_server_value(t_url &server, const std::string &scheme, const std::string &value)
{
  if (value.empty())
  {
    server.set_url("");
    return false;
  }

  std::string s = scheme + ":" + value;
  server.set_url(s);

  if (!server.is_valid() || !server.get_user().empty())
  {
    std::string err_msg = "Invalid server value: ";
    err_msg += value;
    log_file->write_report(err_msg, "t_user::set_server_value",
			   LOG_NORMAL, LOG_WARNING);
    server.set_url("");
    return false;
  }

  return true;
}


t_user::t_user(class t_phone & phone,
	       std::string account_name,
	       Glib::RefPtr<const Gio::Settings> const & account_settings)
  : t_user(phone, std::move(account_name), account_settings,
	   account_settings->get_child("auth"),
	   account_settings->get_child("server"),
	   account_settings->get_child("audio"),
	   account_settings->get_child("protocol"),
	   account_settings->get_child("transport"),
	   account_settings->get_child("timers"),
	   account_settings->get_child("address-format"),
	   account_settings->get_child("ringtone"),
	   account_settings->get_child("scripts"),
	   account_settings->get_child("security"),
	   account_settings->get_child("mwi"),
	   account_settings->get_child("im"),
	   account_settings->get_child("presence"))
{ }

t_user::t_user(class t_phone & phone,
	       std::string account_name,
	       Glib::RefPtr<const Gio::Settings> const & account_settings,
	       Glib::RefPtr<const Gio::Settings> const & auth_settings,
	       Glib::RefPtr<const Gio::Settings> const & server_settings,
	       Glib::RefPtr<const Gio::Settings> const & audio_settings,
	       Glib::RefPtr<const Gio::Settings> const & protocol_settings,
	       Glib::RefPtr<const Gio::Settings> const & transport_settings,
	       Glib::RefPtr<const Gio::Settings> const & timers_settings,
	       Glib::RefPtr<const Gio::Settings> const & address_format_settings,
	       Glib::RefPtr<const Gio::Settings> const & ringtone_settings,
	       Glib::RefPtr<const Gio::Settings> const & scripts_settings,
	       Glib::RefPtr<const Gio::Settings> const & security_settings,
	       Glib::RefPtr<const Gio::Settings> const & mwi_settings,
	       Glib::RefPtr<const Gio::Settings> const & im_settings,
	       Glib::RefPtr<const Gio::Settings> const & presence_settings)
  : phone(phone), profile_name(std::move(account_name)),
    name(account_settings->get_string("name")),
    domain(account_settings->get_string("domain")),
    display(account_settings->get_string("display")),
    organization(account_settings->get_string("organization")),
    auth_realm(auth_settings->get_string("realm")),
    auth_name(auth_settings->get_string("name")),
    auth_pass(get_password(domain, auth_name, auth_realm,
			   auth_settings->get_string("pass"))),
    use_outbound_proxy(set_server_value(outbound_proxy,
					USER_SCHEME,
					server_settings->get_string("outbound-proxy"))),
    all_requests_to_proxy(server_settings->get_boolean("always-use-proxy")),
    non_resolvable_to_proxy(server_settings->get_boolean("non-resolvable-proxy")),
    use_registrar(set_server_value(registrar,
				   USER_SCHEME,
				   server_settings->get_string("registrar"))),
    registration_time(server_settings->get_uint("registration-time")),
    register_at_startup(server_settings->get_boolean("register-at-startup")),
    reg_add_qvalue(server_settings->get_boolean("reg-add-qvalue")),
    reg_qvalue(server_settings->get_uint("reg-qvalue")),
    ptime(audio_settings->get_uint("ptime")),
    out_obey_far_end_codec_pref(audio_settings->get_boolean("out-far-end-codec-pref")),
    in_obey_far_end_codec_pref(audio_settings->get_boolean("in-far-end-codec-pref")),
    speex_nb_payload_type(audio_settings->get_uint("speex-nb-payload-type")),
    speex_wb_payload_type(audio_settings->get_uint("speex-wb-payload-type")),
    speex_uwb_payload_type(audio_settings->get_uint("speex-uwb-payload-type")),
    speex_dsp_vad(audio_settings->get_boolean("speex-dsp-vad")),
    speex_dsp_agc(audio_settings->get_boolean("speex-dsp-agc")),
    speex_dsp_aec(audio_settings->get_boolean("speex-dsp-aec")),
    speex_dsp_nrd(audio_settings->get_boolean("speex-dsp-nrd")),
    speex_dsp_agc_level(audio_settings->get_uint("speex-dsp-agc-level")),
    speex_bit_rate_type(static_cast<t_bit_rate_type>(audio_settings->get_enum("speex-bitrate-type"))),
    speex_abr_nb(audio_settings->get_uint("speex-abr-nb")),
    speex_abr_wb(audio_settings->get_uint("speex-abr-wb")),
    speex_dtx(audio_settings->get_boolean("speex-dtx")),
    speex_penh(audio_settings->get_boolean("speex-penh")),
    speex_complexity(audio_settings->get_uint("speex-complexity")),
    speex_quality(audio_settings->get_uint("speex-quality")),
    ilbc_payload_type(audio_settings->get_uint("ilbc-payload-type")),
    ilbc_mode(audio_settings->get_uint("ilbc-mode")),
    g726_16_payload_type(audio_settings->get_uint("g726-16-payload-type")),
    g726_24_payload_type(audio_settings->get_uint("g726-24-payload-type")),
    g726_32_payload_type(audio_settings->get_uint("g726-32-payload-type")),
    g726_40_payload_type(audio_settings->get_uint("g726-40-payload-type")),
    g726_packing(static_cast<t_g726_packing>(audio_settings->get_enum("g726-packing"))),
    opus_payload_type(audio_settings->get_uint("opus-payload-type")),
    opus_sample_rate(audio_settings->get_uint("opus-sample-rate")),
    opus_average_bitrate(audio_settings->get_uint("opus-average-bitrate")),
    dtmf_transport(static_cast<t_dtmf_transport>(audio_settings->get_enum("dtmf-transport"))),
    dtmf_payload_type(audio_settings->get_uint("dtmf-payload-type")),
    dtmf_duration(audio_settings->get_uint("dtmf-duration")),
    dtmf_pause(audio_settings->get_uint("dtmf-pause")),
    dtmf_volume(audio_settings->get_uint("dtmf-volume")),
    hold_variant(static_cast<t_hold_variant>(protocol_settings->get_enum("hold-variant"))),
    check_max_forwards(protocol_settings->get_boolean("check-max-forwards")),
    allow_missing_contact_reg(protocol_settings->get_boolean("allow-missing-contact-reg")),
    registration_time_in_contact(protocol_settings->get_boolean("registration-time-in-contact")),
    compact_headers(protocol_settings->get_boolean("compact-headers")),
    encode_multi_values_as_list(protocol_settings->get_boolean("encode-multi-values-as-list")),
    use_domain_in_contact(protocol_settings->get_boolean("use-domain-in-contact")),
    allow_sdp_change(protocol_settings->get_boolean("allow-sdp-change")),
    allow_redirection(protocol_settings->get_boolean("allow-redirection")),
    ask_user_to_redirect(protocol_settings->get_boolean("ask-user-to-redirect")),
    max_redirections(protocol_settings->get_uint("max-redirections")),
    ext_100rel(static_cast<t_ext_support>(protocol_settings->get_enum("ext-100rel"))),
    ext_replaces(protocol_settings->get_boolean("ext-replaces")),
    referee_hold(protocol_settings->get_boolean("referee-hold")),
    referrer_hold(protocol_settings->get_boolean("referrer-hold")),
    allow_refer(protocol_settings->get_boolean("allow-refer")),
    ask_user_to_refer(protocol_settings->get_boolean("ask-user-to-refer")),
    auto_refresh_refer_sub(protocol_settings->get_boolean("auto-refresh-refer-sub")),
    attended_refer_to_aor(protocol_settings->get_boolean("attended-refer-to-aor")),
    allow_transfer_consultation_inprog(protocol_settings->get_boolean("allow-xfer-consult-inprog")),
    send_p_preferred_id(protocol_settings->get_boolean("send-p-preferred-id")),
    sip_transport(static_cast<t_sip_transport>(transport_settings->get_enum("sip-transport"))),
    sip_transport_udp_threshold(transport_settings->get_uint("sip-transport-udp-threshold")),
    nat_public_ip(transport_settings->get_string("nat-public-ip")),
    use_stun(set_server_value(stun_server,
			      USER_SCHEME,
			      transport_settings->get_string("stun-server"))),
    persistent_tcp(transport_settings->get_boolean("persisten-tcp")),
    enable_nat_keepalive(transport_settings->get_boolean("enable-nat-keepalive")),
    timer_noanswer(timers_settings->get_uint("noanswer")),
    timer_nat_keepalive(timers_settings->get_uint("nat-keepalive")),
    timer_tcp_ping(timers_settings->get_uint("tcp-ping")),
    display_useronly_phone(address_format_settings->get_boolean("display-useronly-phone")),
    numerical_user_is_phone(address_format_settings->get_boolean("numerical-user-is-phone")),
    remove_special_phone_symbols(address_format_settings->get_boolean("remove-special-phone-symbols")),
    special_phone_symbols(address_format_settings->get_string("special-phone-symbols")),
    use_tel_uri_for_phone(address_format_settings->get_boolean("use-tel-uri-for-phone")),
    ringtone_file(ringtone_settings->get_string("ringtone-file")),
    ringback_file(ringtone_settings->get_string("ringback-file")),
    script_incoming_call(scripts_settings->get_string("incoming-call")),
    script_in_call_answered(scripts_settings->get_string("in-call-answered")),
    script_in_call_failed(scripts_settings->get_string("in-call-failed")),
    script_outgoing_call(scripts_settings->get_string("outgoing-call")),
    script_out_call_answered(scripts_settings->get_string("out-call-answered")),
    script_out_call_failed(scripts_settings->get_string("out-call-failed")),
    script_local_release(scripts_settings->get_string("local-release")),
    script_remote_release(scripts_settings->get_string("remote-release")),
    zrtp_enabled(security_settings->get_boolean("enabled")),
    zrtp_goclear_warning(security_settings->get_boolean("goclear-warning")),
    zrtp_sdp(security_settings->get_boolean("sdp")),
    zrtp_send_if_supported(security_settings->get_boolean("send-if-supported")),
    mwi_sollicited(mwi_settings->get_boolean("solicited")),
    mwi_user(mwi_settings->get_string("user")),
    mwi_server(mwi_settings->get_string("server")),
    mwi_via_proxy(mwi_settings->get_boolean("via-proxy")),
    mwi_subscription_time(mwi_settings->get_uint("subscription-time")),
    mwi_vm_address(mwi_settings->get_string("vm-address")),
    im_max_sessions(im_settings->get_uint("max-sessions")),
    im_send_iscomposing(im_settings->get_boolean("send-iscomposing")),
    pres_subscription_time(presence_settings->get_uint("subscription-time")),
    pres_publication_time(presence_settings->get_uint("publication-time")),
    pres_publish_startup(presence_settings->get_boolean("publish-startup"))
{
  static_assert(offsetof(t_user, outbound_proxy) < offsetof(t_user, use_outbound_proxy),
		"Need to have outbound-proxy t_url before flag in class");
  static_assert(offsetof(t_user, registrar) < offsetof(t_user, use_registrar),
		"Need to have registrar t_url before flag in class");
  static_assert(offsetof(t_user, stun_server) < offsetof(t_user, use_stun),
		"Need to have stun t_url before flag in class");

  const std::string &aka_op_hex(auth_settings->get_string("aka-op"));
  if (aka_op_hex.length() == 2 * sizeof(auth_aka_op))
  {
    hex2binary(aka_op_hex, auth_aka_op);
  }

  const std::string &aka_amf_hex(auth_settings->get_string("aka-amf"));
  if (aka_amf_hex.length() == 2 * sizeof(auth_aka_amf))
  {
    hex2binary(aka_amf_hex, auth_aka_amf);
  }


  for (const Glib::ustring & codec : audio_settings->get_string_array("codecs"))
  {
    if (codec == "g711a")
    {
      codecs.push_back(CODEC_G711_ALAW);
    }
    else if (codec == "g711u")
    {
      codecs.push_back(CODEC_G711_ULAW);
    }
    else if (codec == "gsm")
    {
      codecs.push_back(CODEC_GSM);
    }
    else if (codec == "g722")
    {
      codecs.push_back(CODEC_G722);
#ifdef HAVE_SPEEX
    }
    else if (codec == "speex-nb")
    {
      codecs.push_back(CODEC_SPEEX_NB);
    }
    else if (codec == "speex-wb")
    {
      codecs.push_back(CODEC_SPEEX_WB);
    }
    else if (codec == "speex-uwb")
    {
      codecs.push_back(CODEC_SPEEX_UWB);
#endif
#ifdef HAVE_ILBC
    }
    else if (codec == "ilbc")
    {
      codecs.push_back(CODEC_ILBC);
#endif
    }
    else if (codec == "g726-16")
    {
      codecs.push_back(CODEC_G726_16);
    }
    else if (codec == "g726-24")
    {
      codecs.push_back(CODEC_G726_24);
    }
    else if (codec == "g726-32")
    {
      codecs.push_back(CODEC_G726_32);
    }
    else if (codec == "g726-40")
    {
      codecs.push_back(CODEC_G726_40);
#ifdef HAVE_OPUS
    }
    else if (codec == "opus")
    {
      codecs.push_back(CODEC_OPUS);
#endif
    }
    else
    {
      std::string msg;
      msg = "Invalid codec: ";
      msg += codec;
      log_file->write_report(msg,
			     "t_user::read_config",
			     LOG_NORMAL, LOG_WARNING);
    }
  }

  // Set parser options
  t_parser::check_max_forwards = check_max_forwards;
  t_parser::compact_headers = compact_headers;
  t_parser::multi_values_as_list = encode_multi_values_as_list;
}


////////////////////
// Public
////////////////////

t_user::t_user(t_phone &_phone,
    Glib::RefPtr<const Gio::Settings> const & accounts_settings,
    const std::string &account_name)
  : t_user(_phone, account_name,
      Gio::Settings::create("net.cmeerw.GTwinkle.account",
	  "/net/cmeerw/gtwinkle/accounts/" + account_name + "/"))
{ }

t_user::t_user(const t_user &) = default;

t_user::t_user(t_user &&) = default;

std::unique_ptr<t_user> t_user::copy() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);

  std::unique_ptr<t_user> u(new t_user(*this));
  return u;
}

std::string t_user::get_name() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return name;
}

std::string t_user::get_domain() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return domain;
}

std::string t_user::get_display(bool anonymous) const
{
  if (anonymous) return ANONYMOUS_DISPLAY;

  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return display;
}

std::string t_user::get_organization() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return organization;
}

std::string t_user::get_auth_realm() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return auth_realm;
}

std::string t_user::get_auth_name() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return auth_name;
}

std::string t_user::get_auth_pass() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return auth_pass;
}

void t_user::get_auth_aka_op(uint8 *aka_op) const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  memcpy(aka_op, auth_aka_op, AKA_OPLEN);
}

void t_user::get_auth_aka_amf(uint8 *aka_amf) const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  memcpy(aka_amf, auth_aka_amf, AKA_AMFLEN);
}

bool t_user::get_use_outbound_proxy() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return use_outbound_proxy;
}

t_url t_user::get_outbound_proxy() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return outbound_proxy;
}

bool t_user::get_all_requests_to_proxy() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return all_requests_to_proxy;
}

bool t_user::get_non_resolvable_to_proxy() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return non_resolvable_to_proxy;
}

bool t_user::get_use_registrar() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return use_registrar;
}

t_url t_user::get_registrar() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return registrar;
}

unsigned long t_user::get_registration_time() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return registration_time;
}

bool t_user::get_register_at_startup() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return register_at_startup;
}

bool t_user::get_reg_add_qvalue() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return reg_add_qvalue;
}

float t_user::get_reg_qvalue() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return reg_qvalue;
}

std::vector<t_audio_codec> t_user::get_codecs() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return codecs;
}

unsigned short t_user::get_ptime() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return ptime;
}

bool t_user::get_out_obey_far_end_codec_pref() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return out_obey_far_end_codec_pref;
}

bool t_user::get_in_obey_far_end_codec_pref() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return in_obey_far_end_codec_pref;
}

unsigned short t_user::get_speex_nb_payload_type() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return speex_nb_payload_type;
}

unsigned short t_user::get_speex_wb_payload_type() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return speex_wb_payload_type;
}

unsigned short t_user::get_speex_uwb_payload_type() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return speex_uwb_payload_type;
}

t_bit_rate_type t_user::get_speex_bit_rate_type() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return speex_bit_rate_type;
}

int t_user::get_speex_abr_nb() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return speex_abr_nb;
}

int t_user::get_speex_abr_wb() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return speex_abr_wb;
}

bool t_user::get_speex_dtx() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return speex_dtx;
}

bool t_user::get_speex_penh() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return speex_penh;
}

unsigned short t_user::get_speex_quality() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return speex_quality;
}

unsigned short t_user::get_speex_complexity() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return speex_complexity;
}

bool t_user::get_speex_dsp_vad() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return speex_dsp_vad;
}

bool t_user::get_speex_dsp_agc() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return speex_dsp_agc;
}

unsigned short t_user::get_speex_dsp_agc_level() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return speex_dsp_agc_level;
}

bool t_user::get_speex_dsp_aec() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return speex_dsp_aec;
}

bool t_user::get_speex_dsp_nrd() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return speex_dsp_nrd;
}

unsigned short t_user::get_ilbc_payload_type() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return ilbc_payload_type;
}

unsigned short t_user::get_ilbc_mode() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return ilbc_mode;
}

unsigned short t_user::get_g726_16_payload_type() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return g726_16_payload_type;
}

unsigned short t_user::get_g726_24_payload_type() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return g726_24_payload_type;
}

unsigned short t_user::get_g726_32_payload_type() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return g726_32_payload_type;
}

unsigned short t_user::get_g726_40_payload_type() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return g726_40_payload_type;
}

t_g726_packing t_user::get_g726_packing() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return g726_packing;
}

unsigned short t_user::get_opus_payload_type() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return opus_payload_type;
}

unsigned short t_user::get_opus_sample_rate() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return opus_sample_rate;
}

unsigned int t_user::get_opus_average_bitrate() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return opus_average_bitrate;
}

t_dtmf_transport t_user::get_dtmf_transport() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return dtmf_transport;
}

unsigned short t_user::get_dtmf_payload_type() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return dtmf_payload_type;
}

unsigned short t_user::get_dtmf_duration() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return dtmf_duration;
}

unsigned short t_user::get_dtmf_pause() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return dtmf_pause;
}

unsigned short t_user::get_dtmf_volume() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return dtmf_volume;
}

t_hold_variant t_user::get_hold_variant() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return hold_variant;
}

bool t_user::get_check_max_forwards() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return check_max_forwards;
}

bool t_user::get_allow_missing_contact_reg() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return allow_missing_contact_reg;
}

bool t_user::get_registration_time_in_contact() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return registration_time_in_contact;
}

bool t_user::get_compact_headers() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return compact_headers;
}

bool t_user::get_encode_multi_values_as_list() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return encode_multi_values_as_list;
}

bool t_user::get_use_domain_in_contact() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return use_domain_in_contact;
}

bool t_user::get_allow_sdp_change() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return allow_sdp_change;
}

bool t_user::get_allow_redirection() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return allow_redirection;
}

bool t_user::get_ask_user_to_redirect() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return ask_user_to_redirect;
}

unsigned short t_user::get_max_redirections() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return max_redirections;
}

t_ext_support t_user::get_ext_100rel() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return ext_100rel;
}

bool t_user::get_ext_replaces() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return ext_replaces;
}

bool t_user::get_referee_hold() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return referee_hold;
}

bool t_user::get_referrer_hold() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return referrer_hold;
}

bool t_user::get_allow_refer() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return allow_refer;
}

bool t_user::get_ask_user_to_refer() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return ask_user_to_refer;
}

bool t_user::get_auto_refresh_refer_sub() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return auto_refresh_refer_sub;
}

bool t_user::get_attended_refer_to_aor() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return attended_refer_to_aor;
}

bool t_user::get_allow_transfer_consultation_inprog() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return allow_transfer_consultation_inprog;
}

bool t_user::get_send_p_preferred_id() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return send_p_preferred_id;
}

t_sip_transport t_user::get_sip_transport() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return sip_transport;
}

unsigned short t_user::get_sip_transport_udp_threshold() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return sip_transport_udp_threshold;
}

bool t_user::get_use_nat_public_ip() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return !nat_public_ip.empty();
}

std::string t_user::get_nat_public_ip() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return nat_public_ip;
}

bool t_user::get_use_stun() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return use_stun;
}

t_url t_user::get_stun_server() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return stun_server;
}

bool t_user::get_persistent_tcp() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return persistent_tcp;
}

bool t_user::get_enable_nat_keepalive() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return enable_nat_keepalive;
}

unsigned short t_user::get_timer_noanswer() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return timer_noanswer;
}

unsigned short t_user::get_timer_nat_keepalive() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return timer_nat_keepalive;
}

unsigned short t_user::get_timer_tcp_ping() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return timer_tcp_ping;
}

bool t_user::get_display_useronly_phone() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return display_useronly_phone;
}

bool t_user::get_numerical_user_is_phone() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return numerical_user_is_phone;
}

bool t_user::get_remove_special_phone_symbols() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return remove_special_phone_symbols;
}

std::string t_user::get_special_phone_symbols() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return special_phone_symbols;
}

bool t_user::get_use_tel_uri_for_phone() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return use_tel_uri_for_phone;
}

std::string t_user::get_ringtone_file() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return ringtone_file;
}

std::string t_user::get_ringback_file() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return ringback_file;
}

std::string t_user::get_script_incoming_call() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return script_incoming_call;
}

std::string t_user::get_script_in_call_answered() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return script_in_call_answered;
}

std::string t_user::get_script_in_call_failed() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return script_in_call_failed;
}

std::string t_user::get_script_outgoing_call() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return script_outgoing_call;
}

std::string t_user::get_script_out_call_answered() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return script_out_call_answered;
}

std::string t_user::get_script_out_call_failed() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return script_out_call_failed;
}

std::string t_user::get_script_local_release() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return script_local_release;
}

std::string t_user::get_script_remote_release() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return script_remote_release;
}

std::vector<t_number_conversion> t_user::get_number_conversions() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return number_conversions;
}

bool t_user::get_zrtp_enabled() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return zrtp_enabled;
}

bool t_user::get_zrtp_goclear_warning() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return zrtp_goclear_warning;
}

bool t_user::get_zrtp_sdp() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return zrtp_sdp;
}

bool t_user::get_zrtp_send_if_supported() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return zrtp_send_if_supported;
}

bool t_user::get_mwi_sollicited() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return mwi_sollicited;
}

std::string t_user::get_mwi_user() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return mwi_user;
}

t_url t_user::get_mwi_server() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return mwi_server;
}

bool t_user::get_mwi_via_proxy() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return mwi_via_proxy;
}

unsigned long t_user::get_mwi_subscription_time() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return mwi_subscription_time;
}

std::string t_user::get_mwi_vm_address() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return mwi_vm_address;
}

unsigned short t_user::get_im_max_sessions() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return im_max_sessions;
}

bool t_user::get_im_send_iscomposing() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return im_send_iscomposing;
}

unsigned long t_user::get_pres_subscription_time() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return pres_subscription_time;
}

unsigned long t_user::get_pres_publication_time() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return pres_publication_time;
}

bool t_user::get_pres_publish_startup() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return pres_publish_startup;
}


void t_user::set_name(const std::string &_name)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  name = _name;
}

void t_user::set_domain(const std::string &_domain)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  domain = _domain;
}

void t_user::set_display(const std::string &_display)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  display = _display;
}

void t_user::set_organization(const std::string &_organization)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  organization = _organization;
}

void t_user::set_auth_realm(const std::string &realm)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  auth_realm = realm;
}

void t_user::set_auth_name(const std::string &name)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  auth_name = name;
}

void t_user::set_auth_pass(const std::string &pass)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  auth_pass = pass;
}

void t_user::set_auth_aka_op(const uint8 *aka_op)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  memcpy(auth_aka_op, aka_op, AKA_OPLEN);
}

void t_user::set_auth_aka_amf(const uint8 *aka_amf)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  memcpy(auth_aka_amf, aka_amf, AKA_AMFLEN);
}

void t_user::set_use_outbound_proxy(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  use_outbound_proxy = b;
}

void t_user::set_outbound_proxy(const t_url &url)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  outbound_proxy = url;
}

void t_user::set_all_requests_to_proxy(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  all_requests_to_proxy = b;
}

void t_user::set_non_resolvable_to_proxy(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  non_resolvable_to_proxy = b;
}

void t_user::set_use_registrar(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  use_registrar = b;
}

void t_user::set_registrar(const t_url &url)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  registrar = url;
}

void t_user::set_registration_time(const unsigned long time)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  registration_time = time;
}

void t_user::set_register_at_startup(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  register_at_startup = b;
}

void t_user::set_reg_add_qvalue(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  reg_add_qvalue = b;
}

void t_user::set_reg_qvalue(float q)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  reg_qvalue = q;
}

void t_user::set_codecs(const std::vector<t_audio_codec> &_codecs)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  codecs = _codecs;
}

void t_user::set_ptime(unsigned short _ptime)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  ptime = _ptime;
}

void t_user::set_out_obey_far_end_codec_pref(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  out_obey_far_end_codec_pref = b;
}

void t_user::set_in_obey_far_end_codec_pref(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  in_obey_far_end_codec_pref = b;
}

void t_user::set_speex_nb_payload_type(unsigned short payload_type)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  speex_nb_payload_type = payload_type;
}

void t_user::set_speex_wb_payload_type(unsigned short payload_type)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  speex_wb_payload_type = payload_type;
}

void t_user::set_speex_uwb_payload_type(unsigned short payload_type)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  speex_uwb_payload_type = payload_type;
}

void t_user::set_speex_bit_rate_type(t_bit_rate_type bit_rate_type)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  speex_bit_rate_type = bit_rate_type;
}

void t_user::set_speex_abr_nb(int abr)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  speex_abr_nb = abr;
}

void t_user::set_speex_abr_wb(int abr)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  speex_abr_wb = abr;
}

void t_user::set_speex_dtx(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  speex_dtx = b;
}

void t_user::set_speex_penh(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  speex_penh = b;
}

void t_user::set_speex_quality(unsigned short quality)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  speex_quality = quality;
}

void t_user::set_speex_complexity(unsigned short complexity)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  speex_complexity = complexity;
}

void t_user::set_speex_dsp_vad(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  speex_dsp_vad = b;
}

void t_user::set_speex_dsp_agc(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  speex_dsp_agc = b;
}

void t_user::set_speex_dsp_agc_level(unsigned short level)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  speex_dsp_agc_level = level;
}

void t_user::set_speex_dsp_aec(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  speex_dsp_aec = b;
}

void t_user::set_speex_dsp_nrd(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  speex_dsp_nrd = b;
}

void t_user::set_ilbc_payload_type(unsigned short payload_type)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  ilbc_payload_type = payload_type;
}

void t_user::set_ilbc_mode(unsigned short mode)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  ilbc_mode = mode;
}

void t_user::set_g726_16_payload_type(unsigned short payload_type)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  g726_16_payload_type = payload_type;
}

void t_user::set_g726_24_payload_type(unsigned short payload_type)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  g726_24_payload_type = payload_type;
}

void t_user::set_g726_32_payload_type(unsigned short payload_type)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  g726_32_payload_type = payload_type;
}

void t_user::set_g726_40_payload_type(unsigned short payload_type)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  g726_40_payload_type = payload_type;
}

void t_user::set_g726_packing(t_g726_packing packing)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  g726_packing = packing;
}

void t_user::set_opus_payload_type(unsigned short payload_type)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  opus_payload_type = payload_type;
}

void t_user::set_opus_sample_rate(unsigned short sample_rate)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  opus_sample_rate = sample_rate;
}

void t_user::set_opus_average_bitrate(unsigned int average_bitrate)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  opus_average_bitrate = average_bitrate;
}

void t_user::set_dtmf_transport(t_dtmf_transport _dtmf_transport)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  dtmf_transport = _dtmf_transport;
}

void t_user::set_dtmf_payload_type(unsigned short payload_type)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  dtmf_payload_type = payload_type;
}

void t_user::set_dtmf_duration(unsigned short duration)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  dtmf_duration = duration;
}

void t_user::set_dtmf_pause(unsigned short pause)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  dtmf_pause = pause;
}

void t_user::set_dtmf_volume(unsigned short volume)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  dtmf_volume = volume;
}

void t_user::set_hold_variant(t_hold_variant _hold_variant)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  hold_variant = _hold_variant;
}

void t_user::set_check_max_forwards(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  check_max_forwards = b;
}

void t_user::set_allow_missing_contact_reg(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  allow_missing_contact_reg = b;
}

void t_user::set_registration_time_in_contact(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  registration_time_in_contact = b;
}

void t_user::set_compact_headers(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  compact_headers = b;
}

void t_user::set_encode_multi_values_as_list(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  encode_multi_values_as_list = b;
}

void t_user::set_use_domain_in_contact(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  use_domain_in_contact = b;
}

void t_user::set_allow_sdp_change(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  allow_sdp_change = b;
}

void t_user::set_allow_redirection(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  allow_redirection = b;
}

void t_user::set_ask_user_to_redirect(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  ask_user_to_redirect = b;
}

void t_user::set_max_redirections(unsigned short _max_redirections)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  max_redirections = _max_redirections;
}

void t_user::set_ext_100rel(t_ext_support ext_support)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  ext_100rel = ext_support;
}

void t_user::set_ext_replaces(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  ext_replaces = b;
}

void t_user::set_referee_hold(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  referee_hold = b;
}

void t_user::set_referrer_hold(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  referrer_hold = b;
}

void t_user::set_allow_refer(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  allow_refer = b;
}

void t_user::set_ask_user_to_refer(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  ask_user_to_refer = b;
}

void t_user::set_auto_refresh_refer_sub(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  auto_refresh_refer_sub = b;
}

void t_user::set_attended_refer_to_aor(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  attended_refer_to_aor = b;
}

void t_user::set_allow_transfer_consultation_inprog(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  allow_transfer_consultation_inprog = b;
}

void t_user::set_send_p_preferred_id(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  send_p_preferred_id = b;
}

void t_user::set_sip_transport(t_sip_transport transport)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  sip_transport = transport;
}

void t_user::set_sip_transport_udp_threshold(unsigned short threshold)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  sip_transport_udp_threshold = threshold;
}

void t_user::set_nat_public_ip(const std::string &public_ip)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  nat_public_ip = public_ip;
}

void t_user::set_use_stun(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  use_stun = b;
}

void t_user::set_stun_server(const t_url &url)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  stun_server = url;
}

void t_user::set_persistent_tcp(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  persistent_tcp = b;
}

void t_user::set_enable_nat_keepalive(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  enable_nat_keepalive = b;
}

void t_user::set_timer_noanswer(unsigned short timer)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  timer_noanswer = timer;
}

void t_user::set_timer_nat_keepalive(unsigned short timer)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  timer_nat_keepalive = timer;
}

void t_user::set_timer_tcp_ping(unsigned short timer)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  timer_tcp_ping = timer;
}

void t_user::set_display_useronly_phone(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  display_useronly_phone = b;
}

void t_user::set_numerical_user_is_phone(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  numerical_user_is_phone = b;
}

void t_user::set_remove_special_phone_symbols(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  remove_special_phone_symbols = b;
}

void t_user::set_special_phone_symbols(const std::string &symbols)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  special_phone_symbols = symbols;
}

void t_user::set_use_tel_uri_for_phone(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  use_tel_uri_for_phone = b;
}

void t_user::set_ringtone_file(const std::string &file)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  ringtone_file = file;
}

void t_user::set_ringback_file(const std::string &file)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  ringback_file = file;
}

void t_user::set_script_incoming_call(const std::string &script)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  script_incoming_call = script;
}

void t_user::set_script_in_call_answered(const std::string &script)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  script_in_call_answered = script;
}

void t_user::set_script_in_call_failed(const std::string &script)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  script_in_call_failed = script;
}

void t_user::set_script_outgoing_call(const std::string &script)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  script_outgoing_call = script;
}

void t_user::set_script_out_call_answered(const std::string &script)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  script_out_call_answered = script;
}

void t_user::set_script_out_call_failed(const std::string &script)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  script_out_call_failed = script;
}

void t_user::set_script_local_release(const std::string &script)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  script_local_release = script;
}

void t_user::set_script_remote_release(const std::string &script)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  script_remote_release = script;
}

void t_user::set_number_conversions(const std::vector<t_number_conversion> &l)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  number_conversions = l;
}

void t_user::set_zrtp_enabled(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  zrtp_enabled = b;
}

void t_user::set_zrtp_goclear_warning(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  zrtp_goclear_warning = b;
}

void t_user::set_zrtp_sdp(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  zrtp_sdp = b;
}

void t_user::set_zrtp_send_if_supported(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  zrtp_send_if_supported = b;
}

void t_user::set_mwi_sollicited(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  mwi_sollicited = b;
}

void t_user::set_mwi_user(const std::string &user)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  mwi_user = user;
}

void t_user::set_mwi_server(const t_url &url)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  mwi_server = url;
}

void t_user::set_mwi_via_proxy(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  mwi_via_proxy = b;
}

void t_user::set_mwi_subscription_time(unsigned long t)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  mwi_subscription_time = t;
}

void t_user::set_mwi_vm_address(const std::string &address)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  mwi_vm_address = address;
}

void t_user::set_im_max_sessions(unsigned short max_sessions)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  im_max_sessions = max_sessions;
}

void t_user::set_im_send_iscomposing(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  im_send_iscomposing = b;
}

void t_user::set_pres_subscription_time(unsigned long t)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  pres_subscription_time = t;
}

void t_user::set_pres_publication_time(unsigned long t)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  pres_publication_time = t;
}

void t_user::set_pres_publish_startup(bool b)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  pres_publish_startup = b;
}

std::string t_user::get_profile_name() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  return profile_name;
}

std::string t_user::get_contact_name() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);

  std::string s = name;

  // Some broken proxies expect the contact name to be the same
  // as the SIP user name.
  if (!use_domain_in_contact)
  {
    return s;
  }

  // Create a unique contact name from the user name and domain:
  //
  //   username_domain, where all dots in domain are replace
  //
  // This way it is possible to activate 2 profiles that have the
  // same username, but different domains, e.g.
  //
  //   michel@domainA
  //   michel@domainB

  s += '_';

  // Cut of port and/or uri-parameters if present in domain
  std::string::size_type i = domain.find_first_of(":;");
  if (i != std::string::npos)
  {
    // Some broken SIP proxies think that their own address appears
    // in the contact header when they see the domain in the user part.
    // By replacing the dots with underscores Twinkle interoperates
    // with those proxies (yuck).
    s += replace_char(domain.substr(0, i), '.', '_');
  }
  else
  {
    s += replace_char(domain, '.', '_');
  }

  return s;
}

std::string t_user::get_display_uri() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_user);
  std::string s;

  s = display;
  if (!s.empty()) s += ' ';
  s += '<';
  s += USER_SCHEME;
  s += ':';
  s += name;
  s += '@';
  s += domain;
  s += '>';

  return s;
}

bool t_user::check_required_ext(t_request &r, std::vector<std::string> &unsupported) const
{
  bool all_supported = true;

  std::lock_guard<std::recursive_mutex> guard(mtx_user);

  unsupported.clear();
  if (!r.hdr_require.is_populated())
  {
    return true;
  }

  for (const std::string & feature : r.hdr_require.features)
  {
    if (feature == EXT_100REL)
    {
      if (ext_100rel != EXT_DISABLED) continue;
    }
    else if (feature == EXT_REPLACES)
    {
      if (ext_replaces) continue;
    }
    else if (feature == EXT_NOREFERSUB)
    {
      continue;
    }

    // Extension is not supported
    unsupported.push_back(feature);
    all_supported = false;
  }

  return all_supported;
}

std::string t_user::create_user_contact(bool anonymous, net::ip::address const &auto_ip)
{
  std::string s;
  std::lock_guard<std::recursive_mutex> guard(mtx_user);

  s = USER_SCHEME;
  s += ':';

  if (!anonymous)
  {
    s += t_url::escape_user_value(get_contact_name());
    s += '@';
  }

  s += h_ip2str(USER_HOST(*this, auto_ip));

  if (PUBLIC_SIP_PORT(*this) != get_default_port(USER_SCHEME))
  {
    s += ':';
    s += int2str(PUBLIC_SIP_PORT(*this));
  }

  if (phone.use_stun(*this))
  {
    // The port discovered via STUN can only be used for UDP.
    s += ";transport=udp";
  }
  else
  {
    // Add transport parameter if a single transport is provisioned only.
    switch (sip_transport)
    {
    case SIP_TRANS_UDP:
     s += ";transport=udp";
     break;
    case SIP_TRANS_TCP:
     s += ";transport=tcp";
     break;
    default:
     break;
    }
  }

  if (!anonymous &&
      numerical_user_is_phone && looks_like_phone(name, special_phone_symbols))
  {
    // RFC 3261 19.1.1
    // If the URI contains a telephone number it SHOULD contain
    // the user=phone parameter.
    s += ";user=phone";
  }

  return s;
}

std::string t_user::create_user_uri(bool anonymous)
{
  if (anonymous) return ANONYMOUS_URI;

  std::string s;
  std::lock_guard<std::recursive_mutex> guard(mtx_user);

  s = USER_SCHEME;
  s += ':';
  s += t_url::escape_user_value(name);
  s += '@';
  s += domain;

  if (numerical_user_is_phone && looks_like_phone(name, special_phone_symbols))
  {
    // RFC 3261 19.1.1
    // If the URI contains a telephone number it SHOULD contain
    // the user=phone parameter.
    s += ";user=phone";
  }

  return s;
}

std::string t_user::convert_number(const std::string &number, const std::vector<t_number_conversion> &l) const
{
  for (const t_number_conversion &conv : l)
  {
    std::smatch m;

    try
    {
      if (std::regex_match(number, m, conv.re))
      {
	std::string result(m.format(conv.fmt));

	log_file->write_header("t_user::convert_number",
			       LOG_NORMAL, LOG_DEBUG);
	log_file->write_raw("Apply conversion: ");
	log_file->write_raw(conv.str());
	log_file->write_endl();
	log_file->write_raw(number);
	log_file->write_raw(" converted to ");
	log_file->write_raw(result);
	log_file->write_endl();
	log_file->write_footer();

	return result;
      }
    }
    catch (const std::runtime_error &)
    {
      log_file->write_header("t_user::convert_number",
			     LOG_NORMAL, LOG_WARNING);
      log_file->write_raw("Number conversion rule too complex:\n");
      log_file->write_raw("Number: ");
      log_file->write_raw(number);
      log_file->write_endl();
      log_file->write_raw(conv.str());
      log_file->write_endl();
      log_file->write_footer();

      return number;
    }
  }

  // No match found
  return number;
}

std::string t_user::convert_number(const std::string &number) const
{
  return convert_number(number, number_conversions);
}

t_url t_user::get_mwi_uri() const
{
  t_url u(mwi_server);
  u.set_user(mwi_user);

  return u;
}
