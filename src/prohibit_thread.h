/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _PROHIBIT_THREAD_H
#define _PROHIBIT_THREAD_H

#include "threads/thread.h"

#include <set>
#include <mutex>


// This class implements an interface to keep track of thread id's that are
// prohibited from some actions, e.g. taking a certain lock

template<typename T>
class i_prohibit_thread
{
private:
  static __thread bool prohibited;

public:
  // Operations on the prohibited set of thread id's
  // Add/remove the thread id of the calling thread
  static void add_prohibited_thread()
  {
    prohibited = true;
  }

  static void remove_prohibited_thread()
  {
    prohibited = false;
  }

  // Returns true if the current thread is prohibited
  static bool is_prohibited_thread()
  {
    return prohibited;
  }
};

template<typename T>
__thread bool i_prohibit_thread<T>::prohibited = false;

#endif
