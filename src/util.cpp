/*
  Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <algorithm>
#include <functional>
#include <iostream>
#include <iomanip>
#include <iterator>
#include <random>
#include <sstream>
#include <cassert>
#include <cctype>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <sys/time.h>

#include "util.h"

#include "gtwinkle_config.h"

namespace
{
  const std::string month_abbrv[] = {
    "Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
  };

  const std::string month_full[] = {
    "January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  };

  const std::string day_abbrv[] = {
    "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
  };

  uint8 hexdig2value(char hexdig)
  {
    uint8 val = 0;

    if (hexdig >= '0' && hexdig <= '9')
    {
      val = hexdig - '0';
    }
    else if (hexdig >= 'a' && hexdig <= 'f')
    {
      val = hexdig - 'a' + 10;
    }
    else if (hexdig >= 'A' && hexdig <= 'F')
    {
      val = hexdig - 'A' + 10;
    }

    return val;
  }

  char value2hexdig(uint8 val)
  {
    char c = '0';

    if (val <= 9)
    {
      c = '0' + val;
    }
    else if (val <= 15)
    {
      c = 'a' + val - 10;
    }

    return c;
  }
}

std::string random_token(int length)
{
  thread_local std::mt19937 gen{std::random_device()()};

  std::uniform_int_distribution<unsigned int> dis(0, 25);
  std::ostringstream s;
  std::generate_n(std::ostream_iterator<char>(s), length,
		  [&dis] () { return char(dis(gen) + 97); });

  return s.str();
}

std::string random_hexstr(int length)
{
  thread_local std::mt19937 gen{std::random_device()()};

  std::uniform_int_distribution<unsigned int> dis(0, 15);
  std::ostringstream s;

  for (int i = 0; i < length; i++)
  {
    s << value2hexdig(dis(gen));
  }

  return s.str();
}

std::string float2str(float f, int precision)
{
  std::ostringstream s;
  s.setf(std::ios::fixed, std::ios::floatfield);
  s.precision(precision);
  s << f;

  return s.str();
}

std::string int2str(int i, const char *format)
{
  char buf[32];

  snprintf(buf, 32, format, i);
  return std::string(buf);
}

std::string int2str(int i)
{
  std::ostringstream s;
  s << i;
  return s.str();
}

std::string ulong2str(unsigned long i, const char *format)
{
  char buf[32];

  snprintf(buf, 32, format, i);
  return std::string(buf);
}

std::string ulong2str(unsigned long i)
{
  std::ostringstream s;
  s << i;
  return s.str();
}

std::string ptr2str(void *p)
{
  std::ostringstream s;
  s << p;
  return s.str();
}

std::string bool2str(bool b)
{
  return (b ? "true" : "false");
}

std::string time2str(time_t t, const char *format)
{
  struct tm tm;
  char buf[64];

  localtime_r(&t, &tm);
  strftime(buf, 64, format, &tm);
  return std::string(buf);
}

std::string current_time2str(const char *format)
{
  struct timeval t;

  gettimeofday(&t, nullptr);
  return time2str(t.tv_sec, format);
}

std::string weekday2str(int wkday)
{
  if (wkday >= 0 && wkday <= 6) return day_abbrv[wkday];
  return "XXX";
}

std::string month2str(int month)
{
  if (month >= 0 && month <= 11) return month_abbrv[month];
  return "XXX";
}

int str2month_full(const std::string &month)
{
  for (int i = 0; i < 12; i++)
  {
    if (cmp_nocase(month_full[i], month) == 0)
    {
      return i;
    }
  }

  return 0;
}

std::string duration2str(unsigned long seconds)
{
  const unsigned long h = seconds / 3600;
  const unsigned int remainder = seconds % 3600;
  const unsigned int m = remainder / 60;
  const unsigned int s = remainder % 60;

  std::ostringstream str;

  if (h > 0)
  {
    str << h << "h ";
  }

  if (h > 0 || m > 0)
  {
    str << m << "m ";
  }

  str << s << 's';

  return str.str();
}

std::string timer2str(unsigned long seconds)
{
  const unsigned long h = seconds / 3600;
  const unsigned int remainder = seconds % 3600;
  const unsigned int m = remainder / 60;
  const unsigned int s = remainder % 60;

  std::ostringstream str;
  str << std::setfill('0')
      << std::setw(1) << h << ':'
      << std::setw(2) << m << ':'
      << std::setw(2) << s;
  return str.str();
}

unsigned long hex2int(const std::string &h)
{
  unsigned long u = 0;

  unsigned int shift = 0;
  for (auto iter = h.crbegin(); iter != h.crend(); ++iter)
  {
    u += static_cast<unsigned long>(hexdig2value(*iter)) << shift;
    shift += 4;
  }

  return u;
}

void hex2binary(const std::string &h, uint8 *buf)
{
  uint8 *p = buf;

  bool hi_nibble = true;
  for (auto ch : h)
  {
    if (hi_nibble)
    {
      *p = hexdig2value(ch) << 4;
    }
    else
    {
      *(p++) |= hexdig2value(ch);
    }

    hi_nibble = !hi_nibble;
  }
}

std::string binary2hex(uint8 *buf, unsigned long len)
{
  std::string s;

  for (uint8 *p = buf; p < buf + len; ++p)
  {
    s += value2hexdig((*p >> 4) & 0xf);
    s += value2hexdig(*p & 0xf);
  }

  return s;
}

std::string tolower(const std::string &s)
{
  std::string result;

  for (auto ch : s)
  {
    result += tolower(ch);
  }

  return result;
}

std::string toupper(const std::string &s)
{
  std::string result;

  for (auto ch : s)
  {
    result += toupper(ch);
  }

  return result;
}

std::string rtrim(const std::string &s)
{
  std::string::size_type i;

  i = s.find_last_not_of(' ');
  if (i == std::string::npos) return "";
  if (i == s.size()-1) return s;
  return s.substr(0, i+1);
}

std::string ltrim(const std::string &s)
{
  std::string::size_type i;

  i = s.find_first_not_of(' ');
  if (i == std::string::npos) return "";
  if (i == 0) return s;
  return s.substr(i, s.size()-i+1);
}

std::string trim(const std::string &s)
{
  return ltrim(rtrim(s));
}

std::string padleft(const std::string &s, char c, unsigned long len)
{
  std::string result(c, len);
  result += s;
  return result.substr(result.size() - len);
}

int cmp_nocase(const std::string &s1, const std::string &s2)
{
  std::string::const_iterator i1 = s1.begin();
  std::string::const_iterator i2 = s2.begin();

  while (i1 != s1.end() && i2 != s2.end())
  {
    if (toupper(*i1) != toupper(*i2))
    {
      return (toupper(*i1) < toupper(*i2)) ? -1 : 1;
    }
    ++i1;
    ++i2;
  }

  if (s1.size() == s2.size()) return 0;
  if (s1.size() < s2.size()) return -1;
  return 1;
}

bool must_quote(const std::string &s)
{
  std::string special("()<>@,;:\\\"/[]?={} \t");

  return (s.find_first_of(special) != std::string::npos);
}

std::string escape(const std::string &s, char c)
{
  std::string result;

  for (char ch : s)
  {
    if (ch == '\\' || ch == c)
    {
      result += '\\';
    }

    result += ch;
  }

  return result;
}

std::string unescape(const std::string &s)
{
  std::string result;

  for (std::string::size_type i = 0; i < s.size(); i++)
  {
    if (s[i] == '\\' && i < s.size() - 1)
    {
      i++;
    }

    result += s[i];
  }

  return result;
}

std::string escape_hex(const std::string &s, const std::string &unreserved)
{
  std::string result;

  for (char ch : s)
  {
    if (unreserved.find(ch, 0) != std::string::npos)
    {
      // Unreserved symbol
      result += ch;
    }
    else
    {
      // Reserved symbol
      result += int2str((int)ch, "%%%02x");
    }
  }

  return result;
}
std::string unescape_hex(const std::string &s)
{
  std::string result;

  for (std::string::size_type i = 0; i < s.size(); i++)
  {
    if (s[i] == '%' && i < s.size() - 2 &&
	isxdigit(s[i+1]) && isxdigit(s[i+2]))
    {
      // Escaped hex-value
      std::string hexval = s.substr(i+1, 2);
      result += static_cast<char>(hex2int(hexval));
      i += 2;
    }
    else
    {
      result += s[i];
    }
  }

  return result;
}

std::string replace_char(std::string s, char from, char to)
{
  for (char &ch : s)
  {
    if (ch == from) ch = to;
  }

  return s;
}

std::string replace_first(std::string s, const std::string &from, const std::string &to)
{
  auto iter(s.find(from, 0u));
  if (iter != std::string::npos)
  {
    s.replace(iter, from.size(), to);
  }

  return s;
}

std::vector<std::string> split(const std::string &s, char c)
{
  std::string::size_type i;
  std::string::size_type j = 0;
  std::vector<std::string> l;

  while (true)
  {
    i = s.find(c, j);
    if (i == std::string::npos)
    {
      l.push_back(s.substr(j));
      return l;
    }

    if (i == j)
      l.emplace_back("");
    else
      l.push_back(s.substr(j, i-j));

    j = i+1;

    if (j == s.size())
    {
      l.emplace_back("");
      return l;
    }
  }
}

std::vector<std::string> split(const std::string &s, const std::string& separator)
{
  std::string::size_type i;
  std::string::size_type j = 0;
  std::vector<std::string> l;

  while (true)
  {
    i = s.find(separator, j);
    if (i == std::string::npos)
    {
      l.push_back(s.substr(j));
      return l;
    }

    if (i == j)
      l.emplace_back("");
    else
      l.push_back(s.substr(j, i-j));

    j = i + separator.size();

    if (j == s.size())
    {
      l.emplace_back("");
      return l;
    }
  }
}

std::vector<std::string> split_linebreak(const std::string &s)
{
  if (s.find("\r\n") != std::string::npos)
  {
    return split(s, "\r\n");
  }
  else if (s.find('\r') != std::string::npos)
  {
    return split(s, "\r");
  }

  return split(s, "\n");
}

std::vector<std::string> split_on_first(const std::string &s, char c)
{
  std::vector<std::string> l;
  std::string::size_type i = s.find(c);
  if (i == std::string::npos)
  {
    l.push_back(s);
  }
  else
  {
    if (i == 0)
    {
      l.emplace_back("");
    }
    else
    {
      l.push_back(s.substr(0, i));
    }

    if (i == s.size() - 1)
    {
      l.emplace_back("");
    }
    else
    {
      l.push_back(s.substr(i + 1));
    }
  }

  return l;
}

std::vector<std::string> split_on_last(const std::string &s, char c)
{
  std::vector<std::string> l;
  std::string::size_type i = s.find_last_of(c);
  if (i == std::string::npos)
  {
    l.push_back(s);
  }
  else
  {
    if (i == 0)
    {
      l.emplace_back("");
    }
    else
    {
      l.push_back(s.substr(0, i));
    }

    if (i == s.size() - 1)
    {
      l.emplace_back("");
    }
    else
    {
      l.push_back(s.substr(i + 1));
    }
  }

  return l;
}

std::vector<std::string> split_escaped(const std::string &s, char c)
{
  std::vector<std::string> l;

  std::string::size_type start_pos = 0;
  for (std::string::size_type i = 0; i < s.size(); i++)
  {
    if (s[i] == '\\')
    {
      // Skip escaped character
      if (i < s.size()) i++;
      continue;
    }

    if (s[i] == c)
    {
      l.push_back(unescape(s.substr(start_pos, i - start_pos)));
      start_pos = i + 1;
    }
  }

  if (start_pos < s.size())
  {
    l.push_back(unescape(s.substr(start_pos, s.size() - start_pos)));
  }
  else if (start_pos == s.size())
  {
    l.emplace_back("");
  }

  return l;
}

std::vector<std::string> split_ws(const std::string &s, bool quote_sensitive)
{
  std::vector<std::string> l;
  bool in_quotes = false;

  std::string::size_type start_pos = 0;
  for (std::string::size_type i = 0; i < s.size(); i++ )
  {
    if (quote_sensitive && s[i] == '"')
    {
      in_quotes = !in_quotes;
      continue;
    }

    if (in_quotes) continue;

    if (s[i] == ' ' || s[i] == '\t')
    {
      // Skip consecutive white space
      if (start_pos != i)
      {
	l.push_back(s.substr(start_pos, i - start_pos));
      }
      start_pos = i + 1;
    }
  }

  if (start_pos < s.size())
  {
    l.push_back(s.substr(start_pos, s.size() - start_pos));
  }

  return l;
}

std::string join_strings(const std::vector<std::string> &v, const std::string &separator)
{
  std::string text;
  bool first = true;
  for (auto const & s : v)
  {
    if (! first)
    {
      text += separator;
    }
    else
    {
      first = false;
    }

    text += s;
  }

  return text;
}

std::string unquote(const std::string &s)
{
  if (!s.empty() && s[0] == '"' && s[s.size() - 1] == '"')
  {
    return s.substr(1, s.size() - 2);
  }

  return s;
}

bool is_number(const std::string &s)
{
  if (s.empty()) return false;

  const bool result =
    std::find_if(s.cbegin(), s.cend(),
		 std::not1(std::ptr_fun(isdigit))) == s.cend();
  return result;
}

bool is_ipaddr(const std::string &s)
{
  std::vector<std::string> l = split(s, '.');
  if (l.size() != 4) return false;

  const bool result =
    std::find_if(l.cbegin(), l.cend(),
		 [] (const std::string &p) {
		   return !is_number(p) || (atoi(p.c_str()) > 255);
		 }) != l.cend();
  return result;
}

bool yesno2bool(const std::string &yesno)
{
  return (yesno == "yes");
}
std::string bool2yesno(bool b)
{
  return (b ? "yes" : "no");
}

std::string str2dtmf(const std::string &s)
{
  std::string result;

  std::for_each(s.cbegin(), s.cend(),
		[&result] (char c) {
		  switch (tolower(c))
		  {
		  case '1':
		   result += '1';
		   break;

		  case '2': case 'a': case 'b': case 'c':
		   result += '2';
		   break;

		  case '3': case 'd': case 'e': case 'f':
		   result += '3';
		   break;

		  case '4': case 'g': case 'h': case 'i':
		   result += '4';
		   break;

		  case '5': case 'j': case 'k': case 'l':
		   result += '5';
		   break;

		  case '6': case 'm': case 'n': case 'o':
		   result += '6';
		   break;

		  case '7': case 'p': case 'q': case 'r': case 's':
		   result += '7';
		   break;

		  case '8': case 't': case 'u': case 'v':
		   result += '8';
		   break;

		  case '9': case 'w': case 'x': case 'y': case 'z':
		   result += '9';
		   break;

		  case '0': case ' ':
		   result += '0';
		   break;

		  case '#': case '*':
		   result += c;
		   break;
		  }
		});

  return result;
}

bool looks_like_phone(const std::string &s, const std::string &special_symbols)
{
  const std::string phone_symbols (special_symbols + "0123456789*#+ \t");

  const bool result =
    std::find_if(s.cbegin(), s.cend(),
		 [&phone_symbols] (char c) {
		   return phone_symbols.find(c) == std::string::npos;
		 }) == s.cend();
  return result;
}

std::string remove_symbols(const std::string &s, const std::string &special_symbols)
{
  std::string result;

  std::copy_if(s.cbegin(), s.cend(), std::back_inserter(result),
	       [&special_symbols] (char c) {
		 return special_symbols.find(c) == std::string::npos;
	       });
  return result;
}

std::string remove_white_space(const std::string &s)
{
  std::string result;

  std::copy_if(s.cbegin(), s.cend(), std::back_inserter(result),
	       [] (char c) {
		 return c != ' ' && c != '\t';
	       });
  return result;
}

std::string dotted_truncate(const std::string &s, std::string::size_type len)
{
  if (len >= s.size()) return s;

  return s.substr(0, len) + "...";
}

std::string to_printable(const std::string &s)
{
  std::string result;

  std::transform(s.cbegin(), s.cend(), std::back_inserter(result),
		 [] (char c) {
		   return (isprint(c) || c == '\n' || c == '\r') ? c : '.';
		 });
  return result;
}

std::string get_error_str(int errnum)
{
#if HAVE_STRERROR_R
  char buf[81] = { };
#if STRERROR_R_CHAR_P
  std::string errmsg(strerror_r(errnum, buf, sizeof(buf)-1));
#else
  std::string errmsg;
  if (strerror_r(errnum, buf, sizeof(buf)-1) == 0)
  {
    errmsg = buf;
  }
  else
  {
    errmsg = "unknown error: ";
    errmsg += int2str(errnum);
  }
#endif
#else
  std::string errmsg("strerror_r is not available: ");
  errmsg += int2str(errnum);
#endif
  return errmsg;
}

std::string escape_html(std::string s, bool use_non_breaking_space)
{
  std::string::size_type pos = 0;
  while (true)
  {
    static constexpr char repl_chars[] = { '&', '<', '>', ' ' };
    static_assert(repl_chars[sizeof(repl_chars) - 1] == ' ',
		  "Last character must be a whitespace for non-breaking-space "
		  "logic");

    pos = s.find_first_of(repl_chars, pos,
	sizeof(repl_chars) - (use_non_breaking_space ? 0u : 1u));
    if (pos == std::string::npos)
    {
      break;
    }

    switch (s[pos])
    {
    case '<':
     s.replace(pos, 1, "&lt;", 4);
     pos += 4;
     break;

    case '>':
     s.replace(pos, 1, "&gt;", 4);
     pos += 4;
     break;

    case '&':
     s.replace(pos, 1, "&amp;", 5);
     pos += 5;
     break;

    case ' ':		      // replace with UTF-8 non-breaking space
     s.replace(pos, 1, u8"\u00a0", 2);
     pos += 2;
     break;

    default:
     ++pos;
    }
  }

  return s;
}
