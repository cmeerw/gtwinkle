/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "observer.h"

#include <algorithm>

using namespace patterns;

t_subject::~t_subject()
{
  std::lock_guard<std::recursive_mutex> g(mtx_observers);

  for (auto * const o : observers)
  {
    o->subject_destroyed();
  }
}

void t_subject::attach(t_observer *o)
{
  std::lock_guard<std::recursive_mutex> g(mtx_observers);

  observers.push_back(o);
}

void t_subject::detach(t_observer *o)
{
  std::lock_guard<std::recursive_mutex> g(mtx_observers);

  auto iter = std::find(observers.begin(), observers.end(), o);
  if (iter != observers.end())
  {
    observers.erase(iter);
  }
}

void t_subject::notify() const
{
  std::lock_guard<std::recursive_mutex> g(mtx_observers);

  for (auto * const o : observers)
  {
    o->update();
  }
}
