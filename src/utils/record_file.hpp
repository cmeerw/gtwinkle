/*
    Copyright (C) 2005-2007  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#define COMMENT_SYMBOL	'#'

template< class R >
void t_record_file<R>::split_record(const std::string &record, std::vector<std::string> &v) const
{
  v = split_escaped(record, field_separator);

  for (auto & field : v)
  {
    field = unescape(field);
  }
}

template< class R >
std::string t_record_file<R>::join_fields(const std::vector<std::string> &v) const
{
  std::string s;

  bool first = true;
  for (auto const & field : v)
  {
    if (!first) s += field_separator;
    else first = false;
		
    // Escape comment symbol.
    if (!field.empty() && (field[0] == COMMENT_SYMBOL)) s += '\\';
		
    s += escape(field, field_separator);
  }
	
  return s;
}

template< class R >
void t_record_file<R>::add_record(const R &record)
{
  records.push_back(record);
}

template< class R >
t_record_file<R>::t_record_file()
  : field_separator('|')
{ }

template< class R >
t_record_file<R>::t_record_file(const std::string &_header, char _field_separator, 
				const std::string &_filename)
  : header(_header),
    field_separator(_field_separator),
    filename(_filename)
{ }

template< class R >
void t_record_file<R>::set_header(const std::string &_header)
{
  header = _header;
}

template< class R >
void t_record_file<R>::set_separator(char _separator)
{
  field_separator = _separator;
}

template< class R >
void t_record_file<R>::set_filename(const std::string &_filename)
{
  filename = _filename;
}

template< class R >
std::deque<R> *t_record_file<R>::get_records()
{
  return &records;
}

template< class R >
bool t_record_file<R>::load(std::string &error_msg)
{
  struct stat stat_buf;

  std::lock_guard<std::recursive_mutex> g (mtx_records);
	
  records.clear();
	
  // Check if file exists
  if (filename.empty() || stat(filename.c_str(), &stat_buf) != 0)
  {
    // There is no file.
    return true;
  }
	
  // Open call ile
  std::ifstream file(filename.c_str());
  if (!file)
  {
    error_msg = TRANSLATE("Cannot open file for reading: %1");
    error_msg = replace_first(error_msg, "%1", filename);
    return false;
  }
	
  // Read and parse history file.
  while (!file.eof())
  {
    std::string line;
		
    getline(file, line);

    // Check if read operation succeeded
    if (!file.good() && !file.eof())
    {
      error_msg = TRANSLATE("File system error while reading file %1 .");
      error_msg = replace_first(error_msg, "%1", filename);
      return false;
    }

    line = trim(line);

    // Skip empty lines
    if (line.size() == 0) continue;

    // Skip comment lines
    if (line[0] == COMMENT_SYMBOL) continue;
		
    // Add record. Skip records that cannot be parsed.
    R record;
    std::vector<std::string> v;
    split_record(line, v);
    if (record.populate_from_file_record(v))
    {
      add_record(record);
    }
  }
	
  return true;
}

template< class R >
bool t_record_file<R>::save(std::string &error_msg) const
{	
  if (filename.empty()) return false;

  std::ofstream file;

  {	
    std::lock_guard<std::recursive_mutex> g (mtx_records);
	
    // Open file
    file.open(filename.c_str());
    if (!file)
    {
      error_msg = TRANSLATE("Cannot open file for writing: %1");
      error_msg = replace_first(error_msg, "%1", filename);
      return false;
    }
	
    // Write file header
    file << "# " << header << std::endl;
	      
    // Write records
    for (auto const & rec : records)
    {
      std::vector<std::string> v;
      if (rec.create_file_record(v))
      {
	file << join_fields(v);
	file << std::endl;
      }
    }
  }	
	 
  if (!file.good())
  {
    error_msg = TRANSLATE("File system error while writing file %1 .");
    error_msg = replace_first(error_msg, "%1", filename);
    return false;
  }
	
  return true;
}
