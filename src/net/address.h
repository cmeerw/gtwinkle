/*	-*- C++ -*-
 * Copyright (C) 2012, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#if !defined(NET__ADDRESS__H)
#define NET__ADDRESS__H

#include <arpa/inet.h>
#include <netinet/in.h>

#include <algorithm>
#include <system_error>

namespace net
{
  namespace ip
  {
    class address;

    class v4_address
    {
      friend class address;

      struct in_addr data_;

    public:
      explicit v4_address(in_addr_t addr)
      {
        data_.s_addr = addr;
      }

      explicit v4_address(struct in_addr const & addr)
        : data_ (addr)
      { }

      explicit v4_address(std::string const & addr)
      {
        if (::inet_pton(AF_INET, addr.c_str(), &data_) != 1)
        {
          throw std::system_error(std::make_error_code(static_cast<std::errc>(errno)));
        }
      }

      static v4_address any()
      {
	return v4_address(INADDR_ANY);
      }

      static v4_address loopback()
      {
	return v4_address(INADDR_LOOPBACK);
      }

      static v4_address none()
      {
	return v4_address(INADDR_NONE);
      }

      friend bool operator ==(const v4_address &lhs, const v4_address &rhs) noexcept
      {
        return lhs.data_.s_addr == rhs.data_.s_addr;
      }

      friend bool operator !=(const v4_address &lhs, const v4_address &rhs) noexcept
      {
        return !(lhs == rhs);
      }

      std::string to_string() const
      {
	char buf[INET_ADDRSTRLEN];
	if (const char *result = ::inet_ntop(AF_INET, &data_, buf, sizeof(buf)))
	{
	  return result;
	}
	else
	{
          throw std::system_error(std::make_error_code(static_cast<std::errc>(errno)));
	}
      }
    };

    class v6_address
    {
      friend class address;

      struct in6_addr data_;

    public:
      explicit v6_address(struct in6_addr const & addr)
        : data_ (addr)
      { }

      explicit v6_address(std::string const & addr)
      {
        if (::inet_pton(AF_INET6, addr.c_str(), &data_) != 1)
        {
          throw std::system_error(std::make_error_code(static_cast<std::errc>(errno)));
        }
      }

      static v6_address any()
      {
	return v6_address(in6addr_any);
      }

      static v6_address loopback()
      {
	return v6_address(in6addr_loopback);
      }

      friend bool operator ==(const v6_address &lhs, const v6_address &rhs) noexcept
      {
        return std::mismatch(rhs.data_.s6_addr, lhs.data_.s6_addr + 16,
            rhs.data_.s6_addr).first == lhs.data_.s6_addr + 16;
      }

      friend bool operator !=(const v6_address &lhs, const v6_address &rhs) noexcept
      {
        return !(lhs == rhs);
      }

      std::string to_string() const
      {
	char buf[INET6_ADDRSTRLEN];
	if (const char *result = ::inet_ntop(AF_INET6, &data_, buf, sizeof(buf)))
	{
	  return result;
	}
	else
	{
          throw std::system_error(std::make_error_code(static_cast<std::errc>(errno)));
	}
      }
    };

    class address
    {
    public:
      enum addr_type
      {
	unspec_addr,
	v4_addr,
	v6_addr
      };

    private:
      addr_type type_;
      union address_union
      {
        address_union()
        { }

        explicit address_union(in_addr_t addr)
          : v4(addr)
        { }

        explicit address_union(struct in_addr const & addr)
          : v4(addr)
        { }

        explicit address_union(struct in6_addr const & addr)
          : v6(addr)
        { }

        unsigned char raw[sizeof(struct in6_addr)];
	v4_address v4;
	v6_address v6;
      } data_;

    protected:
      explicit address(addr_type type)
      : type_(type)
      { }

    public:
      address()
	: type_(unspec_addr)
      { }

      address(v4_address const & addr)
        : type_(v4_addr), data_(addr.data_)
      { }

      address(v6_address const & addr)
        : type_(v6_addr), data_(addr.data_)
      { }

      explicit address(in_addr_t addr)
        : type_(v4_addr), data_(addr)
      { }

      explicit address(struct in_addr const &addr)
        : type_(v4_addr), data_(addr)
      { }

      explicit address(struct in6_addr const &addr)
	: type_(v6_addr), data_(addr)
      { }

      address(addr_type type, const std::string &addr)
        : type_(type)
      {
	if (type == v4_addr)
	{
	  if (::inet_pton(AF_INET, addr.c_str(), &data_.v4.data_) != 1)
	  {
	    throw std::system_error(std::make_error_code(static_cast<std::errc>(errno)));
	  }
	}
	else if (type == v6_addr)
	{
	  if (::inet_pton(AF_INET6, addr.c_str(), &data_.v6.data_) != 1)
	  {
	    throw std::system_error(std::make_error_code(static_cast<std::errc>(errno)));
	  }
	}
      }

      friend bool operator ==(const address &lhs, const address &rhs) noexcept
      {
	if (lhs.type_ == rhs.type_)
	{
	  switch (lhs.type_)
	  {
	  case unspec_addr:
	   return true;

	  case v4_addr:
	   return lhs.data_.v4 == rhs.data_.v4;

	  case v6_addr:
           return lhs.data_.v6 == rhs.data_.v6;
	  }
	}

	return false;
      }

      friend bool operator !=(const address &lhs, const address &rhs) noexcept
      {
	return !(lhs == rhs);
      }

      inline addr_type type() const noexcept
      {
	return type_;
      }

      inline bool is_unspec() const noexcept
      {
	return type_ == unspec_addr;
      }

      inline bool is_v4() const noexcept
      {
	return type_ == v4_addr;
      }

      v4_address const & as_v4() const noexcept
      {
        return data_.v4;
      }

      inline bool is_v6() const noexcept
      {
	return type_ == v6_addr;
      }

      v6_address const & as_v6() const noexcept
      {
        return data_.v6;
      }

      inline struct in_addr const & get_v4() const noexcept
      {
	return data_.v4.data_;
      }

      inline struct in6_addr const & get_v6() const noexcept
      {
	return data_.v6.data_;
      }

      inline bool is_any() const noexcept
      {
	return (type_ == address::v6_addr) ? (data_.v6 == v6_address::any())
	  : (type_ == address::v4_addr) ? (data_.v4 == v4_address::any())
	  : false;
      }

      inline bool is_loopback() const noexcept
      {
	return (type_ == address::v6_addr) ? (data_.v6 == v6_address::loopback())
	  : (type_ == address::v4_addr) ? (data_.v4 == v4_address::loopback())
	  : false;
      }

      std::string to_string() const
      {
	return (type_ == address::v6_addr) ? data_.v6.to_string()
	  : (type_ == address::v4_addr) ? data_.v4.to_string()
	  : "";
      }
    };
  }
}

#endif

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
