/*	-*- C++ -*-
 * Copyright (C) 2012, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#ifndef NET__ADDRINFO__H
#define NET__ADDRINFO__H

#include <cstring>

#include <sys/socket.h>
#include <netdb.h>

namespace net
{
  class addrinfo
  {
    struct ::addrinfo *res_;
    int rc_;

  public:
    class const_iterator
    {
      const struct ::addrinfo *ptr_;

    public:
      inline explicit const_iterator(const struct ::addrinfo *ptr)
	: ptr_(ptr)
      { }

      inline const struct ::addrinfo *operator ->() const
      {
	return ptr_;
      }

      inline const struct ::addrinfo &operator *() const
      {
	return *ptr_;
      }

      inline const_iterator &operator ++()
      {
	ptr_ = ptr_->ai_next;
	return *this;
      }

      inline const_iterator operator ++(int)
      {
	return const_iterator(ptr_->ai_next);
      }

      inline bool operator ==(const const_iterator &iter)
      {
	return ptr_ == iter.ptr_;
      }

      inline bool operator !=(const const_iterator &iter)
      {
	return ptr_ != iter.ptr_;
      }
    };

    inline addrinfo(const char *hostname, const char *service)
      : res_(nullptr), rc_(::getaddrinfo(hostname, service, nullptr, &res_))
    { }

    inline addrinfo(const char *hostname, const char *service,
		    const struct ::addrinfo &hints)
      : res_(nullptr), rc_(::getaddrinfo(hostname, service, &hints, &res_))
    { }

    inline addrinfo(const char *hostname, const char *service,
		    int family, int flags = 0, int socktype = 0,
		    int protocol = 0)
      : res_(nullptr)
    {
      struct ::addrinfo hints;
      std::memset(&hints, 0, sizeof(hints));

      hints.ai_flags = flags;
      hints.ai_family = family;
      hints.ai_socktype = socktype;
      hints.ai_protocol = protocol;

      rc_ = ::getaddrinfo(hostname, service, &hints, &res_);
    }

    inline ~addrinfo()
    {
      if (res_)
      {
	::freeaddrinfo(res_);
      }
    }

    inline operator bool() const
    {
      return rc_ == 0;
    }

    inline int error() const
    {
      return rc_;
    }

    inline const_iterator begin() const
    {
      return const_iterator(res_);
    }

    inline const_iterator end() const
    {
      static const const_iterator end_iter(nullptr);
      return end_iter;
    }
  };
}

#endif

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
