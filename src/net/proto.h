/*	-*- C++ -*-
 * Copyright (C) 2012, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#if !defined(NET__PROTO__H)
#define NET__PROTO__H

#include <netinet/in.h>

#include <string>

namespace net
{
  // forward declaration
  template<typename proto> class basic_stream_socket;
  template<typename proto> class basic_datagram_socket;

  namespace ip
  {
    // forward declarations
    class endpoint;

    namespace proto
    {
      struct tcp
      {
	typedef basic_stream_socket<tcp> socket_type;
	typedef endpoint endpoint_type;

	static const int native_protocol = IPPROTO_TCP;
      };

      struct udp
      {
	typedef basic_datagram_socket<udp> socket_type;
	typedef endpoint endpoint_type;

	static const int native_protocol = IPPROTO_UDP;
      };
    }
  }

  namespace posix
  {
    namespace proto
    {
      struct unix_domain
      {
        typedef basic_stream_socket<unix_domain> socket_type;
        typedef std::string endpoint_type;

        static const int native_addr_type = AF_UNIX;
        static const int native_protocol = 0;
      };
    }
  }
}

#endif

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
