/*	-*- C++ -*-
 * Copyright (C) 2012-2023, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#if !defined(NET__SOCKET__H)
#define NET__SOCKET__H

#include "address.h"
#include "endpoint.h"
#include "proto.h"

#include <cstring>
#include <errno.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <type_traits>
#include <utility>

namespace net
{
  typedef int native_socket_t;

  namespace detail
  {
    static const native_socket_t invalid_socket_handle = -1;

    template<typename AddrType>
    void convert_addr_type(AddrType type);

    constexpr int convert_addr_type(ip::address::addr_type type)
    {
      return (type == ip::address::v4_addr) ? AF_INET :
	     (type == ip::address::v6_addr) ? AF_INET6 :
	     AF_UNSPEC;
    }

    template<typename Endpoint>
    class sockaddr_converter;

    template<>
    class sockaddr_converter<ip::endpoint>
    {
      union
      {
	struct sockaddr_in in;
	struct sockaddr_in6 in6;
      } u_;

      socklen_t len_;

    public:
      sockaddr_converter()
	: len_(sizeof(u_))
      {
	std::memset(&u_, 0, sizeof(u_));
      }

      explicit sockaddr_converter(ip::endpoint const &ep)
	: len_(ep.addr().type() == ip::address::v4_addr ?
	       sizeof(u_.in) : sizeof(u_.in6))
      {
	if (ep.addr().type() == ip::address::v4_addr)
	{
	  std::memset(&u_.in, 0, sizeof(u_.in));
	  u_.in.sin_family = AF_INET;
	  u_.in.sin_port = htons(ep.port());
	  u_.in.sin_addr = ep.addr().get_v4();
	}
	else if (ep.addr().type() == ip::address::v6_addr)
	{
	  std::memset(&u_.in6, 0, sizeof(u_.in6));
	  u_.in6.sin6_family = AF_INET6;
	  u_.in6.sin6_port = htons(ep.port());
	  u_.in6.sin6_addr = ep.addr().get_v6();
	}
      }

      struct sockaddr &native_data()
      {
	return reinterpret_cast<struct sockaddr &>(u_);
      }

      struct sockaddr const &native_data() const
      {
	return reinterpret_cast<struct sockaddr const &>(u_);
      }

      socklen_t &native_size()
      {
	return len_;
      }

      socklen_t native_size() const
      {
	return len_;
      }

      ip::endpoint get_endpoint() const
      {
	ip::endpoint ep;

	struct sockaddr const &addr(reinterpret_cast<struct sockaddr const &>(u_));

	if (addr.sa_family == AF_INET)
	{
	  ep.set_addr(ip::address(u_.in.sin_addr.s_addr));
	  ep.set_port(ntohs(u_.in.sin_port));
	}
	else if (addr.sa_family == AF_INET6)
	{
	  ep.set_addr(ip::address(u_.in6.sin6_addr));
	  ep.set_port(ntohs(u_.in6.sin6_port));
	}

	return ep;
      }
    };

    template<>
    class sockaddr_converter<std::string>
    {
      struct sockaddr_un un_;
      socklen_t len_;

    public:
      sockaddr_converter()
	: len_(sizeof(un_))
      {
	std::memset(&un_, 0, sizeof(un_));
      }

      explicit sockaddr_converter(std::string const &ep)
      {
	std::memset(&un_, 0, sizeof(un_));
        un_.sun_family = AF_UNIX;
        size_t const l = std::max(sizeof(un_.sun_path) - 1, ep.length());
        std::memcpy(un_.sun_path, ep.data(), l);
        un_.sun_path[l] = '\0';
        len_ = SUN_LEN(&un_);
      }

      struct sockaddr &native_data()
      {
	return reinterpret_cast<struct sockaddr &>(un_);
      }

      struct sockaddr const &native_data() const
      {
	return reinterpret_cast<struct sockaddr const &>(un_);
      }

      socklen_t &native_size()
      {
	return len_;
      }

      socklen_t native_size() const
      {
	return len_;
      }

      std::string get_endpoint() const
      {
        std::string ep;

	if (un_.sun_family == AF_UNIX)
	{
          ep = un_.sun_path;
	}

	return ep;
      }
    };

    template<typename Endpoint>
    auto connect(native_socket_t sock, Endpoint const &ep,
	std::error_code &ec) ->
      typename std::enable_if<sizeof(detail::sockaddr_converter<Endpoint>) != 0u>::type
    {
      detail::sockaddr_converter<Endpoint> addr(ep);
      if (::connect(sock, &addr.native_data(), addr.native_size()))
      {
	ec = std::make_error_code(static_cast<std::errc>(errno));
      }
    }

    inline size_t recv(native_socket_t sock, char *buf, size_t len,
	std::error_code &ec)
    {
      int const ret = ::recv(sock, buf, len, 0);
      if (ret < 0)
      {
	ec = std::make_error_code(static_cast<std::errc>(errno));
	return static_cast<size_t>(-1);
      }

      return ret;
    }

    inline size_t send(native_socket_t sock, char const *buf, size_t len,
	std::error_code &ec)
    {
      int const ret = ::send(sock, buf, len, 0);
      if (ret < 0)
      {
	ec = std::make_error_code(static_cast<std::errc>(errno));
	return static_cast<size_t>(-1);
      }

      return ret;
    }

    template<typename Endpoint>
    auto recvfrom(native_socket_t sock, char *buf, size_t len, Endpoint &ep,
	std::error_code &ec) ->
      typename std::enable_if<sizeof(detail::sockaddr_converter<Endpoint>) != 0u, size_t>::type
    {
      detail::sockaddr_converter<Endpoint> addr;
      int const ret = ::recvfrom(sock, buf, len, 0,
			  &addr.native_data(), &addr.native_size());
      if (ret < 0)
      {
	ec = std::make_error_code(static_cast<std::errc>(errno));
	return static_cast<size_t>(-1);
      }

      ep = addr.get_endpoint();

      return ret;
    }

    template<typename Endpoint>
    size_t sendto(native_socket_t sock, char const *buf, size_t len,
	Endpoint const &ep, std::error_code &ec)
    {
      detail::sockaddr_converter<Endpoint> addr(ep);
      int const ret = ::sendto(sock, buf, len, 0,
			  &addr.native_data(), addr.native_size());
      if (ret < 0)
      {
	ec = std::make_error_code(static_cast<std::errc>(errno));
	return static_cast<size_t>(-1);
      }

      return ret;
    }

    template<typename proto>
    auto accept(native_socket_t sock, typename proto::endpoint_type &ep,
	std::error_code &ec) ->
      typename proto::socket_type
    {
      detail::sockaddr_converter<typename proto::endpoint_type> addr;
      native_socket_t ret(::accept(sock, &addr.native_data(), &addr.native_size()));
      if (ret < 0)
      {
	ec = std::make_error_code(static_cast<std::errc>(errno));
	return typename proto::socket_type(+invalid_socket_handle);
      }

      ep = addr.get_endpoint();

      return typename proto::socket_type(std::move(ret));
    }

    template<typename proto>
    auto accept(native_socket_t sock, std::error_code &ec) ->
      typename proto::socket_type
    {
      native_socket_t ret(::accept(sock, nullptr, nullptr));
      if (ret < 0)
      {
	ec = std::make_error_code(static_cast<std::errc>(errno));
	return typename proto::socket_type(+invalid_socket_handle);
      }

      return typename proto::socket_type(std::move(ret));
    }
  }

  class socket_base
  {
  private:
    native_socket_t sock_;

  protected:
    static const native_socket_t invalid_handle = detail::invalid_socket_handle;

    static native_socket_t make_native_socket(int domain, int type,
	int protocol)
    {
      native_socket_t const sock = ::socket(domain, type, protocol);
      if (sock == invalid_handle)
      {
	throw std::system_error(std::make_error_code(static_cast<std::errc>(errno)));
      }

      return sock;
    }

    socket_base()
      : sock_(invalid_handle)
    { }

    explicit socket_base(int && sock)
      : sock_(sock)
    {
      sock = invalid_handle;
    }

  public:
    socket_base(socket_base const &) = delete;
    socket_base(socket_base && s)
      : sock_(s.sock_)
    {
      s.sock_ = invalid_handle;
    }

    socket_base &operator =(socket_base const &) & = delete;
    socket_base &operator =(socket_base && s) &
    {
      sock_ = s.sock_;
      return *this;
    }

    ~socket_base()
    {
      if (sock_ != invalid_handle)
      {
	close();
      }
    }

    int native_handle() const
    {
      return sock_;
    }

    bool is_valid() const
    {
      return sock_ != invalid_handle;
    }

    void close(std::error_code &ec)
    {
      if (::close(sock_))
      {
	ec = std::make_error_code(static_cast<std::errc>(errno));
      }
    }

    void close()
    {
      std::error_code ec;
      close(ec);
      if (ec) { throw std::system_error(ec); }
    }

    template<typename Endpoint>
    auto bind(Endpoint const &ep, std::error_code &ec) ->
      typename std::enable_if<sizeof(detail::sockaddr_converter<Endpoint>) != 0u>::type
    {
      detail::sockaddr_converter<Endpoint> addr(ep);
      if (::bind(native_handle(), &addr.native_data(), addr.native_size()))
      {
	ec = std::make_error_code(static_cast<std::errc>(errno));
      }
    }

    template<typename Endpoint>
    auto bind(Endpoint const &ep) ->
      typename std::enable_if<sizeof(detail::sockaddr_converter<Endpoint>) != 0u>::type
    {
      std::error_code ec;
      bind(ep, ec);
      if (ec) { throw std::system_error(ec); }
    }

    template<typename Endpoint>
    auto connect(Endpoint const &ep, std::error_code &ec) ->
      typename std::enable_if<sizeof(detail::sockaddr_converter<Endpoint>) != 0u>::type
    {
      return detail::connect(native_handle(), ep, ec);
    }

    template<typename Endpoint>
    auto connect(Endpoint const &ep) ->
      typename std::enable_if<sizeof(detail::sockaddr_converter<Endpoint>) != 0u>::type
    {
      std::error_code ec;
      detail::connect(native_handle(), ep, ec);
      if (ec) { throw std::system_error(ec); }
    }

    size_t recv(char *buf, size_t len, std::error_code &ec)
    {
      return detail::recv(native_handle(), buf, len, ec);
    }

    size_t recv(char *buf, size_t len)
    {
      std::error_code ec;
      const size_t ret = detail::recv(native_handle(), buf, len, ec);
      if (ec) { throw std::system_error(ec); }
      return ret;
    }

    size_t send(char const *buf, size_t len, std::error_code &ec)
    {
      return detail::send(native_handle(), buf, len, ec);
    }

    size_t send(char const *buf, size_t len)
    {
      std::error_code ec;
      const size_t ret = detail::send(native_handle(), buf, len, ec);
      if (ec) { throw std::system_error(ec); }
      return ret;
    }

    template<typename Endpoint>
    auto getsockname(std::error_code &ec) const ->
      typename std::enable_if<sizeof(detail::sockaddr_converter<Endpoint>) != 0u, Endpoint>::type
    {
      detail::sockaddr_converter<Endpoint> addr;
      if (::getsockname(native_handle(), &addr.native_data(), &addr.native_size()))
      {
	ec = std::make_error_code(static_cast<std::errc>(errno));
	return ip::endpoint();
      }

      return addr.get_endpoint();
    }

    template<typename Endpoint>
    auto getsockname() const ->
      typename std::enable_if<sizeof(detail::sockaddr_converter<Endpoint>) != 0u, Endpoint>::type
    {
      std::error_code ec;
      ip::endpoint const ep(getsockname<Endpoint>(ec));
      if (ec) { throw std::system_error(ec); }
      return ep;
    }

    template<typename Endpoint>
    auto getpeername(std::error_code &ec) const ->
      typename std::enable_if<sizeof(detail::sockaddr_converter<Endpoint>) != 0u, Endpoint>::type
    {
      detail::sockaddr_converter<Endpoint> addr;
      if (::getpeername(native_handle(), &addr.native_data(), &addr.native_size()))
      {
	ec = std::make_error_code(static_cast<std::errc>(errno));
	return ip::endpoint();
      }

      return addr.get_endpoint();
    }

    template<typename Endpoint>
    auto getpeername() const ->
      typename std::enable_if<sizeof(detail::sockaddr_converter<Endpoint>) != 0u, Endpoint>::type
    {
      std::error_code ec;
      ip::endpoint const ep(getpeername<Endpoint>(ec));
      if (ec) { throw std::system_error(ec); }
      return ep;
    }

    void getsockopt(int level, int optname, void *optval, socklen_t *optlen,
	std::error_code &ec) const
    {
      if (::getsockopt(native_handle(), level, optname, optval, optlen))
      {
	ec = std::make_error_code(static_cast<std::errc>(errno));
      }
    }

    void getsockopt(int level, int optname, void *optval, socklen_t *optlen) const
    {
      std::error_code ec;
      getsockopt(level, optname, optval, optlen, ec);
      if (ec) { throw std::system_error(ec); }
    }

    void setsockopt(int level, int optname, const void *optval, socklen_t optlen, std::error_code &ec)
    {
      if (::setsockopt(native_handle(), level, optname, optval, optlen))
      {
	ec = std::make_error_code(static_cast<std::errc>(errno));
      }
    }

    void setsockopt(int level, int optname, const void *optval, socklen_t optlen)
    {
      std::error_code ec;
      setsockopt(level, optname, optval, optlen, ec);
      if (ec) { throw std::system_error(ec); }
    }
  };

  class datagram_socket_base
    : public socket_base
  {
  protected:
    datagram_socket_base()
    { }

    explicit datagram_socket_base(int &&sock)
      : socket_base(std::move(sock))
    { }

  public:
    datagram_socket_base(datagram_socket_base && sock) = default;

    template<typename Endpoint>
    auto recvfrom(char *buf, size_t len, Endpoint &ep, std::error_code &ec) ->
      typename std::enable_if<sizeof(detail::sockaddr_converter<Endpoint>) != 0u, size_t>::type
    {
      return detail::recvfrom(native_handle(), buf, len, ep, ec);
    }

    size_t recvfrom(char *buf, size_t len, ip::endpoint &ep)
    {
      std::error_code ec;
      size_t const ret = detail::recvfrom(native_handle(), buf, len, ep, ec);
      if (ec) { throw std::system_error(ec); }
      return ret;
    }

    template<typename Endpoint>
    size_t sendto(char const *buf, size_t len, Endpoint const &ep,
	std::error_code &ec)
    {
      return detail::sendto(native_handle(), buf, len, ep, ec);
    }

    template<typename Endpoint>
    size_t sendto(char const *buf, size_t len, Endpoint const &ep)
    {
      std::error_code ec;
      size_t const ret = detail::sendto(native_handle(), buf, len, ep, ec);
      if (ec) { throw std::system_error(ec); }
      return ret;
    }
  };

  class stream_socket_base
    : public socket_base
  {
  protected:
    stream_socket_base()
    { }

    explicit stream_socket_base(int && sock)
      : socket_base(std::move(sock))
    { }

  public:
    stream_socket_base(stream_socket_base && sock) = default;

    void shutdown(int what, std::error_code &ec)
    {
      if (::shutdown(native_handle(), what))
      {
	ec = std::make_error_code(static_cast<std::errc>(errno));
      }
    }

    void shutdown(int what)
    {
      std::error_code ec;
      shutdown(what, ec);
      if (ec) { throw std::system_error(ec); }
    }

    void listen(int backlog, std::error_code &ec)
    {
      if (::listen(native_handle(), backlog))
      {
	ec = std::make_error_code(static_cast<std::errc>(errno));
      }
    }

    void listen(int backlog)
    {
      std::error_code ec;
      listen(backlog, ec);
      if (ec) { throw std::system_error(ec); }
    }
  };

  template<typename proto>
  class basic_datagram_socket
    : public datagram_socket_base
  {
  protected:
    explicit basic_datagram_socket(int && sock)
      : datagram_socket_base(std::move(sock))
    { }

  public:
    basic_datagram_socket(basic_datagram_socket && sock) = default;

    basic_datagram_socket()
      : datagram_socket_base(make_native_socket(proto::native_addr_type,
				 SOCK_DGRAM, proto::native_protocol))
    { }

    template<typename AddrType>
    explicit basic_datagram_socket(AddrType type,
	typename std::enable_if<sizeof(detail::convert_addr_type(std::declval<AddrType>())) != 0u, int>::type = 0)
      : datagram_socket_base(make_native_socket(detail::convert_addr_type(type),
				 SOCK_DGRAM, proto::native_protocol))
    { }

    typename proto::endpoint_type getsockname(std::error_code &ec) const
    {
      return socket_base::getsockname<typename proto::endpoint_type>(ec);
    }

    typename proto::endpoint_type getsockname() const
    {
      return socket_base::getsockname<typename proto::endpoint_type>();
    }

    typename proto::endpoint_type getpeername(std::error_code &ec) const
    {
      return socket_base::getpeername<typename proto::endpoint_type>(ec);
    }

    typename proto::endpoint_type getpeername() const
    {
      return socket_base::getpeername<typename proto::endpoint_type>();
    }
  };

  template<typename proto>
  class basic_stream_socket
    : public stream_socket_base
  {
    template<typename _proto>
    friend auto detail::accept(native_socket_t sock,
	typename _proto::endpoint_type &ep,
	std::error_code &ec) ->
      typename _proto::socket_type;
    template<typename _proto>
    friend auto detail::accept(native_socket_t sock, std::error_code &ec) ->
      typename _proto::socket_type;

  protected:
    explicit basic_stream_socket(int && sock)
      : stream_socket_base(std::move(sock))
    { }

  public:
    basic_stream_socket(basic_stream_socket && sock) = default;

    explicit basic_stream_socket()
      : stream_socket_base(make_native_socket(proto::native_addr_type,
			       SOCK_DGRAM, proto::native_protocol))
    { }

    template<typename AddrType>
    explicit basic_stream_socket(AddrType type,
	typename std::enable_if<sizeof(detail::convert_addr_type(std::declval<AddrType>())) != 0u, int>::type = 0)
      : stream_socket_base(make_native_socket(detail::convert_addr_type(type),
			       SOCK_STREAM, proto::native_protocol))
    { }

    typename proto::endpoint_type getsockname(std::error_code &ec) const
    {
      return socket_base::getsockname<typename proto::endpoint_type>(ec);
    }

    typename proto::endpoint_type getsockname() const
    {
      return socket_base::getsockname<typename proto::endpoint_type>();
    }

    typename proto::endpoint_type getpeername(std::error_code &ec) const
    {
      return socket_base::getpeername<typename proto::endpoint_type>(ec);
    }

    typename proto::endpoint_type getpeername() const
    {
      return socket_base::getpeername<typename proto::endpoint_type>();
    }

    typename proto::socket_type accept(typename proto::endpoint_type &ep,
	std::error_code &ec)
    {
      return detail::accept<proto>(native_handle(), ep, ec);
    }

    typename proto::socket_type accept(typename proto::endpoint_type &ep)
    {
      std::error_code ec;
      typename proto::socket_type ret(detail::accept<proto>(native_handle(),
	  ep, ec));
      if (ec) { throw std::system_error(ec); }
      return ret;
    }

    typename proto::socket_type accept(std::error_code &ec)
    {
      return detail::accept<proto>(native_handle(), ec);
    }

    typename proto::socket_type accept()
    {
      std::error_code ec;
      typename proto::socket_type ret(accept(ec));
      if (ec) { throw std::system_error(ec); }
      return ret;
    }
  };

  namespace ip
  {
    typedef basic_datagram_socket<proto::udp> udp_socket;
    typedef basic_stream_socket<proto::tcp> tcp_socket;
  }

  namespace posix
  {
    typedef basic_datagram_socket<proto::unix_domain> unix_domain_datagram_socket;
    typedef basic_stream_socket<proto::unix_domain> unix_domain_stream_socket;
  }
}

#endif

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
