/*	-*- C++ -*-
 * Copyright (C) 2012, Christof Meerwald
 * http://cmeerw.org
 *
 * See bottom of file for licensing information.
 */
#if !defined(NET__ENDPOINT__H)
#define NET__ENDPOINT__H

#include "address.h"

namespace net
{
  namespace ip
  {
    class endpoint
    {
    private:
      address addr_;
      unsigned short port_;

    public:
      endpoint()
	: port_(0)
      { }

      endpoint(address const &addr, unsigned short port)
	: addr_(addr), port_(port)
      { }

      friend bool operator == (const endpoint &lhs, const endpoint &rhs) noexcept
      {
	return (lhs.addr_ == rhs.addr_) && (lhs.port_ == rhs.port_);
      }

      friend bool operator != (const endpoint &lhs, const endpoint &rhs) noexcept
      {
	return !(lhs == rhs);
      }

      address const &addr() const
      {
	return addr_;
      }

      void set_addr(address const &addr)
      {
	addr_ = addr;
      }

      unsigned short port() const
      {
	return port_;
      }

      void set_port(unsigned short port)
      {
	port_ = port;
      }
    };
  }
}

#endif

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Alternatively, the contents of this file may be used under the
 * terms of either the GNU General Public License Version 2 (the
 * "GPL"), or the GNU Library General Public License Version 2 (the
 * "LGPL"), in which case the provisions of the GPL or the LGPL are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of either the GPL or the
 * LGPL, and not to allow others to use your version of this file
 * under the terms of the MPL, indicate your decision by deleting the
 * provisions above and replace them with the notice and other
 * provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file
 * under the terms of any one of the MPL, the GPL or the LGPL.
 */
