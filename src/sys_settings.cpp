/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "gtwinkle_config.h"


#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/soundcard.h>
#include <dirent.h>
#include <unistd.h>
#include <fstream>
#include <iostream>
#include <cstring>

#include "sys_settings.h"

#include "log.h"
#include "translator.h"
#include "user.h"
#include "userintf.h"
#include "util.h"
#include "utils/file_utils.h"

using namespace utils;

// Share directory containing files applicable to all users
#define DIR_SHARE	DATADIR

// Lock file to guarantee that a user is running the application only once
#define LOCK_FILENAME	"twinkle.lck"

// System config file
#define SYS_CONFIG_FILE	"twinkle.sys"

// Default location of the shared mime database
#define DFLT_SHARED_MIME_DB	"/usr/share/mime/globs"


/////////////////////////
// class t_sys_settings
/////////////////////////

t_sys_settings::t_sys_settings()
{
  fd_lock_file = -1;
  dir_share = DIR_SHARE;
  filename = std::string(DIR_HOME);
  filename += "/";
  filename += USER_DIR;
  filename += "/";
  filename += SYS_CONFIG_FILE;

  mime_shared_database = DFLT_SHARED_MIME_DB;
}

// Getters
std::string t_sys_settings::get_mime_shared_database() const
{
  std::lock_guard<std::recursive_mutex> guard(mtx_sys);
  return mime_shared_database;
}

// Setters
void t_sys_settings::set_mime_shared_database(const std::string &filename)
{
  std::lock_guard<std::recursive_mutex> guard(mtx_sys);
  mime_shared_database = filename;
}

std::string t_sys_settings::about(bool html) const
{
  std::string s = PRODUCT_NAME;
  s += ' ';
  s += PRODUCT_VERSION;
  s += " - ";
  s += get_product_date();
  if (html) s += "<BR>";
  s += "\n";

  s += "Copyright (C) 2005-2009  ";
  s += PRODUCT_AUTHOR;
  if (html) s += "<BR>";
  s += "\n";

  s += "http://www.twinklephone.com";
  if (html) s += "<BR><BR>";
  s += "\n\n";

  std::string options_built = get_options_built();
  if (!options_built.empty())
  {
    s += TRANSLATE("Built with support for:");
    s += " ";
    s += options_built;
    if (html) s += "<BR><BR>";
    s += "\n\n";
  }

  s += TRANSLATE("Contributions:");
  if (html) s += "<BR>";
  s += "\n";

  s += "* Werner Dittmann (ZRTP/SRTP)\n";
  if (html) s += "<BR>";

  s += "* Bogdan Harjoc (AKAv1-MD5, Service-Route)\n";
  if (html) s += "<BR>";

  s += "* Roman Imankulov (command line editing)\n";
  if (html) s += "<BR>";

  if (html)
  {
    s += "* Ondrej Mori&scaron; (codec preprocessing)<BR>\n";
  }
  else
  {
    s += "* Ondrej Moris (codec preprocessing)\n";
  }

  if (html)
  {
    s += "* Rickard Petz&auml;ll (ALSA)<BR>\n";
  }
  else
  {
    s += "* Rickard Petzall (ALSA)\n";
  }

  if (html) s += "<BR>";
  s += "\n";

  s += TRANSLATE("This software contains the following software from 3rd parties:");
  if (html) s += "<BR>";
  s += "\n";

  s += TRANSLATE("* GSM codec from Jutta Degener and Carsten Bormann, University of Berlin");
  if (html) s += "<BR>";
  s += "\n";

  s += TRANSLATE("* G.711/G.726 codecs from Sun Microsystems (public domain)");
  if (html) s += "<BR>";
  s += "\n";

  s += TRANSLATE("* G.722 codec from BELLCORE/Carnegie Mellon University/Steve Underwood (public domain)");
  if (html) s += "<BR>";
  s += "\n";

#ifdef HAVE_ILBC
  s += TRANSLATE("* iLBC implementation from RFC 3951 (www.ilbcfreeware.org)");
  if (html) s += "<BR>";
  s += "\n";
#endif

  s += TRANSLATE("* Parts of the STUN project at http://sourceforge.net/projects/stun");
  if (html) s += "<BR>";
  s += "\n";

  s += TRANSLATE("* Parts of libsrv at http://libsrv.sourceforge.net/");
  if (html) s += "<BR>";
  s += "\n";

  if (html) s += "<BR>";
  s += "\n";

  s += TRANSLATE("For RTP the following dynamic libraries are linked:");
  if (html) s += "<BR>";
  s += "\n";

  s += "* GNU ccRTP - http://www.gnu.org/software/ccrtp";
  if (html) s += "<BR>";
  s += "\n";

  s += "* GNU CommonC++ - http://www.gnu.org/software/commoncpp";
  if (html) s += "<BR><BR>";
  s += "\n\n";

  // Display information about translator only on non-english version.
  std::string translated_by = TRANSLATE("Translated to english by <your name>");
  if (translated_by != "Translated to english by <your name>")
  {
    s += translated_by;
    if (html) s += "<BR><BR>";
    s += "\n\n";
  }

  s += PRODUCT_NAME;
  s += " comes with ABSOLUTELY NO WARRANTY.";
  if (html) s += "<BR>";
  s += "\n";

  s += "This program is free software; you can redistribute it and/or modify";
  if (html) s += "<BR>";
  s += "\n";

  s += "it under the terms of the GNU General Public License as published by";
  if (html) s += "<BR>";
  s += "\n";

  s += "the Free Software Foundation; either version 2 of the License, or";
  if (html) s += "<BR>";
  s += "\n";

  s += "(at your option) any later version.";
  if (html) s += "<BR>";
  s += "\n";

  return s;
}

std::string t_sys_settings::get_product_date() const
{
  struct tm t;
  t.tm_sec = 0;
  t.tm_min = 0;
  t.tm_hour = 0;

  std::vector<std::string> l = split(PRODUCT_DATE, ' ');
  assert(l.size() == 3);
  t.tm_mon = str2month_full(l[0]);
  t.tm_mday = atoi(l[1].c_str());
  t.tm_year = atoi(l[2].c_str()) - 1900;

  char buf[64];
  strftime(buf, 64, "%d %B %Y", &t);
  return std::string(buf);
}

std::string t_sys_settings::get_options_built() const
{
  std::string options_built;
#ifdef HAVE_SPEEX
  if (!options_built.empty()) options_built += ", ";
  options_built += "Speex";
#endif
#ifdef HAVE_OPUS
  if (!options_built.empty()) options_built += ", ";
  options_built += "Opus";
#endif
#ifdef HAVE_ILBC
  if (!options_built.empty()) options_built += ", ";
  options_built += "iLBC";
#endif
#ifdef HAVE_ZRTP
  if (!options_built.empty()) options_built += ", ";
  options_built += "ZRTP";
#endif

  return options_built;
}

bool t_sys_settings::check_environment(std::string &error_msg) const
{
  struct stat stat_buf;
  std::string filename, dirname;

  std::lock_guard<std::recursive_mutex> g (mtx_sys);

  // Check if share directory exists
  if (stat(dir_share.c_str(), &stat_buf) != 0)
  {
    error_msg = TRANSLATE("Directory %1 does not exist.");
    error_msg = replace_first(error_msg, "%1", dir_share);
    return false;
  }

  // Check if audio file for ring tone exist
  filename = dir_share;
  filename += '/';
  filename += FILE_RINGTONE;
  std::ifstream f_ringtone(filename.c_str());
  if (!f_ringtone)
  {
    error_msg = TRANSLATE("Cannot open file %1 .");
    error_msg = replace_first(error_msg, "%1", filename);
    return false;
  }

  // Check if audio file for ring back exist
  filename = dir_share;
  filename += '/';
  filename += FILE_RINGBACK;
  std::ifstream f_ringback(filename.c_str());
  if (!f_ringback)
  {
    error_msg = TRANSLATE("Cannot open file %1 .");
    error_msg = replace_first(error_msg, "%1", filename);
    return false;
  }

  // Check if $HOME is set correctly
  if (std::string(DIR_HOME) == "")
  {
    error_msg = TRANSLATE("%1 is not set to your home directory.");
    error_msg = replace_first(error_msg, "%1", "$HOME");
    return false;
  }
  if (stat(DIR_HOME, &stat_buf) != 0)
  {
    error_msg = TRANSLATE("Directory %1 (%2) does not exist.");
    error_msg = replace_first(error_msg, "%1", DIR_HOME);
    error_msg = replace_first(error_msg, "%2", "$HOME");
    return false;
  }

  // Check if user directory exists
  dirname = get_dir_user();
  if (stat(dirname.c_str(), &stat_buf) != 0)
  {
    // User directory does not exist. Create it now.
    if (mkdir(dirname.c_str(), S_IRUSR | S_IWUSR | S_IXUSR) != 0)
    {
      // Failed to create the user directory
      error_msg = TRANSLATE("Cannot create directory %1 .");
      error_msg = replace_first(error_msg, "%1", dirname);
      return false;
    }
  }

  // Check if tmp file directory exists
  dirname = get_dir_tmpfile();
  if (stat(dirname.c_str(), &stat_buf) != 0)
  {
    // Tmp file directory does not exist. Create it now.
    if (mkdir(dirname.c_str(), S_IRUSR | S_IWUSR | S_IXUSR) != 0)
    {
      // Failed to create the tmp file directory
      error_msg = TRANSLATE("Cannot create directory %1 .");
      error_msg = replace_first(error_msg, "%1", dirname);
      return false;
    }
  }

  return true;
}

void t_sys_settings::set_dir_share(const std::string &dir)
{
  std::lock_guard<std::recursive_mutex> g (mtx_sys);
  dir_share = dir;
}

std::string t_sys_settings::get_dir_share() const
{
  std::string result;
  std::lock_guard<std::recursive_mutex> g (mtx_sys);
  result = dir_share;
  return result;
}

std::string t_sys_settings::get_dir_lang() const
{
  std::string result = get_dir_share();
  result += "/lang";
  return result;
}

std::string t_sys_settings::get_dir_user() const
{
  std::string dir = DIR_HOME;
  dir += "/";
  dir += DIR_USER;

  return dir;
}

std::string t_sys_settings::get_history_file() const
{
  std::string dir = get_dir_user();
  dir += "/";
  dir += FILE_CLI_HISTORY;

  return dir;
}

std::string t_sys_settings::get_dir_tmpfile() const
{
  std::string dir = get_dir_user();
  dir += "/";
  dir += DIR_TMPFILE;

  return dir;
}

bool t_sys_settings::is_tmpfile(const std::string &filename) const
{
  std::string tmpdir = get_dir_tmpfile();

  return filename.substr(0, tmpdir.size()) == tmpdir;
}

bool t_sys_settings::save_tmp_file(const std::string &data, const std::string &file_extension,
				   std::string &filename, std::string &error_msg)
{
  std::string fname = get_dir_tmpfile();
  fname += "/XXXXXX";

  std::unique_ptr<char, decltype(&::free)> tmpfile(strdup(fname.c_str()), &::free);
  int fd = mkstemp(tmpfile.get());

  if (fd < 0)
  {
    error_msg = get_error_str(errno);
    return false;
  }

  close(fd);
  std::ofstream f(tmpfile.get());
  if (!f)
  {
    error_msg = TRANSLATE("Failed to create file %1");
    error_msg = replace_first(error_msg, "%1", tmpfile.get());
    return false;
  }

  f.write(data.c_str(), data.size());
  if (!f.good())
  {
    error_msg = TRANSLATE("Failed to write data to file %1");
    error_msg = replace_first(error_msg, "%1", tmpfile.get());
    f.close();
    return false;
  }

  f.close();

  // Rename to name with extension
  filename = apply_glob_to_filename(tmpfile.get(), file_extension);

  if (rename(tmpfile.get(), filename.c_str()) < 0)
  {
    error_msg = get_error_str(errno);
    return false;
  }

  return true;
}

void t_sys_settings::remove_all_tmp_files() const
{
  DIR *tmpdir = opendir(get_dir_tmpfile().c_str());

  if (!tmpdir)
  {
    log_file->write_report(get_error_str(errno), "t_sys_settings::remove_all_tmp_files");
    return;
  }

  struct dirent *entry = readdir(tmpdir);
  while (entry)
  {
    if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0)
    {
      std::string fname = get_dir_tmpfile();
      fname += PATH_SEPARATOR;
      fname += entry->d_name;

      log_file->write_header("t_sys_settings::remove_all_tmp_files");
      log_file->write_raw("Remove tmp file ");
      log_file->write_raw(fname);
      log_file->write_endl();
      log_file->write_footer();

      unlink(fname.c_str());
    }

    entry = readdir(tmpdir);
  }

  closedir(tmpdir);
}

bool t_sys_settings::create_lock_file(bool shared_lock, std::string &error_msg,
                                      bool &already_running)
{
  std::string lck_filename;
  already_running = false;

  lck_filename = DIR_HOME;
  lck_filename += "/";
  lck_filename += DIR_USER;
  lck_filename += "/";
  lck_filename += LOCK_FILENAME;

  fd_lock_file = open(lck_filename.c_str(), O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
  if (fd_lock_file < 0)
  {
    error_msg = TRANSLATE("Cannot create %1 .");
    error_msg = replace_first(error_msg, "%1", lck_filename);
    error_msg += "\n";
    error_msg += get_error_str(errno);
    return false;
  }

  struct flock lock_options = { };

  // Try to acquire an exclusive lock
  if (!shared_lock)
  {
    lock_options.l_type = F_WRLCK;
    lock_options.l_whence = SEEK_SET;

    if (fcntl(fd_lock_file, F_SETLK, &lock_options) < 0)
    {
      already_running = true;
      error_msg = TRANSLATE("%1 is already running.\nLock file %2 already exists.");
      error_msg = replace_first(error_msg, "%1", PRODUCT_NAME);
      error_msg = replace_first(error_msg, "%2", lck_filename);
      return false;
    }
  }

  // Convert the lock to a shared lock. If the user forces multiple
  // instances of Twinkle to run, then each will have a shared lock.
  lock_options.l_type = F_RDLCK;
  lock_options.l_whence = SEEK_SET;

  if (fcntl(fd_lock_file, F_SETLK, &lock_options) < 0)
  {
    error_msg = TRANSLATE("Cannot lock %1 .");
    error_msg = replace_first(error_msg, "%1", lck_filename);
    return false;
  }

  return true;
}

void t_sys_settings::delete_lock_file()
{
  if (fd_lock_file >= 0)
  {
    struct flock lock_options = { };
    lock_options.l_type = F_UNLCK;
    lock_options.l_whence = SEEK_SET;

    fcntl(fd_lock_file, F_SETLK, &lock_options);

    close(fd_lock_file);
    fd_lock_file = -1;
  }
}

bool t_sys_settings::exec_audio_validation(bool ringtone, bool speaker, bool mic,
					   std::string &error_msg) const
{
  error_msg.clear();

  auto const &audio_settings(settings->get_child("audio"));
  if (!audio_settings->get_boolean("validate-audio-dev"))
  {
    return true;
  }

  const std::string &dev_ringtone(audio_settings->get_string("dev-ringtone"));
  const std::string &dev_speaker(audio_settings->get_string("dev-speaker"));
  const std::string &dev_mic(audio_settings->get_string("dev-mic"));

  bool valid = true;
  bool full_duplex = speaker && mic && (dev_speaker == dev_mic);

  if (ringtone && !t_audio_io::validate(dev_ringtone, true, false))
  {
    std::string msg = TRANSLATE("Cannot access the ring tone device (%1).");
    error_msg += replace_first(std::move(msg), "%1", dev_ringtone);
    error_msg += "\n";
    valid = false;
  }
  if (speaker && !t_audio_io::validate(dev_speaker, true, full_duplex))
  {
    std::string msg = TRANSLATE("Cannot access the speaker (%1).");
    error_msg += replace_first(std::move(msg), "%1", dev_speaker);
    error_msg += "\n";
    valid = false;
  }
  if (mic && !t_audio_io::validate(dev_mic, full_duplex, true))
  {
    std::string msg = TRANSLATE("Cannot access the microphone (%1).");
    error_msg += replace_first(std::move(msg), "%1", dev_mic);
    error_msg += "\n";
    valid = false;
  }

  return valid;
}
