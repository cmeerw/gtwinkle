/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "sdp_parse_ctrl.h"
#include "sdp_parser.tab.hpp"

#include <memory>

// Interface to Flex
extern int yysdplex_init_extra(t_sdp_parser::yycontext *, void **);
extern int yysdplex_destroy(void *);

struct yy_buffer_state;
extern struct yy_buffer_state *yysdp_scan_bytes(const char *, int, void *);
extern void yysdp_delete_buffer(struct yy_buffer_state *, void *);


std::unique_ptr<t_sdp> t_sdp_parser::parse(const std::string &s)
{
  yycontext ctx;
  yysdplex_init_extra(&ctx, &ctx.scanner);

  // The SDP body should end with a CRLF. Some implementations
  // do not send this last CRLF. Allow this deviation by adding
  // the last CRLF if it is missing.
  const bool has_nl = (s.back() == '\n' || s.back() == '\r');

  struct yy_buffer_state * const b(
    yysdp_scan_bytes(has_nl ? s.c_str() : (s + "\r\n").c_str(),
		     has_nl ? s.length() + 1 : s.length() + 3,
		     ctx.scanner));

  yy::sdp_parser parser(ctx);
  int const ret = parser.parse();

  yysdp_delete_buffer(b, ctx.scanner);
  yysdplex_destroy(ctx.scanner);

  if (ret != 0)
  {
    throw t_sdp_parse_error("sdp parse error", ret);
  }

  return std::move(ctx.sdp);
}
