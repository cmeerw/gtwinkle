/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

%{
#include <string>
#include "sdp/sdp_parse_ctrl.h"
#include "sdp_parser.tab.hpp"

#define yyterminate() return yy::sdp_parser::symbol_type(yy::sdp_parser::token_type())
#define YY_DECL yy::sdp_parser::symbol_type yysdplex (void *yyscanner)
#define YY_EXTRA_TYPE t_sdp_parser::yycontext *
%}

%option case-insensitive
%option reentrant
%option noyywrap
%option stack
%option nounput
%option prefix="yysdp"

DIGIT		[0-9]
ALPHA		[a-zA-Z]
ALNUM		[a-zA-Z0-9]
TOKEN_SYM	[[:alnum:]\-\.!%\*_\+\'~]
SAFE_SYM	[[:alnum:]'`\-\.\/:\?\"#\$&\*;=@\[\]\^_\{|\}\+~\"]

%x C_NUM
%x C_LINE
%x C_SAFE

%%
	switch (yyextra->lex_state) {
	case t_sdp_parser::X_NUM:	BEGIN(C_NUM); break;
	case t_sdp_parser::X_LINE:	BEGIN(C_LINE); break;
	case t_sdp_parser::X_SAFE:	BEGIN(C_SAFE); break;
	default:			BEGIN(INITIAL);
	}

	/* SDP lines */
^v=		{ return yy::sdp_parser::make_T_LINE_VERSION(); }
^o=		{ return yy::sdp_parser::make_T_LINE_ORIGIN(); }
^s=		{ return yy::sdp_parser::make_T_LINE_SESSION_NAME(); }
^c=		{ return yy::sdp_parser::make_T_LINE_CONNECTION(); }
^a=		{ return yy::sdp_parser::make_T_LINE_ATTRIBUTE(); }
^m=		{ return yy::sdp_parser::make_T_LINE_MEDIA(); }
^{ALPHA}=	{ return yy::sdp_parser::make_T_LINE_UNKNOWN(); }

	/* Token as define in RFC 3261 */
{TOKEN_SYM}+ 	{ return yy::sdp_parser::make_T_TOKEN(yytext); }

	/* End of line */
\r\n		{ return yy::sdp_parser::make_T_CRLF(); }
\n		{ return yy::sdp_parser::make_T_CRLF(); }

[[:blank:]]	/* Skip white space */

	/* Single character token */
.		{ return yy::sdp_parser::symbol_type(static_cast<yy::sdp_parser::token_type>(yytext[0])); }

	/* Number */
<C_NUM>{DIGIT}+		{ return yy::sdp_parser::make_T_NUM(atoi(yytext)); }
<C_NUM>[[:blank:]]	/* Skip white space */
<C_NUM>.		{ return yy::sdp_parser::symbol_type(static_cast<yy::sdp_parser::token_type>(yytext[0])); }
<C_NUM>\r\n		{ return yy::sdp_parser::make_T_CRLF(); }
<C_NUM>\n		{ return yy::sdp_parser::make_T_CRLF(); }

	/* Safe */
<C_SAFE>{SAFE_SYM}+	{ return yy::sdp_parser::make_T_SAFE(yytext); }
<C_SAFE>[[:blank:]]	/* Skip white space */
<C_SAFE>.		{ return yy::sdp_parser::symbol_type(static_cast<yy::sdp_parser::token_type>(yytext[0])); }
<C_SAFE>\r\n		{ return yy::sdp_parser::make_T_CRLF(); }
<C_SAFE>\n		{ return yy::sdp_parser::make_T_CRLF(); }

	/* Get all text till end of line */
<C_LINE>[^\r\n]+	{ return yy::sdp_parser::make_T_LINE(yytext); }
<C_LINE>\r\n		{ return yy::sdp_parser::make_T_CRLF(); }
<C_LINE>\n		{ return yy::sdp_parser::make_T_CRLF(); }
<C_LINE>\r		{ return yy::sdp_parser::make_T_CRLF(); }
