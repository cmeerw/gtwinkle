/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// Parser control

#ifndef _SDP_PARSE_CTRL_H
#define _SDP_PARSE_CTRL_H

#include "sdp.h"

#include <memory>
#include <stdexcept>
#include <string>


// The t_sdp_parser controls the direction of the scanner/parser
// process and it stores the results from the parser.
class t_sdp_parser
{
public:
  enum t_context
  {
    X_INITIAL,			// Initial context
    X_SAFE,			// Safe context
    X_NUM,			// Number context
    X_LINE,			// Whole line context
  };

  struct yycontext
  {
    yycontext()
      : sdp(new t_sdp), scanner(), lex_state(X_INITIAL)
    { }

    std::unique_ptr<t_sdp> sdp;
    void *scanner;
    t_context lex_state;
  };

  // Parse string s.
  static std::unique_ptr<t_sdp> parse(const std::string &s);
};

// Error that can be thrown as exception
class t_sdp_syntax_error
  : public std::runtime_error
{
public:
  t_sdp_syntax_error(const std::string &e)
    : std::runtime_error(e)
  { }
};

class t_sdp_parse_error
  : public std::runtime_error
{
public:
  t_sdp_parse_error(const std::string &e, int rc)
    : std::runtime_error(e), rc_(rc)
  { }

  int rc() const
  {
    return rc_;
  }

private:
  const int rc_;
};

#endif
