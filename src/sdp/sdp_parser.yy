/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

%code requires
{
#include <algorithm>
#include <cstdio>
#include <string>
#include <vector>
#include <experimental/optional>

#include "sdp/sdp_parse_ctrl.h"
#include "sdp/sdp.h"
#include "util.h"
}

%language "C++"
%defines
%define api.value.type variant
%define api.token.constructor
%define api.parser.class {sdp_parser}

%parse-param { t_sdp_parser::yycontext &ctx }
%lex-param { void* scanner }
%expect 2
/* See below for the expected shift/reduce conflicts. */

%code
{
extern yy::sdp_parser::symbol_type yysdplex (void *scanner);

#define SDP		ctx.sdp
#define CTX(state)	(ctx.lex_state = t_sdp_parser::X_ ## state)

#define scanner ctx.scanner
#define yylex yysdplex
}

%token <int>		T_NUM
%token <std::string>	T_TOKEN
%token <std::string>	T_SAFE
%token <std::string>	T_LINE

%token			T_CRLF

%token			T_LINE_VERSION
%token			T_LINE_ORIGIN
%token			T_LINE_SESSION_NAME
%token			T_LINE_CONNECTION
%token			T_LINE_ATTRIBUTE
%token			T_LINE_MEDIA
%token			T_LINE_UNKNOWN

// The token T_NULL is never returned by the scanner.
%token			T_NULL

%type <net::ip::address::addr_type>	address_type
%type <t_sdp_connection>	connection
%type <t_sdp_ntwk_type>		network_type
%type <std::vector<t_sdp_attr>>	attributes
%type <std::experimental::fundamentals_v1::optional<t_sdp_attr>>	attribute
%type <std::experimental::fundamentals_v1::optional<t_sdp_attr>>	attribute2
%type <t_sdp_media>		media
%type <std::string>		transport
%type <std::vector<std::string>>	formats

%%

/* The unknown_lines cause an expected shift/reduce conflict */
sdp_body:	  version
		  origin
		  session_name
		  unknown_lines
		  sess_connection
		  unknown_lines
		  sess_attributes
		  media_list {
		  	/* Parsing stops here. Remaining text is
			 * not parsed.
			 */
		  	YYACCEPT; }
		| error T_NULL {
			/* KLUDGE to avoid memory leak in bison.
			 * See the SIP parser for an explanation.
			 */
			YYABORT;
		}
;

version:	  T_LINE_VERSION { CTX(NUM); } T_NUM { CTX(INITIAL); }
		  T_CRLF {
			SDP->version = $3; }
;

origin:		  T_LINE_ORIGIN { CTX(SAFE); } T_SAFE { CTX(INITIAL); }
		  T_TOKEN T_TOKEN network_type address_type { CTX(SAFE); }
		  T_SAFE { CTX(INITIAL); } T_CRLF {
			SDP->origin.username = $3;
			SDP->origin.session_id = $5;
			SDP->origin.session_version = $6;
			SDP->origin.network_type = $7;
			SDP->origin.address_type = $8;
			SDP->origin.address = $10;
		  }
;

network_type:	  T_TOKEN { try {
				$$ = str2sdp_ntwk_type($1);
			    }
			    catch (const t_sdp_syntax_error &) {
			    	// Invalid network type.
				// Set network type to NULL. This way the message
				// will not be discarded and the error can be
				// handled on the SIP level (error response or
				// call tear down).
				$$ = SDP_NTWK_NULL;
			    } }
;

address_type:	  T_TOKEN { try {
				$$ = str2sdp_addr_type($1);
			    }
			    catch (const t_sdp_syntax_error &) {
			    	// Invalid address type
				$$ = net::ip::address::unspec_addr;
			    } }
;

session_name:	  T_LINE_SESSION_NAME { CTX(LINE); } T_LINE
		  { CTX(INITIAL); } T_CRLF {
			SDP->session_name = $3;
		  }
;

sess_connection:  connection {
			SDP->connection = $1;
		  }
;

connection:	  /* empty */	{ }
		| T_LINE_CONNECTION network_type address_type { CTX(SAFE); }
		  T_SAFE { CTX(INITIAL); } T_CRLF {
			$$.network_type = $2;
			$$.address_type = $3;
			$$.address = $5;
		  }
;

sess_attributes:  attributes {
			SDP->attributes = $1;
		  }
;

attributes:	  /* emtpy */	{ }
		| attributes attribute {
			$$ = std::move($1);
			$$.push_back($2.value());
		  }
;

attribute:	  T_LINE_ATTRIBUTE attribute2 T_CRLF {
			$$ = $2; }
;

attribute2:	  T_TOKEN {
			$$.emplace($1);
		  }
		| T_TOKEN ':' { CTX(LINE); } T_LINE { CTX(INITIAL); } {
			$$.emplace($1, $4);
		  }
;

media_list:	  /* empty */
		| media_list media {
			SDP->media.push_back($2);
		  }
;

/* The unknown_lines cause an expected shift/reduce conflict */
media:		  T_LINE_MEDIA T_TOKEN { CTX(NUM); } T_NUM { CTX(INITIAL); }
		  transport formats T_CRLF unknown_lines connection
		  unknown_lines attributes {
			if ($4 > 65535) YYERROR;
			
			$$.media_type = tolower($2);
			$$.port = $4;
			$$.transport = $6;
			
			/* The format depends on the media type */
			switch($$.get_media_type()) {
			case SDP_AUDIO:
			case SDP_VIDEO:
				/* Numeric format */
				for (auto const & s : $7) {
					if (is_number(s)) $$.formats.push_back(atoi(s.c_str()));
				}
				
				break;
			default:
				/* Alpha numeric format */
				$$.alpha_num_formats = $7;
			}
			
			$$.connection = $10;
			$$.attributes = $12;
		  }
;

transport:	  T_TOKEN { 
			$$ = $1;
		  }
		| transport '/' T_TOKEN {
			$$ = $1 + '/' + $3;
		  }

// For RTP/AVP a format is a number. For other transport protocols,
// non-numerical formats are possible.
formats:	  /* empty */	{ }
		| formats T_TOKEN {
			$$ = std::move($1);
			$$.push_back($2);
		  }
;

/* Skip unknown lines */
unknown_lines:	  /* empty */
		| unknown_lines unknown_line
;

unknown_line:	  T_LINE_UNKNOWN { CTX(LINE); } T_LINE { CTX(INITIAL); }
		  T_CRLF { }
;

%%

void yy::sdp_parser::error (const std::string& m)
{ }
