/*
  Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "protocol.h"
#include "sdp_parse_ctrl.h"
#include "sdp.h"
#include "util.h"
#include "parser/hdr_warning.h"
#include "parser/parameter.h"

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <iterator>

std::string sdp_ntwk_type2str(t_sdp_ntwk_type n)
{
  switch(n)
  {
  case SDP_NTWK_NULL:	return "NULL";
  case SDP_NTWK_IN:	return "IN";
  default:
   assert(false);
  }
}

t_sdp_ntwk_type str2sdp_ntwk_type(const std::string &s)
{
  if (s == "IN") return SDP_NTWK_IN;

  throw (t_sdp_syntax_error("unknown network type: " + s));
}

std::string sdp_addr_type2str(net::ip::address::addr_type a)
{
  switch(a)
  {
  case net::ip::address::unspec_addr:	return "NULL";
  case net::ip::address::v4_addr:	return "IP4";
  case net::ip::address::v6_addr:	return "IP6";
  default:
   assert(false);
  }
}

net::ip::address::addr_type str2sdp_addr_type(const std::string &s)
{
  if (s == "IP4") return net::ip::address::v4_addr;
  if (s == "IP6") return net::ip::address::v6_addr;

  throw (t_sdp_syntax_error("unknown address type: " + s));
}

std::string sdp_transport2str(t_sdp_transport t)
{
  switch(t)
  {
  case SDP_TRANS_RTP:	return "RTP/AVP";
  case SDP_TRANS_UDP:	return "udp";
  default:
   assert(false);
  }
}

t_sdp_transport str2sdp_transport(const std::string &s)
{
  if (s == "RTP/AVP") return SDP_TRANS_RTP;
  if (s == "udp") return SDP_TRANS_UDP;

  // Other transports are not recognized and are mapped to other.
  return SDP_TRANS_OTHER;
}

t_sdp_media_type str2sdp_media_type(const std::string &s)
{
  if (s == "audio") return SDP_AUDIO;
  if (s == "video") return SDP_VIDEO;
  return SDP_OTHER;
}

std::string sdp_media_type2str(t_sdp_media_type m)
{
  switch(m)
  {
  case SDP_AUDIO: return "audio";
  case SDP_VIDEO: return "video";
  default:
   assert(false);
  }
}

std::string get_rtpmap(unsigned format, t_audio_codec codec)
{
  std::string rtpmap;

  rtpmap = int2str(format);
  rtpmap += ' ';

  switch(codec)
  {
  case CODEC_G711_ULAW:
   rtpmap += SDP_RTPMAP_G711_ULAW;
   break;

  case CODEC_G711_ALAW:
   rtpmap += SDP_RTPMAP_G711_ALAW;
   break;

  case CODEC_GSM:
   rtpmap += SDP_RTPMAP_GSM;
   break;

  case CODEC_G722:
   rtpmap += SDP_RTPMAP_G722;
   break;

  case CODEC_SPEEX_NB:
   rtpmap += SDP_RTPMAP_SPEEX_NB;
   break;

  case CODEC_SPEEX_WB:
   rtpmap += SDP_RTPMAP_SPEEX_WB;
   break;

  case CODEC_SPEEX_UWB:
   rtpmap += SDP_RTPMAP_SPEEX_UWB;
   break;

  case CODEC_OPUS:
   rtpmap += SDP_RTPMAP_OPUS;
   break;

  case CODEC_ILBC:
   rtpmap += SDP_RTPMAP_ILBC;
   break;

  case CODEC_G726_16:
   rtpmap += SDP_RTPMAP_G726_16;
   break;

  case CODEC_G726_24:
   rtpmap += SDP_RTPMAP_G726_24;
   break;

  case CODEC_G726_32:
   rtpmap += SDP_RTPMAP_G726_32;
   break;

  case CODEC_G726_40:
   rtpmap += SDP_RTPMAP_G726_40;
   break;

  case CODEC_TELEPHONE_EVENT:
   rtpmap += SDP_RTPMAP_TELEPHONE_EV;
   break;

  default:
   assert(false);
  }

  return rtpmap;
}

std::string sdp_media_direction2str(t_sdp_media_direction d)
{
  switch(d)
  {
  case SDP_INACTIVE: return "inactive";
  case SDP_SENDONLY: return "sendonly";
  case SDP_RECVONLY: return "recvonly";
  case SDP_SENDRECV: return "sendrecv";
  default:
   assert(false);
  }
}

///////////////////////////////////
// class t_sdp_origin
///////////////////////////////////

t_sdp_origin::t_sdp_origin()
{
  network_type = SDP_NTWK_NULL;
}

t_sdp_origin::t_sdp_origin(std::string _username,
			   std::string _session_id,
			   std::string _session_version,
			   net::ip::address::addr_type _address_type,
			   std::string _address)
  : username(std::move(_username)), session_id(std::move(_session_id)),
    session_version(std::move(_session_version)),
    address_type(_address_type), address(std::move(_address))
{
  network_type = SDP_NTWK_IN;
}

std::string t_sdp_origin::encode() const
{
  std::string s;

  s = "o=";
  s += username;
  s += ' ' + session_id;
  s += ' ' + session_version;
  s += ' ' + sdp_ntwk_type2str(network_type);
  s += ' ' + sdp_addr_type2str(address_type);
  s += ' ' + address;
  s += CRLF;

  return s;
}


///////////////////////////////////
// class t_sdp_connection
///////////////////////////////////

t_sdp_connection::t_sdp_connection()
{
  network_type = SDP_NTWK_NULL;
}

t_sdp_connection::t_sdp_connection(net::ip::address::addr_type _address_type,
    std::string _address)
  : address_type(_address_type), address(std::move(_address))
{
  network_type = SDP_NTWK_IN;
}

std::string t_sdp_connection::encode() const
{
  std::string s;

  s = "c=";
  s += sdp_ntwk_type2str(network_type);
  s += ' ' + sdp_addr_type2str(address_type);
  s += ' ' + address;
  s += CRLF;

  return s;
}


///////////////////////////////////
// class t_sdp_attr
///////////////////////////////////

t_sdp_attr::t_sdp_attr(std::string _name)
  : name(std::move(_name))
{ }

t_sdp_attr::t_sdp_attr(std::string _name, std::string _value)
  : name(std::move(_name)), value(std::move(_value))
{ }

std::string t_sdp_attr::encode() const
{
  std::string s;

  s = "a=";
  s += name;

  if (!value.empty())
  {
    s += ':' + value;
  }

  s += CRLF;

  return s;
}


///////////////////////////////////
// class t_sdp_media
///////////////////////////////////

t_sdp_media::t_sdp_media()
  : format_dtmf(0),
    port(0)
{ }

t_sdp_media::t_sdp_media(t_sdp_media_type _media_type,
    unsigned short _port,
    const std::vector<t_audio_codec> &_codecs,
    unsigned short _format_dtmf,
    const std::vector<unsigned short> &ac2format)
  : format_dtmf(_format_dtmf),
    media_type(sdp_media_type2str(_media_type)),
    port(_port),
    transport(sdp_transport2str(SDP_TRANS_RTP))
{
  for (t_audio_codec codec : _codecs)
  {
    add_format(ac2format[codec], codec);
  }

  if (format_dtmf > 0)
  {
    add_format(format_dtmf, CODEC_TELEPHONE_EVENT);
  }
}

std::string t_sdp_media::encode() const
{
  std::string s;

  s = "m=";
  s += media_type;
  s += ' ' + int2str(port);
  s += ' ' + transport;

  // Encode media formats. Note that only one of the format lists
  // will be populated depending on the media type.

  // Numeric formats.
  for (auto const & format : formats)
  {
    s += ' ' + int2str(format);
  }

  // Alpha numeric formats.
  for (auto const & afmt : alpha_num_formats)
  {
    s += ' ' + afmt;
  }

  s += CRLF;

  // Connection information.
  if (connection.network_type != SDP_NTWK_NULL)
  {
    s += connection.encode();
  }

  // Attributes.
  for (auto const & attr : attributes)
  {
    s += attr.encode();
  }

  return s;
}

void t_sdp_media::add_format(unsigned short f, t_audio_codec codec)
{
  formats.push_back(f);

  // RFC 3264 5.1
  // All media descriptions SHOULD contain an rtpmap
  std::string rtpmap = get_rtpmap(f, codec);
  attributes.emplace_back("rtpmap", rtpmap);

  // RFC 2833 3.9
  // Add fmtp parameter
  if (format_dtmf > 0 && f == format_dtmf)
  {
    std::string fmtp = int2str(f);
    fmtp += ' ';
    fmtp += "0-15";
    attributes.emplace_back("fmtp", fmtp);
  }
}

t_sdp_attr *t_sdp_media::get_attribute(const std::string &name)
{
  for (t_sdp_attr & attr : attributes)
  {
    if (cmp_nocase(attr.name, name) == 0)
    {
      return &attr;
    }
  }

  // Attribute does not exist
  return nullptr;
}

std::vector<t_sdp_attr *> t_sdp_media::get_attributes(const std::string &name)
{
  std::vector<t_sdp_attr *> l;

  for (t_sdp_attr & attr : attributes)
  {
    if (cmp_nocase(attr.name, name) == 0)
    {
      l.push_back(&attr);
    }
  }

  return l;
}

t_sdp_media_direction t_sdp_media::get_direction() const
{
  auto * const self(const_cast<t_sdp_media *>(this));

  if (self->get_attribute("inactive"))
  {
    return SDP_INACTIVE;
  }

  if (self->get_attribute("sendonly"))
  {
    return SDP_SENDONLY;
  }

  if (self->get_attribute("recvonly"))
  {
    return SDP_RECVONLY;
  }

  return SDP_SENDRECV;
}

t_sdp_media_type t_sdp_media::get_media_type() const
{
  return str2sdp_media_type(media_type);
}

t_sdp_transport t_sdp_media::get_transport() const
{
  return str2sdp_transport(transport);
}

///////////////////////////////////
// class t_sdp
///////////////////////////////////

t_sdp::t_sdp()
  : t_sip_body(), version(0)
{ }

t_sdp::t_sdp(const std::string &user,
    const std::string &sess_id, const std::string &sess_version,
    net::ip::address::addr_type user_host_type, const std::string &user_host,
    net::ip::address::addr_type media_host_type, const std::string &media_host,
    unsigned short media_port,
    const std::vector<t_audio_codec> &codecs, unsigned short format_dtmf,
    const std::vector<unsigned short> &ac2format)
  : t_sdp(user, sess_id, sess_version,
      user_host_type, user_host,
      media_host_type, media_host)
{
  media.emplace_back(SDP_AUDIO, media_port, codecs, format_dtmf, ac2format);
}

t_sdp::t_sdp(const std::string &user,
    const std::string &sess_id, const std::string &sess_version,
    net::ip::address::addr_type user_host_type, const std::string &user_host,
    net::ip::address::addr_type media_host_type, const std::string &media_host)
  : t_sip_body(),
    version(0),
    origin(user, sess_id, sess_version, user_host_type, user_host),
    connection(media_host_type, media_host)
{ }

void t_sdp::add_media(const t_sdp_media &m)
{
  media.push_back(m);
}

std::string t_sdp::encode() const
{
  std::string s;

  s = "v=" + int2str(version) + CRLF;
  s += origin.encode();

  if (session_name.empty())
  {
    // RFC 3264 5
    // Session name may no be empty. Recommende is '-'
    s += "s=-";
    s += CRLF;
  }
  else
  {
    s += "s=" + session_name + CRLF;
  }

  if (connection.network_type != SDP_NTWK_NULL)
  {
    s += connection.encode();
  }

  // RFC 3264 5
  // Time parameter should be 0 0
  s += "t=0 0";
  s += CRLF;

  for (auto const & attr : attributes)
  {
    s += attr.encode();
  }

  for (auto const & m : media)
  {
    s += m.encode();
  }

  return s;
}

t_sdp *t_sdp::copy() const
{
  std::unique_ptr<t_sdp> s(new t_sdp(*this));
  return s.release();
}

t_body_type t_sdp::get_type() const
{
  return BODY_SDP;
}

t_media t_sdp::get_media() const
{
  return t_media("application", "sdp");
}

bool t_sdp::is_supported(int &warn_code, std::string &warn_text) const
{
  warn_text = "";

  if (version != 0)
  {
    warn_code = W_399_MISCELLANEOUS;
    warn_text = "SDP version ";
    warn_text += int2str(version);
    warn_text += " not supported";
    return false;
  }

  const t_sdp_media *m = get_first_media(SDP_AUDIO);

  // Connection information must be present at the session level
  // and/or the media level
  if (connection.network_type == SDP_NTWK_NULL)
  {
    if (m == nullptr || m->connection.network_type == SDP_NTWK_NULL)
    {
      warn_code = W_399_MISCELLANEOUS;
      warn_text = "c-line missing";
      return false;
    }
  }
  else
  {
    // Only Internet is supported
    if (connection.network_type != SDP_NTWK_IN)
    {
      warn_code = W_300_INCOMPATIBLE_NWK_PROT;
      return false;
    }

    // Only IPv4 and IPv6 are supported
    if ((connection.address_type != net::ip::address::v4_addr) &&
	(connection.address_type != net::ip::address::v6_addr))
    {
      warn_code = W_301_INCOMPATIBLE_ADDR_FORMAT;
      return false;
    }
  }

  // There must be at least 1 audio stream with a non-zero port value
  if (m == nullptr && !media.empty())
  {
    warn_code = W_304_MEDIA_TYPE_NOT_AVAILABLE;
    warn_text = "Valid media stream for audio is missing";
    return false;
  }

  // RFC 3264 5, RFC 3725 flow IV
  // There may be 0 media streams
  if (media.empty())
  {
    return true;
  }

  // Check connection information on media level
  if (m->connection.network_type == SDP_NTWK_IN &&
      m->connection.address_type != net::ip::address::v4_addr &&
      m->connection.address_type != net::ip::address::v6_addr)
  {
    warn_code = W_301_INCOMPATIBLE_ADDR_FORMAT;
    return false;
  }

  if (m->get_transport() != SDP_TRANS_RTP)
  {
    warn_code = W_302_INCOMPATIBLE_TRANS_PROT;
    return false;
  }

  t_sdp_media *m2 = const_cast<t_sdp_media *>(m);
  if (const t_sdp_attr * const a = m2->get_attribute("ptime"))
  {
    unsigned short p = atoi(a->value.c_str());
    if (p < MIN_PTIME)
    {
      warn_code = W_306_ATTRIBUTE_NOT_UNDERSTOOD;
      warn_text = "Attribute 'ptime' too small. must be >= ";
      warn_text += int2str(MIN_PTIME);
      return false;
    }

    if (p > MAX_PTIME)
    {
      warn_code = W_306_ATTRIBUTE_NOT_UNDERSTOOD;
      warn_text = "Attribute 'ptime' too big. must be <= ";
      warn_text += int2str(MAX_PTIME);
      return false;
    }
  }

  return true;
}

net::ip::address::addr_type t_sdp::get_rtp_host_type(t_sdp_media_type media_type) const
{
  const t_sdp_media *m = get_first_media(media_type);
  assert(m != nullptr);

  // If the media line has its own connection information, then
  // take the host information from there.
  if (m->connection.network_type == SDP_NTWK_IN)
  {
    return m->connection.address_type;
  }

  // The host information must be in the session connection info
  assert(connection.network_type == SDP_NTWK_IN);
  return connection.address_type;
}

std::string t_sdp::get_rtp_host(t_sdp_media_type media_type) const
{
  const t_sdp_media *m = get_first_media(media_type);
  assert(m != nullptr);

  // If the media line has its own connection information, then
  // take the host information from there.
  if (m->connection.network_type == SDP_NTWK_IN)
  {
    return m->connection.address;
  }

  // The host information must be in the session connection info
  assert(connection.network_type == SDP_NTWK_IN);
  return connection.address;
}

unsigned short t_sdp::get_rtp_port(t_sdp_media_type media_type) const
{
  const t_sdp_media *m = get_first_media(media_type);
  assert(m != nullptr);

  return m->port;
}

std::vector<unsigned short> t_sdp::get_codecs(t_sdp_media_type media_type) const
{
  const t_sdp_media *m = get_first_media(media_type);
  assert(m != nullptr);

  return m->formats;
}

std::string t_sdp::get_codec_description(t_sdp_media_type media_type,
					 unsigned short codec) const
{
  auto * const m(const_cast<t_sdp_media *>(get_first_media(media_type)));
  assert(m != nullptr);

  for (auto *attr : m->get_attributes("rtpmap"))
  {
    auto const l(split_ws(attr->value));
    if (atoi(l.front().c_str()) == codec)
    {
      return l.back();
    }
  }

  return "";
}

t_audio_codec t_sdp::get_rtpmap_codec(const std::string &rtpmap) const
{
  if (rtpmap.empty()) return CODEC_NULL;

  std::vector<std::string> rtpmap_elems = split(rtpmap, '/');
  if (rtpmap_elems.size() < 2)
  {
    // RFC 2327
    // The rtpmap should at least contain the encoding name
    // and sample rate
    return CODEC_UNSUPPORTED;
  }

  std::string codec_name = trim(rtpmap_elems[0]);
  int sample_rate = atoi(trim(rtpmap_elems[1]).c_str());

  if (cmp_nocase(codec_name, SDP_AC_NAME_G711_ULAW) == 0 && sample_rate == 8000)
  {
    return CODEC_G711_ULAW;
  }
  else if (cmp_nocase(codec_name, SDP_AC_NAME_G711_ALAW) == 0 && sample_rate == 8000)
  {
    return CODEC_G711_ALAW;
  }
  else if (cmp_nocase(codec_name, SDP_AC_NAME_GSM) == 0 && sample_rate == 8000)
  {
    return CODEC_GSM;
  }
  else if (cmp_nocase(codec_name, SDP_AC_NAME_G722) == 0 && sample_rate == 8000)
  {
    return CODEC_G722;
  }
  else if (cmp_nocase(codec_name, SDP_AC_NAME_SPEEX) == 0 && sample_rate == 8000)
  {
    return CODEC_SPEEX_NB;
  }
  else if (cmp_nocase(codec_name, SDP_AC_NAME_SPEEX) == 0 && sample_rate == 16000)
  {
    return CODEC_SPEEX_WB;
  }
  else if (cmp_nocase(codec_name, SDP_AC_NAME_SPEEX) == 0 && sample_rate == 32000)
  {
    return CODEC_SPEEX_UWB;
  }
  else if (cmp_nocase(codec_name, SDP_AC_NAME_OPUS) == 0 && sample_rate == 48000)
  {
    return CODEC_OPUS;
  }
  else if (cmp_nocase(codec_name, SDP_AC_NAME_ILBC) == 0 && sample_rate == 8000)
  {
    return CODEC_ILBC;
  }
  else if (cmp_nocase(codec_name, SDP_AC_NAME_G726_16) == 0 && sample_rate == 8000)
  {
    return CODEC_G726_16;
  }
  else if (cmp_nocase(codec_name, SDP_AC_NAME_G726_24) == 0 && sample_rate == 8000)
  {
    return CODEC_G726_24;
  }
  else if (cmp_nocase(codec_name, SDP_AC_NAME_G726_32) == 0 && sample_rate == 8000)
  {
    return CODEC_G726_32;
  }
  else if (cmp_nocase(codec_name, SDP_AC_NAME_G726_40) == 0 && sample_rate == 8000)
  {
    return CODEC_G726_40;
  }
  else if (cmp_nocase(codec_name, SDP_AC_NAME_TELEPHONE_EV) == 0)
  {
    return CODEC_TELEPHONE_EVENT;
  }

  return CODEC_UNSUPPORTED;
}

t_audio_codec t_sdp::get_codec(t_sdp_media_type media_type,
			       unsigned short codec) const
{
  std::string rtpmap = get_codec_description(media_type, codec);

  // If there is no rtpmap description then use the static
  // payload definition as defined by RFC 3551
  if (rtpmap.empty())
  {
    switch(codec)
    {
    case SDP_FORMAT_G711_ULAW:
     return CODEC_G711_ULAW;
    case SDP_FORMAT_G711_ALAW:
     return CODEC_G711_ALAW;
    case SDP_FORMAT_GSM:
     return CODEC_GSM;
    case SDP_FORMAT_G722:
     return CODEC_G722;
    default:
     return CODEC_UNSUPPORTED;
    }
  }

  // Use the rtpmap description to map the payload number
  // to a codec
  return get_rtpmap_codec(rtpmap);
}

t_sdp_media_direction t_sdp::get_direction(t_sdp_media_type media_type) const
{
  const t_sdp_media *m = get_first_media(media_type);
  assert(m != nullptr);

  return m->get_direction();
}

std::string t_sdp::get_fmtp(t_sdp_media_type media_type, unsigned short codec) const
{
  auto * const m(const_cast<t_sdp_media *>(get_first_media(media_type)));
  assert(m != nullptr);

  for (auto attr : m->get_attributes("fmtp"))
  {
    auto const l(split_ws(attr->value));
    if (atoi(l.front().c_str()) == codec)
    {
      return l.back();
    }
  }

  return "";
}

int t_sdp::get_fmtp_int_param(t_sdp_media_type media_type, unsigned short codec,
			      const std::string &param) const
{
  std::string fmtp = get_fmtp(media_type, codec);
  if (fmtp.empty()) return -1;

  int value;
  auto l = str2param_list(fmtp);
  auto it = find(l.begin(), l.end(), t_parameter(param, ""));
  if (it != l.end())
  {
    value = atoi(it->value.c_str());
  }
  else
  {
    value = -1;
  }

  return value;
}

unsigned short t_sdp::get_ptime(t_sdp_media_type media_type) const
{
  auto * const m(const_cast<t_sdp_media *>(get_first_media(media_type)));
  assert(m != nullptr);

  const t_sdp_attr *a = m->get_attribute("ptime");
  return a ? atoi(a->value.c_str()) : 0u;
}

bool t_sdp::get_zrtp_support(t_sdp_media_type media_type) const
{
  auto * const m(const_cast<t_sdp_media *>(get_first_media(media_type)));
  assert(m != nullptr);

  const t_sdp_attr *a = m->get_attribute("zrtp");
  return a != nullptr;
}

void t_sdp::set_ptime(t_sdp_media_type media_type, unsigned short ptime)
{
  auto * const m(const_cast<t_sdp_media *>(get_first_media(media_type)));
  assert(m != nullptr);

  t_sdp_attr a("ptime", int2str(ptime));
  m->attributes.push_back(a);
}

void t_sdp::set_direction(t_sdp_media_type media_type, t_sdp_media_direction direction)
{
  auto * const m(const_cast<t_sdp_media *>(get_first_media(media_type)));
  assert(m != nullptr);

  t_sdp_attr a(sdp_media_direction2str(direction));
  m->attributes.push_back(a);
}

void t_sdp::set_fmtp(t_sdp_media_type media_type, unsigned short codec, const std::string &fmtp)
{
  auto * const m(const_cast<t_sdp_media *>(get_first_media(media_type)));
  assert(m != nullptr);

  std::string s = int2str(codec);
  s += ' ';
  s += fmtp;
  t_sdp_attr a("fmtp", s);
  m->attributes.push_back(a);
}

void t_sdp::set_fmtp_int_param(t_sdp_media_type media_type, unsigned short codec,
			       const std::string &param, int value)
{
  std::string fmtp(param);
  fmtp += '=';
  fmtp += int2str(value);
  set_fmtp(media_type, codec, fmtp);
}

void t_sdp::set_zrtp_support(t_sdp_media_type media_type)
{
  auto * const m(const_cast<t_sdp_media *>(get_first_media(media_type)));
  assert(m != nullptr);

  t_sdp_attr a("zrtp");
  m->attributes.push_back(a);
}

const t_sdp_media *t_sdp::get_first_media(t_sdp_media_type media_type) const
{
  for (const t_sdp_media &m : media)
  {
    if (m.get_media_type() == media_type && m.port != 0)
    {
      return &m;
    }
  }

  return nullptr;
}

bool t_sdp::local_ip_check() const
{
  if (origin.address == AUTO_IP4_ADDRESS) return false;

  if (connection.address == AUTO_IP4_ADDRESS) return false;

  for (auto const & m : media)
  {
    if (m.connection.address == AUTO_IP4_ADDRESS) return false;
  }

  return true;
}
