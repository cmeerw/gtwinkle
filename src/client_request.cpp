/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "client_request.h"

std::atomic<t_tuid> t_client_request::next_tuid(1);

t_client_request::t_client_request(t_user &user, t_request &r, const t_tid _tid)
  : request(r.copy()),
    tuid(get_next_tuid()), tid(_tid),
    redirector(r.uri, user.get_max_redirections())
{ }

t_client_request::t_client_request(t_user &user, StunMessage &r, const t_tid _tid)
  : stun_request(new StunMessage(r)),
    tuid(get_next_tuid()), tid(_tid),
    redirector(t_url(), user.get_max_redirections())
{ }

t_client_request::t_client_request(t_client_request const &other)
  : request(other.request ? other.request->copy() : nullptr),
    stun_request(other.stun_request ?
		 new StunMessage(*other.stun_request) : nullptr),
    tuid(other.tuid), tid(other.tid), redirector(other.redirector)
{ }

t_client_request::~t_client_request() = default;

std::unique_ptr<t_client_request> t_client_request::copy()
{
  std::unique_ptr<t_client_request> cr(new t_client_request(*this));
  return cr;
}

t_request *t_client_request::get_request() const
{
  return request.get();
}

StunMessage *t_client_request::get_stun_request() const
{
  return stun_request.get();
}

t_tuid t_client_request::get_tuid() const
{
  return tuid;
}

t_tid t_client_request::get_tid() const
{
  return tid;
}

void t_client_request::set_tid(t_tid _tid)
{
  tid = _tid;
}

void t_client_request::renew(t_tid _tid)
{
  tuid = get_next_tuid();

  tid = _tid;
}

t_tuid t_client_request::get_next_tuid()
{
  t_tuid id(next_tuid);
  while (!next_tuid.compare_exchange_weak(id, (id == 65535) ? 1 : (id + 1)))
  { }

  return id;
}
