/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _UTIL_H
#define _UTIL_H

/**
 * @file
 * Utility functions
 */

#include <string>
#include <utility>
#include <vector>
#include <commoncpp/config.h>


template<typename Base>
struct noop_copy_move : public Base
{
  noop_copy_move()
    : Base()
  { }

  noop_copy_move(const noop_copy_move &)
    : Base()
  { }

  noop_copy_move(noop_copy_move &&)
    : Base()
  { }

  noop_copy_move &operator =(const noop_copy_move &) &
  { return *this; }

  noop_copy_move &operator =(noop_copy_move &&) &
  { return *this; }
};

std::string random_token(int length);
std::string random_hexstr(int length);

/**
 * Convert a float to a string.
 * @param f [in] Float to convert.
 * @param precision [in] Number of digits after the decimal point in output.
 * @return String representation of the float.
 */
std::string float2str(float f, int precision);

// Convert an int to a string. format is a printf format
std::string int2str(int i, const char *format);
std::string int2str(int i);

// Convert a ulong to a string. format is a printf format
std::string ulong2str(unsigned long i, const char *format);
std::string ulong2str(unsigned long i);

// Convert a pointer to a string (hexadecimal)
std::string ptr2str(void *p);

// Convert a bool to a string: "false", "true"
std::string bool2str(bool b);

// Convert time/date to string
// The format parameter is a strftime() format string
std::string time2str(time_t t, const char *format);
std::string current_time2str(const char *format);

std::string weekday2str(int wkday);
std::string month2str(int month);

// Convert a full month name to an int (0-11)
int str2month_full(const std::string &month);

// Convert a duration in seconds to a string with hours, minutes seconds.
// The hours and minutes are only present if there is at least 1 hour/minute.
// E.g. 65s -> "1m 5s"
//      3601s -> "1h 0m 1s"
std::string duration2str(unsigned long seconds);

// Convert a timer in seconds to a string (h:mm:ss)
std::string timer2str(unsigned long seconds);

/**
 * Convert a hex string to an integer.
 * @param h [in] A hex string.
 * @return The integer.
 */
unsigned long hex2int(const std::string &h);

/**
 * Convert a hex string to a binary blob representing the hex value.
 * @param h [in] A hex string.
 * @param buf [in] A pointer to a buffer to store the binary blob.
 * @pre The buffer must be large enough to contain the binary blob.
 * @post buf contains the binary representation of the hex string.
 */
void hex2binary(const std::string &h, uint8 *buf);

/**
 * Convert a binary blob to a hexadecimal string.
 * @param buf [in] Pointer to the binary blob.
 * @param len [in] Length of the blob.
 * @return The hexadecimal string.
 */
std::string binary2hex(uint8 *buf, unsigned long len);

// Convert a string to lower case
std::string tolower(const std::string &s);

// Convert a string to upper case
std::string toupper(const std::string &s);

// Trim a string
std::string rtrim(const std::string &s);
std::string ltrim(const std::string &s);
std::string trim(const std::string &s);

/**
 * Pad a string on the left side till a certain length.
 * @param s [in] The string to pad.
 * @param c [in] The pad character.
 * @param len [in] The length to which the string must be padded.
 * @return The padded string.
 */
std::string padleft(const std::string &s, char c, unsigned long len);

// Compare 2 strings case insensive, return
// -1 --> s1 < s2
// 0  --> s1 == s2
// 1  --> s1 > s2
int cmp_nocase(const std::string &s1, const std::string &s2);

// Return true if a string must be quoted in text encoding
bool must_quote(const std::string &s);

// Escape character c in string by prepending it with a backslash.
// Backslashed are automatically escaped as well
std::string escape(const std::string &s, char c);

// Unescape a string
std::string unescape(const std::string &s);

// Escape reserved chars in s by there hex-notation (%HEX)
// All chars that are not in unreserved are considered as reserved.
std::string escape_hex(const std::string &s, const std::string &unreserved);

// Unescape the hex-values in a string
std::string unescape_hex(const std::string &s);

// Replace all occurrences of 'from' char 'to' char in s
std::string replace_char(std::string s, char from, char to);

// Replace first occurrence of 'from'-string to 'to'-string in s
std::string replace_first(std::string s, const std::string &from, const std::string &to);

/**
 *  Split a string into elements using a single character as separator.
 * @param s [in] The string to split.
 * @param c [in] The character separator.
 * @return Vector containing the split parts.
 */
std::vector<std::string> split(const std::string &s, char c);

/**
 * Split a string into elements using a string separator.
 * @param s [in] The string to split.
 * @param separator [in] The string separator.
 * @return Vector containing the split parts.
 */
std::vector<std::string> split(const std::string &s, const std::string &separator);

/**
 * Split a string into elements using line breaks as seperator
 * If the string contains a CRLF, then CRLF is used as line break.
 * Otherwise if the string contains a CR, then CR is used as line break.
 * Otherwise LF is used as line break.
 * @param s [in] The string to split.
 * @return Vector containing the split parts.
 */
std::vector<std::string> split_linebreak(const std::string &s);

/**
 * Split a string in two on the first occurrence of a separator.
 * @param s [in] The string to split.
 * @param c [in] The separator.
 * @return Vector containing the split parts.
 */
std::vector<std::string> split_on_first(const std::string &s, char c);

/**
 * Split a string in two on the last occurrence of a separator.
 * @param s [in] The string to split.
 * @param c [in] The separator.
 * @return Vector containing the split parts.
 */
std::vector<std::string> split_on_last(const std::string &s, char c);

// Split an escaped string into elements using c as a separator
// Escaped means: \c will not be seen as a seperator and backslash is
//                escaped itself (\\)
std::vector<std::string> split_escaped(const std::string &s, char c);

// Split a string into elements using spaces as separator
// If quote_sensitive = true, then spaces within quoted strings will
// not be used to split the string.
std::vector<std::string> split_ws(const std::string &s, bool quote_sensitive = false);

/**
 * Join a vector of strings into one string.
 * @param v Vector of strings.
 * @param separator String to be inserted between the strings to join.
 * @return A string containing the concatenarion of all strings in v.
 *         The invidual strings are separated by separator.
 */
std::string join_strings(const std::vector<std::string> &v, const std::string &separator);

// Remove surrounding quotes of a string if present.
std::string unquote(const std::string &s);

// Check if a string is a number
bool is_number(const std::string &s);

// Check if a string is an IP address
bool is_ipaddr(const std::string &s);

// Conversion between yes/no values and bool
bool yesno2bool(const std::string &yesno);
std::string bool2yesno(bool b);

// Convert a text string to DTMF digits
// Characters that cannot be converted will be removed
std::string str2dtmf(const std::string &s);

// Return true if string s looks like a phone number
// A string looks like a phone number if it consists of digits,
// *, #, special symbols and white space
bool looks_like_phone(const std::string &s, const std::string &special_symbols);

/**
 * Remove all special symbols from a string.
 * @param s [in] The string to convert.
 * @param special_symbols [in] The special symbols to remove.
 * @return The string without the special symbols.
 */
std::string remove_symbols(const std::string &s, const std::string &special_symbols);

/**
 * Remove spaces and tabs from a string.
 * @param s [in] The string to convert.
 * @return The string without spaces and tabs.
 */
std::string remove_white_space(const std::string &s);

/**
 * Truncate a string. If the string was longer than the truncated
 * result, then "..." will be appended.
 * @param s [in] The string to truncate.
 * @param len [in] The length in bytes to truncate to.
 * @return The truncated string.
 */
std::string dotted_truncate(const std::string &s, std::string::size_type len);

/**
 * Convert a string to a printable representation, i.e. change
 * all non-printable chars into dots.
 * @param s [in] The string to convert.
 * @return The converted string.
 */
std::string to_printable(const std::string &s);

/**
 * Get the error message describing an error number.
 * @param errnum [in] The error number.
 * @return The error message.
 */
std::string get_error_str(int errnum);

/**
 * HTML escape string.
 */
std::string escape_html(std::string s, bool use_non_breaking_space = false);

#endif
