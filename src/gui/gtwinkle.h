/*
  Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>
  Copyright (C) 2012  Christof Meerwald, http://cmeerw.org

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef TWINKLE__GTWINKLE_H
#define TWINKLE__GTWINKLE_H

#include "userintf.h"

#include <gtkmm.h>

#include <glibmm/dispatcher.h>
#include <glibmm/thread.h>

#include <deque>
#include <memory>
#include <mutex>


class t_main
{
protected:
  Gtk::Main kit;

public:
  t_main();
};


class t_gtwinkle
  : public t_userintf, public t_main, public Gtk::Window
{
  static const unsigned int LINES = 2;

private:
  Gtk::Box box_;
  Gtk::Label user_agent_label_;
  Gtk::ComboBoxText user_agent_;
  Gtk::Grid line_grid_;
  Gtk::ToggleButton mute_button_;
  sigc::connection mute_conn_;
  Gtk::Button conference_button_;
  sigc::connection conference_conn_;
  Gtk::Grid button_grid_;
  Gtk::RadioButtonGroup line_group_;

  struct Line_Widget
  {
    Line_Widget(t_gtwinkle &gtwinkle, unsigned int line, const char *text)
      : line_(line)
      , line_label_(gtwinkle.line_group_, text, Gtk::ALIGN_START)
      , uri_(true)
      , dial_hangup_("Dial")
    {
      uri_.set_sensitive(true);
      dial_hangup_.set_sensitive(false);
      update_button(gtwinkle, &t_gtwinkle::on_dial);

      line_conn_ = line_label_.signal_clicked().connect(sigc::bind(sigc::mem_fun(gtwinkle, &t_gtwinkle::on_switch_line), line_));
    }

    void update_button(t_gtwinkle &gtwinkle,
		       void (t_gtwinkle::*ptr) (unsigned int))
    {
      dial_hangup_conn_.disconnect();
      dial_hangup_conn_ = dial_hangup_.signal_clicked().connect(sigc::bind(sigc::mem_fun(gtwinkle, ptr), line_));
    }

    void update_button(t_gtwinkle &gtwinkle,
		       const char *text,
		       bool uri_sensitive, bool button_sensitive,
		       void (t_gtwinkle::*ptr) (unsigned int))
    {
      uri_.set_sensitive(uri_sensitive);
      dial_hangup_.set_sensitive(button_sensitive);
      dial_hangup_.set_label(text);
      update_button(gtwinkle, ptr);
    }

    unsigned int const line_;
    Gtk::RadioButton line_label_;
    sigc::connection line_conn_;
    Gtk::ComboBoxText uri_;
    Gtk::Button dial_hangup_;
    sigc::connection dial_hangup_conn_;
  } line_widgets_[LINES];

  Gtk::Grid num_pad_;

  Glib::RefPtr<Gtk::UIManager> ui_manager_;
  Glib::RefPtr<Gtk::ActionGroup> action_group_;
  Glib::RefPtr<Gtk::StatusIcon> status_icon_;

  Glib::Dispatcher cb_dispatcher_;

  std::deque<std::function<void ()> > cb_queue_;
  std::mutex cb_queue_sync_;

private:
  void populate_history_dropdown();
  void on_outgoing_call(unsigned int line, t_url const &uri);
  void on_call_ended(unsigned int line);

  void on_menu_about();
  void on_menu_accounts();
  void on_menu_hangup();
  void on_menu_preferences();
  void on_menu_quit();

  void on_icon_activate();
  void on_icon_popup_menu(guint, guint32);

  void on_user_agent_changed();

  void on_switch_line(unsigned int line);

  void on_dial(unsigned int line);
  void on_hangup(unsigned int line);

  void on_mute();
  void on_conference();

  void on_num_pad(char digit);

  void process_callback();
  void schedule_callback(std::function<void ()> cb);

public:
  explicit t_gtwinkle(t_phone &phone);
  ~t_gtwinkle();

  void action_call_answer(unsigned int line);
  void action_call_reject(unsigned int line);

  virtual void run();

  virtual void cb_outgoing_call(t_user &user_config, unsigned int line,
                                const t_request &r);
  virtual void cb_incoming_call(t_user &user_config, unsigned int line,
                                const t_request &r);
  virtual void cb_notify_call(unsigned int line, std::string profile_name, std::string from_party);

  virtual void cb_call_answered(t_user &user_config, unsigned int line,
                                const t_response &r);
  virtual void cb_call_failed(t_user &user_config, unsigned int line,
                              const t_response &r);
  virtual void cb_call_ended(unsigned int line);
  virtual void cb_call_cancelled(unsigned int line);
  virtual void cb_call_established(unsigned int line);

  virtual void cb_far_end_hung_up(unsigned int line);

  virtual void cb_register_inprog(t_user &user_config,
                                  t_register_type register_type);

  virtual void cb_register_success(t_user &user_config, 
				   const t_response &r, unsigned long expires,
                                   bool first_success);
  virtual void cb_register_failed(t_user &user_config, 
				  const t_response &r, bool first_failure);

  virtual void cb_deregister_success(t_user &user_config, const t_response &r);
  virtual void cb_deregister_failed(t_user &user_config, const t_response &r);

protected:
  virtual void do_quit();
};

#endif
