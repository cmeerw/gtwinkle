/*
  Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>
  Copyright (C) 2012-2015  Christof Meerwald, http://cmeerw.org
    
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
    
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
    
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "gtwinkle.h"
#include "line.h"
#include "util.h"
#include "sockets/url.h"

#include <libnotify/notify.h>

#include <memory>
#include <new>
#include <thread>
#include <unordered_set>


t_main::t_main()
  : kit(true)
{
  notify_init("GTwinkle");
}


t_gtwinkle::t_gtwinkle(t_phone &phone)
  : t_userintf(phone)
  , box_(Gtk::ORIENTATION_VERTICAL)
  , user_agent_label_("Account", Gtk::ALIGN_START)
  , mute_button_("Mute")
  , conference_button_("Conference")
  , line_widgets_{ { *this, 0, "Line 1" }, { *this, 1, "Line 2" } }
{
  set_title("GTwinkle");

  action_group_ = Gtk::ActionGroup::create();
  action_group_->add(Gtk::Action::create("Hangup",
					 Gtk::Stock::CANCEL,
					 "_Hangup", "Hang Up"),
		     sigc::mem_fun(*this, &t_gtwinkle::on_menu_hangup));
  action_group_->add(Gtk::Action::create("Quit",
					 Gtk::Stock::QUIT,
					 "_Quit", "Quit"),
		     sigc::mem_fun(*this, &t_gtwinkle::on_menu_quit));
  action_group_->add(Gtk::Action::create("Accounts",
					 Gtk::Stock::EDIT,
					 "_Accounts", "Accounts"),
		     sigc::mem_fun(*this, &t_gtwinkle::on_menu_accounts));
  action_group_->add(Gtk::Action::create("Preferences",
					 Gtk::Stock::PREFERENCES,
					 "_Preferences", "Preferences"),
		     sigc::mem_fun(*this, &t_gtwinkle::on_menu_preferences));
  action_group_->add(Gtk::Action::create("About",
					 Gtk::Stock::ABOUT,
					 "_About", "About"),
		     sigc::mem_fun(*this, &t_gtwinkle::on_menu_about));
  action_group_->add(Gtk::Action::create("CallMenu", "_Call"));
  action_group_->add(Gtk::Action::create("EditMenu", "_Edit"));
  action_group_->add(Gtk::Action::create("HelpMenu", "_Help"));
  action_group_->add(Gtk::Action::create("Show", "Show"),
		     sigc::mem_fun(*this, &Gtk::Window::show));
  action_group_->add(Gtk::Action::create("Hide", "Hide"),
		     sigc::mem_fun(*this, &Gtk::Window::hide));

  ui_manager_ = Gtk::UIManager::create();
  ui_manager_->insert_action_group(action_group_);
  add_accel_group(ui_manager_->get_accel_group());

  static const Glib::ustring ui_info = 
    "<ui>"
    "  <menubar name='MenuBar'>"
    "    <menu action='CallMenu'>"
    "      <menuitem action='Hangup'/>"
    "      <separator/>"
    "      <menuitem action='Quit'/>"
    "    </menu>"
    "    <menu action='EditMenu'>"
    "      <menuitem action='Accounts'/>"
    "      <separator/>"
    "      <menuitem action='Preferences'/>"
    "    </menu>"
    "    <menu action='HelpMenu'>"
    "      <menuitem action='About'/>"
    "    </menu>"
    "  </menubar>"
    "  <popup name='StatusMenu'>"
    "    <menuitem action='Show'/>"
    "    <menuitem action='Hide'/>"
    "    <separator/>"
    "    <menuitem action='Quit'/>"
    "  </popup>"
    "  <toolbar name='ToolBar'>"
    "    <toolitem action='Quit'/>"
    "    <toolitem action='Preferences'/>"
    "    <toolitem action='Hangup'/>"
    "  </toolbar>"
    "</ui>";

  ui_manager_->add_ui_from_string(ui_info);

  if (Gtk::Widget *menu_bar = ui_manager_->get_widget("/MenuBar"))
  {
    box_.pack_start(*menu_bar, Gtk::PACK_SHRINK);
  }

  if (Gtk::Widget *tool_bar = ui_manager_->get_widget("/ToolBar"))
  {
    box_.pack_start(*tool_bar, Gtk::PACK_SHRINK);
  }

  line_grid_.set_border_width(8);
  line_grid_.set_row_spacing(8);
  line_grid_.set_column_spacing(8);

  line_grid_.attach(user_agent_label_, 0, 0, 1, 1);
  user_agent_.signal_changed().connect(sigc::mem_fun(*this, &t_gtwinkle::on_user_agent_changed));
  line_grid_.attach(user_agent_, 1, 0, 1, 1);

  for (unsigned int i = 0; i < LINES; ++i)
  {
    line_grid_.attach(line_widgets_[i].line_label_, 0, i + 1, 1, 1);
    line_grid_.attach(line_widgets_[i].uri_, 1, i + 1, 1, 1);
    line_grid_.attach(line_widgets_[i].dial_hangup_, 2, i + 1, 1, 1);
  }

  box_.pack_start(line_grid_, Gtk::PACK_SHRINK);

  button_grid_.set_border_width(8);
  button_grid_.set_row_spacing(8);
  button_grid_.set_column_spacing(8);

  mute_button_.signal_toggled().connect(sigc::mem_fun(*this, &t_gtwinkle::on_mute), true);
  button_grid_.attach(mute_button_, 0, 0, 1, 1);

  conference_button_.signal_clicked().connect(sigc::mem_fun(*this, &t_gtwinkle::on_conference));
  button_grid_.attach(conference_button_, 1, 0, 1, 1);

  box_.pack_start(button_grid_, Gtk::PACK_SHRINK);

  num_pad_.set_column_homogeneous(true);
  num_pad_.set_row_homogeneous(true);
  num_pad_.set_border_width(16);
  num_pad_.set_row_spacing(8);
  num_pad_.set_column_spacing(8);

  static const char digits[] = "123456789*0#";
  static const char * const letters[] = {
    NULL, "abc", "def",
    "ghi", "jkl", "mno",
    "pqrs", "tuv", "wxyz",
    NULL, NULL, NULL
  };

  for (size_t pos = 0; pos != sizeof(digits) - 1; ++pos)
  {
    std::unique_ptr<Gtk::Button> b(new Gtk::Button);
    std::unique_ptr<Gtk::Label> l(new Gtk::Label);

    std::ostringstream text;
    text << "<big><b>" << digits[pos] << "</b></big>\n";
    if (letters[pos])
    {
      text << "<small>" << letters[pos] << "</small>";
    }

    l->set_justify(Gtk::JUSTIFY_CENTER);
    l->set_markup(text.str().c_str());
    b->add(*Gtk::manage(l.release()));

    b->signal_clicked().connect(sigc::bind(sigc::mem_fun(*this, &t_gtwinkle::on_num_pad), digits[pos]));
    num_pad_.attach(*Gtk::manage(b.release()), pos % 3, pos / 3, 1, 1);
  }

  box_.pack_start(num_pad_, Gtk::PACK_SHRINK);

  add(box_);

  status_icon_ = Gtk::StatusIcon::create_from_file(DATADIR "/gtwinkle.svg");
  status_icon_->set_tooltip_text("GTwinkle");

  status_icon_->signal_activate().connect(sigc::mem_fun(*this, &t_gtwinkle::on_icon_activate));
  status_icon_->signal_popup_menu().connect(sigc::mem_fun(*this, &t_gtwinkle::on_icon_popup_menu));

  show_all_children();
  show();

  cb_dispatcher_.connect(sigc::bind(sigc::mem_fun(&t_gtwinkle::process_callback), this));
}

t_gtwinkle::~t_gtwinkle()
{ }

void t_gtwinkle::run()
{
  auto user_list (phone.ref_users());
  for (auto elem : user_list)
  {
    user_agent_.append(elem->get_profile_name());
  }

  if (!user_list.empty())
  {
    user_agent_.set_active(0);
    line_widgets_[0].dial_hangup_.set_sensitive(true);
  }

  populate_history_dropdown();

  // Initialize phone functions
  phone.init();

  kit.run();

  phone.terminate();
}

namespace
{
  std::unordered_set<std::string> &unique_uris()
  {
    static std::unordered_set<std::string> obj;
    return obj;
  }
}

void t_gtwinkle::populate_history_dropdown()
{
  std::deque<t_call_record> history { call_history->get_history() };

  line_widgets_[0].uri_.remove_all();
  line_widgets_[1].uri_.remove_all();
  std::for_each(history.rbegin(), history.rend(),
		[this] (t_call_record const &record) {
		  if (record.direction == t_call_record::DIR_OUT)
		  {
		    const std::string &encoded_uri { record.to_uri.encode_no_params_hdrs() };
		    if (unique_uris().insert(encoded_uri).second)
		    {
		      line_widgets_[0].uri_.append(encoded_uri);
		      line_widgets_[1].uri_.append(encoded_uri);
		    }
		  }
		});
}

void t_gtwinkle::on_call_ended(unsigned int line)
{
  schedule_callback([this, line] () {
      line_widgets_[line].update_button(*this, "Dial", true,
					user_agent_.get_active_row_number() >= 0,
					&t_gtwinkle::on_dial);
    });
}

void t_gtwinkle::on_outgoing_call(unsigned int line, t_url const & uri)
{
  const std::string &encoded_uri { uri.encode_no_params_hdrs() };

  schedule_callback([this, line, encoded_uri] () {
      if (unique_uris().insert(encoded_uri).second)
      {
	line_widgets_[0].uri_.prepend(encoded_uri);
	line_widgets_[1].uri_.prepend(encoded_uri);
      }

      line_widgets_[line].update_button(*this, "Hangup", false, true,
					&t_gtwinkle::on_hangup);
    });
}

void t_gtwinkle::on_menu_about()
{
  Gtk::AboutDialog about;

  about.set_title(PRODUCT_NAME);
  about.set_program_name(PRODUCT_NAME);
  about.set_version(PRODUCT_VERSION);
  about.set_authors({ Glib::ustring(PRODUCT_AUTHOR) });
  about.set_website("https://gtwinkle.cmeerw.org");
  about.set_website_label("https://gtwinkle.cmeerw.org");
  about.set_logo(Gdk::Pixbuf::create_from_file(DATADIR "/gtwinkle.svg"));

  about.show();
  about.run();
}

void t_gtwinkle::on_menu_accounts()
{
  Gtk::Dialog dlg;

  dlg.set_title("Accounts");

  auto * dlg_box (dlg.get_content_area());

  dlg.add_button("Close", 0);

  dlg.show_all_children();
  dlg.show();
  dlg.run();
}

void t_gtwinkle::on_menu_hangup()
{
}

void t_gtwinkle::on_menu_preferences()
{
  Gtk::Dialog dlg;

  dlg.set_title("Preferences");

  auto * dlg_box (dlg.get_content_area());
  Gtk::Notebook notebook;

  Gtk::Box box_general(Gtk::ORIENTATION_VERTICAL, 6);
  Gtk::Frame frame_ab;
  frame_ab.set_label("Address book");
  Gtk::Box box_ab(Gtk::ORIENTATION_VERTICAL, 6);

  Gtk::Box box_ab_lookup(Gtk::ORIENTATION_HORIZONTAL);
  Gtk::Label lbl_ab_lookup("Lookup", Gtk::ALIGN_START);
  box_ab_lookup.pack_start(lbl_ab_lookup, Gtk::PACK_SHRINK);
  Gtk::Switch sw_ab_lookup;
  box_ab_lookup.pack_end(sw_ab_lookup, Gtk::PACK_SHRINK);
  box_ab.add(box_ab_lookup);

  Gtk::Box box_ab_override(Gtk::ORIENTATION_HORIZONTAL);
  Gtk::Label lbl_ab_override("Override", Gtk::ALIGN_START);
  box_ab_override.pack_start(lbl_ab_override, Gtk::PACK_SHRINK);
  Gtk::Switch sw_ab_override;
  box_ab_override.pack_end(sw_ab_override, Gtk::PACK_SHRINK);
  box_ab.add(box_ab_override);

  frame_ab.add(box_ab);
  box_general.add(frame_ab);

  Gtk::Frame frame_call;
  frame_call.set_label("Call");
  Gtk::Box box_call(Gtk::ORIENTATION_VERTICAL, 6);

  Gtk::Box box_call_waiting(Gtk::ORIENTATION_HORIZONTAL);
  Gtk::Label lbl_call_waiting("Call waiting", Gtk::ALIGN_START);
  box_call_waiting.pack_start(lbl_call_waiting, Gtk::PACK_SHRINK);
  Gtk::Switch sw_call_waiting;
  box_call_waiting.pack_end(sw_call_waiting, Gtk::PACK_SHRINK);
  box_call.add(box_call_waiting);

  Gtk::Box box_call_override(Gtk::ORIENTATION_HORIZONTAL);
  Gtk::Label lbl_hangup_both_3way("Hangup both 3-way", Gtk::ALIGN_START);
  box_call_override.pack_start(lbl_hangup_both_3way, Gtk::PACK_SHRINK);
  Gtk::Switch sw_hangup_both_3way;
  box_call_override.pack_end(sw_hangup_both_3way, Gtk::PACK_SHRINK);
  box_call.add(box_call_override);

  frame_call.add(box_call);
  box_general.add(frame_call);

  Gtk::Frame frame_ringtone;
  frame_ringtone.set_label("Ringtone");
  Gtk::Box box_ringtone(Gtk::ORIENTATION_VERTICAL, 6);

  Gtk::Box box_play_ringtone(Gtk::ORIENTATION_HORIZONTAL);
  Gtk::Label lbl_play_ringtone("Play ringtone", Gtk::ALIGN_START);
  box_play_ringtone.pack_start(lbl_play_ringtone, Gtk::PACK_SHRINK);
  Gtk::Switch sw_play_ringtone;
  box_play_ringtone.pack_end(sw_play_ringtone, Gtk::PACK_SHRINK);
  box_ringtone.add(box_play_ringtone);

  Gtk::CheckButton check_ringtone_file("Ringtone file");
  box_ringtone.add(check_ringtone_file);

  Gtk::FileChooserButton file_ringtone_file;
  box_ringtone.add(file_ringtone_file);

  Gtk::Box box_play_ringback(Gtk::ORIENTATION_HORIZONTAL);
  Gtk::Label lbl_play_ringback("Play ringback", Gtk::ALIGN_START);
  box_play_ringback.pack_start(lbl_play_ringback, Gtk::PACK_SHRINK);
  Gtk::Switch sw_play_ringback;
  box_play_ringback.pack_end(sw_play_ringback, Gtk::PACK_SHRINK);
  box_ringtone.add(box_play_ringback);

  Gtk::CheckButton check_ringback_file("Ringback file");
  box_ringtone.add(check_ringback_file);

  Gtk::FileChooserButton file_ringback_file;
  box_ringtone.add(file_ringback_file);

  frame_ringtone.add(box_ringtone);
  box_general.add(frame_ringtone);

  notebook.append_page(box_general, "General");

  Gtk::Box box_network(Gtk::ORIENTATION_VERTICAL, 6);
  Gtk::Box box_ipv6(Gtk::ORIENTATION_HORIZONTAL);
  Gtk::Label lbl_ipv6("IPv6", Gtk::ALIGN_START);
  box_ipv6.pack_start(lbl_ipv6, Gtk::PACK_SHRINK);
  Gtk::Switch sw_ipv6;
  box_ipv6.pack_end(sw_ipv6, Gtk::PACK_SHRINK);
  box_network.add(box_ipv6);

  Gtk::Box box_sip_port(Gtk::ORIENTATION_HORIZONTAL);
  Gtk::Label lbl_sip_port("SIP port", Gtk::ALIGN_START);
  box_sip_port.pack_start(lbl_sip_port, Gtk::PACK_SHRINK);
  Gtk::Entry entry_sip_port;
  entry_sip_port.set_alignment(Gtk::ALIGN_END);
  entry_sip_port.set_max_length(5);
  entry_sip_port.set_width_chars(5);
  box_sip_port.pack_end(entry_sip_port, Gtk::PACK_SHRINK);
  box_network.add(box_sip_port);

  Gtk::Box box_rtp_port(Gtk::ORIENTATION_HORIZONTAL);
  Gtk::Label lbl_rtp_port("RTP port", Gtk::ALIGN_START);
  box_rtp_port.pack_start(lbl_rtp_port, Gtk::PACK_SHRINK);
  Gtk::Entry entry_rtp_port;
  entry_rtp_port.set_alignment(Gtk::ALIGN_END);
  entry_rtp_port.set_max_length(5);
  entry_rtp_port.set_width_chars(5);
  box_rtp_port.pack_end(entry_rtp_port, Gtk::PACK_SHRINK);
  box_network.add(box_rtp_port);

  Gtk::Box box_max_udp_size(Gtk::ORIENTATION_HORIZONTAL);
  Gtk::Label lbl_max_udp_size("Max UDP size", Gtk::ALIGN_START);
  box_max_udp_size.pack_start(lbl_max_udp_size, Gtk::PACK_SHRINK);
  Gtk::Entry entry_max_udp_size;
  entry_max_udp_size.set_alignment(Gtk::ALIGN_END);
  entry_max_udp_size.set_max_length(7);
  entry_max_udp_size.set_width_chars(7);
  box_max_udp_size.pack_end(entry_max_udp_size, Gtk::PACK_SHRINK);
  box_network.add(box_max_udp_size);

  Gtk::Box box_max_tcp_size(Gtk::ORIENTATION_HORIZONTAL);
  Gtk::Label lbl_max_tcp_size("Max TCP size", Gtk::ALIGN_START);
  box_max_tcp_size.pack_start(lbl_max_tcp_size, Gtk::PACK_SHRINK);
  Gtk::Entry entry_max_tcp_size;
  entry_max_tcp_size.set_alignment(Gtk::ALIGN_END);
  entry_max_tcp_size.set_max_length(7);
  entry_max_tcp_size.set_width_chars(7);
  box_max_tcp_size.pack_end(entry_max_tcp_size, Gtk::PACK_SHRINK);
  box_network.add(box_max_tcp_size);

  notebook.append_page(box_network, "Network");

  Gtk::Box box_audio(Gtk::ORIENTATION_VERTICAL, 6);

  Gtk::Box box_dev_speaker(Gtk::ORIENTATION_HORIZONTAL);
  Gtk::Label lbl_dev_speaker("Speaker device", Gtk::ALIGN_START);
  box_dev_speaker.pack_start(lbl_dev_speaker, Gtk::PACK_SHRINK);
  Gtk::ComboBoxText entry_dev_speaker(true);
  box_dev_speaker.pack_end(entry_dev_speaker, Gtk::PACK_SHRINK);
  box_audio.add(box_dev_speaker);

  Gtk::Box box_dev_mic(Gtk::ORIENTATION_HORIZONTAL);
  Gtk::Label lbl_dev_mic("Microphone device", Gtk::ALIGN_START);
  box_dev_mic.pack_start(lbl_dev_mic, Gtk::PACK_SHRINK);
  Gtk::ComboBoxText entry_dev_mic(true);
  box_dev_mic.pack_end(entry_dev_mic, Gtk::PACK_SHRINK);
  box_audio.add(box_dev_mic);

  Gtk::Box box_validate_dev(Gtk::ORIENTATION_HORIZONTAL);
  Gtk::Label lbl_validate_dev("Validate audio devices", Gtk::ALIGN_START);
  box_validate_dev.pack_start(lbl_validate_dev, Gtk::PACK_SHRINK);
  Gtk::Switch sw_validate_dev;
  box_validate_dev.pack_end(sw_validate_dev, Gtk::PACK_SHRINK);
  box_audio.add(box_validate_dev);

  Gtk::Box box_play_period_size(Gtk::ORIENTATION_HORIZONTAL);
  Gtk::Label lbl_play_period_size("Play period size", Gtk::ALIGN_START);
  box_play_period_size.pack_start(lbl_play_period_size, Gtk::PACK_SHRINK);
  Gtk::Entry entry_play_period_size;
  entry_play_period_size.set_alignment(Gtk::ALIGN_END);
  entry_play_period_size.set_max_length(3);
  entry_play_period_size.set_width_chars(3);
  box_play_period_size.pack_end(entry_play_period_size, Gtk::PACK_SHRINK);
  box_audio.add(box_play_period_size);

  Gtk::Box box_capture_period_size(Gtk::ORIENTATION_HORIZONTAL);
  Gtk::Label lbl_capture_period_size("Capture period size", Gtk::ALIGN_START);
  box_capture_period_size.pack_start(lbl_capture_period_size, Gtk::PACK_SHRINK);
  Gtk::Entry entry_capture_period_size;
  entry_capture_period_size.set_alignment(Gtk::ALIGN_END);
  entry_capture_period_size.set_max_length(3);
  entry_capture_period_size.set_width_chars(3);
  box_capture_period_size.pack_end(entry_capture_period_size, Gtk::PACK_SHRINK);
  box_audio.add(box_capture_period_size);

  notebook.append_page(box_audio, "Audio");

  Gtk::Box box_advanced(Gtk::ORIENTATION_VERTICAL, 6);

  Gtk::Frame frame_log;
  frame_log.set_label("Logging");
  Gtk::Box box_log(Gtk::ORIENTATION_VERTICAL, 6);

  Gtk::Box box_log_max_size(Gtk::ORIENTATION_HORIZONTAL);
  Gtk::Label lbl_log_max_size("Maximum size", Gtk::ALIGN_START);
  box_log_max_size.pack_start(lbl_log_max_size, Gtk::PACK_SHRINK);
  Gtk::Entry entry_log_max_size;
  entry_log_max_size.set_alignment(Gtk::ALIGN_END);
  entry_log_max_size.set_max_length(3);
  entry_log_max_size.set_width_chars(3);
  box_log_max_size.pack_end(entry_log_max_size, Gtk::PACK_SHRINK);
  box_log.add(box_log_max_size);

  Gtk::Box box_log_show_sip(Gtk::ORIENTATION_HORIZONTAL);
  Gtk::Label lbl_log_show_sip("Show SIP", Gtk::ALIGN_START);
  box_log_show_sip.pack_start(lbl_log_show_sip, Gtk::PACK_SHRINK);
  Gtk::Switch sw_log_show_sip;
  box_log_show_sip.pack_end(sw_log_show_sip, Gtk::PACK_SHRINK);
  box_log.add(box_log_show_sip);

  Gtk::Box box_log_show_stun(Gtk::ORIENTATION_HORIZONTAL);
  Gtk::Label lbl_log_show_stun("Show STUN", Gtk::ALIGN_START);
  box_log_show_stun.pack_start(lbl_log_show_stun, Gtk::PACK_SHRINK);
  Gtk::Switch sw_log_show_stun;
  box_log_show_stun.pack_end(sw_log_show_stun, Gtk::PACK_SHRINK);
  box_log.add(box_log_show_stun);

  Gtk::Box box_log_show_memory(Gtk::ORIENTATION_HORIZONTAL);
  Gtk::Label lbl_log_show_memory("Show memory", Gtk::ALIGN_START);
  box_log_show_memory.pack_start(lbl_log_show_memory, Gtk::PACK_SHRINK);
  Gtk::Switch sw_log_show_memory;
  box_log_show_memory.pack_end(sw_log_show_memory, Gtk::PACK_SHRINK);
  box_log.add(box_log_show_memory);

  Gtk::Box box_log_show_debug(Gtk::ORIENTATION_HORIZONTAL);
  Gtk::Label lbl_log_show_debug("Show debug", Gtk::ALIGN_START);
  box_log_show_debug.pack_start(lbl_log_show_debug, Gtk::PACK_SHRINK);
  Gtk::Switch sw_log_show_debug;
  box_log_show_debug.pack_end(sw_log_show_debug, Gtk::PACK_SHRINK);
  box_log.add(box_log_show_debug);
  frame_log.add(box_log);

  box_advanced.add(frame_log);
  notebook.append_page(box_advanced, "Advanced");

  dlg_box->pack_start(notebook, Gtk::PACK_SHRINK);

  dlg.add_button("Close", 0);

  dlg.show_all_children();
  dlg.show();
  dlg.run();
}

void t_gtwinkle::on_menu_quit()
{
  hide();
  Gtk::Main::quit();
}

void t_gtwinkle::on_icon_activate()
{
  set_visible(!get_visible());
}

void t_gtwinkle::on_icon_popup_menu(guint button, guint32 activate_time)
{
  if (Gtk::Menu *menu = static_cast<Gtk::Menu *>(ui_manager_->get_widget("/StatusMenu")))
  {
    menu->popup(button, activate_time);
  }
}

void t_gtwinkle::on_user_agent_changed()
{
}

void t_gtwinkle::on_switch_line(unsigned int line)
{
  for (unsigned int i = 0 ; i < LINES; ++i)
  {
    line_widgets_[i].dial_hangup_.set_sensitive((i == line) &&
						(user_agent_.get_active_row_number() >= 0));
  }

  phone.pub_activate_line(line);
}

void t_gtwinkle::on_dial(unsigned int line)
{
  t_user * const user (phone.ref_user_profile(user_agent_.get_active_text()));
  phone.pub_invite(user, t_url(line_widgets_[line].uri_.get_active_text()),
		   "", "", false);
}

void t_gtwinkle::on_hangup(unsigned int line)
{
  phone.pub_end_call();
}

void t_gtwinkle::on_mute()
{
  phone.mute(mute_button_.get_active());
}

void t_gtwinkle::on_conference()
{
  phone.join_3way(0, 1);
}

void t_gtwinkle::on_num_pad(char digit)
{
  const t_call_info call_info = phone.get_call_info(phone.get_active_line());
  if (call_info.dtmf_supported)
  {
    phone.pub_send_dtmf(digit, call_info.dtmf_inband, call_info.dtmf_info);
  }
}


void t_gtwinkle::action_call_answer(unsigned int line)
{
  do_answer(line);
}

void t_gtwinkle::action_call_reject(unsigned int line)
{
  do_reject(line);
}


void t_gtwinkle::schedule_callback(std::function<void ()> cb)
{
  {
    std::lock_guard<std::mutex> guard(cb_queue_sync_);
    cb_queue_.push_back(std::move(cb));
  }

  cb_dispatcher_();
}

void t_gtwinkle::process_callback()
{
  std::unique_lock<std::mutex> guard(cb_queue_sync_);

  while (!cb_queue_.empty())
  {
    std::function<void ()> cb(std::move(cb_queue_.front()));
    cb_queue_.pop_front();

    guard.unlock();
    cb ();
    guard.lock();
  }
}

void t_gtwinkle::cb_outgoing_call(t_user &user_config, unsigned int line,
				  const t_request &r)
{
  t_userintf::cb_outgoing_call(user_config, line, r);

  on_outgoing_call(line, r.uri);
}

void t_gtwinkle::cb_incoming_call(t_user &user_config, unsigned int line,
				  const t_request &r)
{
  t_userintf::cb_incoming_call(user_config, line, r);
}


namespace
{
  struct Notify_Callback_Data
  {
    t_gtwinkle &self;
    unsigned int line;
  };

  void callback_data_free(void *arg)
  {
    delete static_cast <Notify_Callback_Data *> (arg);
  }

  extern "C" void call_answer_cb(NotifyNotification *notify, char *action,
				 void *arg)
  {
    Notify_Callback_Data *data (static_cast<Notify_Callback_Data *>(arg));
    data->self.action_call_answer(data->line);

    GError *error (nullptr);
    notify_notification_close(notify, &error);
  }

  extern "C" void call_reject_cb(NotifyNotification *notify, char *action,
				 void *arg)
  {
    Notify_Callback_Data *data (static_cast<Notify_Callback_Data *>(arg));
    data->self.action_call_reject(data->line);

    GError *error (nullptr);
    notify_notification_close(notify, &error);
  }
}

void t_gtwinkle::cb_notify_call(unsigned int line, std::string profile_name, std::string from_party)
{
  t_userintf::cb_notify_call(line, profile_name, from_party);

  std::string body{ escape_html(std::move(from_party), true) };

  schedule_callback([this, line, profile_name = std::move(profile_name), body = std::move(body)] () {
      GError *error (nullptr);
      std::string summary{ "Incoming Call: " };
      summary += profile_name;

      NotifyNotification *notify (
	notify_notification_new(summary.c_str(), body.c_str(), nullptr));

      notify_notification_add_action(notify, "answer", "Answer",
				     &call_answer_cb,
				     new Notify_Callback_Data { *this, line },
				     callback_data_free);
      notify_notification_add_action(notify, "reject", "Reject",
				     &call_reject_cb,
				     new Notify_Callback_Data { *this, line },
				     callback_data_free);

      notify_notification_show(notify, &error);
    });
}

void t_gtwinkle::cb_call_answered(t_user &user_config, unsigned int line,
				  const t_response &r)
{
}

void t_gtwinkle::cb_call_failed(t_user &user_config, unsigned int line,
				const t_response &r)
{
  if (line < LINES)
  {
    on_call_ended(line);
  }
}

void t_gtwinkle::cb_call_ended(unsigned int line)
{
  if (line < LINES)
  {
    on_call_ended(line);
  }
}

void t_gtwinkle::cb_call_cancelled(unsigned int line)
{
  if (line < LINES)
  {
    t_userintf::cb_call_cancelled(line);
    on_call_ended(line);
  }
}

void t_gtwinkle::cb_far_end_hung_up(unsigned int line)
{
  on_call_ended(line);
}

void t_gtwinkle::cb_call_established(unsigned int line)
{
  schedule_callback([this, line] () {
      line_widgets_[line].update_button(*this, "Hangup", false, true,
					&t_gtwinkle::on_hangup);
    });
}

void t_gtwinkle::cb_register_inprog(t_user &user_config,
				    t_register_type register_type)
{
}

void t_gtwinkle::cb_register_success(t_user &user_config, const t_response &r,
				     unsigned long expires, bool first_success)
{
}

void t_gtwinkle::cb_register_failed(t_user &user_config, const t_response &r,
				    bool first_failure)
{
}

void t_gtwinkle::cb_deregister_success(t_user &user_config, const t_response &r)
{
}

void t_gtwinkle::cb_deregister_failed(t_user &user_config, const t_response &r)
{
}

void t_gtwinkle::do_quit()
{
  hide();
  Gtk::Main::quit();
}
