/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "pidf_body.h"

#include <cassert>
#include <libxml/parser.h>

#include "log.h"
#include "util.h"

#define PIDF_XML_VERSION	"1.0"
#define PIDF_NAMESPACE		"urn:ietf:params:xml:ns:pidf"

#define IS_PIDF_TAG(node, tag)	IS_XML_TAG(node, tag, PIDF_NAMESPACE)

#define IS_PIDF_ATTR(attr, attr_name) IS_XML_ATTR(attr, attr_name, PIDF_NAMESPACE)

bool t_pidf_xml_body::extract_status()
{
  assert(xml_doc);

  // Get root
  if (xmlNode * const root_element = xmlDocGetRootElement(xml_doc.get()))
  {
    // Check if root is <presence>
    if (!IS_PIDF_TAG(root_element, "presence"))
    {
      log_file->write_report("PIDF document has invalid root element.",
	  "t_pidf_xml_body::extract_status",
	  LOG_NORMAL, LOG_WARNING);
      return false;
    }

    pres_entity.clear();
    tuple_id.clear();
    basic_status.clear();

    // Get presence entity
    if (xmlChar * const prop_entity = xmlGetProp(root_element, BAD_CAST "entity"))
    {
      pres_entity = reinterpret_cast<char *>(prop_entity);
    }
    else
    {
      log_file->write_report("Presence entity is missing.",
	  "t_pidf_xml_body::extract_status",
	  LOG_NORMAL, LOG_WARNING);
    }

    xmlNode *child = root_element->children;

    // Process children of root.
    for (xmlNode *cur_node = child; cur_node; cur_node = cur_node->next)
    {
      // Process tuple
      if (IS_PIDF_TAG(cur_node, "tuple"))
      {
	process_pidf_tuple(cur_node);

	// Process the first tuple and then stop.
	// Currently there is no support for multiple tuples
	// or additional elements.
	break;
      }
    }
  }
  else
  {
    log_file->write_report("PIDF document has no root element.",
	"t_pidf_xml_body::extract_status",
	LOG_NORMAL, LOG_WARNING);
    return false;
  }

  return true;
}

void t_pidf_xml_body::process_pidf_tuple(xmlNode *tuple)
{
  assert(tuple);

  // Get tuple id.
  xmlChar *id = xmlGetProp(tuple, BAD_CAST "id");
  if (id)
  {
    tuple_id = (char *)id;
  }
  else
  {
    log_file->write_report("Tuple id is missing.",
			   "t_pidf_xml_body::process_pidf_tuple",
			   LOG_NORMAL, LOG_WARNING);
  }

  // Find status element
  xmlNode *child = tuple->children;
  for (xmlNode *cur_node = child; cur_node; cur_node = cur_node->next)
  {
    // Process status
    if (IS_PIDF_TAG(cur_node, "status"))
    {
      process_pidf_status(cur_node);
      break;
    }
  }
}

void t_pidf_xml_body::process_pidf_status(xmlNode *status)
{
  assert(status);

  xmlNode *child = status->children;
  for (xmlNode *cur_node = child; cur_node; cur_node = cur_node->next)
  {
    // Process status
    if (IS_PIDF_TAG(cur_node, "basic"))
    {
      process_pidf_basic(cur_node);
      break;
    }
  }
}

void t_pidf_xml_body::process_pidf_basic(xmlNode *basic)
{
  assert(basic);

  xmlNode *child = basic->children;
  if (child && child->type == XML_TEXT_NODE)
  {
    basic_status = tolower((char*)child->content);
  }
  else
  {
    log_file->write_report("<basic> element has no content.",
			   "t_pidf_xml_body::process_pidf_basic",
			   LOG_NORMAL, LOG_WARNING);
  }
}

void t_pidf_xml_body::create_xml_doc(const std::string &xml_version, const std::string &charset)
{
  t_sip_body_xml::create_xml_doc(xml_version, charset);

  // presence
  xmlNode *node_presence = xmlNewNode(nullptr, BAD_CAST "presence");
  xmlNs *ns_pidf = xmlNewNs(node_presence, BAD_CAST PIDF_NAMESPACE, nullptr);
  xmlNewProp(node_presence, BAD_CAST "entity", BAD_CAST pres_entity.c_str());
  xmlDocSetRootElement(xml_doc.get(), node_presence);

  // tuple
  xmlNode *node_tuple = xmlNewChild(node_presence, ns_pidf, BAD_CAST "tuple", nullptr);
  xmlNewProp(node_tuple, BAD_CAST "id", BAD_CAST tuple_id.c_str());

  // status
  xmlNode *node_status = xmlNewChild(node_tuple, ns_pidf, BAD_CAST "status", nullptr);

  // basic
  xmlNewChild(node_status, ns_pidf,
	      BAD_CAST "basic", BAD_CAST basic_status.c_str());
}

t_pidf_xml_body::t_pidf_xml_body() = default;

t_pidf_xml_body *t_pidf_xml_body::copy() const
{
  std::unique_ptr<t_pidf_xml_body> body(new t_pidf_xml_body(*this));
  return body.release();
}

t_body_type t_pidf_xml_body::get_type() const
{
  return BODY_PIDF_XML;
}

t_media t_pidf_xml_body::get_media() const
{
  return t_media("application", "pidf+xml");
}

std::string t_pidf_xml_body::get_pres_entity() const
{
  return pres_entity;
}

std::string t_pidf_xml_body::get_tuple_id() const
{
  return tuple_id;
}

std::string t_pidf_xml_body::get_basic_status() const
{
  return basic_status;
}

void t_pidf_xml_body::set_pres_entity(const std::string &_pres_entity)
{
  xml_doc.reset();
  pres_entity = _pres_entity;
}

void t_pidf_xml_body::set_tuple_id(const std::string &_tuple_id)
{
  xml_doc.reset();
  tuple_id = _tuple_id;
}

void t_pidf_xml_body::set_basic_status(const std::string &_basic_status)
{
  xml_doc.reset();
  basic_status = _basic_status;
}

bool t_pidf_xml_body::parse(const std::string &s)
{
  if (t_sip_body_xml::parse(s))
  {
    if (!extract_status())
    {
      xml_doc.reset();
    }
  }

  return static_cast<bool>(xml_doc);
}
