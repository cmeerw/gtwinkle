/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "mwi_dialog.h"

#include "mwi_subscription.h"
#include "phone_user.h"


t_mwi_dialog::t_mwi_dialog(t_phone &_phone, t_phone_user &_phone_user)
  : t_subscription_dialog(_phone, _phone_user)
{
  subscription = std::make_unique<t_mwi_subscription>(*this, phone_user.mwi);
}

t_mwi_dialog *t_mwi_dialog::copy()
{
  // Copy is not needed.
  assert(false);
  return nullptr;
}
