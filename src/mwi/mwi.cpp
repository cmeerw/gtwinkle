/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "mwi.h"

t_mwi::t_mwi()
  : status(MWI_UNKNOWN)
{ }

t_mwi::t_mwi(t_mwi const &other) = default;

t_mwi &t_mwi::operator =(t_mwi const &other) & = default;

t_mwi::t_status t_mwi::get_status() const
{
  std::lock_guard<std::mutex> guard(mtx_mwi);
  return status;
}

bool t_mwi::get_msg_waiting() const
{
  std::lock_guard<std::mutex> guard(mtx_mwi);
  return msg_waiting;
}

t_msg_summary t_mwi::get_voice_msg_summary() const
{
  std::lock_guard<std::mutex> guard(mtx_mwi);
  return voice_msg_summary;
}

void t_mwi::set_status(t_status _status)
{
  std::lock_guard<std::mutex> guard(mtx_mwi);
  status = _status;
}

void t_mwi::set_msg_waiting(bool _msg_waiting)
{
  std::lock_guard<std::mutex> guard(mtx_mwi);
  msg_waiting = _msg_waiting;
}

void t_mwi::set_voice_msg_summary(const t_msg_summary &summary)
{
  std::lock_guard<std::mutex> guard(mtx_mwi);
  voice_msg_summary = summary;
}

void t_mwi::clear_voice_msg_summary()
{
  std::lock_guard<std::mutex> guard(mtx_mwi);
  voice_msg_summary.clear();
}
