/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <cassert>
#include <iostream>
#include <csignal>
#include "exceptions.h"
#include "line.h"
#include "log.h"
#include "sdp/sdp.h"
#include "util.h"
#include "user.h"
#include "userintf.h"

#include <algorithm>

extern std::unique_ptr<t_event_queue> evq_timekeeper;

///////////////
// t_call_info
///////////////

t_call_info::t_call_info()
{
  clear();
}

void t_call_info::clear()
{
  from_uri.set_url("");
  from_display.clear();
  from_display_override.clear();
  from_organization.clear();
  to_uri.set_url("");
  to_display.clear();
  to_organization.clear();
  subject.clear();
  dtmf_supported = false;
  hdr_referred_by = t_hdr_referred_by();
  last_provisional_reason.clear();
  send_codec = CODEC_NULL;
  recv_codec = CODEC_NULL;
  refer_supported = false;
}

std::string t_call_info::get_from_display_presentation() const
{
  if (from_display_override.empty())
  {
    return from_display;
  }
  else
  {
    return from_display_override;
  }
}


///////////
// t_line
///////////

///////////
// Private
///////////

t_line::dialogs_t::iterator t_line::match_response(
  t_response &r, dialogs_t &l) const
{
  return std::find_if(l.begin(), l.end(),
      [&r] (const std::unique_ptr<t_dialog> &elem) {
	return elem->match_response(r, 0);
      });
}

t_line::dialogs_t::const_iterator t_line::match_response(
  t_response &r, dialogs_t const &l) const
{
  return std::find_if(l.begin(), l.end(),
      [&r] (const std::unique_ptr<t_dialog> &elem) {
	return elem->match_response(r, 0);
      });
}

t_line::dialogs_t::iterator t_line::match_response(
  StunMessage &r, t_tuid tuid, dialogs_t &l) const
{
  return std::find_if(l.begin(), l.end(),
      [&r, &tuid] (std::unique_ptr<t_dialog> const & elem) {
	return elem->match_response(r, tuid);
      });
}

t_line::dialogs_t::const_iterator t_line::match_response(
  StunMessage &r, t_tuid tuid, dialogs_t const &l) const
{
  return std::find_if(l.begin(), l.end(),
      [&r, &tuid] (const std::unique_ptr<t_dialog> &elem) {
	return elem->match_response(r, tuid);
      });
}

t_dialog *t_line::match_call_id_tags(
  const std::string &call_id, const std::string &to_tag,
  const std::string &from_tag, const dialogs_t &l) const
{
  auto iter(std::find_if(l.begin(), l.end(),
	  [&call_id, &to_tag, &from_tag] (const std::unique_ptr<t_dialog> &elem) {
	    return elem->match(call_id, to_tag, from_tag);
	  }));

  return (iter != l.end()) ? iter->get() : nullptr;
}

t_dialog *t_line::get_dialog(t_object_id did) const
{
  if (did == 0) return nullptr;

  if (open_dialog && open_dialog->get_object_id() == did)
  {
    return open_dialog.get();
  }

  if (active_dialog && active_dialog->get_object_id() == did)
  {
    return active_dialog.get();
  }

  for (auto const & elem : pending_dialogs)
  {
    if (elem->get_object_id() == did) return elem.get();
  }

  for (auto const & elem : dying_dialogs)
  {
    if (elem->get_object_id() == did) return elem.get();
  }

  return nullptr;
}

void t_line::cleanup()
{
  if (open_dialog && open_dialog->get_state() == DS_TERMINATED)
  {
    open_dialog.reset();
  }

  if (active_dialog && active_dialog->get_state() == DS_TERMINATED)
  {
    active_dialog.reset();

    stop_timer(LTMR_INVITE_COMP);
    stop_timer(LTMR_NO_ANSWER);

    // If the call has been ended within 64*T1 seconds
    // after the reception of the first 2XX response, there
    // might still be open and pending dialogs. To be nice these
    // dialogs should be kept till the 64*T1 timer expires.
    // This complicates the setup of new call however. For
    // now the dialogs will be killed. If a slow UAS
    // still responds, it has bad luck and will time out.
    //
    // TODO:
    // A nice solution would be to move the pending and open
    // dialog to the dying dialog and start a new time 64*T1
    // timer to keep the dying dialogs alive. A sequence of
    // a few short calls would add to the dying dialogs and
    // keep some dialogs alive longer than necessary. This
    // only has an impact on resources, not on signalling.
    // Note that the open dialog must be appended after the
    // pending dialogs, otherwise all received responses for
    // a pending dialog will match the open dialog if that
    // match is tried first by match_response()
    pending_dialogs.clear();

    open_dialog.reset();
  }

  if (active_dialog)
  {
    if (active_dialog->get_state() == DS_CONFIRMED_SUB)
    {
      // The calls have been released but a subscription is
      // still active.
      substate = LSSUB_RELEASING;
    }
    else if (active_dialog->will_release())
    {
      substate = LSSUB_RELEASING;
    }
  }

  pending_dialogs.erase(std::remove_if(pending_dialogs.begin(),
	  pending_dialogs.end(),
	  [] (std::unique_ptr<t_dialog> const & elem) {
	    return elem->get_state() == DS_TERMINATED;
	  }),
      pending_dialogs.end());

  dying_dialogs.erase(std::remove_if(dying_dialogs.begin(),
	  dying_dialogs.end(),
	  [] (std::unique_ptr<t_dialog> const & elem) {
	    return elem->get_state() == DS_TERMINATED;
	  }),
      dying_dialogs.end());

  if (!open_dialog && !active_dialog && pending_dialogs.empty())
  {
    state = LS_IDLE;

    if (keep_seized)
    {
      substate = LSSUB_SEIZED;
    }
    else
    {
      substate = LSSUB_IDLE;
    }

    is_on_hold = false;
    is_muted = false;
    hide_user = false;
    cleanup_transfer_consult_state();
    try_to_encrypt = false;
    auto_answer = false;
    call_info.clear();
    call_history->add_call_record(call_hist_record);
    call_hist_record.renew();
    phone_user = nullptr;
    user_defined_ringtone.clear();
    ui->cb_line_state_changed();
  }
}

void t_line::cleanup_open_pending()
{
  open_dialog.reset();

  pending_dialogs.clear();

  if (!active_dialog)
  {
    is_on_hold = false;
    is_muted = false;
    hide_user = false;
    cleanup_transfer_consult_state();
    try_to_encrypt = false;
    auto_answer = false;
    state = LS_IDLE;

    if (keep_seized)
    {
      substate = LSSUB_SEIZED;
    }
    else
    {
      substate = LSSUB_IDLE;
    }

    call_info.clear();
    call_history->add_call_record(call_hist_record);
    call_hist_record.renew();
    phone_user = nullptr;
    user_defined_ringtone.clear();
    ui->cb_line_state_changed();
  }
}

void t_line::cleanup_forced()
{
  open_dialog.reset();
  active_dialog.reset();

  pending_dialogs.clear();
  dying_dialogs.clear();

  // TODO: stop running timers?

  state = LS_IDLE;
  substate = LSSUB_IDLE;
  keep_seized = false;
  is_on_hold = false;
  is_muted = false;
  hide_user = false;
  cleanup_transfer_consult_state();
  auto_answer = false;
  call_info.clear();
  call_history->add_call_record(call_hist_record);
  call_hist_record.renew();
  phone_user = nullptr;
  user_defined_ringtone.clear();
  ui->cb_line_state_changed();
}

void t_line::cleanup_transfer_consult_state()
{
  if (is_transfer_consult)
  {
    t_line *from_line = phone.get_line(consult_transfer_from_line);
    from_line->set_to_be_transferred(false, 0);
    is_transfer_consult = false;
  }

  if (to_be_transferred)
  {
    t_line *to_line = phone.get_line(consult_transfer_to_line);
    to_line->set_is_transfer_consult(false, 0);
    to_be_transferred = false;
  }
}


///////////
// Public
///////////

t_line::t_line(t_phone &_phone, unsigned int _line_number)
  : t_id_object(),
    phone(_phone), line_number(_line_number),
    state(LS_IDLE), substate(LSSUB_IDLE),
    is_on_hold(false), is_muted(false), hide_user(false),
    is_transfer_consult(false), to_be_transferred(false),
    try_to_encrypt(false), auto_answer(false),
    id_invite_comp(0), id_no_answer(0),
    rtp_port(settings->get_child("network")->get_uint("rtp-port") + line_number * 2),
    phone_user(nullptr), keep_seized(false)
{
}

t_line::~t_line()
{
  // Stop timers
  if (id_invite_comp != 0u)
  {
    stop_timer(LTMR_INVITE_COMP);
  }
  if (id_no_answer != 0u)
  {
    stop_timer(LTMR_NO_ANSWER);
  }
}

t_line_state t_line::get_state() const
{
  return state;
}

t_line_substate t_line::get_substate() const
{
  return substate;
}

t_refer_state t_line::get_refer_state() const
{
  if (active_dialog) return active_dialog->refer_state;
  return REFST_NULL;
}

void t_line::start_timer(t_line_timer timer, t_object_id did)
{
  std::unique_ptr<t_tmr_line> t;
  t_dialog *dialog = get_dialog(did);
  unsigned long	dur;

  assert(phone_user);

  switch(timer)
  {
  case LTMR_ACK_TIMEOUT:
   assert(dialog);
   // RFC 3261 13.3.1.4
   if (dialog->dur_ack_timeout == 0)
   {
     dialog->dur_ack_timeout = DURATION_T1;
   }
   else
   {
     dialog->dur_ack_timeout *= 2;
     if (dialog->dur_ack_timeout > DURATION_T2)
     {
       dialog->dur_ack_timeout = DURATION_T2;
     }
   }
   t = std::make_unique<t_tmr_line>(dialog->dur_ack_timeout , timer,
       get_object_id(), did);
   dialog->id_ack_timeout = t->get_object_id();
   break;

  case LTMR_ACK_GUARD:
   assert(dialog);
   // RFC 3261 13.3.1.4
   t = std::make_unique<t_tmr_line>(64 * DURATION_T1, timer, get_object_id(),
       did);
   dialog->id_ack_guard = t->get_object_id();
   break;

  case LTMR_INVITE_COMP:
   // RFC 3261 13.2.2.4
   t = std::make_unique<t_tmr_line>(64 * DURATION_T1, timer, get_object_id(),
       did);
   id_invite_comp = t->get_object_id();
   break;

  case LTMR_NO_ANSWER:
   t = std::make_unique<t_tmr_line>(DUR_NO_ANSWER(phone_user->get_user_profile()),
       timer, get_object_id(), did);
   id_no_answer = t->get_object_id();
   break;

  case LTMR_RE_INVITE_GUARD:
   assert(dialog);
   t = std::make_unique<t_tmr_line>(DUR_RE_INVITE_GUARD, timer, get_object_id(),
       did);
   dialog->id_re_invite_guard = t->get_object_id();
   break;

  case LTMR_GLARE_RETRY:
   assert(dialog);
   if (dialog->is_call_id_owner())
   {
     dur = DUR_GLARE_RETRY_OWN;
   }
   else
   {
     dur = DUR_GLARE_RETRY_NOT_OWN;
   }
   t = std::make_unique<t_tmr_line>(dur, timer, get_object_id(), did);
   dialog->id_glare_retry = t->get_object_id();
   break;

  case LTMR_100REL_TIMEOUT:
   assert(dialog);
   // RFC 3262 3
   if (dialog->dur_100rel_timeout == 0)
   {
     dialog->dur_100rel_timeout = DUR_100REL_TIMEOUT;
   }
   else
   {
     dialog->dur_100rel_timeout *= 2;
   }
   t = std::make_unique<t_tmr_line>(dialog->dur_100rel_timeout , timer,
       get_object_id(), did);
   dialog->id_100rel_timeout = t->get_object_id();
   break;

  case LTMR_100REL_GUARD:
   assert(dialog);
   // RFC 3262 3
   t = std::make_unique<t_tmr_line>(DUR_100REL_GUARD, timer, get_object_id(),
       did);
   dialog->id_100rel_guard = t->get_object_id();
   break;

  case LTMR_CANCEL_GUARD:
   assert(dialog);
   t = std::make_unique<t_tmr_line>(DUR_CANCEL_GUARD, timer, get_object_id(),
       did);
   dialog->id_cancel_guard = t->get_object_id();
   break;

  default:
   assert(false);
  }

  evq_timekeeper->push_start_timer(std::move(t));
}

void t_line::stop_timer(t_line_timer timer, t_object_id did)
{
  t_object_id *id;
  t_dialog *dialog = get_dialog(did);

  switch(timer)
  {
  case LTMR_ACK_TIMEOUT:
   assert(dialog);
   dialog->dur_ack_timeout = 0;
   id = &dialog->id_ack_timeout;
   break;

  case LTMR_ACK_GUARD:
   assert(dialog);
   id = &dialog->id_ack_guard;
   break;

  case LTMR_INVITE_COMP:
   id = &id_invite_comp;
   break;

  case LTMR_NO_ANSWER:
   id = &id_no_answer;
   break;

  case LTMR_RE_INVITE_GUARD:
   assert(dialog);
   id = &dialog->id_re_invite_guard;
   break;

  case LTMR_GLARE_RETRY:
   assert(dialog);
   id = &dialog->id_glare_retry;
   break;

  case LTMR_100REL_TIMEOUT:
   assert(dialog);
   dialog->dur_100rel_timeout = 0;
   id = &dialog->id_100rel_timeout;
   break;

  case LTMR_100REL_GUARD:
   assert(dialog);
   id = &dialog->id_100rel_guard;
   break;

  case LTMR_CANCEL_GUARD:
   assert(dialog);
   id = &dialog->id_cancel_guard;

   // KLUDGE
   if (*id == 0)
   {
     // Cancel is always sent on the open dialog.
     // The timer is probably stopped from a pending dialog,
     // therefore the timer is stopped on the wrong dialog.
     // Check if the open dialog has a CANCEL guard timer.
     if (open_dialog) id = &open_dialog->id_cancel_guard;
   }
   break;

  default:
   assert(false);
  }

  if (*id != 0)
  {
    evq_timekeeper->push_stop_timer(*id);
    *id = 0;
  }
}

void t_line::invite(t_phone_user *pu, const t_url &to_uri,
		    const std::string &to_display, const std::string &subject,
		    bool no_fork, bool anonymous)
{
  t_hdr_request_disposition hdr_request_disposition;

  if (no_fork)
  {
    hdr_request_disposition.set_fork_directive(
      t_hdr_request_disposition::NO_FORK);
  }

  invite(pu, to_uri, to_display, subject, t_hdr_referred_by(),
	 t_hdr_replaces(), t_hdr_require(), hdr_request_disposition,
	 anonymous);
}

void t_line::invite(t_phone_user *pu, const t_url &to_uri,
		    const std::string &to_display, const std::string &subject,
		    const t_hdr_referred_by &hdr_referred_by,
		    const t_hdr_replaces &hdr_replaces,
		    const t_hdr_require &hdr_require,
		    const t_hdr_request_disposition &hdr_request_disposition,
		    bool anonymous)
{
  assert(pu);

  // Ignore if line is not idle
  if (state != LS_IDLE)
  {
    return;
  }

  assert(!open_dialog);

  // Validate speaker and mic
  std::string error_msg;
  if (!sys_config->exec_audio_validation(false, true, true, error_msg))
  {
    ui->cb_show_msg(error_msg, MSG_CRITICAL);
    return;
  }

  phone_user = pu;
  t_user &user_config(pu->get_user_profile());

  call_info.from_uri = create_user_uri(); // NOTE: hide_user is not set yet
  call_info.from_display = user_config.get_display(false);
  call_info.from_organization = user_config.get_organization();
  call_info.to_uri = to_uri;
  call_info.to_display = to_display;
  call_info.to_organization.clear();
  call_info.subject = subject;
  call_info.hdr_referred_by = hdr_referred_by;

  try_to_encrypt = user_config.get_zrtp_enabled();

  state = LS_BUSY;
  substate = LSSUB_OUTGOING_PROGRESS;
  hide_user = anonymous;
  ui->cb_line_state_changed();

  open_dialog = std::make_unique<t_dialog>(phone, *this);
  open_dialog->send_invite(to_uri, to_display, subject, hdr_referred_by,
			   hdr_replaces, hdr_require, hdr_request_disposition,
			   anonymous);

  cleanup();
}

void t_line::answer()
{
  // Ignore if line is idle
  if ((state == LS_IDLE) || (substate != LSSUB_INCOMING_PROGRESS))
  {
    return;
  }
  assert(active_dialog);

  // Validate speaker and mic
  std::string error_msg;
  if (!sys_config->exec_audio_validation(false, true, true, error_msg))
  {
    ui->cb_show_msg(error_msg, MSG_CRITICAL);
    return;
  }

  stop_timer(LTMR_NO_ANSWER);

  try
  {
    substate = LSSUB_ANSWERING;
    ui->cb_line_state_changed();
    active_dialog->answer();
  }
  catch (const t_dialog_error &x)
  {
    // TODO: there is no call to answer
  }

  cleanup();
}

void t_line::reject()
{
  // Ignore if line is idle
  if ((state == LS_IDLE) || (substate != LSSUB_INCOMING_PROGRESS))
  {
    return;
  }
  assert(active_dialog);

  stop_timer(LTMR_NO_ANSWER);

  try
  {
    active_dialog->reject(R_486_BUSY_HERE);
  }
  catch (const t_dialog_error &x)
  {
    // TODO: there is no call to reject
  }

  cleanup();
}

void t_line::redirect(const std::vector<t_display_url> &destinations, int code, std::string reason)
{
  // Ignore if line is idle
  if ((state == LS_IDLE) || (substate != LSSUB_INCOMING_PROGRESS))
  {
    return;
  }
  assert(active_dialog);

  stop_timer(LTMR_NO_ANSWER);

  try
  {
    active_dialog->redirect(destinations, code, std::move(reason));
  }
  catch (const t_dialog_error &x)
  {
    // TODO: there is no call to redirect
  }

  cleanup();
}

void t_line::end_call()
{
  // Ignore if phone is idle
  if (state == LS_IDLE) return;

  if (active_dialog)
  {
    substate = LSSUB_RELEASING;
    ui->cb_line_state_changed();
    ui->cb_stop_call_notification(line_number);
    active_dialog->send_bye();

    // If the line was part of a transfer with consultation,
    // then clean the consultation state as the transfer cannot
    // proceed anymore.
    cleanup_transfer_consult_state();

    cleanup();
    return;
  }

  // Always send the CANCEL on the open dialog.
  // The pending dialogs will be cleared when the INVITE gets
  // terminated.
  // CANCEL is send on the open dialog as the CANCEL must have
  // the same tags as the INVITE.
  if (open_dialog)
  {
    substate = LSSUB_RELEASING;
    ui->cb_line_state_changed();
    ui->cb_stop_call_notification(line_number);
    open_dialog->send_cancel(!pending_dialogs.empty());

    // Make sure dialog is terminated if CANCEL glares with
    // 2XX on INVITE.
    for (auto const & elem : pending_dialogs)
    {
      elem->set_end_after_2xx_invite(true);
    }

    cleanup();
    return;
  }

  // NOTE:
  // The call is only ended for real when the dialog reaches
  // the DS_TERMINATED state, i.e. a 200 OK on BYE is received
  // or a 487 TERMINATED on INVITE is received.
}

void t_line::send_dtmf(char digit, bool inband, bool info)
{
  // DTMF may be sent on an early media session, so find
  // a dialog that has an RTP session. There can be at most 1.
  if (t_dialog * const d = get_dialog_with_active_session())
  {
    d->send_dtmf(digit, inband, info);
    cleanup();
    return;
  }
}

void t_line::options()
{
  if (active_dialog && active_dialog->get_state() == DS_CONFIRMED)
  {
    active_dialog->send_options();
    cleanup();
    return;
  }
}

bool t_line::hold(bool rtponly)
{
  if (is_on_hold) return true;

  if (active_dialog && active_dialog->get_state() == DS_CONFIRMED)
  {
    active_dialog->hold(rtponly);
    is_on_hold = true;
    ui->cb_line_state_changed();
    cleanup();
    return true;
  }

  return false;
}

void t_line::retrieve()
{
  if (!is_on_hold) return;

  if (active_dialog && active_dialog->get_state() == DS_CONFIRMED)
  {
    active_dialog->retrieve();
    is_on_hold = false;
    ui->cb_line_state_changed();
    cleanup();
    return;
  }
}

void t_line::kill_rtp()
{
  if (active_dialog) active_dialog->kill_rtp();

  for (auto const & elem : pending_dialogs)
  {
    elem->kill_rtp();
  }

  for (auto const & elem : dying_dialogs)
  {
    elem->kill_rtp();
  }
}

void t_line::refer(const t_url &uri, const std::string &display)
{
  if (active_dialog && active_dialog->get_state() == DS_CONFIRMED)
  {
    active_dialog->send_refer(uri, display);
    ui->cb_line_state_changed();
    cleanup();
    return;
  }
}

void t_line::mute(bool enable)
{
  is_muted = enable;
}

void t_line::recvd_provisional(t_response &r, t_tuid tuid, t_tid tid)
{
  if (active_dialog && active_dialog->match_response(r, 0))
  {
    active_dialog->recvd_response(r, tuid, tid);
    cleanup();
    return;
  }

  {
    auto iter (match_response(r, pending_dialogs));
    if (iter != pending_dialogs.end())
    {
      (*iter)->recvd_response(r, tuid, tid);
      cleanup();
      return;
    }
  }

  {
    auto iter (match_response(r, dying_dialogs));
    if (iter != dying_dialogs.end())
    {
      (*iter)->recvd_response(r, tuid, tid);
      cleanup();
      return;
    }
  }

  if (open_dialog && open_dialog->match_response(r, tuid))
  {
    if (r.hdr_cseq.method == INVITE)
    {
      if (r.hdr_to.tag.size() > 0)
      {
	// Create a new pending dialog
	std::unique_ptr<t_dialog> d(open_dialog->copy());
	d->recvd_response(r, tuid, tid);
	pending_dialogs.push_back(std::move(d));
      }
      else
      {
	open_dialog->recvd_response(r, tuid, tid);
      }
    }
    else
    {
      open_dialog->recvd_response(r, tuid, tid);
    }

    cleanup();
    return;
  }

  // out-of-dialog response
  // Provisional responses should only be given for INVITE.
  // A response for an INVITE is always in a dialog.
  // Ignore provisional responses for other requests.
}

void t_line::recvd_success(t_response &r, t_tuid tuid, t_tid tid)
{
  if (active_dialog && active_dialog->match_response(r, 0))
  {
    active_dialog->recvd_response(r, tuid, tid);
    cleanup();
    return;
  }

  {
    auto iter(match_response(r, pending_dialogs));
    if (iter != pending_dialogs.end())
    {
      (*iter)->recvd_response(r, tuid, tid);
      if (r.hdr_cseq.method == INVITE)
      {
	if (!active_dialog)
	{
	  // Make the dialog the active dialog
	  active_dialog = std::move(*iter);
	  pending_dialogs.erase(iter);
	  start_timer(LTMR_INVITE_COMP);
	  substate = LSSUB_ESTABLISHED;
	  ui->cb_line_state_changed();
	}
	else
	{
	  // An active dialog already exists.
	  // Terminate this dialog by sending BYE
	  (*iter)->send_bye();
	}
      }

      cleanup();
      return;
    }
  }

  {
    auto iter(match_response(r, dying_dialogs));
    if (iter != dying_dialogs.end())
    {
      (*iter)->recvd_response(r, tuid, tid);
      if (r.hdr_cseq.method == INVITE)
      {
	(*iter)->send_bye();
      }
      cleanup();
      return;
    }
  }

  if (open_dialog && open_dialog->match_response(r, tuid))
  {
    if (r.hdr_cseq.method == INVITE)
    {
      // Create a new dialog
      std::unique_ptr<t_dialog> d(open_dialog->copy());

      if (!active_dialog)
      {
	active_dialog = std::move(d);
	active_dialog->recvd_response(r, tuid, tid);
	start_timer(LTMR_INVITE_COMP);
	substate = LSSUB_ESTABLISHED;
	ui->cb_line_state_changed();
      } else {
	d->recvd_response(r, tuid, tid);

	// An active dialog already exists.
	// Terminate this dialog by sending BYE
	d->send_bye();
	pending_dialogs.push_back(std::move(d));
      }
    } else {
      open_dialog->recvd_response(r, tuid, tid);
    }

    cleanup();
    return;
  }

  // Response does not match with any pending request. Discard.
}

void t_line::recvd_redirect(t_response &r, t_tuid tuid, t_tid tid)
{
  assert(phone_user);
  t_user &user_config(phone_user->get_user_profile());

  if (active_dialog)
  {
    // If an active dialog exists then non-2XX should
    // only be for this dialog.
    if (active_dialog->match_response(r, 0))
    {
      // Redirection of mid-dialog request
      if (!user_config.get_allow_redirection() ||
	  !active_dialog->redirect_request(r))
      {
	// Redirection not allowed/failed
	active_dialog->recvd_response(r, tuid, tid);
      }

      // Retrieve a held line after a REFER failure
      if (r.hdr_cseq.method == REFER &&
	  active_dialog->out_refer_req_failed)
      {
	active_dialog->out_refer_req_failed = false;
	if (phone.get_active_line() == line_number &&
	    user_config.get_referrer_hold())
	{
	  retrieve();
	}
      }
    }

    cleanup();
    return;
  }

  {
    auto iter(match_response(r, pending_dialogs));
    if (iter != pending_dialogs.end())
    {
      (*iter)->recvd_response(r, tuid, tid);
      if (r.hdr_cseq.method == INVITE)
      {
	pending_dialogs.erase(iter);

	// RFC 3261 13.2.2.3
	// All early dialogs are considered terminated
	// upon reception of the non-2xx final response.
	pending_dialogs.clear();

	if (open_dialog)
	{
	  if (!user_config.get_allow_redirection() ||
	      !open_dialog->redirect_invite(r))
	  {
	    open_dialog.reset();
	  }
	}
      }

      cleanup();
      return;
    }
  }

  {
    auto iter(match_response(r, dying_dialogs));
    if (iter != dying_dialogs.end())
    {
      (*iter)->recvd_response(r, tuid, tid);
      cleanup();
      return;
    }
  }

  if (open_dialog && open_dialog->match_response(r, tuid))
  {
    if (r.hdr_cseq.method != INVITE)
    {
      // TODO: can there be a non-INVITE response for an
      //       open dialog??
      open_dialog->recvd_response(r, tuid, tid);
    }

    if (r.hdr_cseq.method == INVITE)
    {
      if (!user_config.get_allow_redirection() ||
	  !open_dialog->redirect_invite(r))
      {
	// Redirection failed/not allowed
	open_dialog->recvd_response(r, tuid, tid);
	open_dialog.reset();
      }

      // RFC 3261 13.2.2.3
      // All early dialogs are considered terminated
      // upon reception of the non-2xx final response.
      pending_dialogs.clear();
    }

    cleanup();
    return;
  }

  // out-of-dialog responses should be handled by the phone
}

void t_line::recvd_client_error(t_response &r, t_tuid tuid, t_tid tid)
{
  assert(phone_user);
  t_user &user_config(phone_user->get_user_profile());

  if (active_dialog)
  {
    // If an active dialog exists then non-2XX should
    // only be for this dialog.
    if (active_dialog->match_response(r, 0))
    {
      bool response_processed = false;

      if (r.must_authenticate())
      {
	// Authentication for mid-dialog request
	if (active_dialog->resend_request_auth(r))
	{
	  // Authorization successul.
	  // The response does not need to be
	  // processed any further
	  response_processed = true;
	}
      }

      if (!response_processed)
      {
	// The request failed, redirect it if there
	// are other destinations available.
	if (!user_config.get_allow_redirection() ||
	    !active_dialog->redirect_request(r))
	{
	  // Request failed
	  active_dialog->
	    recvd_response(r, tuid, tid);
	}
      }

      // Retrieve a held line after a REFER failure
      if (r.hdr_cseq.method == REFER &&
	  active_dialog->out_refer_req_failed)
      {
	active_dialog->out_refer_req_failed = false;
	if (phone.get_active_line() == line_number &&
	    user_config.get_referrer_hold())
	{
	  retrieve();
	}
      }
    }

    cleanup();
    return;
  }

  {
    auto iter(match_response(r, pending_dialogs));
    if (iter != pending_dialogs.end())
    {
      if (r.hdr_cseq.method != INVITE)
      {
	if (r.must_authenticate())
	{
	  // Authentication for non-INVITE request in pending dialog
	  if (!(*iter)->resend_request_auth(r))
	  {
	    // Could not authorize, send response to dialog
	    // where it will be handle as a client failure.
	    (*iter)->recvd_response(r, tuid, tid);
	  }
	}
	else
	{
	  (*iter)->recvd_response(r, tuid, tid);
	}
      }
      else
      {
	(*iter)->recvd_response(r, tuid, tid);
	pending_dialogs.erase(iter);

	// RFC 3261 13.2.2.3
	// All early dialogs are considered terminated
	// upon reception of the non-2xx final response.
	pending_dialogs.clear();

	if (open_dialog)
	{
	  bool response_processed = false;

	  if (r.must_authenticate())
	  {
	    // INVITE authentication
	    if (open_dialog->resend_invite_auth(r))
	    {
	      // Authorization successul.
	      // The response does not need to
	      // be processed any further
	      response_processed = true;
	    }
	  }

	  // Resend INVITE if the response indicated that
	  // required extensions are not supported.
	  if (!response_processed &&
	      open_dialog->resend_invite_unsupported(r))
	  {
	    response_processed = true;
	  }

	  if (!response_processed)
	  {
	    // The request failed, redirect it if there
	    // are other destinations available.
	    if (!user_config.get_allow_redirection() ||
		!open_dialog->redirect_invite(r))
	    {
	      // Request failed
	      open_dialog.reset();
	    }
	  }
	}
      }

      cleanup();
      return;
    }
  }

  {
    auto iter(match_response(r, dying_dialogs));
    if (iter != dying_dialogs.end())
    {
      (*iter)->recvd_response(r, tuid, tid);
      cleanup();
      return;
    }
  }

  if (open_dialog && open_dialog->match_response(r, tuid))
  {
    // If the response is a 401/407 then do not send the
    // response to the dialog as the request must be resent.
    // For an INVITE request, the transaction layer has already
    // sent ACK for a failure response.
    if (r.hdr_cseq.method != INVITE)
    {
      if (r.must_authenticate())
      {
	// Authenticate non-INVITE request
	if (!open_dialog->resend_request_auth(r))
	{
	  // Could not authorize, handle as other client
	  // errors.
	  open_dialog->recvd_response(r, tuid, tid);
	}
      }
      else
      {
	open_dialog->recvd_response(r, tuid, tid);
      }
    }

    if (r.hdr_cseq.method == INVITE)
    {
      bool response_processed = false;

      if (r.must_authenticate())
      {
	// INVITE authentication
	if (open_dialog->resend_invite_auth(r))
	{
	  // Authorization successul.
	  // The response does not need to
	  // be processed any further
	  response_processed = true;
	}
      }

      // Resend INVITE if the response indicated that
      // required extensions are not supported.
      if (!response_processed &&
	  open_dialog->resend_invite_unsupported(r))
      {
	response_processed = true;
      }

      if (!response_processed)
      {
	// The request failed, redirect it if there
	// are other destinations available.
	if (!user_config.get_allow_redirection() ||
	    !open_dialog->redirect_invite(r))
	{
	  // Request failed
	  open_dialog->recvd_response(r, tuid, tid);
	  open_dialog.reset();
	}
      }

      // RFC 3261 13.2.2.3
      // All early dialogs are considered terminated
      // upon reception of the non-2xx final response.
      pending_dialogs.clear();
    }

    cleanup();
    return;
  }

  // out-of-dialog responses should be handled by the phone
}

void t_line::recvd_server_error(t_response &r, t_tuid tuid, t_tid tid)
{
  assert(phone_user);
  t_user &user_config(phone_user->get_user_profile());

  if (active_dialog)
  {
    // If an active dialog exists then non-2XX should
    // only be for this dialog.
    if (active_dialog->match_response(r, 0))
    {
      bool response_processed = false;

      if (r.code == R_503_SERVICE_UNAVAILABLE)
      {
	// RFC 3263 4.3
	// Failover to next destination
	if (active_dialog->failover_request(r))
	{
	  // Failover successul.
	  // The response does not need to be
	  // processed any further
	  response_processed = true;
	}
      }

      if (!response_processed)
      {
	// The request failed, redirect it if there
	// are other destinations available.
	if (!user_config.get_allow_redirection() ||
	    !active_dialog->redirect_request(r))
	{
	  // Request failed
	  active_dialog->
	    recvd_response(r, tuid, tid);
	}
      }

      // Retrieve a held line after a REFER failure
      if (r.hdr_cseq.method == REFER &&
	  active_dialog->out_refer_req_failed)
      {
	active_dialog->out_refer_req_failed = false;
	if (phone.get_active_line() == line_number &&
	    user_config.get_referrer_hold())
	{
	  retrieve();
	}
      }
    }

    cleanup();
    return;
  }

  {
    auto iter(match_response(r, pending_dialogs));
    if (iter != pending_dialogs.end())
    {
      (*iter)->recvd_response(r, tuid, tid);
      if (r.hdr_cseq.method == INVITE)
      {
	pending_dialogs.erase(iter);

	// RFC 3261 13.2.2.3
	// All early dialogs are considered terminated
	// upon reception of the non-2xx final response.
	pending_dialogs.clear();

	if (open_dialog)
	{
	  bool response_processed = false;

	  if (r.code == R_503_SERVICE_UNAVAILABLE)
	  {
	    // INVITE failover
	    if (open_dialog->failover_invite())
	    {
	      // Failover successul.
	      // The response does not need to
	      // be processed any further
	      response_processed = true;
	    }
	  }

	  if (!response_processed)
	  {
	    // The request failed, redirect it if there
	    // are other destinations available.
	    if (!user_config.get_allow_redirection() ||
		!open_dialog->redirect_invite(r))
	    {
	      // Request failed
	      open_dialog.reset();
	    }
	  }
	}
      }

      cleanup();
      return;
    }
  }

  {
    auto iter(match_response(r, dying_dialogs));
    if (iter != dying_dialogs.end())
    {
      (*iter)->recvd_response(r, tuid, tid);
      cleanup();
      return;
    }
  }

  if (open_dialog && open_dialog->match_response(r, tuid))
  {
    // If the response is a 503 then do not send the
    // response to the dialog as the request must be resent.
    // For an INVITE request, the transaction layer has already
    // sent ACK for a failure response.
    if (r.code != R_503_SERVICE_UNAVAILABLE && r.hdr_cseq.method != INVITE)
    {
      open_dialog->recvd_response(r, tuid, tid);
    }

    if (r.hdr_cseq.method == INVITE)
    {
      bool response_processed = false;

      if (r.code == R_503_SERVICE_UNAVAILABLE)
      {
	// INVITE failover
	if (open_dialog->failover_invite())
	{
	  // Failover successul.
	  // The response does not need to
	  // be processed any further
	  response_processed = true;
	}
      }

      if (!response_processed)
      {
	// The request failed, redirect it if there
	// are other destinations available.
	if (!user_config.get_allow_redirection() ||
	    !open_dialog->redirect_invite(r))
	{
	  // Request failed
	  open_dialog->recvd_response(r, tuid, tid);
	  open_dialog.reset();
	}
      }

      // RFC 3261 13.2.2.3
      // All early dialogs are considered terminated
      // upon reception of the non-2xx final response.
      pending_dialogs.clear();
    }

    cleanup();
    return;
  }

  // out-of-dialog responses should be handled by the phone
}

void t_line::recvd_global_error(t_response &r, t_tuid tuid, t_tid tid)
{
  recvd_redirect(r, tuid, tid);
}

void t_line::recvd_invite(t_phone_user *pu, t_request &r, t_tid tid, const std::string &ringtone)
{
  switch (state)
  {
  case LS_IDLE:
  {
    assert(!active_dialog);
    assert(r.hdr_to.tag == "");

    /*
    // TEST ONLY
    // Test code to test INVITE authentication
    if (!r->hdr_authorization.is_populated()) {
    resp = r->create_response(R_401_UNAUTHORIZED);
    t_challenge c;
    c.auth_scheme = AUTH_DIGEST;
    c.digest_challenge.realm = "mtel.nl";
    c.digest_challenge.nonce = "0123456789abcdef";
    c.digest_challenge.opaque = "secret";
    c.digest_challenge.algorithm = ALG_MD5;
    c.digest_challenge.qop_options.push_back(QOP_AUTH);
    c.digest_challenge.qop_options.push_back(QOP_AUTH_INT);
    resp->hdr_www_authenticate.set_challenge(c);
    send_response(resp, 0, tid);
    return;
    }
    */

    assert(pu);
    phone_user = pu;
    t_user &user_config(phone_user->get_user_profile());
    user_defined_ringtone = ringtone;

    call_info.from_uri = r.hdr_from.uri;
    call_info.from_display = r.hdr_from.display;
    call_info.from_display_override = r.hdr_from.display_override;
    if (r.hdr_organization.is_populated())
    {
      call_info.from_organization = r.hdr_organization.name;
    }
    else
    {
      call_info.from_organization.clear();
    }
    call_info.to_uri = r.hdr_to.uri;
    call_info.to_display = r.hdr_to.display;
    call_info.to_organization.clear();
    call_info.subject = r.hdr_subject.subject;

    try_to_encrypt = user_config.get_zrtp_enabled();

    // Check for REFER support
    // If the Allow header is not present then assume REFER
    // is supported.
    if (!r.hdr_allow.is_populated() ||
	r.hdr_allow.contains_method(REFER))
    {
      call_info.refer_supported = true;
    }

    active_dialog = std::make_unique<t_dialog>(phone, *this);
    active_dialog->recvd_request(r, 0, tid);
    state = LS_BUSY;
    substate = LSSUB_INCOMING_PROGRESS;
    ui->cb_line_state_changed();
    start_timer(LTMR_NO_ANSWER);
    cleanup();

    // Answer if auto answer mode is activated
    if (auto_answer)
    {
      // Validate speaker and mic
      std::string error_msg;
      if (!sys_config->exec_audio_validation(false, true, true, error_msg))
      {
	ui->cb_display_msg(error_msg, MSG_CRITICAL);
      }
      else
      {
	answer();
      }
    }
  }
  break;

  case LS_BUSY:
  {
    // Only re-INVITEs can be sent to a busy line
    assert(r.hdr_to.tag != "");

    /*
    // TEST ONLY
    // Test code to test re-INVITE authentication
    if (!r->hdr_authorization.is_populated()) {
    resp = r->create_response(R_401_UNAUTHORIZED);
    t_challenge c;
    c.auth_scheme = AUTH_DIGEST;
    c.digest_challenge.realm = "mtel.nl";
    c.digest_challenge.nonce = "0123456789abcdef";
    c.digest_challenge.opaque = "secret";
    c.digest_challenge.algorithm = ALG_MD5;
    c.digest_challenge.qop_options.push_back(QOP_AUTH);
    c.digest_challenge.qop_options.push_back(QOP_AUTH_INT);
    resp->hdr_www_authenticate.set_challenge(c);
    send_response(resp, 0, tid);
    return;
    }
    */

    if (active_dialog && active_dialog->match_request(r))
    {
      // re-INVITE
      active_dialog->recvd_request(r, 0, tid);
      cleanup();
      return;
    }

    // Should not get here as phone already checked that
    // the request matched with this line
    assert(false);
  }
  break;

  default:
   assert(false);
  }
}

void t_line::recvd_ack(t_request &r, t_tid tid)
{
  if (active_dialog && active_dialog->match_request(r))
  {
    active_dialog->recvd_request(r, 0, tid);
    substate = LSSUB_ESTABLISHED;
    ui->cb_line_state_changed();
  }
  else
  {
    // Should not get here as phone already checked that
    // the request matched with this line
    assert(false);
  }
  cleanup();
}

void t_line::recvd_cancel(t_request &r, t_tid cancel_tid,
			  t_tid target_tid)
{
  // A CANCEL matches a dialog if the target tid equals the tid
  // of the INVITE request. This will be checked by
  // dialog::recvd_cancel() itself.
  if (active_dialog)
  {
    active_dialog->recvd_cancel(r, cancel_tid, target_tid);
  }
  else
  {
    // Should not get here as phone already checked that
    // the request matched with this line
    assert(false);
  }
  cleanup();
}

void t_line::recvd_bye(t_request &r, t_tid tid)
{
  if (active_dialog && active_dialog->match_request(r))
  {
    active_dialog->recvd_request(r, 0, tid);
  }
  else
  {
    // Should not get here as phone already checked that
    // the request matched with this line
    assert(false);
  }
  cleanup();
}

void t_line::recvd_options(t_request &r, t_tid tid)
{
  if (active_dialog && active_dialog->match_request(r))
  {
    active_dialog->recvd_request(r, 0, tid);
  }
  else
  {
    // Should not get here as phone already checked that
    // the request matched with this line
    assert(false);
  }
  cleanup();
}

void t_line::recvd_prack(t_request &r, t_tid tid)
{
  if (active_dialog && active_dialog->match_request(r))
  {
    active_dialog->recvd_request(r, 0, tid);
  }
  else
  {
    // Should not get here as phone already checked that
    // the request matched with this line
    assert(false);
  }
  cleanup();
}

void t_line::recvd_subscribe(t_request &r, t_tid tid)
{
  if (active_dialog && active_dialog->match_request(r))
  {
    active_dialog->recvd_request(r, 0, tid);
  }
  else
  {
    // Should not get here as phone already checked that
    // the request matched with this line
    assert(false);
  }
  cleanup();
}

void t_line::recvd_notify(t_request &r, t_tid tid)
{
  if (active_dialog && active_dialog->match_request(r))
  {
    active_dialog->recvd_request(r, 0, tid);
  }
  else
  {
    // Should not get here as phone already checked that
    // the request matched with this line
    assert(false);
  }
  cleanup();
}

void t_line::recvd_info(t_request &r, t_tid tid)
{
  if (active_dialog && active_dialog->match_request(r))
  {
    active_dialog->recvd_request(r, 0, tid);
  }
  else
  {
    // Should not get here as phone already checked that
    // the request matched with this line
    assert(false);
  }
  cleanup();
}

void t_line::recvd_message(t_request &r, t_tid tid)
{
  if (active_dialog && active_dialog->match_request(r))
  {
    active_dialog->recvd_request(r, 0, tid);
  }
  else
  {
    // Should not get here as phone already checked that
    // the request matched with this line
    assert(false);
  }
  cleanup();
}

bool t_line::recvd_refer(t_request &r, t_tid tid)
{
  bool retval = false;

  if (active_dialog && active_dialog->match_request(r))
  {
    active_dialog->recvd_request(r, 0, tid);
    retval = active_dialog->refer_accepted;
  }
  else
  {
    // Should not get here as phone already checked that
    // the request matched with this line
    assert(false);
  }
  cleanup();
  return retval;
}

void t_line::recvd_refer_permission(bool permission, t_request &r)
{
  if (active_dialog && active_dialog->match_request(r))
  {
    active_dialog->recvd_refer_permission(permission, r);
  }
  cleanup();
}

void t_line::recvd_stun_resp(StunMessage &r, t_tuid tuid, t_tid tid)
{
  if (active_dialog && active_dialog->match_response(r, tuid))
  {
    active_dialog->recvd_stun_resp(r, tuid, tid);
    cleanup();
    return;
  }

  if (open_dialog && open_dialog->match_response(r, tuid))
  {
    open_dialog->recvd_stun_resp(r, tuid, tid);
    cleanup();
    return;
  }

  {
    auto iter(match_response(r, tuid, pending_dialogs));
    if (iter != pending_dialogs.end())
    {
      (*iter)->recvd_stun_resp(r, tuid, tid);
      cleanup();
      return;
    }
  }

  {
    auto iter(match_response(r, tuid, dying_dialogs));
    if (iter != dying_dialogs.end())
    {
      (*iter)->recvd_stun_resp(r, tuid, tid);
      cleanup();
      return;
    }
  }
}

void t_line::failure(t_failure failure, t_tid tid)
{
  // TODO
}

void t_line::timeout(t_line_timer timer, t_object_id did)
{
  t_dialog *dialog = get_dialog(did);
  std::vector<t_display_url> cf_dest; // call forwarding destinations

  switch (timer)
  {
  case LTMR_ACK_TIMEOUT:
   // If there is no dialog then ignore the timeout
   if (dialog)
   {
     dialog->id_ack_timeout = 0;
     dialog->timeout(timer);
   }
   break;

  case LTMR_ACK_GUARD:
   // If there is no dialog then ignore the timeout
   if (dialog)
   {
     dialog->id_ack_guard = 0;
     dialog->dur_ack_timeout = 0;
     dialog->timeout(timer);
   }
   break;

  case LTMR_INVITE_COMP:
   id_invite_comp = 0;
   // RFC 3261 13.2.2.4
   // The UAC core considers the INVITE transaction completed
   // 64*T1 seconds after the reception of the first 2XX
   // response.
   // Cleanup all open and pending dialogs
   cleanup_open_pending();
   break;

  case LTMR_NO_ANSWER:
   // User did not answer the call.
   // Reject call or redirect it if CF_NOANSWER is active.
   // If there is no active dialog then ignore the timeout.
   // The timer should have been stopped already.
   log_file->write_report("No answer timeout",
			  "t_line::timeout");

   if (active_dialog)
   {
     assert(phone_user);
     t_user &user_config(phone_user->get_user_profile());
     t_service *srv = phone.ref_service(&user_config);
     if (srv->get_cf_active(CF_NOANSWER, cf_dest))
     {
       log_file->write_report("Call redirection no answer",
			      "t_line::timeout");
       active_dialog->redirect(cf_dest,
			       R_302_MOVED_TEMPORARILY);
     }
     else
     {
       active_dialog->reject(R_480_TEMP_NOT_AVAILABLE,
			     REASON_480_NO_ANSWER);
     }

     ui->cb_answer_timeout(get_line_number());
   }
   break;

  case LTMR_RE_INVITE_GUARD:
   // If there is no dialog then ignore the timeout
   if (dialog)
   {
     dialog->id_re_invite_guard = 0;
     dialog->timeout(timer);
   }
   break;

  case LTMR_GLARE_RETRY:
   // If there is no dialog then ignore the timeout
   if (dialog)
   {
     dialog->id_glare_retry = 0;
     dialog->timeout(timer);
   }
   break;

  case LTMR_100REL_TIMEOUT:
   // If there is no dialog then ignore the timeout
   if (dialog)
   {
     dialog->id_100rel_timeout = 0;
     dialog->timeout(timer);
   }
   break;

  case LTMR_100REL_GUARD:
   // If there is no dialog then ignore the timeout
   if (dialog)
   {
     dialog->id_100rel_guard = 0;
     dialog->dur_100rel_timeout = 0;
     dialog->timeout(timer);
   }
   break;

  case LTMR_CANCEL_GUARD:
   // If there is no dialog then ignore the timeout
   if (dialog)
   {
     dialog->id_cancel_guard = 0;
     dialog->timeout(timer);
   }
   break;

  default:
   assert(false);
  }

  cleanup();
}

void t_line::timeout_sub(t_subscribe_timer timer, t_object_id did,
			 const std::string &event_type,
			 const std::string &event_id)
{
  t_dialog *dialog = get_dialog(did);
  if (dialog) dialog->timeout_sub(timer, event_type, event_id);
  cleanup();
}

bool t_line::match(t_response &r, t_tuid tuid) const
{
  if (open_dialog && open_dialog->match_response(r, tuid))
  {
    return true;
  }

  if (active_dialog && active_dialog->match_response(r, 0))
  {
    return true;
  }

  if (match_response(r, pending_dialogs) != pending_dialogs.end())
  {
    return true;
  }

  if (match_response(r, dying_dialogs) != dying_dialogs.end())
  {
    return true;
  }

  return false;
}

bool t_line::match(t_request &r) const
{
  assert(r.method != CANCEL);
  return (active_dialog && active_dialog->match_request(r));
}

bool t_line::match_cancel(t_request &r, t_tid target_tid) const
{
  assert(r.method == CANCEL);

  // A CANCEL matches a dialog if the target tid equals the tid
  // of the INVITE request.
  return (active_dialog && active_dialog->match_cancel(r, target_tid));
}

bool t_line::match(StunMessage &r, t_tuid tuid) const
{
  if (open_dialog && open_dialog->match_response(r, tuid))
  {
    return true;
  }

  if (active_dialog && active_dialog->match_response(r, tuid))
  {
    return true;
  }

  if (match_response(r, tuid, pending_dialogs) != pending_dialogs.end())
  {
    return true;
  }

  if (match_response(r, tuid, dying_dialogs) != dying_dialogs.end())
  {
    return true;
  }

  return false;
}

bool t_line::match_replaces(const std::string &call_id,
			    const std::string &to_tag,
			    const std::string &from_tag,
			    bool no_fork_req_disposition,
			    bool &early_matched) const
{
  if (active_dialog && active_dialog->match(call_id, to_tag, from_tag))
  {
    early_matched = false;
    return true;
  }

  // RFC 3891 3
  // An early dialog only matches when it was created by the UA
  // As an exception to this rule we accept a match when the incoming
  // request contained a no-fork request disposition. This disposition
  // indicated that the request did not fork. The reason why RFC 3891 3
  // does not allow a match is to avoid problems with forked requests.
  // With this exception, call transfer scenario's during ringing can
  // be implemented.
  t_dialog *d;
  if ((d = match_call_id_tags(call_id, to_tag, from_tag, pending_dialogs)) != nullptr &&
      (d->is_call_id_owner() || no_fork_req_disposition))
  {
    early_matched = true;
    return true;
  }

  return false;
}

bool t_line::is_invite_retrans(t_request &r)
{
  assert(r.method == INVITE);
  return (active_dialog && active_dialog->is_invite_retrans(r));
}

void t_line::process_invite_retrans()
{
  if (active_dialog) active_dialog->process_invite_retrans();
}

std::string t_line::create_user_contact(net::ip::address const &auto_ip) const
{
  assert(phone_user);
  t_user &user_config(phone_user->get_user_profile());
  return user_config.create_user_contact(hide_user, auto_ip);
}

std::string t_line::create_user_uri() const
{
  assert(phone_user);
  t_user &user_config(phone_user->get_user_profile());
  return user_config.create_user_uri(hide_user);
}

std::unique_ptr<t_response>
t_line::create_options_response(t_request &r, bool in_dialog) const
{
  assert(phone_user);
  t_user &user_config(phone_user->get_user_profile());
  return phone.create_options_response(user_config, r, in_dialog);
}

void t_line::send_response(t_response &r, t_tuid tuid, t_tid tid)
{
  if (hide_user)
  {
    r.hdr_privacy.add_privacy(PRIVACY_ID);
  }

  phone.send_response(r, tuid, tid);
}

void t_line::send_request(t_request &r, t_tuid tuid)
{
  assert(phone_user);
  t_user &user_config(phone_user->get_user_profile());
  phone.send_request(user_config, r, tuid);
}

t_phone &t_line::get_phone() const
{
  return phone;
}

unsigned int t_line::get_line_number() const
{
  return line_number;
}

bool t_line::get_is_on_hold() const
{
  return is_on_hold;
}

bool t_line::get_is_muted() const
{
  return is_muted;
}

bool t_line::get_hide_user() const
{
  return hide_user;
}

bool t_line::get_is_transfer_consult(unsigned int &lineno) const
{
  lineno = consult_transfer_from_line;
  return is_transfer_consult;
}

void t_line::set_is_transfer_consult(bool enable, unsigned int lineno)
{
  is_transfer_consult = enable;
  consult_transfer_from_line = lineno;
}

bool t_line::get_to_be_transferred(unsigned int &lineno) const
{
  lineno = consult_transfer_to_line;
  return to_be_transferred;
}

void t_line::set_to_be_transferred(bool enable, unsigned int lineno)
{
  to_be_transferred = enable;
  consult_transfer_to_line = lineno;
}

bool t_line::get_is_encrypted() const
{
  t_audio_session *as = get_audio_session();
  if (as) return as->get_is_encrypted();
  return false;
}

bool t_line::get_try_to_encrypt() const
{
  return try_to_encrypt;
}

bool t_line::get_auto_answer() const
{
  return auto_answer;
}

void t_line::set_auto_answer(bool enable)
{
  auto_answer = enable;
}

bool t_line::is_refer_succeeded() const
{
  if (active_dialog) return active_dialog->refer_succeeded;
  return false;
}

bool t_line::has_media() const
{
  t_session * const session = get_session();
  return (session && !session->receive_host.is_unspec() &&
	  !session->dst_rtp_endpoint.addr().is_unspec());
}

t_url t_line::get_remote_target_uri() const
{
  if (!active_dialog) return t_url();
  return active_dialog->get_remote_target_uri();
}

t_url t_line::get_remote_target_uri_pending() const
{
  if (pending_dialogs.empty()) return t_url();
  return pending_dialogs.front()->get_remote_target_uri();
}

std::string t_line::get_remote_target_display() const
{
  if (!active_dialog) return std::string();
  return active_dialog->get_remote_target_display();
}

std::string t_line::get_remote_target_display_pending() const
{
  if (pending_dialogs.empty()) return std::string();
  return pending_dialogs.front()->get_remote_target_display();
}

t_url t_line::get_remote_uri() const
{
  if (!active_dialog) return t_url();
  return active_dialog->get_remote_uri();
}

t_url t_line::get_remote_uri_pending() const
{
  if (pending_dialogs.empty()) return t_url();
  return pending_dialogs.front()->get_remote_uri();
}

std::string t_line::get_remote_display() const
{
  if (!active_dialog) return std::string();
  return active_dialog->get_remote_display();
}

std::string t_line::get_remote_display_pending() const
{
  if (pending_dialogs.empty()) return std::string();
  return pending_dialogs.front()->get_remote_display();
}

std::string t_line::get_call_id() const
{
  if (!active_dialog) return std::string();
  return active_dialog->get_call_id();
}

std::string t_line::get_call_id_pending() const
{
  if (pending_dialogs.empty()) return std::string();
  return pending_dialogs.front()->get_call_id();
}

std::string t_line::get_local_tag() const
{
  if (!active_dialog) return std::string();
  return active_dialog->get_local_tag();
}

std::string t_line::get_local_tag_pending() const
{
  if (pending_dialogs.empty()) return std::string();
  return pending_dialogs.front()->get_local_tag();
}

std::string t_line::get_remote_tag() const
{
  if (!active_dialog) return std::string();
  return active_dialog->get_remote_tag();
}

std::string t_line::get_remote_tag_pending() const
{
  if (pending_dialogs.empty()) return std::string();
  return pending_dialogs.front()->get_remote_tag();
}

bool t_line::remote_extension_supported(const std::string &extension) const
{
  if (!active_dialog) return false;
  return active_dialog->remote_extension_supported(extension);
}

bool t_line::seize()
{
  // Only an idle line can be seized.
  if (substate != LSSUB_IDLE) return false;

  substate = LSSUB_SEIZED;
  ui->cb_line_state_changed();

  return true;
}

void t_line::unseize()
{
  // Only a seized line can be unseized.
  if (substate != LSSUB_SEIZED) return;

  substate = LSSUB_IDLE;
  ui->cb_line_state_changed();
}

t_session *t_line::get_session() const
{
  return active_dialog ? &active_dialog->get_session() : nullptr;
}

t_audio_session *t_line::get_audio_session() const
{
  return active_dialog ? active_dialog->get_audio_session() : nullptr;
}

void t_line::notify_refer_progress(t_response &r)
{
  if (active_dialog)
  {
    active_dialog->notify_refer_progress(r);
  }
}

void t_line::failed_retrieve()
{
  // Call retrieve failed, so line is still on-hold
  is_on_hold = true;
  ui->cb_line_state_changed();
}

void t_line::failed_hold()
{
  // Call hold failed, so line is not on-hold
  is_on_hold = false;
  ui->cb_line_state_changed();
}

void t_line::retry_retrieve_succeeded()
{
  // Retry of retrieve succeeded, so line is not on-hold anymore
  is_on_hold = false;
  ui->cb_line_state_changed();
}

t_call_info t_line::get_call_info() const
{
  return call_info;
}

void t_line::ci_set_dtmf_supported(bool supported, bool inband, bool info)
{
  call_info.dtmf_supported = supported;
  call_info.dtmf_inband = inband;
  call_info.dtmf_info = info;
}

void t_line::ci_set_last_provisional_reason(const std::string &reason)
{
  call_info.last_provisional_reason = reason;
}

void t_line::ci_set_send_codec(t_audio_codec codec)
{
  call_info.send_codec = codec;
}

void t_line::ci_set_recv_codec(t_audio_codec codec)
{
  call_info.recv_codec = codec;
}

void t_line::ci_set_refer_supported(bool supported)
{
  call_info.refer_supported = supported;
}

unsigned short t_line::get_rtp_port() const
{
  return rtp_port;
}

t_user *t_line::get_user() const
{
  t_user * const user_config =
    phone_user ? &phone_user->get_user_profile() : nullptr;
  return user_config;
}

t_phone_user *t_line::get_phone_user() const
{
  return phone_user;
}

std::string t_line::get_ringtone() const
{
  assert(phone_user);
  t_user &user_config(phone_user->get_user_profile());

  if (!user_defined_ringtone.empty())
  {
    // Ring tone returned by incoming call script
    return user_defined_ringtone;
  }
  else if (!user_config.get_ringtone_file().empty())
  {
    // Ring tone from user profile
    return user_config.get_ringtone_file();
  }
  else
  {
    const std::string &ringtone_file(settings->get_child("ringtone")->get_string("ringtone-file"));
    if (!ringtone_file.empty())
    {
      // Ring tone from system settings
      return ringtone_file;
    }
    else
    {
      // Twinkle default
      return FILE_RINGTONE;
    }
  }
}

void t_line::confirm_zrtp_sas()
{
  if (t_audio_session * const as = get_audio_session())
  {
    if (!as->get_zrtp_sas_confirmed())
    {
      as->confirm_zrtp_sas();
      ui->cb_zrtp_sas_confirmed(line_number);
      ui->cb_line_state_changed();
      log_file->write_header("t_line::confirm_zrtp_sas");
      log_file->write_raw("Line ");
      log_file->write_raw(line_number + 1);
      log_file->write_raw(": User confirmed ZRTP SAS\n");
      log_file->write_footer();
    }
  }
}

void t_line::reset_zrtp_sas_confirmation()
{
  if (t_audio_session * const as = get_audio_session())
  {
    if (as->get_zrtp_sas_confirmed())
    {
      as->reset_zrtp_sas_confirmation();
      ui->cb_zrtp_sas_confirmation_reset(line_number);
      ui->cb_line_state_changed();
      log_file->write_header("t_line::reset_zrtp_sas_confirmation");
      log_file->write_raw("Line ");
      log_file->write_raw(line_number + 1);
      log_file->write_raw(": User reset ZRTP SAS confirmation\n");
      log_file->write_footer();
    }
  }
}

void t_line::enable_zrtp()
{
  if (t_audio_session * const as = get_audio_session())
  {
    as->enable_zrtp();
  }
}

void t_line::zrtp_request_go_clear()
{
  if (t_audio_session * const as = get_audio_session())
  {
    as->zrtp_request_go_clear();
  }
}

void t_line::zrtp_go_clear_ok()
{
  if (t_audio_session * const as = get_audio_session())
  {
    as->zrtp_go_clear_ok();
  }
}

void t_line::force_idle()
{
  cleanup_forced();
}

void t_line::set_keep_seized(bool seize)
{
  keep_seized = seize;
  cleanup();
}

bool t_line::get_keep_seized() const
{
  return keep_seized;
}

t_dialog *t_line::get_dialog_with_active_session() const
{
  if (open_dialog && open_dialog->has_active_session())
  {
    return open_dialog.get();
  }

  if (active_dialog && active_dialog->has_active_session())
  {
    return active_dialog.get();
  }

  for (const std::unique_ptr<t_dialog> &elem : pending_dialogs)
  {
    if (elem->has_active_session())
    {
      return elem.get();
    }
  }

  for (const std::unique_ptr<t_dialog> &elem : dying_dialogs)
  {
    if (elem->has_active_session())
    {
      return elem.get();
    }
  }

  return nullptr;
}
