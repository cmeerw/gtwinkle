/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _SYS_SETTINGS_H
#define _SYS_SETTINGS_H

#include "parser/sip_message.h"
#include "sockets/url.h"
#include "gtwinkle_config.h"

#include <cstdlib>
#include <list>
#include <memory>
#include <mutex>
#include <string>

#include <giomm.h>

/** @name General system settings */
//@{
/** User directory, relative to the home directory ($HOME) */
#define DIR_USER	".twinkle"

/** Home directory */
#define DIR_HOME	(getenv("HOME"))

/** Directory for storing temporary files, relative to @ref DIR_USER */
#define DIR_TMPFILE	"tmp"

/** Device file for DSP */
#define DEV_DSP		"/dev/dsp"

/** Device prefixes in settings file */
#define PFX_OSS		"oss:"
#define PFX_ALSA	"alsa:"

/** ALSA default device */
#define DEV_ALSA_DFLT	"alsa:default"

/** Device string for other device */
#define DEV_OTHER	"other device"

/** File with CLI command history */
#define FILE_CLI_HISTORY "twinkle.history"
//@}


/** System settings */
class t_sys_settings {
private:
  // Mutex to avoid sync concurrent access
  mutable std::recursive_mutex	mtx_sys;

  /** File descriptor of lock file */
  int fd_lock_file;

  // Share directory for files applicable to all users
  std::string		dir_share;

  // Full file name for config file
  std::string		filename;

  /** The full path name of the shared mime database */
  std::string	mime_shared_database;

public:
  /** Constructor */
  t_sys_settings();

  /** @name Getters */
  //@{
  std::string get_mime_shared_database() const;
  //@}

  /** @name Setters */
  //@{
  void set_mime_shared_database(const std::string &filename);
  //@}

  /**
   * Get "about" text.
   * @param html [in] Indicates if "about" text must be in HTML format.
   * @return The "about" text"
   */
  std::string about(bool html) const;

  /**
   * Get produce release date.
   * @return product release date in locale format
   */
  std::string get_product_date() const;

  /**
   * Get a string of options that are built, e.g. ALSA, KDE
   * @return The string of options.
   */
  std::string get_options_built() const;

  /**
   * Check if the environment of the machine satisfies all requirements.
   * @param error_msg [out] User readable error message when false is returned.
   * @return true if all requirements are met.
   * @return false, otherwise and error_msg contains an appropriate
   * error message to show the user.
   */
  bool check_environment(std::string &error_msg) const;

  /**
   * Set the share directory
   * @param dir [in] Absolute path of the share directory.
   */
  void set_dir_share(const std::string &dir);

  /**
   * Get the share directory.
   * @return Absolute path of the directory with shared files.
   */
  std::string get_dir_share() const;

  /**
   * Get the directory containing language translation files.
   * @return Absolute path of the language directory.
   */
  std::string get_dir_lang() const;

  /**
   * Get the user directory.
   * @return Absolute path of the user directory.
   */
  std::string get_dir_user() const;

  /**
   * Get the CLI command history file.
   * @return Full pathname of the history file.
   */
  std::string get_history_file() const;

  /**
   * Get the temporary file directory.
   * @return The full pathname of the temporary file directory.
   */
  std::string get_dir_tmpfile() const;

  /**
   * Check if a file is located in the temporary file directory.
   * @return true if the file is in the temporary file directory, false otherwise.
   */
  bool is_tmpfile(const std::string &filename) const;

  /**
   * Save data to a temporary file.
   * @param data [in] Data to save.
   * @param file_extension [in] Extension (glob) for file name.
   * @param filename [out] File name of save file, relative to the tmp directory.
   * @param error_msg [out] If saving failed, then this parameter contains an
   *        error message.
   * @return true if saving succeeded, false otherwise.
   */
  bool save_tmp_file(const std::string &data, const std::string &file_extension,
		     std::string &filename, std::string &error_msg);


  /** Remove all files from the temporary file directory */
  void remove_all_tmp_files() const;

  /** @name Lock file operations */
  /**
   * Create a lock file if it does not exist yet and take a file lock on it.
   * @param shared_lock [in] Indicates if the file lock must be shared or exclusive.
   *        A shared lock is needed when the users forces multiple Twinkle processes
   *        to run.
   * @param error_msg [out] Error message if the operation fails.
   * @param already_running [out] If the operation fails, this flag indicates Twinkle
   *        is already running.
   * @return True if the file is locked succesfully.
   * @return False if the file could not be locked.
   */
  bool create_lock_file(bool shared_lock, std::string &error_msg, bool &already_running);

  /** Unlock the lock file. */
  void delete_lock_file();

  // Check validate the audio devices flagged as true.
  // If audio validation is turned off then always true is returned.
  bool exec_audio_validation(bool ringtone, bool speaker, bool mic,
			     std::string &error_msg) const;
};

extern std::unique_ptr<t_sys_settings> sys_config;

extern Glib::RefPtr<Gio::Settings> settings;

#endif
