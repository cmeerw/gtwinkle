/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/**
 * @file
 * Sequence number operations.
 */

#ifndef _SEQUENCE_NUMBER_H
#define _SEQUENCE_NUMBER_H

#include <type_traits>

#include <commoncpp/config.h>

/**
 * Sequence numbers.
 * Sequence numbers with comparison operators that deal with sequence number
 * roll-overs using serial number arithmetic.
 * See http://en.wikipedia.org/wiki/Serial_Number_Arithmetic
 *
 * @param U The unsigned int type for the sequence number.
 */
template< typename U >
class sequence_number_t
{
private:
  /**
   * The sequence number.
   */
  U _number;

public:
  /**
   * Constructor.
   * @param number The sequence number.
   */
  explicit sequence_number_t(U number)
   : _number(number)
  { }

  /**
   * Get the sequence number.
   * @return The sequence number.
   */
  U get_number() const
  {
    return _number;
  }

  /**
   * Cast to the sequence number.
   */
  explicit operator U() const
  {
    return get_number();
  }

  /**
   * Calculate the distance to another sequence number.
   * @param number The sequence number to which the distance must be calculated.
   * @return The distance.
   */
  auto distance(const sequence_number_t &number) const
  {
    return static_cast<std::make_signed_t<U>>(get_number() - number.get_number());
  }

  /**
   * Calculate the distance to another distance sequence number.
   * @param number The sequence number to which the distance must be calculated.
   * @return The distance.
   */
  auto operator-(const sequence_number_t &number) const
  {
    return distance(number);
  }

  /**
   * Less-than comparison.
   * @param number The sequence number to compare with.
   * @return true, if this sequence number is less than number.
   * @return false, otherwise.
   */
  friend bool operator <(const sequence_number_t &lhs,
			 const sequence_number_t &rhs) noexcept
  {
    return lhs.distance(rhs) < 0;
  }

  /**
   * Greater-than comparison.
   * @param number The sequence number to compare with.
   * @return true, if this sequence number is greater than number.
   * @return false, otherwise.
   */
  friend bool operator >(const sequence_number_t &lhs,
			 const sequence_number_t &rhs) noexcept
  {
    return lhs.distance(rhs) > 0;
  }

  /**
   * Equality comparison.
   * @param number The sequence number to compare with.
   * @return true, if this sequence number is equal to number.
   * @return false, otherwise.
   */
  friend bool operator ==(const sequence_number_t &lhs,
			  const sequence_number_t &rhs) noexcept
  {
    return lhs.get_number() == rhs.get_number();
  }

  friend bool operator !=(const sequence_number_t &lhs,
			  const sequence_number_t &rhs) noexcept
  {
    return !(lhs == rhs);
  }

  /**
   * Less-than-equal comparison.
   * @param number The sequence number to compare with.
   * @return true, if this sequence number is less than or equal to number.
   * @return false, otherwise.
   */
  friend bool operator <=(const sequence_number_t &lhs,
			  const sequence_number_t &rhs) noexcept
  {
    return !(lhs > rhs);
  }

  /**
   * Greater-than-equal comparison.
   * @param number The sequence number to compare with.
   * @return true, if this sequence number is greater than or equal to number.
   * @return false, otherwise.
   */
  friend bool operator >=(const sequence_number_t &lhs,
			  const sequence_number_t &rhs) noexcept
  {
    return !(lhs < rhs);
  }
};

/**
 * 16-bit sequence number
 */
using seq16_t = sequence_number_t<uint16>;

/**
 * 32-bit sequence number
 */
using seq32_t = sequence_number_t<uint32>;

#endif
