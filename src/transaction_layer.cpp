/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <cassert>
#include <iostream>
#include "events.h"
#include "transaction_layer.h"
#include "userintf.h"
#include "util.h"

#include <memory>

extern std::unique_ptr<t_event_queue> evq_trans_mgr;
extern std::unique_ptr<t_event_queue> evq_trans_layer;
extern bool end_app;

t_transaction_layer::~t_transaction_layer() = default;

void t_transaction_layer::recvd_response(t_response &r, t_tuid tuid,
					 t_tid tid)
{
  std::lock_guard<t_transaction_layer> g(*this);

  switch(r.get_class())
  {
  case R_1XX:
   recvd_provisional(r, tuid, tid);
   break;
  case R_2XX:
   recvd_success(r, tuid, tid);
   break;
  case R_3XX:
   recvd_redirect(r, tuid, tid);
   break;
  case R_4XX:
   recvd_client_error(r, tuid, tid);
   break;
  case R_5XX:
   recvd_server_error(r, tuid, tid);
   break;
  case R_6XX:
   recvd_global_error(r, tuid, tid);
   break;
  default:
   assert(false);
   break;
  }

  post_process_response(r, tuid, tid);
}

void t_transaction_layer::recvd_request(t_request &r, t_tid tid,
					t_tid tid_cancel_target)
{
  bool fatal;
  std::string reason;

  std::lock_guard<t_transaction_layer> g(*this);

  // Return a 400 response if the SIP headers are wrong
  if (!r.is_valid(fatal, reason))
  {
    std::unique_ptr<t_response> resp(
      r.create_response(R_400_BAD_REQUEST, std::move(reason)));
    send_response(*resp, 0, tid);
    return;
  }

  // Return a 400 response if the SIP body contained a parse error
  if (r.body && r.body->invalid)
  {
    std::unique_ptr<t_response> resp(
      r.create_response(R_400_BAD_REQUEST, "Invalid SIP body."));
    send_response(*resp, 0, tid);
    return;
  }

  // If a message exceeded the maximum message size, than the body
  // is not parsed by the listener.
  if (r.hdr_content_length.is_populated() &&
      r.hdr_content_length.length > 0 &&
      !r.body)
  {
    std::unique_ptr<t_response> resp(
      r.create_response(R_513_MESSAGE_TOO_LARGE));
    send_response(*resp, 0, tid);
    return;
  }

  // RFC 3261 8.2.3
  // Return a 415 response if content encoding is not supported
  if (r.body && r.hdr_content_encoding.is_populated())
  {
    for (auto const & coding : r.hdr_content_encoding.coding_list)
    {
      if (!CONTENT_ENCODING_SUPPORTED(coding.content_coding))
      {
	std::unique_ptr<t_response> resp(
	  r.create_response(R_415_UNSUPPORTED_MEDIA_TYPE));
	SET_HDR_ACCEPT_ENCODING(resp->hdr_accept_encoding);
	send_response(*resp, 0, tid);
	return;
      }
    }
  }

  // Check if URI scheme is supported
  if (r.uri.get_scheme() != "sip")
  {
    std::unique_ptr<t_response> resp(
      r.create_response(R_416_UNSUPPORTED_URI_SCHEME));
    send_response(*resp, 0, tid);
    return;
  }

  switch(r.method)
  {
  case INVITE:
   recvd_invite(r, tid);
   break;
  case ACK:
   recvd_ack(r, tid);
   break;
  case CANCEL:
   recvd_cancel(r, tid, tid_cancel_target);
   break;
  case BYE:
   recvd_bye(r, tid);
   break;
  case OPTIONS:
   recvd_options(r, tid);
   break;
  case REGISTER:
   recvd_register(r, tid);
   break;
  case PRACK:
   recvd_prack(r, tid);
   break;
  case SUBSCRIBE:
   recvd_subscribe(r, tid);
   break;
  case NOTIFY:
   recvd_notify(r, tid);
   break;
  case REFER:
   recvd_refer(r, tid);
   break;
  case INFO:
   recvd_info(r, tid);
   break;
  case MESSAGE:
   recvd_message(r, tid);
   break;
  default:
  {
    std::unique_ptr<t_response> resp(
      r.create_response(R_501_NOT_IMPLEMENTED));
    send_response(*resp, 0, tid);
  }
  break;
  }

  post_process_request(r, tid, tid_cancel_target);
}

void t_transaction_layer::recvd_async_response(t_event_async_response &event)
{
  std::lock_guard<t_transaction_layer> g(*this);

  switch (event.get_response_type())
  {
  case t_event_async_response::RESP_REFER_PERMISSION:
   recvd_refer_permission(event.get_bool_response());
   break;
  default:
   // Ignore other responses
   break;
  }
}

void t_transaction_layer::send_request(t_user &user_config, t_request &r, t_tuid tuid)
{
  evq_trans_mgr->push_user(user_config, (t_sip_message &)r, tuid, 0);
}

void t_transaction_layer::send_request(t_user &user_config, StunMessage &r, t_tuid tuid)
{
  // The transaction manager will determine the destination IP and port,
  // so they can be left to zero in the event.
  evq_trans_mgr->push_stun_request(user_config, r, TYPE_STUN_SIP, tuid, 0, net::ip::endpoint());
}

void t_transaction_layer::send_response(t_response &r, t_tuid tuid,
					t_tid tid)
{
  evq_trans_mgr->push_user((t_sip_message &)r, tuid, tid);
}

void t_transaction_layer::run()
{
  bool quit = false;

  while (!quit)
  {
    std::unique_ptr<t_event> event(evq_trans_layer->pop());

    switch (event->get_type())
    {
    case EV_USER:
    {
      auto &ev_user(static_cast<t_event_user &>(*event));
      auto tid = ev_user.get_tid();
      auto tuid = ev_user.get_tuid();
      auto tid_cancel = ev_user.get_tid_cancel_target();
      auto &msg(ev_user.get_msg());

      switch(msg.get_type())
      {
      case MSG_REQUEST:
       recvd_request(static_cast<t_request &>(msg), tid, tid_cancel);
       break;

      case MSG_RESPONSE:
       recvd_response(static_cast<t_response &>(msg), tuid, tid);
       break;

      default:
       assert(false);
       break;
      }
    }
    break;

    case EV_TIMEOUT:
    {
      auto &ev_timeout(static_cast<t_event_timeout &>(*event));
      handle_event_timeout(ev_timeout);
    }
    break;

    case EV_FAILURE:
    {
      auto &ev_failure(static_cast<t_event_failure &>(*event));
      auto tid = ev_failure.get_tid();

      std::lock_guard<t_transaction_layer> g(*this);
      failure(ev_failure.get_failure(), tid);
    }
    break;

    case EV_STUN_RESPONSE:
    {
      auto &ev_stun_resp(static_cast<t_event_stun_response &>(*event));
      auto tid = ev_stun_resp.get_tid();
      auto tuid = ev_stun_resp.get_tuid();
      auto &stun_msg(ev_stun_resp.get_msg());

      recvd_stun_resp(stun_msg, tuid, tid);
    }
    break;

    case EV_ASYNC_RESPONSE:
    {
      auto &ev_async_resp(static_cast<t_event_async_response &>(*event));

      recvd_async_response(ev_async_resp);
    }
    break;

    case EV_BROKEN_CONNECTION:
    {
      auto &ev_broken_connection(static_cast<t_event_broken_connection &>(*event));

      handle_broken_connection(ev_broken_connection);
    }
    break;

    case EV_QUIT:
    {
      quit = true;
    }
    break;

    default:
    {
      // other types of event are not expected
      assert(false);
    }
    break;
    }
  }
}

void t_transaction_layer::lock() const
{
  // Prohibited threads may not lock the transaction layer
  assert(!is_prohibited_thread());

  // The user interface and transaction layer threads both call
  // functions on the transaction layer. By locking the UI mutex
  // first, a deadlock can never occur as the UI also takes the
  // UI lock first and then the transaction layer lock.
  // During shutdown of Twinkle the GUI has exited already and
  // a lock on an exited QApplication causes a segmentation fault.
  // Therefore the lock on the UI should not be taken during shutdown.
  if (!end_app) ui->lock();
  tl_mutex.lock();
}

void t_transaction_layer::unlock() const
{
  tl_mutex.unlock();
  if (!end_app) ui->unlock();
}
