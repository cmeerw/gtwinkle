/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "sip_body.h"

#include "log.h"
#include "protocol.h"
#include "sip_message.h"
#include "util.h"
#include "audio/rtp_telephone_event.h"

#include <cassert>
#include <cstdlib>
#include <vector>

////////////////////////////////////
// class t_sip_body
////////////////////////////////////

t_sip_body::t_sip_body()
  : invalid (false)
{ }

bool t_sip_body::local_ip_check() const
{
  return true;
}

size_t t_sip_body::get_encoded_size() const
{
  return encode().size();
}

////////////////////////////////////
// class t_sip_xml_body
////////////////////////////////////

void t_sip_body_xml::create_xml_doc(const std::string &xml_version,
				    const std::string &charset)
{
  // XML doc
  xml_doc.reset(xmlNewDoc(BAD_CAST xml_version.c_str()));
  xml_doc->encoding = xmlCharStrdup(charset.c_str());
}

void t_sip_body_xml::copy_xml_doc(t_sip_body_xml *to_body) const
{
  to_body->xml_doc.reset();

  if (xml_doc)
  {
    to_body->xml_doc.reset(xmlCopyDoc(xml_doc.get(), 1));
    if (!to_body->xml_doc)
    {
      log_file->write_report("Failed to copy xml document.",
			     "t_sip_body_xml::copy",
			     LOG_NORMAL, LOG_CRITICAL);
    }
  }
}

t_sip_body_xml::t_sip_body_xml()
  : xml_doc(nullptr, &::xmlFreeDoc)
{ }

t_sip_body_xml::t_sip_body_xml(t_sip_body_xml const &o)
  : t_sip_body(o), xml_doc(nullptr, &::xmlFreeDoc)
{
  o.copy_xml_doc(this);
}

t_sip_body_xml::~t_sip_body_xml() = default;

std::string t_sip_body_xml::encode() const
{
  if (!xml_doc)
  {
    auto * const self(const_cast<t_sip_body_xml *>(this));
    self->create_xml_doc();
  }
  assert(xml_doc);

  xmlChar *buf;
  int buf_size;

  xmlDocDumpMemory(xml_doc.get(), &buf, &buf_size);
  std::string result((char*)buf);
  xmlFree(buf);

  return result;
}

bool t_sip_body_xml::parse(const std::string &s)
{
  assert(!xml_doc);

  xml_doc.reset(xmlReadMemory(s.c_str(), s.size(), "noname.xml", nullptr,
			      XML_PARSE_NOBLANKS | XML_PARSE_NOERROR | XML_PARSE_NOWARNING));
  if (!xml_doc)
  {
    log_file->write_report("Failed to parse xml document.",
			   "t_sip_body_xml::parse",
			   LOG_NORMAL, LOG_WARNING);
  }

  return static_cast<bool>(xml_doc);
}

////////////////////////////////////
// class t_sip_body_opaque
////////////////////////////////////

t_sip_body_opaque::t_sip_body_opaque() = default;

t_sip_body_opaque::t_sip_body_opaque(std::string s)
  : opaque(std::move(s))
{ }

std::string t_sip_body_opaque::encode() const
{
  return opaque;
}

t_sip_body_opaque *t_sip_body_opaque::copy() const
{
  std::unique_ptr<t_sip_body_opaque> sb(new t_sip_body_opaque(*this));
  return sb.release();
}

t_body_type t_sip_body_opaque::get_type() const
{
  return BODY_OPAQUE;
}

t_media t_sip_body_opaque::get_media() const
{
  return t_media("application", "octet-stream");
}

size_t t_sip_body_opaque::get_encoded_size() const
{
  return opaque.size();
}

////////////////////////////////////
// class t_sip_body_sipfrag
////////////////////////////////////

t_sip_body_sipfrag::t_sip_body_sipfrag(t_sip_message const &m)
  : sipfrag(m.copy())
{ }

t_sip_body_sipfrag::~t_sip_body_sipfrag() = default;

std::string t_sip_body_sipfrag::encode() const
{
  return sipfrag->encode(false);
}

t_sip_body_sipfrag *t_sip_body_sipfrag::copy() const
{
  std::unique_ptr<t_sip_body_sipfrag> sb(new t_sip_body_sipfrag(*sipfrag));
  return sb.release();
}

t_body_type t_sip_body_sipfrag::get_type() const
{
  return BODY_SIPFRAG;
}

t_media t_sip_body_sipfrag::get_media() const
{
  return t_media("message", "sipfrag");
}

////////////////////////////////////
// class t_sip_body_dtmf_relay
////////////////////////////////////

t_sip_body_dtmf_relay::t_sip_body_dtmf_relay()
  : signal ('0'), duration (250)
{ }

t_sip_body_dtmf_relay::t_sip_body_dtmf_relay(char _signal,
					     unsigned short _duration)
  : signal (_signal), duration (_duration)
{ }

std::string t_sip_body_dtmf_relay::encode() const
{
  std::string s = "Signal=";
  s += signal;
  s += CRLF;

  s += "Duration=";
  s += int2str(duration);
  s += CRLF;

  return s;
}

t_sip_body_dtmf_relay *t_sip_body_dtmf_relay::copy() const
{
  std::unique_ptr<t_sip_body_dtmf_relay> sb(new t_sip_body_dtmf_relay(*this));
  return sb.release();
}

t_body_type t_sip_body_dtmf_relay::get_type() const
{
  return BODY_DTMF_RELAY;
}

t_media t_sip_body_dtmf_relay::get_media() const
{
  return t_media("application", "dtmf-relay");
}

bool t_sip_body_dtmf_relay::parse(const std::string &s)
{
  signal = 0;
  duration = 250;

  bool valid = false;

  for (const auto & l : split_linebreak(s))
  {
    const std::string &line(trim(l));
    if (line.empty()) continue;

    const std::vector<std::string> &c(split_on_first(line, '='));
    if (c.size() != 2) continue;

    std::string parameter = tolower(trim(c[0]));
    std::string value = tolower(trim(c[1]));

    if (value.empty()) continue;

    if (parameter == "signal")
    {
      if (!VALID_DTMF_SYM(value[0])) return false;
      signal = value[0];
      valid = true;
    }
    else if (parameter == "duration")
    {
      duration = atoi(value.c_str());
      if (duration == 0) return false;
    }
  }

  return valid;
}

////////////////////////////////////
// class t_sip_body_plain_text
////////////////////////////////////

t_sip_body_plain_text::t_sip_body_plain_text() = default;

t_sip_body_plain_text::t_sip_body_plain_text(std::string _text)
  : text(std::move(_text))
{ }

std::string t_sip_body_plain_text::encode() const
{
  return text;
}

t_sip_body_plain_text *t_sip_body_plain_text::copy() const
{
  std::unique_ptr<t_sip_body_plain_text> sb(new t_sip_body_plain_text(*this));
  return sb.release();
}

t_body_type t_sip_body_plain_text::get_type() const
{
  return BODY_PLAIN_TEXT;
}

t_media t_sip_body_plain_text::get_media() const
{
  return t_media("text", "plain");
}

size_t t_sip_body_plain_text::get_encoded_size() const
{
  return text.size();
}

////////////////////////////////////
// class t_sip_body_html_text
////////////////////////////////////

t_sip_body_html_text::t_sip_body_html_text(std::string _text)
  : text(std::move(_text))
{ }

std::string t_sip_body_html_text::encode() const
{
  return text;
}

t_sip_body_html_text *t_sip_body_html_text::copy() const
{
  std::unique_ptr<t_sip_body_html_text> sb(new t_sip_body_html_text(*this));
  return sb.release();
}

t_body_type t_sip_body_html_text::get_type() const
{
  return BODY_HTML_TEXT;
}

t_media t_sip_body_html_text::get_media() const
{
  return t_media("text", "html");
}

size_t t_sip_body_html_text::get_encoded_size() const
{
  return text.size();
}
