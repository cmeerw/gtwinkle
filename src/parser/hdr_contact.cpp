/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "definitions.h"
#include "hdr_contact.h"
#include "parse_ctrl.h"
#include "util.h"

#include <algorithm>

t_contact_param::t_contact_param()
  : expires_present (false), expires (0),
    qvalue_present (false), qvalue (-1.0)
{ }

void t_contact_param::add_extension(const t_parameter &p)
{
  extensions.push_back(p);
}

std::string t_contact_param::encode() const
{
  std::string s;

  if (!display.empty())
  {
    s += '"';
    s += escape(display, '"');
    s += '"';
    s += ' ';
  }

  s += '<';
  s += uri.encode();
  s += '>';

  if (qvalue_present)
  {
    s += ";q=";
    s += float2str(qvalue, 3);
  }

  if (expires_present) s += ulong2str(expires, ";expires=%u");
  s += param_list2str(extensions);

  return s;
}


t_hdr_contact::t_hdr_contact() : t_header("Contact", "m")
{
  any_flag = false;
}

void t_hdr_contact::add_contact(const t_contact_param &contact)
{
  populated = true;
  contact_list.push_back(contact);
}

void t_hdr_contact::add_contacts(const std::vector<t_contact_param> &l)
{
  populated = true;

  for (auto const & contact : l)
  {
    contact_list.push_back(contact);
  }
}

void t_hdr_contact::set_contacts(const std::vector<t_contact_param> &l)
{
  populated = true;
  contact_list = l;
}

void t_hdr_contact::set_contacts(const std::vector<t_url> &l)
{
  t_contact_param c;

  populated = true;

  float q = 0.9;
  contact_list.clear();
  for (const t_url & url : l)
  {
    c.uri = url;
    c.set_qvalue(q);
    contact_list.push_back(c);
    q = std::max(q - 0.1f, 0.1f);
  }
}

void t_hdr_contact::set_contacts(const std::vector<t_display_url> &l)
{
  t_contact_param c;

  populated = true;

  float q = 0.9;
  contact_list.clear();
  for (const t_display_url & display : l)
  {
    c.uri = display.url;
    c.display = display.display;
    c.set_qvalue(q);
    contact_list.push_back(c);
    q = std::max(q - 0.1f, 0.1f);
  }
}

void t_hdr_contact::set_any()
{
  populated = true;
  any_flag = true;
  contact_list.clear();
}

t_contact_param *t_hdr_contact::find_contact(const t_url &u)
{
  for (auto & contact : contact_list)
  {
    if (u.sip_match(contact.uri)) return &contact;
  }

  return nullptr;
}

bool t_contact_param::is_expires_present() const
{
  return expires_present;
}

unsigned long t_contact_param::get_expires() const
{
  return expires;
}

void t_contact_param::set_expires(unsigned long e)
{
  expires_present = true;
  expires = e;
}

float t_contact_param::get_qvalue() const
{
  return qvalue;
}

void t_contact_param::set_qvalue(float q)
{
  qvalue_present = true;
  qvalue = q;
}

std::string t_hdr_contact::encode_value() const
{
  std::string s;

  if (!populated) return s;

  if (any_flag)
  {
    s += '*';
    return s;
  }

  bool first = true;
  for (auto const & contact : contact_list)
  {
    if (! first) s += ", ";
    else first = false;

    s += contact.encode();
  }

  return s;
}
