/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "parameter.h"
#include "util.h"

t_parameter::t_parameter()
  : type(VALUE)
{ }

t_parameter::t_parameter(std::string n)
  : type(NOVALUE), name(std::move(n))
{ }

t_parameter::t_parameter(std::string n, std::string v)
  : type(VALUE), name(std::move(n)), value(std::move(v))
{ }

std::string t_parameter::encode() const
{
  std::string s;

  s += name;

  if (type == VALUE)
  {
    s += '=';
    if (must_quote(value))
    {
      s += '\"' + value + '\"';
    } else {
      s += value;
    }
  }

  return s;
}

t_parameter str2param(const std::string &s)
{
  const std::vector<std::string> &l(split_on_first(s, '='));
  if (l.size() == 1)
  {
    return t_parameter(s);
  }
  else
  {
    return t_parameter(trim(l[0]), trim(l[1]));
  }
}

std::string param_list2str(const std::vector<t_parameter> &l)
{
  std::string s;

  for (const t_parameter &param : l)
  {
    s += ';';
    s += param.encode();
  }

  return s;
}

std::vector<t_parameter> str2param_list(const std::string &s)
{
  std::vector<t_parameter> result;

  for (const std::string &param : split(s, ';'))
  {
    result.push_back(str2param(trim(param)));
  }

  return result;
}
