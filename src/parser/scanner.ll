/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

%{
#include <cstdio>
#include <cstring>
#include <math.h>
#include <string>

#include "parser/parse_ctrl.h"
#include "parser.tab.hpp"
#include "util.h"

#define yyterminate() return yy::sip_parser::symbol_type(yy::sip_parser::token_type())
#define YY_DECL yy::sip_parser::symbol_type yylex (void *yyscanner)
#define YY_EXTRA_TYPE t_parser::yycontext *
#define YYSTYPE void
%}

%option case-insensitive
%option reentrant
%option noyywrap
%option stack
%option nounput

DIGIT		[0-9]
HEXDIG		[0-9a-fA-F]
ALPHA		[a-zA-Z]
CAPITALS	[A-Z]
ALNUM		[a-zA-Z0-9]
TOKEN_SYM	[[:alnum:]\-\.!%\*_\+\`\'~]
WORD_SYM	[[:alnum:]\-\.!%\*_\+\`\'~\(\)<>:\\\"\/\[\]\?\{\}]

%x C_URI
%x C_URI_SPECIAL
%x C_QSTRING
%x C_LANG
%x C_WORD
%x C_NUM
%x C_DATE
%x C_LINE
%x C_COMMENT
%x C_NEW
%x C_AUTH_SCHEME
%x C_RPAREN
%x C_IPV6ADDR
%x C_PARAMVAL

%%
	switch (yyextra->lex_state) {
	case t_parser::X_URI:		BEGIN(C_URI); break;
	case t_parser::X_URI_SPECIAL:	BEGIN(C_URI_SPECIAL); break;
	case t_parser::X_LANG:		BEGIN(C_LANG); break;
	case t_parser::X_WORD:		BEGIN(C_WORD); break;
	case t_parser::X_NUM:		BEGIN(C_NUM); break;
	case t_parser::X_DATE:		BEGIN(C_DATE); break;
	case t_parser::X_LINE:		BEGIN(C_LINE); break;
	case t_parser::X_COMMENT:	BEGIN(C_COMMENT); break;
	case t_parser::X_NEW:		BEGIN(C_NEW); break;
	case t_parser::X_AUTH_SCHEME:	BEGIN(C_AUTH_SCHEME); break;
	case t_parser::X_IPV6ADDR:	BEGIN(C_IPV6ADDR); break;
	case t_parser::X_PARAMVAL:	BEGIN(C_PARAMVAL); break;
	default:			BEGIN(INITIAL);
	}

	/* Headers */
^Accept			{ return yy::sip_parser::make_T_HDR_ACCEPT(); }
^Accept-Encoding	{ return yy::sip_parser::make_T_HDR_ACCEPT_ENCODING(); }
^Accept-Language	{ return yy::sip_parser::make_T_HDR_ACCEPT_LANGUAGE(); }
^Alert-Info		{ return yy::sip_parser::make_T_HDR_ALERT_INFO(); }
^Allow			{ return yy::sip_parser::make_T_HDR_ALLOW(); }
^(Allow-Events)|u	{ return yy::sip_parser::make_T_HDR_ALLOW_EVENTS(); }
^Authentication-Info	{ return yy::sip_parser::make_T_HDR_AUTHENTICATION_INFO(); }
^Authorization		{ return yy::sip_parser::make_T_HDR_AUTHORIZATION(); }
^(Call-ID)|i		{ return yy::sip_parser::make_T_HDR_CALL_ID(); }
^Call-Info		{ return yy::sip_parser::make_T_HDR_CALL_INFO(); }
^(Contact)|m		{ return yy::sip_parser::make_T_HDR_CONTACT(); }
^Content-Disposition	{ return yy::sip_parser::make_T_HDR_CONTENT_DISP(); }
^(Content-Encoding)|e	{ return yy::sip_parser::make_T_HDR_CONTENT_ENCODING(); }
^Content-Language	{ return yy::sip_parser::make_T_HDR_CONTENT_LANGUAGE(); }
^(Content-Length)|l	{ return yy::sip_parser::make_T_HDR_CONTENT_LENGTH(); }
^(Content-Type)|c	{ return yy::sip_parser::make_T_HDR_CONTENT_TYPE(); }
^CSeq			{ return yy::sip_parser::make_T_HDR_CSEQ(); }
^Date			{ return yy::sip_parser::make_T_HDR_DATE(); }
^Error-Info		{ return yy::sip_parser::make_T_HDR_ERROR_INFO(); }
^(Event)|o		{ return yy::sip_parser::make_T_HDR_EVENT(); }
^Expires		{ return yy::sip_parser::make_T_HDR_EXPIRES(); }
^(From|f)		{ return yy::sip_parser::make_T_HDR_FROM(); }
^In-Reply-To		{ return yy::sip_parser::make_T_HDR_IN_REPLY_TO(); }
^Max-Forwards		{ return yy::sip_parser::make_T_HDR_MAX_FORWARDS(); }
^Min-Expires		{ return yy::sip_parser::make_T_HDR_MIN_EXPIRES(); }
^MIME-Version		{ return yy::sip_parser::make_T_HDR_MIME_VERSION(); }
^Organization		{ return yy::sip_parser::make_T_HDR_ORGANIZATION(); }
^P-Asserted-Identity	{ return yy::sip_parser::make_T_HDR_P_ASSERTED_IDENTITY(); }
^P-Preferred-Identity	{ return yy::sip_parser::make_T_HDR_P_PREFERRED_IDENTITY(); }
^Priority		{ return yy::sip_parser::make_T_HDR_PRIORITY(); }
^Privacy		{ return yy::sip_parser::make_T_HDR_PRIVACY(); }
^Proxy-Authenticate	{ return yy::sip_parser::make_T_HDR_PROXY_AUTHENTICATE(); }
^Proxy-Authorization	{ return yy::sip_parser::make_T_HDR_PROXY_AUTHORIZATION(); }
^Proxy-Require		{ return yy::sip_parser::make_T_HDR_PROXY_REQUIRE(); }
^RAck			{ return yy::sip_parser::make_T_HDR_RACK(); }
^Record-Route		{ return yy::sip_parser::make_T_HDR_RECORD_ROUTE(); }
^Service-Route		{ return yy::sip_parser::make_T_HDR_SERVICE_ROUTE(); }
^Refer-Sub		{ return yy::sip_parser::make_T_HDR_REFER_SUB(); }
^(Refer-To)|r		{ return yy::sip_parser::make_T_HDR_REFER_TO(); }
^(Referred-By)|b	{ return yy::sip_parser::make_T_HDR_REFERRED_BY(); }
^Replaces		{ return yy::sip_parser::make_T_HDR_REPLACES(); }
^Reply-To		{ return yy::sip_parser::make_T_HDR_REPLY_TO(); }
^Require		{ return yy::sip_parser::make_T_HDR_REQUIRE(); }
^(Request-Disposition)|d {return yy::sip_parser::make_T_HDR_REQUEST_DISPOSITION(); }
^Retry-After		{ return yy::sip_parser::make_T_HDR_RETRY_AFTER(); }
^Route			{ return yy::sip_parser::make_T_HDR_ROUTE(); }
^RSeq			{ return yy::sip_parser::make_T_HDR_RSEQ(); }
^Server			{ return yy::sip_parser::make_T_HDR_SERVER(); }
^SIP-ETag		{ return yy::sip_parser::make_T_HDR_SIP_ETAG(); }
^SIP-If-Match		{ return yy::sip_parser::make_T_HDR_SIP_IF_MATCH(); }
^(Subject)|s		{ return yy::sip_parser::make_T_HDR_SUBJECT(); }
^Subscription-State	{ return yy::sip_parser::make_T_HDR_SUBSCRIPTION_STATE(); }
^(Supported)|k		{ return yy::sip_parser::make_T_HDR_SUPPORTED(); }
^Timestamp		{ return yy::sip_parser::make_T_HDR_TIMESTAMP(); }
^(To)|t			{ return yy::sip_parser::make_T_HDR_TO(); }
^unsupported		{ return yy::sip_parser::make_T_HDR_UNSUPPORTED(); }
^User-Agent		{ return yy::sip_parser::make_T_HDR_USER_AGENT(); }
^(Via)|v		{ return yy::sip_parser::make_T_HDR_VIA(); }
^Warning		{ return yy::sip_parser::make_T_HDR_WARNING(); }
^WWW-Authenticate	{ return yy::sip_parser::make_T_HDR_WWW_AUTHENTICATE(); }
^{TOKEN_SYM}+		{ return yy::sip_parser::make_T_HDR_UNKNOWN(yytext); }

	/* Token as define in RFC 3261 */
{TOKEN_SYM}+ 	{ return yy::sip_parser::make_T_TOKEN(yytext); }

	/* Switch to quoted string context */
\"		{ yy_push_state(C_QSTRING, yyscanner); }

	/* End of line */
\r\n		{ return yy::sip_parser::make_T_CRLF(); }
\n		{ return yy::sip_parser::make_T_CRLF(); }

[[:blank:]]	/* Skip white space */

	/* Single character token */
.		{ return yy::sip_parser::symbol_type(static_cast<yy::sip_parser::token_type>(yytext[0])); }

	/* URI. 
	   This context scans a URI including parameters.
	   The syntax of a URI will be checked outside the scanner 
	 */
<C_URI>\"		{ yy_push_state(C_QSTRING, yyscanner); }
<C_URI>{TOKEN_SYM}({TOKEN_SYM}|[[:blank:]])*/< {
			return yy::sip_parser::make_T_DISPLAY(yytext); }
<C_URI>[^[:blank:]<>\r\n]+/[[:blank:]]*> {
			return yy::sip_parser::make_T_URI(yytext); }
<C_URI>\*		{ return yy::sip_parser::make_T_URI_WILDCARD(); }
<C_URI>[^[:blank:]<>\"\r\n]+ {
			return yy::sip_parser::make_T_URI(yytext); }
<C_URI>[[:blank:]]	 /* Skip white space */
<C_URI>.		 { return yy::sip_parser::symbol_type(static_cast<yy::sip_parser::token_type>(yytext[0])); }
<C_URI>\n		 { return yy::sip_parser::make_T_ERROR(); }

	/* URI special case.
	   In several headers (eg. From, To, Contact, Reply-To) the URI
	   can be enclosed by < and >
	   If it is enclosed then parameters belong to the URI, if it
	   is not enclosed then parameters belong to the header.
	   Parameters are seperated by a semi-colon. 
	   For the URI special case, parameters belong to the header.
	   If the parser receives a < from the scanner, then the parser
	   will switch to the normal URI case.
	   The syntax of a URI will be checked outside the scanner 
	 */
<C_URI_SPECIAL>\"	{ yy_push_state(C_QSTRING, yyscanner); }
<C_URI_SPECIAL>{TOKEN_SYM}({TOKEN_SYM}|[[:blank:]])*/< {
			return yy::sip_parser::make_T_DISPLAY(yytext); }
<C_URI_SPECIAL>\*		{ return yy::sip_parser::make_T_URI_WILDCARD(); }
<C_URI_SPECIAL>[^[:blank:]<>;\"\r\n]+ {
			return yy::sip_parser::make_T_URI(yytext); }
<C_URI_SPECIAL>[[:blank:]]	 /* Skip white space */
<C_URI_SPECIAL>.		 { return yy::sip_parser::symbol_type(static_cast<yy::sip_parser::token_type>(yytext[0])); }
<C_URI_SPECIAL>\n		 { return yy::sip_parser::make_T_ERROR(); }

	/* Quoted string (starting after open quote, closing quote
	   will be consumed but not returned. */
<C_QSTRING>\\			{ yymore(); }
<C_QSTRING>[^\"\\\r\n]*\\\"	{ yymore(); }
<C_QSTRING>[^\"\\\r\n]*\"	{ yy_pop_state(yyscanner);
			  	  yytext[strlen(yytext)-1] = '\0';
			  	  return yy::sip_parser::make_T_QSTRING(unescape(yytext)); }
<C_QSTRING>[^\"\\\n]*\n		{ yy_pop_state(yyscanner); return yy::sip_parser::make_T_ERROR(); }
<C_QSTRING>.			{ yy_pop_state(yyscanner); return yy::sip_parser::make_T_ERROR(); }

	/* Comment (starting after LPAREN till RPAREN) */
<C_COMMENT>\\			{ yymore(); }
<C_COMMENT>[^\(\)\\\r\n]*\\\)	{ yymore(); }
<C_COMMENT>[^\(\)\\\r\n]*\\\(	{ yymore(); }
<C_COMMENT>[^\(\)\\\r\n]*\(	{ yyextra->inc_comment_level(); yymore(); }
<C_COMMENT>[^\(\)\\\r\n]*/\)	{ if (yyextra->dec_comment_level()) {
					BEGIN(C_RPAREN);
					yymore();
				  } else {
			  	  	return yy::sip_parser::make_T_COMMENT(yytext);
				  }
				}
<C_COMMENT>[^\(\)\\\n]*\n	{ return yy::sip_parser::make_T_ERROR(); }
<C_COMMENT>.			{ return yy::sip_parser::make_T_ERROR(); }
<C_RPAREN>\)			{ BEGIN(C_COMMENT); yymore(); }

	/* Language tag */
<C_LANG>{ALPHA}{1,8}(\-{ALPHA}{1,8})*	{ return yy::sip_parser::make_T_LANG(yytext); }
<C_LANG>[[:blank:]]			/* Skip white space */
<C_LANG>.				{ return yy::sip_parser::symbol_type(static_cast<yy::sip_parser::token_type>(yytext[0])); }
<C_LANG>\r\n				{ return yy::sip_parser::make_T_CRLF(); }
<C_LANG>\n				{ return yy::sip_parser::make_T_CRLF(); }

	/* Word */
<C_WORD>{WORD_SYM}+	{ return yy::sip_parser::make_T_WORD(yytext); }
<C_WORD>[[:blank:]]	/* Skip white space */
<C_WORD>.		{ return yy::sip_parser::symbol_type(static_cast<yy::sip_parser::token_type>(yytext[0])); }
<C_WORD>\r\n				{ return yy::sip_parser::make_T_CRLF(); }
<C_WORD>\n				{ return yy::sip_parser::make_T_CRLF(); }

	/* Number */
<C_NUM>{DIGIT}+		{ return yy::sip_parser::make_T_NUM(strtoul(yytext, NULL, 10)); }
<C_NUM>[[:blank:]]	/* Skip white space */
<C_NUM>.		{ return yy::sip_parser::symbol_type(static_cast<yy::sip_parser::token_type>(yytext[0])); }
<C_NUM>\r\n		{ return yy::sip_parser::make_T_CRLF(); }
<C_NUM>\n		{ return yy::sip_parser::make_T_CRLF(); }

	/* Date */
<C_DATE>Mon		{ return yy::sip_parser::make_T_WKDAY(1); }
<C_DATE>Tue		{ return yy::sip_parser::make_T_WKDAY(2); }
<C_DATE>Wed		{ return yy::sip_parser::make_T_WKDAY(3); }
<C_DATE>Thu		{ return yy::sip_parser::make_T_WKDAY(4); }
<C_DATE>Fri		{ return yy::sip_parser::make_T_WKDAY(5); }
<C_DATE>Sat		{ return yy::sip_parser::make_T_WKDAY(6); }
<C_DATE>Sun		{ return yy::sip_parser::make_T_WKDAY(0); }
<C_DATE>Jan		{ return yy::sip_parser::make_T_MONTH(0); }
<C_DATE>Feb		{ return yy::sip_parser::make_T_MONTH(1); }
<C_DATE>Mar		{ return yy::sip_parser::make_T_MONTH(2); }
<C_DATE>Apr		{ return yy::sip_parser::make_T_MONTH(3); }
<C_DATE>May		{ return yy::sip_parser::make_T_MONTH(4); }
<C_DATE>Jun		{ return yy::sip_parser::make_T_MONTH(5); }
<C_DATE>Jul		{ return yy::sip_parser::make_T_MONTH(6); }
<C_DATE>Aug		{ return yy::sip_parser::make_T_MONTH(7); }
<C_DATE>Sep		{ return yy::sip_parser::make_T_MONTH(8); }
<C_DATE>Oct		{ return yy::sip_parser::make_T_MONTH(9); }
<C_DATE>Nov		{ return yy::sip_parser::make_T_MONTH(10); }
<C_DATE>Dec		{ return yy::sip_parser::make_T_MONTH(11); }
<C_DATE>GMT		{ return yy::sip_parser::make_T_GMT(); }
<C_DATE>{DIGIT}+	{ return yy::sip_parser::make_T_NUM(strtoul(yytext, NULL, 10)); }
<C_DATE>[[:blank:]]	/* Skip white space */
<C_DATE>.		{ return yy::sip_parser::symbol_type(static_cast<yy::sip_parser::token_type>(yytext[0])); }
<C_DATE>\r\n		{ return yy::sip_parser::make_T_CRLF(); }
<C_DATE>\n		{ return yy::sip_parser::make_T_CRLF(); }

	/* Get all text till end of line */
<C_LINE>[^\r\n]+	{ return yy::sip_parser::make_T_LINE(yytext); }
<C_LINE>\r\n		{ return yy::sip_parser::make_T_CRLF(); }
<C_LINE>\n		{ return yy::sip_parser::make_T_CRLF(); }
<C_LINE>\r		{ return yy::sip_parser::make_T_CRLF(); }

	/* Start of a new message */
<C_NEW>SIP		{ return yy::sip_parser::make_T_SIP(); }
<C_NEW>{CAPITALS}+	{ return yy::sip_parser::make_T_METHOD(yytext); }
<C_NEW>[[:blank:]]	/* Skip white space */
<C_NEW>.		{ return yy::sip_parser::make_T_ERROR(); }
<C_NEW>\r\n		{ return yy::sip_parser::make_T_CRLF(); }
<C_NEW>\n		{ return yy::sip_parser::make_T_CRLF(); }

	/* Authorization scheme */
<C_AUTH_SCHEME>Digest		{ return yy::sip_parser::make_T_AUTH_DIGEST(); }
<C_AUTH_SCHEME>{TOKEN_SYM}+ 	{ return yy::sip_parser::make_T_AUTH_OTHER(yytext); }
<C_AUTH_SCHEME>[[:blank:]]	/* Skip white space */
<C_AUTH_SCHEME>.		{ return yy::sip_parser::make_T_ERROR(); }
<C_AUTH_SCHEME>\r\n		{ return yy::sip_parser::make_T_CRLF(); }
<C_AUTH_SCHEME>\n		{ return yy::sip_parser::make_T_CRLF(); }

	/* IPv6 address
	 * NOTE: the validity of the format is not checked here.
	 */
<C_IPV6ADDR>({HEXDIG}|[:\.])+	{ return yy::sip_parser::make_T_IPV6ADDR(yytext); }
<C_IPV6ADDR>[[:blank:]]	/* Skip white space */
<C_IPV6ADDR>.		{ return yy::sip_parser::make_T_ERROR(); }
<C_IPV6ADDR>\r\n	{ return yy::sip_parser::make_T_CRLF(); }
<C_IPV6ADDR>\n		{ return yy::sip_parser::make_T_CRLF(); }

	/* Parameter values may contain an IPv6 address or reference. */
<C_PARAMVAL>({TOKEN_SYM}|[:\[\]])+ { return yy::sip_parser::make_T_PARAMVAL(yytext); }
<C_PARAMVAL>\"		{ yy_push_state(C_QSTRING, yyscanner); }
<C_PARAMVAL>[[:blank:]]	/* Skip white space */
<C_PARAMVAL>.		{ return yy::sip_parser::make_T_ERROR(); }
<C_PARAMVAL>\r\n	{ return yy::sip_parser::make_T_CRLF(); }
<C_PARAMVAL>\n		{ return yy::sip_parser::make_T_CRLF(); }
