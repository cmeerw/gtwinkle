/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "hdr_proxy_authorization.h"
#include "definitions.h"

t_hdr_proxy_authorization::t_hdr_proxy_authorization()
  : t_header("Proxy-Authorization")
{ }

void t_hdr_proxy_authorization::add_credentials(const t_credentials &c)
{
  populated = true;
  credentials_list.push_back(c);
}

std::string t_hdr_proxy_authorization::encode() const
{
  std::string s;

  if (!populated) return s;

  // RFC 3261 20.28
  // Each authorization should appear as a separate header
  for (auto const & cred : credentials_list)
  {
    s += header_name;
    s += ": ";
    s += cred.encode();
    s += CRLF;
  }

  return s;
}

std::string t_hdr_proxy_authorization::encode_value() const
{
  std::string s;

  if (!populated) return s;

  bool first = true;
  for (auto const & cred : credentials_list)
  {
    if (!first) s += ", ";
    else first = false;
    s += cred.encode();
  }

  return s;
}

bool t_hdr_proxy_authorization::contains(const std::string &realm,
					 const t_url &uri) const
{
  for (auto const & cred : credentials_list)
  {
    if (cred.digest_response.realm == realm &&
	cred.digest_response.digest_uri == uri)
    {
      return true;
    }
  }

  return false;
}

void t_hdr_proxy_authorization::remove_credentials(const std::string &realm,
						   const t_url &uri)
{
  for (auto i = credentials_list.begin(); i != credentials_list.end(); ++i)
  {
    if (i->digest_response.realm == realm &&
	i->digest_response.digest_uri == uri)
    {
      credentials_list.erase(i);
      return;
    }
  }
}
