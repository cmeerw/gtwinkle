/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "hdr_in_reply_to.h"
#include "definitions.h"

t_hdr_in_reply_to::t_hdr_in_reply_to()
  : t_header("In-Reply-To")
{ }

void t_hdr_in_reply_to::add_call_id(const std::string &id)
{
  populated = true;
  call_ids.push_back(id);
}

std::string t_hdr_in_reply_to::encode_value() const
{
  std::string s;

  if (!populated) return s;

  bool first = true;
  for (auto const & id : call_ids)
  {
    if (!first) s += ", ";
    else first = false;
    s += id;
  }

  return s;
}
