/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "definitions.h"
#include "hdr_warning.h"
#include "util.h"

t_warning::t_warning()
  : host(), port(0), code(0)
{ }

t_warning::t_warning(std::string _host, int _port, int _code,
		     const std::string &_text)
  : host(std::move(_host)), port(_port), code(_code)
{
  switch(code)
  {
  case 300: text = WARNING_300; break;
  case 301: text = WARNING_301; break;
  case 302: text = WARNING_302; break;
  case 303: text = WARNING_303; break;
  case 304: text = WARNING_304; break;
  case 305: text = WARNING_305; break;
  case 306: text = WARNING_306; break;
  case 307: text = WARNING_307; break;
  case 330: text = WARNING_330; break;
  case 331: text = WARNING_331; break;
  case 370: text = WARNING_370; break;
  case 399: text = WARNING_399; break;
  default: text = "Warning";
  }

  if (!_text.empty ())
  {
    text += ": ";
    text += _text;
  }
}

std::string t_warning::encode() const
{
  std::string s;

  s = int2str(code, "%3d");
  s += ' ';
  s += host;
  if (port > 0) s += int2str(port, ":%d");
  s += ' ';
  s += '"';
  s += text;
  s += '"';
  return s;
}

t_hdr_warning::t_hdr_warning()
  : t_header("Warning")
{ }

void t_hdr_warning::add_warning(const t_warning &w)
{
  populated = true;
  warnings.push_back(w);
}

std::string t_hdr_warning::encode_value() const
{
  std::string s;

  if (!populated) return s;

  bool first = true;
  for (auto const & warn : warnings)
  {
    if (!first) s += ", ";
    else first = false;
    s += warn.encode();
  }

  return s;
}
