/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// NOTE: the date functions are not thread safe

#include "hdr_date.h"
#include "definitions.h"
#include "util.h"

#include <iomanip>
#include <sstream>
#include <sys/time.h>

t_hdr_date::t_hdr_date()
  : t_header("Date")
{ }

void t_hdr_date::set_date_gm(struct tm *tm)
{
  populated = true;
  date = timegm(tm);
}

void t_hdr_date::set_now()
{
  populated = true;
  struct timeval t;
  gettimeofday(&t, nullptr);
  date = t.tv_sec;
}

std::string t_hdr_date::encode_value() const
{
  std::ostringstream s;

  if (populated)
  {
    struct tm tm;
    gmtime_r(&date, &tm);

    s << weekday2str(tm.tm_wday) << ", "
      << std::setfill('0') << std::setw(2) << tm.tm_mday << ' '
      << month2str(tm.tm_mon) << ' '
      << (tm.tm_year + 1900) << ' '
      << std::setw(2) << tm.tm_hour << ':'
      << std::setw(2) << tm.tm_min << ':'
      << std::setw(2) << tm.tm_sec << " GMT";
  }

  return s.str();
}
