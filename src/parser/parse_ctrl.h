/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// Parser control

#ifndef _PARSE_CTRL_H
#define _PARSE_CTRL_H

#include "sip_message.h"

#include <memory>
#include <stdexcept>
#include <string>
#include <vector>


// The t_parser controls the direction of the scanner/parser
// process and it stores the results from the parser.
class t_parser
{
private:
  // Unfold SIP headers
  static std::string unfold(const std::string &h);

public:
  enum t_context
  {
    X_INITIAL,          // Initial context
    X_URI,              // URI context where parameters belong to URI
    X_URI_SPECIAL,      // URI context where parameters belong to SIP header
                        // if URI is not enclosed by < and >
    X_LANG,             // Language tag context
    X_WORD,		// Word context
    X_NUM,		// Number context
    X_DATE,		// Date context
    X_LINE,		// Whole line context
    X_COMMENT,          // Comment context
    X_NEW,		// Start of a new SIP message to distinguish
			// request from responses
    X_AUTH_SCHEME,	// Authorization scheme context
    X_IPV6ADDR,         // IPv6 address context
    X_PARAMVAL,         // Generic parameter value context
  };

  struct yycontext
  {
    yycontext ()
      : msg(), scanner(), lex_state(X_INITIAL), comment_level(0)
    { }

    void enter_comment()
    {
      comment_level = 1;
      lex_state = X_COMMENT;
    }

    void inc_comment_level()
    {
      ++comment_level;
    }

    bool dec_comment_level()
    {
      return (--comment_level != 0);
    }

    void add_header_error(const std::string &header_name)
    {
      parse_errors.push_back("Parse error in header: " + header_name);
    }


    std::unique_ptr<t_sip_message> msg;
    void *scanner;
    t_context lex_state;
    unsigned int comment_level;
    // Non-fatal parse errors generated during parsing.
    std::vector<std::string> parse_errors;
  };

  // Parser options
  // According to RFC3261 the Max-Forwards header is mandatory, but
  // many implementations do not send this header.
  static bool		check_max_forwards;

  // Encode headers in compact forom
  static bool		compact_headers;
	
  // Encode multiple values as comma separated list or multiple headers
  static bool		multi_values_as_list;

  /** 
   * Parse a string representing a SIP message.
   * @param s [in] String to parse.
   * @param parse_errors_ [out] List of non-fatal parse errors.
   * @return The parsed SIP message.
   * @throw int exception when parsing fails.
   */
  static std::unique_ptr<t_sip_message> parse(const std::string &s, std::vector<std::string> &parse_errors_);
	
  /**
   * Parse a string of headers (hdr1=val1;hdr=val2;...)
   * The resulting SIP message is a SIP request with a fake request line.
   * @param s [in] String to parse.
   * @param parse_errors_ [out] List of non-fatal parse errors.
   * @return The parsed SIP message.
   * @throw int exception when parsing fails.
   */
  static std::unique_ptr<t_sip_message> parse_headers(const std::string &s, std::vector<std::string> &parse_errors_);

  // Add parsing error for a header to the list of parse errors
  static void add_header_error(const std::string &header_name);
};

// Error that can be thrown as exception
class t_syntax_error
  : public std::runtime_error
{
public:
  t_syntax_error(const std::string &e)
    : std::runtime_error(e)
  { }
};

class t_parse_error
  : public std::runtime_error
{
public:
  t_parse_error(const std::string &e, int rc)
    : std::runtime_error(e), rc_(rc)
  { }

  int rc() const
  {
    return rc_;
  }

private:
  const int rc_;
};

#endif
