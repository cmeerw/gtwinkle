/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

%code requires
{
#include <cstdio>
#include <cstdlib>
#include <string>

#include "parser/media_type.h"
#include "parser/parameter.h"
#include "parser/parse_ctrl.h"
#include "parser/request.h"
#include "parser/response.h"
#include "util.h"
}

%language "C++"
%defines
%define api.value.type variant
%define api.token.constructor
%define api.parser.class {sip_parser}

%parse-param { t_parser::yycontext & ctx }
%lex-param { void* scanner }
%expect 12

%code
{
extern yy::sip_parser::symbol_type yylex (void *scanner);

#define MSG		ctx.msg
#define CTX(state)	(ctx.lex_state = t_parser::X_ ## state)

#define CTX_COMMENT	(ctx.enter_comment())

#define PARSE_ERROR(h)	{ ctx.add_header_error(h); ctx.lex_state = t_parser::X_INITIAL; }

#define scanner ctx.scanner
}

%token <ulong>		T_NUM
%token <std::string>	T_TOKEN
%token <std::string>	T_QSTRING
%token <std::string>	T_COMMENT
%token <std::string>	T_LINE
%token <std::string>	T_URI
%token			T_URI_WILDCARD
%token <std::string>	T_DISPLAY
%token <std::string>	T_LANG
%token <std::string>	T_WORD
%token <int>		T_WKDAY
%token <int>		T_MONTH
%token			T_GMT
%token			T_SIP
%token <std::string>	T_METHOD
%token			T_AUTH_DIGEST
%token <std::string>	T_AUTH_OTHER
%token <std::string>	T_IPV6ADDR
%token <std::string>	T_PARAMVAL

%token			T_HDR_ACCEPT
%token			T_HDR_ACCEPT_ENCODING
%token			T_HDR_ACCEPT_LANGUAGE
%token			T_HDR_ALERT_INFO
%token			T_HDR_ALLOW
%token			T_HDR_ALLOW_EVENTS
%token			T_HDR_AUTHENTICATION_INFO
%token			T_HDR_AUTHORIZATION
%token			T_HDR_CALL_ID
%token			T_HDR_CALL_INFO
%token			T_HDR_CONTACT
%token			T_HDR_CONTENT_DISP
%token			T_HDR_CONTENT_ENCODING
%token			T_HDR_CONTENT_LANGUAGE
%token			T_HDR_CONTENT_LENGTH
%token			T_HDR_CONTENT_TYPE
%token			T_HDR_CSEQ
%token			T_HDR_DATE
%token			T_HDR_ERROR_INFO
%token			T_HDR_EVENT
%token			T_HDR_EXPIRES
%token			T_HDR_FROM
%token			T_HDR_IN_REPLY_TO
%token			T_HDR_MAX_FORWARDS
%token			T_HDR_MIN_EXPIRES
%token			T_HDR_MIME_VERSION
%token			T_HDR_ORGANIZATION
%token			T_HDR_P_ASSERTED_IDENTITY
%token			T_HDR_P_PREFERRED_IDENTITY
%token			T_HDR_PRIORITY
%token			T_HDR_PRIVACY
%token			T_HDR_PROXY_AUTHENTICATE
%token			T_HDR_PROXY_AUTHORIZATION
%token			T_HDR_PROXY_REQUIRE
%token			T_HDR_RACK
%token			T_HDR_RECORD_ROUTE
%token			T_HDR_SERVICE_ROUTE
%token			T_HDR_REFER_SUB
%token			T_HDR_REFER_TO
%token			T_HDR_REFERRED_BY
%token			T_HDR_REPLACES
%token			T_HDR_REPLY_TO
%token			T_HDR_REQUIRE
%token			T_HDR_REQUEST_DISPOSITION
%token			T_HDR_RETRY_AFTER
%token			T_HDR_ROUTE
%token			T_HDR_RSEQ
%token			T_HDR_SERVER
%token			T_HDR_SIP_ETAG
%token			T_HDR_SIP_IF_MATCH
%token			T_HDR_SUBJECT
%token			T_HDR_SUBSCRIPTION_STATE
%token			T_HDR_SUPPORTED
%token			T_HDR_TIMESTAMP
%token			T_HDR_TO
%token			T_HDR_UNSUPPORTED
%token			T_HDR_USER_AGENT
%token			T_HDR_VIA
%token			T_HDR_WARNING
%token			T_HDR_WWW_AUTHENTICATE
%token <std::string>	T_HDR_UNKNOWN

%token			T_CRLF

%token			T_ERROR

// The token T_NULL is never returned by the scanner.
%token			T_NULL

%type <t_alert_param>	alert_param
%type <std::vector<t_parameter>>	auth_params
%type <std::string>	call_id
%type <t_challenge>	challenge
%type <std::string>	comment
%type <t_contact_param>	contact_addr
%type <t_contact_param>	contact_param
%type <std::vector<t_contact_param>>	contacts
%type <t_coding>	content_coding
%type <t_credentials>	credentials
%type <float>		delay
%type <t_digest_challenge>	digest_challenge
%type <t_digest_response>	digest_response
%type <std::string>	display_name
%type <t_error_param>	error_param
%type <t_identity>	from_addr
%type <std::string>	hdr_unknown
%type <t_via>		host
%type <std::string>	ipv6reference
%type <t_info_param>	info_param
%type <t_language>	language
%type <t_media>		media_range
%type <t_parameter>	parameter
%type <std::string>	parameter_val
%type <std::vector<t_parameter>>	parameters
%type <float>		q_factor
%type <t_route>		rec_route
%type <t_via>		sent_protocol
%type <t_server>	server
%type <std::string>	sip_version
%type <float>		timestamp
%type <t_via>		via_parm
%type <t_warning>	warning

%%
sip_message:	{ CTX(NEW); } sip_message2
;

sip_message2:	  request
		| response
		| error T_NULL {
			/* KLUDGE to work around a memory leak in bison.
			 * T_NULL does never match, so the parser never
			 * gets here. The error keyword causes bison
			 * to eat all input and destroy all tokens returned
			 * by the parser.
			 * Without this workaround the following input causes
			 * the parser to leak:
			 *
			 *   INVITE INVITE ....
			 *
			 * In request_line a T_METHOD is returned as look ahead
			 * token when bison tries to match sip_version.
			 * This does not match, but the look ahead token is
			 * never destructed by Bison.
			 */
			YYABORT;
		}
;

request:	  request_line headers T_CRLF {
		  	/* Parsing stops here. Remaining text is
			 * not parsed.
			 */
		  	YYACCEPT; }
;

// KLUDGE: The use of CTXT_NEW is a kludge, to get the T_SIP symbol
//         for the SIP version
request_line:	  T_METHOD { CTX(URI); } T_URI { CTX(NEW); }
		  sip_version T_CRLF {
		  	MSG.reset(new t_request());
			((t_request *)MSG.get())->set_method($1);
			((t_request *)MSG.get())->uri.set_url($3);
			MSG->version = $5;

			if (!((t_request *)MSG.get())->uri.is_valid()) {
				MSG.reset();
				YYABORT;
			} }
;

sip_version:	  T_SIP { CTX(INITIAL); } '/' T_TOKEN {
			$$ = $4; }
;

response:	  status_line headers T_CRLF {
		  	/* Parsing stops here. Remaining text is
			 * not parsed.
			 */
		  	YYACCEPT; }
;

status_line:	  sip_version { CTX(NUM); } T_NUM { CTX(LINE); } T_LINE
		  { CTX(INITIAL); } T_CRLF {
			MSG.reset(new t_response());
		  	MSG->version = $1;
			((t_response *)MSG.get())->code = $3;
			((t_response *)MSG.get())->reason = trim($5);
		  }
;

headers:	  /* empty */
		| headers header
;

header:		  hd_accept hdr_accept T_CRLF
		| hd_accept_encoding hdr_accept_encoding T_CRLF
		| hd_accept_language hdr_accept_language T_CRLF
		| hd_alert_info hdr_alert_info T_CRLF
		| hd_allow hdr_allow T_CRLF
		| hd_allow_events hdr_allow_events T_CRLF
		| hd_authentication_info hdr_authentication_info T_CRLF
		| hd_authorization hdr_authorization T_CRLF
		| hd_call_id hdr_call_id T_CRLF
		| hd_call_info hdr_call_info T_CRLF
		| hd_contact hdr_contact T_CRLF
		| hd_content_disp hdr_content_disp T_CRLF
		| hd_content_encoding hdr_content_encoding T_CRLF
		| hd_content_language hdr_content_language T_CRLF
		| hd_content_length hdr_content_length T_CRLF
		| hd_content_type hdr_content_type T_CRLF
		| hd_cseq hdr_cseq T_CRLF
		| hd_date hdr_date T_CRLF
		| hd_event hdr_event T_CRLF
		| hd_error_info hdr_error_info T_CRLF
		| hd_expires hdr_expires T_CRLF
		| hd_from hdr_from T_CRLF
		| hd_in_reply_to hdr_in_reply_to T_CRLF
		| hd_max_forwards hdr_max_forwards T_CRLF
		| hd_min_expires hdr_min_expires T_CRLF
		| hd_mime_version hdr_mime_version T_CRLF
		| hd_organization hdr_organization T_CRLF
		| hd_p_asserted_identity hdr_p_asserted_identity T_CRLF
		| hd_p_preferred_identity hdr_p_preferred_identity T_CRLF
		| hd_priority hdr_priority T_CRLF
		| hd_privacy hdr_privacy T_CRLF
		| hd_proxy_authenticate hdr_proxy_authenticate T_CRLF
		| hd_proxy_authorization hdr_proxy_authorization T_CRLF
		| hd_proxy_require hdr_proxy_require T_CRLF
		| hd_rack hdr_rack T_CRLF
		| hd_record_route hdr_record_route T_CRLF
		| hd_refer_sub hdr_refer_sub T_CRLF
		| hd_refer_to hdr_refer_to T_CRLF
		| hd_referred_by hdr_referred_by T_CRLF
		| hd_replaces hdr_replaces T_CRLF
		| hd_reply_to hdr_reply_to T_CRLF
		| hd_require hdr_require T_CRLF
		| hd_request_disposition hdr_request_disposition T_CRLF
		| hd_retry_after hdr_retry_after T_CRLF
		| hd_route hdr_route T_CRLF
		| hd_rseq hdr_rseq T_CRLF
		| hd_server hdr_server T_CRLF
		| hd_service_route hdr_service_route T_CRLF
		| hd_sip_etag hdr_sip_etag T_CRLF
		| hd_sip_if_match hdr_sip_if_match T_CRLF
		| hd_subject hdr_subject T_CRLF
		| hd_subscription_state hdr_subscription_state T_CRLF
		| hd_supported hdr_supported T_CRLF
		| hd_timestamp hdr_timestamp T_CRLF
		| hd_to hdr_to T_CRLF
		| hd_unsupported hdr_unsupported T_CRLF
		| hd_user_agent hdr_user_agent T_CRLF
		| hd_via hdr_via T_CRLF
		| hd_warning hdr_warning T_CRLF
		| hd_www_authenticate hdr_www_authenticate T_CRLF
		| T_HDR_UNKNOWN ':' hdr_unknown T_CRLF {
			MSG->add_unknown_header($1, trim($3));
		  }
		| hd_accept error T_CRLF
			{ PARSE_ERROR("Accept"); }
		| hd_accept_encoding error T_CRLF
			{ PARSE_ERROR("Accept-Encoding"); }
		| hd_accept_language error T_CRLF
			{ PARSE_ERROR("Accept-Language"); }
		| hd_alert_info error T_CRLF
			{ PARSE_ERROR("Alert-Info"); }
		| hd_allow error T_CRLF
			{ PARSE_ERROR("Allow"); }
		| hd_allow_events error T_CRLF
			{ PARSE_ERROR("Allow-Events"); }
		| hd_authentication_info error T_CRLF
			{ PARSE_ERROR("Authentication-Info"); }
		| hd_authorization error T_CRLF
			{ PARSE_ERROR("Authorization"); }
		| hd_call_id error T_CRLF
			{ PARSE_ERROR("Call-ID"); }
		| hd_call_info error T_CRLF
			{ PARSE_ERROR("Call-Info"); }
		| hd_contact error T_CRLF
			{ PARSE_ERROR("Contact"); }
		| hd_content_disp error T_CRLF
			{ PARSE_ERROR("Content-Disposition"); }
		| hd_content_encoding error T_CRLF
			{ PARSE_ERROR("Content-Encoding"); }
		| hd_content_language error T_CRLF
			{ PARSE_ERROR("Content-Language"); }
		| hd_content_length 	error T_CRLF
			{ PARSE_ERROR("Content-Length"); }
		| hd_content_type error T_CRLF
			{ PARSE_ERROR("Content-Type"); }
		| hd_cseq error T_CRLF
			{ PARSE_ERROR("CSeq"); }
		| hd_date error T_CRLF
			{ PARSE_ERROR("Date"); }
		| hd_error_info error T_CRLF
			{ PARSE_ERROR("Error-Info"); }
		| hd_event error T_CRLF
			{ PARSE_ERROR("Event"); }
		| hd_expires error T_CRLF
			{ PARSE_ERROR("Expires"); }
		| hd_from error T_CRLF
			{ PARSE_ERROR("From"); }
		| hd_in_reply_to error T_CRLF
			{ PARSE_ERROR("In-Reply-To"); }
		| hd_max_forwards error T_CRLF
			{ PARSE_ERROR("Max-Forwards"); }
		| hd_min_expires error T_CRLF
			{ PARSE_ERROR("Min-Expires"); }
		| hd_mime_version error T_CRLF
			{ PARSE_ERROR("MIME-Version"); }
		| hd_organization error T_CRLF
			{ PARSE_ERROR("Organization"); }
		| hd_p_asserted_identity error T_CRLF
			{ PARSE_ERROR("P-Asserted-Identity"); }
		| hd_p_preferred_identity error T_CRLF
			{ PARSE_ERROR("P-Preferred-Identity"); }
		| hd_priority error T_CRLF
			{ PARSE_ERROR("Priority"); }
		| hd_privacy error T_CRLF
			{ PARSE_ERROR("Privacy"); }
		| hd_proxy_authenticate error T_CRLF
			{ PARSE_ERROR("Proxy-Authenticate"); }
		| hd_proxy_authorization error T_CRLF
			{ PARSE_ERROR("Proxy-Authorization"); }
		| hd_proxy_require error T_CRLF
			{ PARSE_ERROR("Proxy-Require"); }
		| hd_rack error T_CRLF
			{ PARSE_ERROR("RAck"); }
		| hd_record_route error T_CRLF
			{ PARSE_ERROR("Record-Route"); }
		| hd_refer_sub error T_CRLF
			{ PARSE_ERROR("Refer-Sub"); }
		| hd_refer_to error T_CRLF
			{ PARSE_ERROR("Refer-To"); }
		| hd_referred_by error T_CRLF
			{ PARSE_ERROR("Referred-By"); }
		| hd_replaces error T_CRLF
			{ PARSE_ERROR("Replaces"); }
		| hd_reply_to error T_CRLF
			{ PARSE_ERROR("Reply-To"); }
		| hd_require error T_CRLF
			{ PARSE_ERROR("Require"); }
		| hd_request_disposition error T_CRLF
			{ PARSE_ERROR("Request-Disposition"); }
		| hd_retry_after error T_CRLF
			{ PARSE_ERROR("Retry-After"); }
		| hd_route error T_CRLF
			{ PARSE_ERROR("Route"); }
		| hd_rseq error T_CRLF
			{ PARSE_ERROR("RSeq"); }
		| hd_server error T_CRLF
			{ PARSE_ERROR("Server"); }
		| hd_service_route error T_CRLF
			{ PARSE_ERROR("Service-Route"); }
		| hd_sip_etag error T_CRLF
			{ PARSE_ERROR("SIP-ETag"); }
		| hd_sip_if_match error T_CRLF
			{ PARSE_ERROR("SIP-If-Match"); }
		| hd_subject error T_CRLF
			{ PARSE_ERROR("Subject"); }
		| hd_subscription_state error T_CRLF
			{ PARSE_ERROR("Subscription-State"); }
		| hd_supported error T_CRLF
			{ PARSE_ERROR("Supported"); }
		| hd_timestamp error T_CRLF
			{ PARSE_ERROR("Timestamp"); }
		| hd_to error T_CRLF
			{ PARSE_ERROR("To"); }
		| hd_unsupported error T_CRLF
			{ PARSE_ERROR("Unsupported"); }
		| hd_user_agent error T_CRLF
			{ PARSE_ERROR("User-Agent"); }
		| hd_via error T_CRLF
			{ PARSE_ERROR("Via"); }
		| hd_warning error T_CRLF
			{ PARSE_ERROR("Warning"); }
		| hd_www_authenticate error T_CRLF
			{ PARSE_ERROR("WWW-Authenticate"); }
;


// KLUDGE
// These rules are needed to get the error recovery working.
// Many header rules start with a context change. As Bison uses
// one token look ahead to determine a matching rule, the context
// change must be done before Bison tries to match the error rule.
// Changing the context header and once again at the header rule
// does no harm as the context change operator is idem-potent.

hd_accept:		T_HDR_ACCEPT ':'
;
hd_accept_encoding:	T_HDR_ACCEPT_ENCODING ':'
;
hd_accept_language:	T_HDR_ACCEPT_LANGUAGE ':' { CTX(LANG); }
;
hd_alert_info:		T_HDR_ALERT_INFO ':'
;
hd_allow:		T_HDR_ALLOW ':'
;
hd_allow_events:	T_HDR_ALLOW_EVENTS ':'
;
hd_authentication_info:	T_HDR_AUTHENTICATION_INFO ':'
;
hd_authorization:	T_HDR_AUTHORIZATION ':' { CTX(AUTH_SCHEME); }
;
hd_call_id:		T_HDR_CALL_ID ':' { CTX(WORD); }
;
hd_call_info:		T_HDR_CALL_INFO ':'
;
hd_contact:		T_HDR_CONTACT ':' { CTX(URI_SPECIAL); }
;
hd_content_disp:	T_HDR_CONTENT_DISP ':'
;
hd_content_encoding:	T_HDR_CONTENT_ENCODING ':'
;
hd_content_language:	T_HDR_CONTENT_LANGUAGE ':' { CTX(LANG); }
;
hd_content_length:	T_HDR_CONTENT_LENGTH ':' { CTX(NUM); }
;
hd_content_type:	T_HDR_CONTENT_TYPE ':'
;
hd_cseq:		T_HDR_CSEQ ':' { CTX(NUM); }
;
hd_date:		T_HDR_DATE ':' { CTX(DATE);}
;
hd_error_info:		T_HDR_ERROR_INFO ':'
;
hd_event:		T_HDR_EVENT ':'
;
hd_expires:		T_HDR_EXPIRES ':' { CTX(NUM); }
;
hd_from:		T_HDR_FROM ':' { CTX(URI_SPECIAL); }
;
hd_in_reply_to:		T_HDR_IN_REPLY_TO ':' { CTX(WORD); }
;
hd_max_forwards:	T_HDR_MAX_FORWARDS ':' { CTX(NUM); }
;
hd_min_expires:		T_HDR_MIN_EXPIRES ':' { CTX(NUM); }
;
hd_mime_version:	T_HDR_MIME_VERSION ':'
;
hd_organization:	T_HDR_ORGANIZATION ':' { CTX(LINE); }
;
hd_p_asserted_identity:	T_HDR_P_ASSERTED_IDENTITY ':' { CTX(URI_SPECIAL); }
;
hd_p_preferred_identity: T_HDR_P_PREFERRED_IDENTITY ':' { CTX(URI_SPECIAL); }
;
hd_priority:		T_HDR_PRIORITY ':'
;
hd_privacy:		T_HDR_PRIVACY ':'
;
hd_proxy_authenticate:	T_HDR_PROXY_AUTHENTICATE ':' { CTX(AUTH_SCHEME); }
;
hd_proxy_authorization:	T_HDR_PROXY_AUTHORIZATION ':' { CTX(AUTH_SCHEME); }
;
hd_proxy_require:	T_HDR_PROXY_REQUIRE ':'
;
hd_rack:		T_HDR_RACK ':' { CTX(NUM); }
;
hd_record_route:	T_HDR_RECORD_ROUTE ':' { CTX(URI); }
;
hd_refer_sub:		T_HDR_REFER_SUB ':'
;
hd_refer_to:		T_HDR_REFER_TO ':' { CTX(URI_SPECIAL); }
;
hd_referred_by:		T_HDR_REFERRED_BY ':' { CTX(URI_SPECIAL); }
;
hd_replaces:		T_HDR_REPLACES ':' { CTX(WORD); }
;
hd_reply_to:		T_HDR_REPLY_TO ':' { CTX(URI_SPECIAL); }
;
hd_require:		T_HDR_REQUIRE ':'
;
hd_request_disposition:	T_HDR_REQUEST_DISPOSITION ':'
;
hd_retry_after:		T_HDR_RETRY_AFTER ':' { CTX(NUM); }
;
hd_route:		T_HDR_ROUTE ':' { CTX(URI); }
;
hd_rseq:		T_HDR_RSEQ ':' { CTX(NUM); }
;
hd_server:		T_HDR_SERVER ':'
;
hd_service_route:	T_HDR_SERVICE_ROUTE ':' { CTX(URI); }
;
hd_sip_etag:		T_HDR_SIP_ETAG ':'
;
hd_sip_if_match:	T_HDR_SIP_IF_MATCH ':'
;
hd_subject:		T_HDR_SUBJECT ':' { CTX(LINE); }
;
hd_subscription_state:	T_HDR_SUBSCRIPTION_STATE ':'
;
hd_supported:		T_HDR_SUPPORTED ':'
;
hd_timestamp:		T_HDR_TIMESTAMP ':' { CTX(NUM); }
;
hd_to:			T_HDR_TO ':' { CTX(URI_SPECIAL); }
;
hd_unsupported:		T_HDR_UNSUPPORTED ':'
;
hd_user_agent:		T_HDR_USER_AGENT ':'
;
hd_via:			T_HDR_VIA ':'
;
hd_warning:		T_HDR_WARNING ':' { CTX(NUM); }
;
hd_www_authenticate:	T_HDR_WWW_AUTHENTICATE ':' { CTX(AUTH_SCHEME); }
;

hdr_accept:	  /* empty */
		| media_range parameters {
			$1.add_params($2);
			MSG->hdr_accept.add_media($1);
		  }
		| hdr_accept ',' media_range parameters {
			$3.add_params($4);
			MSG->hdr_accept.add_media($3);
		  }
;

media_range:	  T_TOKEN '/' T_TOKEN { $$ = t_media(tolower($1), tolower($3));
				      }
;

parameters:	  /* empty */	{ }
		| parameters ';' parameter {
			$$ = std::move($1);
			$$.push_back($3);
		  }
;

parameter:	  T_TOKEN {
			$$ = t_parameter(tolower($1));
			}
		| T_TOKEN '=' { CTX(PARAMVAL); } parameter_val { CTX(INITIAL); } {
			$$ = t_parameter(tolower($1), $4);
		  }
;

parameter_val:	  T_PARAMVAL {
			$$ = $1; }
		| T_QSTRING {
			$$ = $1; }
;

hdr_accept_encoding: content_coding {
			MSG->hdr_accept_encoding.add_coding($1);
		     }
		   | hdr_accept_encoding ',' content_coding {
			MSG->hdr_accept_encoding.add_coding($3);
		     }
;

content_coding:	  T_TOKEN {
			$$ = t_coding(tolower($1));
		  }
		| T_TOKEN q_factor {
			$$ = t_coding(tolower($1));
			$$.q = $2;
		  }
;

q_factor:	  ';' parameter {
			if ($2.name != "q") YYERROR;
			$$ = atof($2.value.c_str());
		      }
;

hdr_accept_language: { CTX(LANG); } language {
			MSG->hdr_accept_language.add_language($2);
		     }
		   | hdr_accept_language ',' { CTX(LANG); } language {
			MSG->hdr_accept_language.add_language($4);
		     }
;

language:	  T_LANG {
			CTX(INITIAL);
		  	$$ = t_language(tolower($1));
		  }
		| T_LANG { CTX(INITIAL); } q_factor {
			$$ = t_language(tolower($1));
			$$.q = $3;
		  }
;

hdr_alert_info:	  alert_param {
			MSG->hdr_alert_info.add_param($1);
		  }
		| hdr_alert_info ',' alert_param {
			MSG->hdr_alert_info.add_param($3);
		  }
;

alert_param:	  '<' { CTX(URI); } T_URI { CTX(INITIAL); } '>' parameters {
		  	$$ = t_alert_param();
			$$.uri.set_url($3);
			$$.parameter_list = $6;

			if (!$$.uri.is_valid()) {
				YYERROR;
			}
		      }
;

hdr_allow:	  T_TOKEN {
			MSG->hdr_allow.add_method($1);
		  }
		| hdr_allow ',' T_TOKEN {
			MSG->hdr_allow.add_method($3);
		  }
;

hdr_call_id:	  { CTX(WORD); } call_id { CTX(INITIAL); } {
			MSG->hdr_call_id.set_call_id($2);
		  }
;

call_id:	  T_WORD { $$ = $1; }
		| T_WORD '@' T_WORD {
			$$ = std::string($1 + '@' + $3);
		  }
;

hdr_call_info:	  info_param {
			MSG->hdr_call_info.add_param($1);
		  }
		| hdr_call_info ',' info_param {
			MSG->hdr_call_info.add_param($3);
		  }
;

info_param:	  '<' { CTX(URI); } T_URI { CTX(INITIAL); } '>' parameters {
			$$.uri.set_url($3);
			$$.parameter_list = $6;

			if (!$$.uri.is_valid()) {
				YYERROR;
			}
		  }
;

hdr_contact:	  { CTX(URI_SPECIAL); } T_URI_WILDCARD { CTX(INITIAL); } {
			MSG->hdr_contact.set_any(); }
		| contacts {
			MSG->hdr_contact.add_contacts($1);
		  }
;

contacts:	  contact_param {
			$$.push_back($1);
		  }
		| contacts ',' contact_param {
			$$ = std::move($1);
			$$.push_back($3);
		  }
;

contact_param:	  contact_addr parameters {
			$$ = $1;
			for (auto const & param : $2) {
				if (param.name == "q") {
					$$.set_qvalue(atof(param.value.c_str()));
				} else if (param.name == "expires") {
					$$.set_expires(strtoul(
						param.value.c_str(), NULL, 10));
				} else {
					$$.add_extension(param);
				}
			}
		  }
;

contact_addr:	  { CTX(URI_SPECIAL); } T_URI { CTX(INITIAL); } {
			$$.uri.set_url($2);

			if (!$$.uri.is_valid()) {
				YYERROR;
			}
		  }
		| { CTX(URI_SPECIAL); } display_name '<' { CTX(URI); } T_URI { CTX(INITIAL); } '>' {
			$$.display = $2;
			$$.uri.set_url($5);

			if (!$$.uri.is_valid()) {
				YYERROR;
			}
			 
		  }
;

display_name:	  /* empty */ { }
		| T_DISPLAY {
			$$ = std::string(rtrim($1));
		  }
		| T_QSTRING { $$ = $1; }
;

hdr_content_disp: T_TOKEN parameters {
			MSG->hdr_content_disp.set_type(tolower($1));
			
			for (auto const & param : $2) {
				if (param.name == "filename") {
					MSG->hdr_content_disp.set_filename(param.value);
				} else {
					MSG->hdr_content_disp.add_param(param);
				}
			}

		  }
;

hdr_content_encoding: content_coding {
			MSG->hdr_content_encoding.add_coding($1);
		      }
		    | hdr_content_encoding ',' content_coding {
			MSG->hdr_content_encoding.add_coding($3);
		      }
;

hdr_content_language: { CTX(LANG); } language {
			MSG->hdr_content_language.add_language($2);
		      }
		    | hdr_content_language ',' { CTX(LANG); } language {
			MSG->hdr_content_language.add_language($4);
		      }
;

hdr_content_length: { CTX(NUM); } T_NUM { CTX(INITIAL); } {
			MSG->hdr_content_length.set_length($2); }
;

hdr_content_type:  media_range parameters {
			$1.add_params($2);
			MSG->hdr_content_type.set_media($1);
		  }
;

hdr_cseq: 	{ CTX(NUM); } T_NUM { CTX(INITIAL); } T_TOKEN {
			MSG->hdr_cseq.set_seqnr($2);
			MSG->hdr_cseq.set_method($4);
		}
;

hdr_date:	{ CTX(DATE);}
		  T_WKDAY ',' T_NUM T_MONTH T_NUM
		  T_NUM ':' T_NUM ':' T_NUM T_GMT
		{ CTX(INITIAL); } {
			struct tm t;
			t.tm_mday = $4;
			t.tm_mon = $5;
			t.tm_year = $6 - 1900;
			t.tm_hour = $7;
			t.tm_min = $9;
			t.tm_sec = $11;
			MSG->hdr_date.set_date_gm(&t); }
;

hdr_error_info:	  error_param {
			MSG->hdr_error_info.add_param($1);
		  }
		| hdr_error_info ',' error_param {
			MSG->hdr_error_info.add_param($3);
		  }
;

error_param:	  '<' { CTX(URI); } T_URI { CTX(INITIAL); } '>' parameters {
			$$.uri.set_url($3);
			$$.parameter_list = $6;

			if (!$$.uri.is_valid()) {
				YYERROR;
			}
		  }
;

hdr_expires:	{ CTX(NUM); } T_NUM { CTX(INITIAL); } {
			MSG->hdr_expires.set_time($2); }
;

hdr_from:	  { CTX(URI_SPECIAL); } from_addr parameters {
			MSG->hdr_from.set_display($2.display);
			MSG->hdr_from.set_uri($2.uri);
			for (auto const & param : $3) {
				if (param.name == "tag") {
					MSG->hdr_from.set_tag(param.value);
				} else {
					MSG->hdr_from.add_param(param);
				}
			}
		  }
;

from_addr:	  T_URI { CTX(INITIAL); } {
			$$.set_uri($1);

			if (!$$.uri.is_valid()) {
				YYERROR;
			}
			
		  }
		| display_name '<' { CTX(URI); } T_URI { CTX(INITIAL); } '>' {
			$$.set_display($1);
			$$.set_uri($4);

			if (!$$.uri.is_valid()) {
				YYERROR;
			}
			
		  }
;

hdr_in_reply_to:  { CTX(WORD); } call_id { CTX(INITIAL); } {
			MSG->hdr_in_reply_to.add_call_id($2);
		  }
		| hdr_in_reply_to ',' { CTX(WORD); } call_id { CTX(INITIAL); } {
			MSG->hdr_in_reply_to.add_call_id($4);
		  }
;

hdr_max_forwards: { CTX(NUM); } T_NUM { CTX(INITIAL); } {
			MSG->hdr_max_forwards.set_max_forwards($2); }
;

hdr_min_expires:  { CTX(NUM); } T_NUM { CTX(INITIAL); } {
			MSG->hdr_min_expires.set_time($2); }
;

hdr_mime_version: T_TOKEN {
			MSG->hdr_mime_version.set_version($1);
		  }
;

hdr_organization: { CTX(LINE); } T_LINE { CTX(INITIAL); } {
			MSG->hdr_organization.set_name(trim($2));
		  }
;

hdr_p_asserted_identity:  { CTX(URI_SPECIAL); } from_addr {
				MSG->hdr_p_asserted_identity.add_identity($2);
			  }	
			| hdr_p_asserted_identity ',' from_addr {
				MSG->hdr_p_asserted_identity.add_identity($3);
			  }
;

hdr_p_preferred_identity: { CTX(URI_SPECIAL); } from_addr {
				MSG->hdr_p_preferred_identity.add_identity($2);
			  }
			| hdr_p_preferred_identity ',' from_addr {
				MSG->hdr_p_preferred_identity.add_identity($3);
			  }
;

hdr_priority:	  T_TOKEN {
			MSG->hdr_priority.set_priority(tolower($1));
		  }
;

hdr_privacy:	  T_TOKEN {
			MSG->hdr_privacy.add_privacy(tolower($1));
		  }
		| hdr_privacy ';' T_TOKEN {
			MSG->hdr_privacy.add_privacy(tolower($3));
		  }
;

hdr_proxy_require:   T_TOKEN {
			MSG->hdr_proxy_require.add_feature(tolower($1));
		     }
		   | hdr_proxy_require ',' T_TOKEN {
			MSG->hdr_proxy_require.add_feature(tolower($3));
		     }
;

hdr_record_route:   rec_route {
			MSG->hdr_record_route.add_route($1);
		    }
		  | hdr_record_route ',' rec_route {
		  	MSG->hdr_record_route.add_route($3);
		    }
;

rec_route:	{ CTX(URI); } display_name '<' T_URI { CTX(INITIAL); } '>'
		parameters {
			$$.display = $2;
			$$.uri.set_url($4);
			$$.set_params($7);

			if (!$$.uri.is_valid()) {
				YYERROR;
			}
		}
;

hdr_service_route:  rec_route {
			MSG->hdr_service_route.add_route($1);
		    }
		  | hdr_service_route ',' rec_route {
		  	MSG->hdr_service_route.add_route($3);
		    }
;

hdr_replaces:	  { CTX(WORD); } call_id { CTX(INITIAL); } parameters {
			MSG->hdr_replaces.set_call_id($2);
			
			for (auto const & param : $4) {
				if (param.name == "to-tag") {
					MSG->hdr_replaces.set_to_tag(param.value);
				} else if (param.name == "from-tag") {
					MSG->hdr_replaces.set_from_tag(param.value);
				} else if (param.name == "early-only") {
					MSG->hdr_replaces.set_early_only(true);
				} else {
					MSG->hdr_replaces.add_param(param);
				}
			}
			
			if (!MSG->hdr_replaces.is_valid()) YYERROR;
		  }
;

hdr_reply_to:  { CTX(URI_SPECIAL); } from_addr parameters {
			MSG->hdr_reply_to.set_display($2.display);
			MSG->hdr_reply_to.set_uri($2.uri);
			MSG->hdr_reply_to.set_params($3);
	       }
;

hdr_require:	  T_TOKEN {
			MSG->hdr_require.add_feature(tolower($1));
		  }
		| hdr_proxy_require ',' T_TOKEN {
			MSG->hdr_require.add_feature(tolower($3));
		  }
;

hdr_retry_after:  { CTX(NUM); } T_NUM { CTX(INITIAL); } comment parameters {
			MSG->hdr_retry_after.set_time($2);
			MSG->hdr_retry_after.set_comment($4);
			for (auto const & param : $5) {
				if (param.name == "duration") {
					int d = strtoul(param.value.c_str(), NULL, 10);
					MSG->hdr_retry_after.set_duration(d);
				} else {
					MSG->hdr_retry_after.add_param(param);
				}
			}
		  }
;

comment:	  /* empty */ { }
		| '(' { CTX_COMMENT; } T_COMMENT { CTX(INITIAL); } ')' {
			$$ = $3; }
;

hdr_route:	  rec_route {
			MSG->hdr_route.add_route($1);
		  }
		| hdr_route ',' rec_route {
		  	MSG->hdr_route.add_route($3);
		  }
;

hdr_server:	  server {
			MSG->hdr_server.add_server($1);
		  }
		| hdr_server server {
			MSG->hdr_server.add_server($2);
		  }
;

server:		  comment {
			$$.comment = $1;
		  }
		| T_TOKEN comment {
			$$.product = $1;
			$$.comment = $2;
		  }
		| T_TOKEN '/' T_TOKEN comment {
			$$.product = $1;
			$$.version = $3;
			$$.comment = $4;
		  }
;

hdr_subject:	{ CTX(LINE); } T_LINE { CTX(INITIAL); } {
			MSG->hdr_subject.set_subject(trim($2));
		}
;

hdr_supported:	  /* empty */ {
			MSG->hdr_supported.set_empty(); }
		| T_TOKEN {
			MSG->hdr_supported.add_feature(tolower($1));
		  }
		| hdr_supported ',' T_TOKEN {
			MSG->hdr_supported.add_feature(tolower($3));
		  }
;

hdr_timestamp:	  { CTX(NUM); } hdr_timestamp1 { CTX(INITIAL); }
;

hdr_timestamp1:	  timestamp {
			MSG->hdr_timestamp.set_timestamp($1); }
		| timestamp delay {
			MSG->hdr_timestamp.set_timestamp($1);
			MSG->hdr_timestamp.set_delay($2); }
;

timestamp:	  T_NUM { $$ = $1; }
		| T_NUM '.' T_NUM {
			std::string s = int2str($1) + '.' + int2str($3);
			$$ = atof(s.c_str()); }
;

delay:	  	  T_NUM { $$ = $1; }
		| T_NUM '.' T_NUM {
			std::string s = int2str($1) + '.' + int2str($3);
			$$ = atof(s.c_str()); }
;

hdr_to:		  { CTX(URI_SPECIAL); } from_addr parameters {
			MSG->hdr_to.set_display($2.display);
			MSG->hdr_to.set_uri($2.uri);
			for (auto const & param : $3) {
				if (param.name == "tag") {
					MSG->hdr_to.set_tag(param.value);
				} else {
					MSG->hdr_to.add_param(param);
				}
			}
		  }
;

hdr_unsupported:  T_TOKEN {
			MSG->hdr_unsupported.add_feature(tolower($1));
		  }
		| hdr_unsupported ',' T_TOKEN {
			MSG->hdr_unsupported.add_feature(tolower($3));
		  }
;

hdr_user_agent:	  server {
			MSG->hdr_user_agent.add_server($1);
		  }
		| hdr_user_agent server {
			MSG->hdr_user_agent.add_server($2);
		  }
;

hdr_via:	  via_parm {
			MSG->hdr_via.add_via($1);
		  }
		| hdr_via ',' via_parm {
			MSG->hdr_via.add_via($3);
		  }
;

via_parm:	  sent_protocol host parameters {
			$$ = $1;
			$$.host = $2.host;
			$$.port = $2.port;
			for (auto const & param : $3) {
				if (param.name == "ttl") {
					$$.ttl = atoi(param.value.c_str());
				} else if (param.name == "maddr") {
					$$.maddr = param.value;
				} else if (param.name == "received") {
					$$.received = param.value;
				} else if (param.name == "branch") {
					$$.branch = param.value;
				} else if (param.name == "rport") {
					$$.rport_present = true;
					if (param.type == t_parameter::VALUE) {
						$$.rport =
							atoi(param.value.c_str());
					}
				} else {
					$$.add_extension(param);
				}
			}
		  }
;

sent_protocol:	  T_TOKEN '/' T_TOKEN '/' T_TOKEN {
			$$.protocol_name = toupper($1);
			$$.protocol_version = $3;
			$$.transport = toupper($5);
		  }
;

host:		  T_TOKEN {
			$$.host = $1;
		  }
		| T_TOKEN ':' { CTX(NUM); } T_NUM { CTX(INITIAL); } {
			if ($4 > 65535) YYERROR;
			
			$$.host = $1;
			$$.port = $4;
		  }
		| ipv6reference {
			$$.host = $1;
		  }
		| ipv6reference ':' { CTX(NUM); } T_NUM { CTX(INITIAL); } {
			$$.host = $1;
			$$.port = $4;
		  }
;

ipv6reference:	 '[' { CTX(IPV6ADDR); } T_IPV6ADDR { CTX(INITIAL); } ']' {
			// TODO: check correct format of IPv6 address
			$$ = std::string('[' + $3 + ']');
			}
;

hdr_warning:	  warning {
			MSG->hdr_warning.add_warning($1);
		  }
		| hdr_warning ',' warning {
			MSG->hdr_warning.add_warning($3);
		  }
;

warning:	  { CTX(NUM); } T_NUM { CTX(INITIAL); } host T_QSTRING {
			$$.code = $2;
			$$.host = $4.host;
			$$.port = $4.port;
			$$.text = $5;
		  }
;

hdr_unknown:	  { CTX(LINE); } T_LINE { CTX(INITIAL); } { $$ = $2; }
;

ainfo:		  parameter {
			if ($1.name == "nextnonce")
				MSG->hdr_auth_info.set_next_nonce($1.value);
		 	else if ($1.name == "qop")
				MSG->hdr_auth_info.set_message_qop($1.value);
			else if ($1.name == "rspauth")
				MSG->hdr_auth_info.set_response_auth($1.value);
			else if ($1.name == "cnonce")
				MSG->hdr_auth_info.set_cnonce($1.value);
			else if ($1.name == "nc") {
				MSG->hdr_auth_info.set_nonce_count(
							hex2int($1.value));
			}
			else {
				YYERROR;
			}
		  }
;

hdr_authentication_info:  ainfo
			| hdr_authentication_info ',' ainfo
;

digest_response:  parameter {
			if (!$$.set_attr($1)) {
				YYERROR;
			}
		  }
		| digest_response ',' parameter {
			$$ = $1;
			if (!$$.set_attr($3)) {
				YYERROR;
			}
		  }
;

auth_params:	  parameter {
			$$.push_back($1);
		  }
		| auth_params ',' parameter {
			$$ = std::move($1);
			$$.push_back($3);
		  }
;

credentials:	  T_AUTH_DIGEST { CTX(INITIAL); } digest_response {
			$$.auth_scheme = AUTH_DIGEST;
			$$.digest_response = $3;
		  }
		| T_AUTH_OTHER { CTX(INITIAL); } auth_params {
			$$.auth_scheme = $1;
			$$.auth_params = $3;
		  }
;

hdr_authorization:  { CTX(AUTH_SCHEME); } credentials {
			MSG->hdr_authorization.add_credentials($2);
		    }
;

digest_challenge: parameter {
			if (!$$.set_attr($1)) {
				YYERROR;
			}
		  }
		| digest_challenge ',' parameter {
			$$ = $1;
			if (!$$.set_attr($3)) {
				YYERROR;
			}
		  }
;

challenge:	  T_AUTH_DIGEST { CTX(INITIAL); } digest_challenge {
			$$.auth_scheme = AUTH_DIGEST;
			$$.digest_challenge = $3;
		  }
		| T_AUTH_OTHER { CTX(INITIAL); } auth_params {
			$$.auth_scheme = $1;
			$$.auth_params = $3;
		  }
;

hdr_proxy_authenticate:	{ CTX(AUTH_SCHEME); } challenge {
				MSG->hdr_proxy_authenticate.set_challenge($2);
			}
;

hdr_proxy_authorization: { CTX(AUTH_SCHEME); } credentials {
				MSG->hdr_proxy_authorization.
							add_credentials($2);
			 }
;

hdr_www_authenticate:	{ CTX(AUTH_SCHEME); } challenge {
				MSG->hdr_www_authenticate.set_challenge($2);
			}
;

hdr_rseq: 	{ CTX(NUM); } T_NUM { CTX(INITIAL); } {
			MSG->hdr_rseq.set_resp_nr($2); }
;

hdr_rack: 	{ CTX(NUM); } T_NUM T_NUM { CTX(INITIAL); } T_TOKEN {
			MSG->hdr_rack.set_resp_nr($2);
			MSG->hdr_rack.set_cseq_nr($3);
			MSG->hdr_rack.set_method($5);
		}
;

hdr_event:	T_TOKEN parameters {
			MSG->hdr_event.set_event_type(tolower($1));
			for (auto const & param : $2) {
				if (param.name == "id") {
					MSG->hdr_event.set_id(param.value);
				} else {
					MSG->hdr_event.add_event_param(param);
				}
			}
		}
;

hdr_allow_events:	T_TOKEN {
				MSG->hdr_allow_events.add_event_type(tolower($1));
			}
		      | hdr_allow_events ',' T_TOKEN {
		      		MSG->hdr_allow_events.add_event_type(tolower($3));
			}
;

hdr_subscription_state:	T_TOKEN parameters {
				MSG->hdr_subscription_state.set_substate(tolower($1));
				for (auto const & param : $2) {
					if (param.name == "reason") {
						MSG->hdr_subscription_state.
							set_reason(tolower(param.value));
					} else if (param.name == "expires") {
						MSG->hdr_subscription_state.
							set_expires(strtoul(
							  param.value.c_str(),
							  NULL, 10));
					} else if (param.name == "retry-after") {
						MSG->hdr_subscription_state.
							set_retry_after(strtoul(
							  param.value.c_str(),
							  NULL, 10));
					} else {
						MSG->hdr_subscription_state.
							add_extension(param);
					}
				}
			}
;

hdr_refer_to:	  { CTX(URI_SPECIAL); } from_addr parameters {
			MSG->hdr_refer_to.set_display($2.display);
			MSG->hdr_refer_to.set_uri($2.uri);
			MSG->hdr_refer_to.set_params($3);
		  }
;

hdr_referred_by:  { CTX(URI_SPECIAL); } from_addr parameters {
			MSG->hdr_referred_by.set_display($2.display);
			MSG->hdr_referred_by.set_uri($2.uri);
			for (auto const & param : $3) {
				if (param.name == "cid") {
					MSG->hdr_referred_by.set_cid(param.value);
				} else {
					MSG->hdr_referred_by.add_param(param);
				}
			}
		  }
;

hdr_refer_sub:	  T_TOKEN parameters {
			std::string value(tolower($1));
			if (value != "true" && value != "false") {
				YYERROR;
			}
			MSG->hdr_refer_sub.set_create_refer_sub(value == "true");
			MSG->hdr_refer_sub.set_extensions($2);
		  }
;

hdr_sip_etag:	  T_TOKEN {
			MSG->hdr_sip_etag.set_etag($1);
		  }
;
			
hdr_sip_if_match: T_TOKEN {
			MSG->hdr_sip_if_match.set_etag($1);
		  }
;

hdr_request_disposition:  T_TOKEN {
				bool ret = MSG->hdr_request_disposition.set_directive($1);
				if (!ret) YYERROR;
			  }
			| hdr_request_disposition ',' T_TOKEN {
				bool ret = MSG->hdr_request_disposition.set_directive($3);
				if (!ret) YYERROR;
			  }

%%

void yy::sip_parser::error (const std::string& m)
{ }
