/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "parse_ctrl.h"
#include "parser.tab.hpp"
#include "protocol.h"
#include "util.h"

// Interface to Flex
extern int yylex_init_extra(t_parser::yycontext *, void **);
extern int yylex_destroy(void *);

struct yy_buffer_state;
extern struct yy_buffer_state *yy_scan_bytes(const char *, int, void *);
extern void yy_delete_buffer(struct yy_buffer_state *, void *);

bool t_parser::check_max_forwards = true;
bool t_parser::compact_headers = false;
bool t_parser::multi_values_as_list = true;


std::string t_parser::unfold(const std::string &h)
{
  std::string::size_type i;
  std::string s = h;

  while ((i = s.find("\r\n ")) != std::string::npos)
  {
    s.replace(i, 3, " ");
  }

  while ((i = s.find("\r\n\t")) != std::string::npos)
  {
    s.replace(i, 3, " ");
  }

  // This is only for easy testing of hand edited messages
  // in Linux where the end of line character is \n only.
  while ((i = s.find("\n ")) != std::string::npos)
  {
    s.replace(i, 2, " ");
  }

  while ((i = s.find("\n\t")) != std::string::npos)
  {
    s.replace(i, 2, " ");
  }

  return s;
}

std::unique_ptr<t_sip_message>
t_parser::parse(const std::string &s, std::vector<std::string> &parse_errors_)
{
  yycontext ctx;
  yylex_init_extra(&ctx, &ctx.scanner);

  const std::string &unfolded(unfold(s));
  struct yy_buffer_state * const b(
    yy_scan_bytes(unfolded.c_str(), unfolded.length() + 1,
		  ctx.scanner));

  yy::sip_parser parser(ctx);
  int const ret = parser.parse();

  yy_delete_buffer(b, ctx.scanner);
  yylex_destroy(ctx.scanner);

  if (ret != 0)
  {
    throw ret;
  }

  parse_errors_ = ctx.parse_errors;
  return std::move(ctx.msg);
}

std::unique_ptr<t_sip_message>
t_parser::parse_headers(const std::string &s,
			std::vector<std::string> &parse_errors_)
{
  std::string msg("INVITE sip:fake@fake.invalid SIP/2.0");
  msg += CRLF;

  for (auto const & param : str2param_list(s))
  {
    msg += unescape_hex(param.name);
    msg += ": ";
    msg += unescape_hex(param.value);
    msg += CRLF;
  }

  msg += CRLF;

  return parse(msg, parse_errors_);
}
