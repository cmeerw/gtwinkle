/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "definitions.h"
#include "hdr_server.h"
#include "util.h"

t_server::t_server() = default;

t_server::t_server(std::string _product, std::string _version,
    std::string _comment)
  : product(std::move(_product)), version(std::move(_version)),
    comment(std::move(_comment))
{ }

  std::string t_server::encode() const
  {
    std::string s(product);

    if (!version.empty())
    {
      s += '/';
      s += version;
    }

    if (!comment.empty())
    {
      if (!s.empty()) s += ' ';
      s += "(";
      s += comment;
      s += ')';
    }

    return s;
  }

t_hdr_server::t_hdr_server() : t_header("Server")
{};

void t_hdr_server::add_server(const t_server &s)
{
  populated = true;
  server_info.push_back(s);
}

std::string t_hdr_server::get_server_info() const
{
  std::string s;

  bool first = true;
  for (auto const & server : server_info)
  {
    if (!first) s += ' ';
    else first = false;
    s += server.encode();
  }

  return s;
}

std::string t_hdr_server::encode_value() const
{
  if (!populated) return "";

  return get_server_info();
}
