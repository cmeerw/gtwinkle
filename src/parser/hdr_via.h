/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// Via header

#ifndef _H_HDR_VIA
#define _H_HDR_VIA

#include "header.h"
#include "parameter.h"
#include "sockets/url.h"

#include <string>
#include <vector>

class t_via
{
public:
  std::string protocol_name;
  std::string protocol_version;
  std::string transport;
  std::string host;
  int port;
  int ttl;
  std::string maddr;
  std::string received;
  std::string branch;

  // RFC 3581: symetric response routing
  bool rport_present;
  int rport;

  std::vector<t_parameter> extensions;

  t_via();
  t_via(std::string _host, const int _port, bool add_rport = true);
  void add_extension(const t_parameter &p);
  std::string encode() const;

  // Get the response destination
  t_ip_port get_response_dst() const;

  // Returns true if branch starts with RFC 3261 magic cookie
  bool rfc3261_compliant() const;
};

class t_hdr_via
  : public t_header
{
public:
  std::vector<t_via> via_list;

  t_hdr_via();
  void add_via(const t_via &v);
  std::string encode() const;
  std::string encode_multi_header() const;
  std::string encode_value() const;

  // Get the response destination
  t_ip_port get_response_dst() const;
};

#endif
