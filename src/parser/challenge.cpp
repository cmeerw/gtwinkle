/*
    Copyright (C) 2005-2009  Michel de Boer <michel@twinklephone.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <iostream>
#include "challenge.h"
#include "definitions.h"
#include "log.h"
#include "util.h"

t_digest_challenge::t_digest_challenge()
{
  stale = false;

  // The default algorithm is MD5.
  algorithm = ALG_MD5;
}

std::string t_digest_challenge::encode() const
{
  std::string s;

  if (!realm.empty())
  {
    if (!s.empty()) s += ',';
    s += "realm=";
    s += '"';
    s += realm;
    s += '"';
  }

  if (!domain.empty())
  {
    if (!s.empty()) s += ',';
    s += "domain=";
    s += '"';

    bool first = true;
    for (auto const & url : domain)
    {
      if (! first) s += ' ';
      else first =false;
      s += url.encode();
    }

    s += '"';
  }

  if (!nonce.empty())
  {
    if (!s.empty()) s += ',';
    s += "nonce=";
    s += '"';
    s += nonce;
    s += '"';
  }

  if (!opaque.empty())
  {
    if (!s.empty()) s += ',';
    s += "opaque=";
    s += '"';
    s += opaque;
    s += '"';
  }

  // RFC 2617 3.2.1
  // If the stale flag is absent it means stale=false.
  if (stale)
  {
    if (!s.empty()) s += ',';
    s += "stale=true";
  }

  if (!algorithm.empty())
  {
    if (!s.empty()) s += ',';
    s += "algorithm=";
    s += algorithm;
  }

  if (!qop_options.empty())
  {
    if (!s.empty()) s += ',';
    s += "qop=";
    s += '"';

    bool first = true;
    for (auto const & opt : qop_options)
    {
      if (! first) s +=',';
      else first = false;
      s += opt;
    }

    s += '"';
  }

  for (auto const & param : auth_params)
  {
    if (!s.empty()) s += ',';
    s += param.encode();
  }

  return s;
}

bool t_digest_challenge::set_attr(const t_parameter &p)
{
  if (p.name == "realm")
    realm = p.value;
  else if (p.name == "nonce")
    nonce = p.value;
  else if (p.name == "opaque")
    opaque = p.value;
  else if (p.name == "algorithm")
    algorithm = p.value;
  else if (p.name == "domain")
  {
    for (auto const & s : split_ws(p.value))
    {
      t_url u(s);
      if (u.is_valid())
      {
	domain.push_back(u);
      }
      else
      {
	log_file->write_header("t_digest_challenge::set_attr",
			       LOG_SIP, LOG_WARNING);
	log_file->write_raw("Invalid domain in digest challenge: ");
	log_file->write_raw(s);
	log_file->write_endl();
	log_file->write_footer();
      }
    }
  }
  else if (p.name == "qop")
  {
    for (auto const & s : split(p.value, ','))
    {
      qop_options.push_back(trim(s));
    }
  }
  else if (p.name == "stale")
  {
    stale = cmp_nocase(p.value, "true") == 0;
    // RFC 2617 3.2.1
    // Any value other than false should be interpreted
    // as false.
  }
  else
  {
    auth_params.push_back(p);
  }

  return true;
}

std::string t_challenge::encode() const
{
  std::string s = auth_scheme;
  s += ' ';

  if (auth_scheme == AUTH_DIGEST)
  {
    s += digest_challenge.encode();
  }
  else
  {
    bool first = true;
    for (auto const & param : auth_params)
    {
      if (! first) s += ',';
      else first = false;
      s += param.encode();
    }
  }

  return s;
}
