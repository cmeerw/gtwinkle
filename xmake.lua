set_languages("cxx14")

set_configvar("DATADIR", "/usr/share/gtwinkle")
set_configdir("build/config")
set_version("1.5.0", {build = "%Y%m%d%H%M"})

add_configfiles("src/gtwinkle_config.h.in")
add_configfiles("gtwinkle.desktop.in")

add_includedirs("build/config")
add_includedirs("src")

local required_pkgs = { "giomm-2.4", "glibmm-2.4", "libxml-2.0", "sndfile",
			"libccrtp", "ucommon" }
for _, pkg in ipairs(required_pkgs) do
   add_requires(pkg, { system = true })
   add_packages(pkg)
end

function required_package(...)
   local pkgs = { ... }

   set_showmenu(false)
   before_check(function (option)
	 for _, pkg in ipairs(pkgs) do
	    option:add(find_packages(pkg))
	 end
   end)
end

option("alsa")
   required_package("alsa")

option("ilbc")
   set_showmenu(true)

   before_check(function (option)
	 option:add(find_packages("ilbc"))
   end)
   add_defines("HAVE_ILBC")
   add_defines("HAVE_ILBC_CPP")

option("opus")
   set_showmenu(true)

   before_check(function (option)
	 option:add(find_packages("opus"))
   end)
   add_defines("HAVE_OPUS")

option("secret")
   set_showmenu(true)

   before_check(function (option)
	 option:add(find_packages("libsecret-1"))
   end)
   add_defines("HAVE_LIBSECRET")

option("speexdsp")
   set_showmenu(true)

   before_check(function (option)
	 option:add(find_packages("speex", "speexdsp"))
   end)

   add_defines("HAVE_SPEEX")

option("zrtp")
   set_showmenu(true)

   before_check(function (option)
	 option:add(find_packages("libzrtpcpp"))
   end)
   add_defines("HAVE_ZRTP")

option("gui")
   set_showmenu(true)
   set_default(false)

   before_check(function (option)
	 option:add(find_packages("gtkmm-3.0", "libnotify"))
   end)

option("readline")
   before_check(function (option)
	 option:add(find_packages("readline"))
   end)
   add_defines("HAVE_READLINE_READLINE_H")
   add_defines("HAVE_READLINE_HISTORY_H")

option("editline")
   before_check(function (option)
	 option:add(find_packages("libedit"))
   end)
   add_defines("HAVE_EDITLINE_READLINE_H")
   add_defines("HAVE_EDITLINE_HISTORY_H")

option("unistd_h")
   add_cincludes("unistd.h")
   add_defines("HAVE_UNISTD_H")

option("linux_errqueue_h")
   add_cincludes("sys/time.h")
   add_cincludes("linux/errqueue.h")
   add_defines("HAVE_LINUX_ERRQUEUE_H")

option("linux_types_h")
   add_cincludes("linux/types.h")
   add_defines("HAVE_LINUX_TYPES_H")

option("strerror_r")
   add_cincludes("string.h")
   add_cfuncs("strerror_r")
   add_defines("HAVE_STRERROR_R")

option("res_init")
   add_cincludes("resolv.h")
   add_cfuncs("res_init")
   add_defines("HAVE_RES_INIT")


target("gtwinkle-audio")
   set_kind("static")

   add_options("alsa", "ccrtp", "ilbc", "opus", "speexdsp", "ucommon", "zrtp")
   add_files("src/audio/*.cpp")
   add_links("gsm", "magic")

target("gtwinkle-dbus")
   set_kind("static")

   add_options("giomm")
   add_files("src/dbus/*.cpp")

target("gtwinkle-im")
   set_kind("static")

   add_files("src/im/*.cpp")

target("gtwinkle-mwi")
   set_kind("static")

   add_files("src/mwi/*.cpp")

target("gtwinkle-parser")
   set_kind("static")

   add_rules("yacc")
   add_files("src/parser/scanner.ll", { rule = "lex" })
   add_files("src/parser/parser.yy")
   add_files("src/parser/*.cpp")

target("gtwinkle-patterns")
   set_kind("static")

   add_files("src/patterns/*.cpp")

target("gtwinkle-presence")
   set_kind("static")

   add_files("src/presence/*.cpp")

target("gtwinkle-sdp")
   set_kind("static")

   add_rules("yacc")
   add_files("src/sdp/sdp_scanner.ll", { rule = "lex" })
   add_files("src/sdp/sdp_parser.yy")
   add_files("src/sdp/*.cpp")

target("gtwinkle-sockets")
   set_kind("static")

   add_options("linux_errqueue_h", "linux_types_h", "unistd_h", "res_init")
   add_files("src/sockets/*.cpp")
   add_links("resolv")

target("gtwinkle-stun")
   set_kind("static")

   add_files("src/stun/*.cpp", "src/stun/*.cxx")

target("gtwinkle-threads")
   set_kind("static")

   add_files("src/threads/*.cpp")

target("gtwinkle-utils")
   set_kind("static")

   add_files("src/utils/*.cpp")
   add_links("magic")

target("gtwinkle-common")
   set_kind("static")

   add_options("secret", "editline", "readline", "ilbc", "opus", "speexdsp", "strerror_r", "zrtp")

   add_deps("gtwinkle-audio")
   add_deps("gtwinkle-dbus")
   add_deps("gtwinkle-im")
   add_deps("gtwinkle-mwi")
   add_deps("gtwinkle-parser")
   add_deps("gtwinkle-patterns")
   add_deps("gtwinkle-presence")
   add_deps("gtwinkle-sdp")
   add_deps("gtwinkle-sockets")
   add_deps("gtwinkle-stun")
   add_deps("gtwinkle-threads")
   add_deps("gtwinkle-utils")
   add_files("src/*.cpp|main.cpp")


target("gtwinkle-cli")
   set_kind("binary")


   add_deps("gtwinkle-common")
   add_files("src/main.cpp")

target("gtwinkle")
   set_kind("binary")

   if not has_config("gui") then
      set_enabled(false)
   end

   add_options("gui")

   add_deps("gtwinkle-common")
   add_files("src/gui/*.cpp")

target("gtwinkle-data")
   set_kind("phony")

   on_install(function (target)
      for _, fname in ipairs({ "sip.protocol" }) do
         os.cp(path.join("$(projectdir)", fname),
               path.join(target:installdir(), "share/services", fname))
      end

      for _, fname in ipairs({ "gtwinkle.svg", "ringback.wav", "ringtone.wav" }) do
         os.cp(path.join("$(projectdir)/data", fname),
               path.join(target:installdir(), "share/gtwinkle", fname))
      end

      for _, fname in ipairs({ "gtwinkle.desktop" }) do
         os.cp(path.join("$(buildir)/config", fname),
               path.join(target:installdir(), "share/applications", fname))
      end

      for _, fname in ipairs({ "net.cmeerw.gtwinkle.gschema.xml" }) do
         os.cp(path.join("$(projectdir)/data", fname),
               path.join(target:installdir(), "share/glib-2.0/schemas", fname))
      end
   end)

target("all")
   set_kind("phony")

   if has_config("gui") then
      add_deps("gtwinkle")
   end

   add_deps("gtwinkle-cli")
   add_deps("gtwinkle-data")
